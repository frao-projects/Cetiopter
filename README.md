# Introduction and Setup

*Cetiopter* is the frao AVX and AVX2 API, providing an abstraction over AVX and AVX2 instrinsics,
depending on the selected instruction set. It provides a unified API to access Vector and Matrix
functionality, without having to concern yourself with instruction sets at all times.

## Structure
For the Cetiopter API, each different type of function resides in its own header. For example, 2DComparisons are in 2DComparisons.hpp, trig functions are in ComponentMaths.hpp, and more non-AVX storage types are in DataStorageTypes.hpp.

## Setup
Cetiopter, as an API, is designed to be used as a meson subproject. In that case, one needs simply to use as any other meson subproject. If one wishes to use Cetiopter as a primary project however, and use its scripts, extra care must be taken.

First, one should download/clone the Cetiopter gitlab repo, to a suitable location. Since Cetiopter uses out of source builds, as is typical with meson, the source file should be located next to a seperate build directory.

There are three ways of setting up Cetiopter, as a primary project (ie: not a subproject) with the ability to run scripts. They are:
1. [With VSCode](#with-vscode), running setup scripts pre-configured, using vscode tasks
2. [With Powershell Scripts](#with-powershell-scripts), running the powershell scripts yourself, outside of vscode
3. [Manually](#manually), setting up meson build directory yourself, and building your own paths

### With VSCode
For VSCode, simply open the Cetiopter source folder in VSCode, and run the 'Generate Build folders' task. It will setup a default meson build structure, in ../Build/, as well as writing required build and script data to the .frao directory. The default build structure will be based on current operating system, and installed compilers. Assuming it runs successfully, you will now be setup for running any Cetiopter scripts, via the other tasks. 

### With Powershell Scripts
In this case, you will have to run the SetupBuildSystem.ps1 powershell script, from the .frao directory. For this script, the following parameters are mandatory:
- 'SourceFolder'; this is the location of repo folder itself
- 'BuildFolder'; this is the location of the build folder, into which all build artifacts, libraries, and documentation will be built
- 'InstallType'; one of 'NoInstall' (for using meson install defaults), 'Install' (for installing to root/projectname/os/compiler/config), or 'NoOSInstall' (for installing to root/projectname/compiler/config). If you do not intend to install the API anywhere, just pass NoInstall.
- 'DefaultLibrary'; This is the default type of library to build. Either shared (.so/.dll), static (.lib/.a), or both.

In addition, you will want to pass "Dfrao_AVX=AVX_USED" to the script, where AVX_USED, is AVX2, AVX, or NoAVX, depending on which instruction set you want to use (AVX2, AVX, sse accordingly)

### Manually
If you setup vscode manually, you simply set up the meson build folders as you would for any other project, except that you should set the frao_AVX option to AVX2, AVX, or NoAVX, according to which you require. In addition, after you have setup the build system, you should compile the ./Build_Paths target. This will create the proper script information in the .frao directory