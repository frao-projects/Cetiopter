
//! \file
//!
//! \brief Source of version retreival functions, for this
//! project. Header stores constexpr variables, and a function,
//! that encode the current Cetiopter version. This file just exists
//! to make sure that the header is compiled
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include "Version/VersionIncl.hpp"