//! \file
//!
//! \brief Implementation of 2D comparisons between, and on, vectors
//! and matrices
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "2DComparisons.hpp"
#include <Nucleus_inc\BitUtilities.hpp>	//for set/extract bits in non-avx mode
#include "ComponentComparison.hpp"
#include "SpecialVectors.hpp"  //for IMPL constexpr variables
#include "VectorTypes.hpp"	   //for complete vector types

namespace frao
{
namespace Maths
{
namespace IMPL
{
uint_least64_t create2DMask(uint_least64_t xRecord,
							uint_least64_t yRecord) noexcept
{
	uint_least64_t halfRecord =
		frao::SetBits(xRecord * 0xFFFF, yRecord * 0xFFFF, 0xFFFF, 16);

	return frao::SetBits(halfRecord & 0xFFFF'FFFF, halfRecord,
						 0xFFFFFFFF, 32);
}
uint_least64_t FRAO_VECTORCALL
conv2DRecord(RegIntVector source) noexcept
{
#if defined(FRAO_AVX2)
	uint_least64_t maskBits = static_cast<uint_least64_t>(
					   _mm256_movemask_epi8(source)),
				   components[2] = {
					   frao::ExtractBits(maskBits, 1, 0),
					   frao::ExtractBits(maskBits, 1, 8)};
#else
	uint_least64_t components[2] = {
		reinterpret_cast<const uint_least64_t*>(source.m_Values)[0]
			& 0b1,
		reinterpret_cast<const uint_least64_t*>(source.m_Values)[1]
			& 0b1};
#endif

	return create2DMask(components[0], components[1]);
}

bool FRAO_VECTORCALL
result2DAllTrue(RegIntVector fullResult) noexcept
{
#ifdef FRAO_AVX2
	int_least32_t mask = _mm256_movemask_epi8(fullResult);

	// eg: all bits up to y coord are set
	return (mask & 0xFFFF) == 0xFFFF;
#else
	return (fullResult.m_Values[0] & fullResult.m_Values[1])
		   == IMPL::g_TrueBits;
#endif
}
bool FRAO_VECTORCALL
result2DAnyTrue(RegIntVector fullResult) noexcept
{
#ifdef FRAO_AVX2
	int_least32_t mask = _mm256_movemask_epi8(fullResult);

	// eg: any bits up to y coord are set
	return (mask & 0xFFFF) != 0;
#else
	return (fullResult.m_Values[0] | fullResult.m_Values[1])
		   == IMPL::g_TrueBits;
#endif
}
}  // namespace IMPL

// vector 2D comparisons
bool FRAO_VECTORCALL vector2DLess(RegArgVector lhsVec,
								   RegArgVector rhsVec) noexcept
{
	return IMPL::result2DAllTrue(vectorLess(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector2DLess(RegIntVector lhsVec,
								   RegIntVector rhsVec) noexcept
{
	return IMPL::result2DAllTrue(vectorLess(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector2DLessRecord(uint_least64_t& record,
										 RegArgVector	 lhsVec,
										 RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorLess(lhsVec, rhsVec);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAllTrue(fullResult);
}
bool FRAO_VECTORCALL vector2DLessRecord(uint_least64_t& record,
										 RegIntVector	 lhsVec,
										 RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorLess(lhsVec, rhsVec);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector2DLessEqual(RegArgVector lhsVec,
										RegArgVector rhsVec) noexcept
{
	return IMPL::result2DAllTrue(vectorLessEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector2DLessEqual(RegIntVector lhsVec,
										RegIntVector rhsVec) noexcept
{
	return IMPL::result2DAllTrue(vectorLessEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL
vector2DLessEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
						RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorLessEqual(lhsVec, rhsVec);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAllTrue(fullResult);
}
bool FRAO_VECTORCALL
vector2DLessEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
						RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorLessEqual(lhsVec, rhsVec);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector2DGreater(RegArgVector lhsVec,
									  RegArgVector rhsVec) noexcept
{
	return IMPL::result2DAllTrue(vectorGreater(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector2DGreater(RegIntVector lhsVec,
									  RegIntVector rhsVec) noexcept
{
	return IMPL::result2DAllTrue(vectorGreater(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL
vector2DGreaterRecord(uint_least64_t& record, RegArgVector lhsVec,
					  RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorGreater(lhsVec, rhsVec);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAllTrue(fullResult);
}
bool FRAO_VECTORCALL
vector2DGreaterRecord(uint_least64_t& record, RegIntVector lhsVec,
					  RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorGreater(lhsVec, rhsVec);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector2DGreaterEqual(
	RegArgVector lhsVec, RegArgVector rhsVec) noexcept
{
	return IMPL::result2DAllTrue(vectorGreaterEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector2DGreaterEqual(
	RegIntVector lhsVec, RegIntVector rhsVec) noexcept
{
	return IMPL::result2DAllTrue(vectorGreaterEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector2DGreaterEqualRecord(
	uint_least64_t& record, RegArgVector lhsVec,
	RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorGreaterEqual(lhsVec, rhsVec);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAllTrue(fullResult);
}
bool FRAO_VECTORCALL vector2DGreaterEqualRecord(
	uint_least64_t& record, RegIntVector lhsVec,
	RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorGreaterEqual(lhsVec, rhsVec);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector2DEqual(RegArgVector lhsVec,
									RegArgVector rhsVec) noexcept
{
	return IMPL::result2DAllTrue(vectorEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector2DEqual(RegIntVector lhsVec,
									RegIntVector rhsVec) noexcept
{
	return IMPL::result2DAllTrue(vectorEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL
vector2DEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorEqual(lhsVec, rhsVec);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAllTrue(fullResult);
}
bool FRAO_VECTORCALL
vector2DEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorEqual(lhsVec, rhsVec);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector2DNotEqual(RegArgVector lhsVec,
									   RegArgVector rhsVec) noexcept
{
	return IMPL::result2DAnyTrue(vectorNotEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector2DNotEqual(RegIntVector lhsVec,
									   RegIntVector rhsVec) noexcept
{
	return IMPL::result2DAnyTrue(vectorNotEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL
vector2DNotEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					   RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorNotEqual(lhsVec, rhsVec);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAnyTrue(fullResult);
}
bool FRAO_VECTORCALL
vector2DNotEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					   RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorNotEqual(lhsVec, rhsVec);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAnyTrue(fullResult);
}
//
bool FRAO_VECTORCALL
vector2DInBounds(RegArgVector checked, RegArgVector lowerBound,
				 RegArgVector upperBound) noexcept
{
	return IMPL::result2DAllTrue(
		vectorInBounds(checked, lowerBound, upperBound));
}
bool FRAO_VECTORCALL
vector2DInBounds(RegIntVector checked, RegIntVector lowerBound,
				 RegIntVector upperBound) noexcept
{
	return IMPL::result2DAllTrue(
		vectorInBounds(checked, lowerBound, upperBound));
}
bool FRAO_VECTORCALL vector2DInBoundsRecord(
	uint_least64_t& record, RegArgVector checked,
	RegArgVector lowerBound, RegArgVector upperBound) noexcept
{
	WideIntVector fullResult =
		vectorInBounds(checked, lowerBound, upperBound);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAllTrue(fullResult);
}
bool FRAO_VECTORCALL vector2DInBoundsRecord(
	uint_least64_t& record, RegIntVector checked,
	RegIntVector lowerBound, RegIntVector upperBound) noexcept
{
	WideIntVector fullResult =
		vectorInBounds(checked, lowerBound, upperBound);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector2DIsNaN(RegArgVector vector) noexcept
{
	return IMPL::result2DAnyTrue(vectorIsNaN(vector));
}
bool FRAO_VECTORCALL vector2DIsNaNRecord(
	uint_least64_t& record, RegArgVector vector) noexcept
{
	WideIntVector fullResult = vectorIsNaN(vector);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAnyTrue(fullResult);
}
bool FRAO_VECTORCALL vector2DIsInf(RegArgVector vector) noexcept
{
	return IMPL::result2DAnyTrue(vectorIsInf(vector));
}
bool FRAO_VECTORCALL vector2DIsInfRecord(
	uint_least64_t& record, RegArgVector vector) noexcept
{
	WideIntVector fullResult = vectorIsInf(vector);

	record = IMPL::conv2DRecord(fullResult);
	return IMPL::result2DAnyTrue(fullResult);
}

// 2D matrix comparison functions
bool FRAO_VECTORCALL matrix2DEqual(RegArgMatrix  lhsMatrix,
									LateArgMatrix rhsMatrix) noexcept
{
	// two matrices are equal iff all of their elements are, and hence
	// if all of their rows are
	return (
		vector2DEqual(lhsMatrix.m_Tuples[0], rhsMatrix.m_Tuples[0])
		& vector2DEqual(lhsMatrix.m_Tuples[1],
						rhsMatrix.m_Tuples[1]));
}
bool FRAO_VECTORCALL matrix2DNotEqual(
	RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept
{
	// two matrices are not equal iff any of their elements are, and
	// hence if any of their rows are
	return (
		vector2DNotEqual(lhsMatrix.m_Tuples[0], rhsMatrix.m_Tuples[0])
		| vector2DNotEqual(lhsMatrix.m_Tuples[1],
						   rhsMatrix.m_Tuples[1]));
}
//
bool FRAO_VECTORCALL matrix2DIsNaN(RegArgMatrix matrix) noexcept
{
	// a matrix is NaN iff any of its elements are, and hence if any
	// of its rows are
	return (vector2DIsNaN(matrix.m_Tuples[0])
			| vector2DIsNaN(matrix.m_Tuples[1]));
}
bool FRAO_VECTORCALL matrix2DIsInf(RegArgMatrix matrix) noexcept
{
	// a matrix is Infinite iff any of its elements are, and hence if
	// any of its rows are
	return (vector2DIsInf(matrix.m_Tuples[0])
			| vector2DIsInf(matrix.m_Tuples[1]));
}
}  // namespace Maths
}  // namespace frao