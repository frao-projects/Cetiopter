//! \file
//!
//! \brief Implementation of specifically 2/3/4D mathematics
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "DimensionalMaths.hpp"
#include <Nucleus_inc\Environment.hpp>	//for EMIT_WARNING
#include "2DComparisons.hpp"			//for vector2DIsInf
#include "3DComparisons.hpp"			//for vector3DIsInf
#include "4DComparisons.hpp"			//for vector4DIsInf
#include "ComponentAccess.hpp"			//for swizzles/permutes etc
#include "ComponentMaths.hpp"			//for multiply, etc
#include "SpecialVectors.hpp"			//for vectorZero etc
#include "VectorTypes.hpp"				//for complete vector types

namespace frao
{
namespace Maths
{
// vector 2D specific maths operations
WideVector FRAO_VECTORCALL vector2DDot(RegArgVector lhsVec,
										RegArgVector rhsVec) noexcept
{
#ifdef FRAO_AVX
	WideVector intermediate = vectorMultiply(lhsVec, rhsVec);
	intermediate = _mm256_hadd_pd(intermediate, intermediate);

	__m128d halfInter = _mm256_castpd256_pd128(intermediate);

	return _mm256_set_m128d(halfInter, halfInter);
#else

	WideVector intermediate = vectorMultiply(lhsVec, rhsVec);

	double res = intermediate.m_Values[0] + intermediate.m_Values[1];

	return WideVector(res, res, res, res);
#endif
}

// vector 2D length operations
WideVector FRAO_VECTORCALL
vector2DLengthSq(RegArgVector vector) noexcept
{
	return vector2DDot(vector, vector);
}
WideVector FRAO_VECTORCALL
vector2DLength(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	return _mm256_sqrt_pd(vector2DLengthSq(vector));
#else

	WideVector lengthSq = vector2DLengthSq(vector), result;

	result.m_Values[0] = result.m_Values[1] = result.m_Values[2] =
		result.m_Values[3] = sqrt(lengthSq.m_Values[0]);

	return result;
#endif
}
WideVector FRAO_VECTORCALL
vector2DNormalised(RegArgVector vector) noexcept
{
	return vectorDivide(vector, vector2DLength(vector));
}
//
WideVector FRAO_VECTORCALL
vector2DTransform(RegArgVector vector, RegArgMatrix matrix) noexcept
{
	WideVector rowOne[2] = {vector2DDot(vector, matrix.m_Tuples[0]),
							vector2DDot(vector, matrix.m_Tuples[1])};
	return vectorPermute(
		vectorPermute(rowOne[0], rowOne[1], 0, 4, 0, 4), vectorZero(),
		0, 1, 4, 5);
}
WideVector FRAO_VECTORCALL
matrix2DDeterminant(RegArgMatrix matrix) noexcept
{
	WideVector rowOne = matrix.m_Tuples[0],
			   rowTwo = matrix.m_Tuples[1],
			   // calculate row1_x*row2_y
		firstTerm =
			vectorMultiply(rowOne, vectorRotateLeft(rowTwo, 1)),
			   // calculate row1_y*row2_x
		secondTerm =
			vectorMultiply(vectorRotateLeft(rowOne, 1), rowTwo);

	// calculate full row1_x*row2_y - row1_y*row2_x, and replicate to
	// all components
	return vectorSplatX(vectorSubtract(firstTerm, secondTerm));
}
bool FRAO_VECTORCALL matrix2DInverse(WideMatrix&  result,
									  RegArgMatrix matrix) noexcept
{
	// define basic vectors, and find inverse determinant
	WideVector signVector		  = vectorSet(1.0, -1.0, 0.0, 0.0),
			   signVectorNegative = vectorSet(-1.0, 1.0, 0.0, 0.0),
			   idColThree		  = vectorSet(0.0, 0.0, 1.0, 0.0),
			   idColFour		  = vectorSet(0.0, 0.0, 0.0, 1.0),
			   rowOne			  = matrix.m_Tuples[0],
			   swizzleOne		  = vectorSwizzle(rowOne, 1, 0, 1, 0),
			   rowTwo			  = matrix.m_Tuples[1],
			   swizzleTwo		  = vectorSwizzle(rowTwo, 1, 0, 1, 0),
			   // calculate row1_x*row2_y
		detFirstTerm = vectorMultiply(rowOne, swizzleTwo),
			   // calculate row1_y*row2_x
		detSecondTerm = vectorMultiply(swizzleOne, rowTwo),
			   inverseDeterminant =
				   vectorDivide(vectorSplatOne(),
								vectorSplatX(vectorSubtract(
									detFirstTerm, detSecondTerm)));

	if (vector2DIsInf(inverseDeterminant))
	{
		// if the inverse determinant was infinite, then we have no
		// solutions, since determinant was approx. 0
		return false;
	}

	// calculate cofactor matrix rows (which are columns in the
	// inverse)
	WideVector col1 = vectorMultiply(
				   vectorMultiply(swizzleTwo, signVector),
				   inverseDeterminant),
			   col2 = vectorMultiply(
				   vectorMultiply(swizzleOne, signVectorNegative),
				   inverseDeterminant);

	result = matrixSetByColumn(col1, col2, idColThree, idColFour);

	return true;
}

bool FRAO_VECTORCALL
matrix2DIsInvertible(RegArgMatrix matrix) noexcept
{
	// instead of comparing det(matrix) to 0, compare 1/det(matrix) to
	// inf, to check for values very close to 0, that are
	// (practically-speaking) uninvertible (due to lack of floating
	// point precision)
	return !vector2DIsInf(
		vectorDivide(vectorSplatOne(), matrix2DDeterminant(matrix)));
}

namespace IMPL
{
WideVector FRAO_VECTORCALL matrix3D_2DDeterminants(
	RegArgVector rowOne, RegArgVector rowTwo) noexcept
{
	// swizzle vectors how we need them
	WideVector elements[4] = {vectorSwizzle(rowOne, 1, 0, 0, 0),
							  vectorSwizzle(rowOne, 2, 2, 1, 1),
							  vectorSwizzle(rowTwo, 1, 0, 0, 0),
							  vectorSwizzle(rowTwo, 2, 2, 1, 1)};

	return vectorSubtract(vectorMultiply(elements[0], elements[3]),
						  vectorMultiply(elements[1], elements[2]));
}
}  // namespace IMPL

// vector 3D specific maths operations
WideVector FRAO_VECTORCALL vector3DDot(RegArgVector lhsVec,
										RegArgVector rhsVec) noexcept
{
#ifdef FRAO_AVX
	WideVector preMult = vectorMultiply(lhsVec, rhsVec);

	// prepare halves of splatted (xL*xR + yL*yR) and (zL*zR, ?)
	__m128d lhsHalf = _mm256_castpd256_pd128(
				_mm256_hadd_pd(preMult, preMult)),
			rhsHalf = _mm256_extractf128_pd(preMult, 0b1),
			sum		= _mm_add_pd(lhsHalf, rhsHalf);

	// combine halves for full sum
	return _mm256_movedup_pd(_mm256_set_m128d(sum, sum));
#else

	WideVector intermediate = vectorMultiply(lhsVec, rhsVec);

	double res = intermediate.m_Values[0] + intermediate.m_Values[1]
				 + intermediate.m_Values[2];

	return WideVector(res, res, res, res);
#endif
}
WideVector FRAO_VECTORCALL
vector3DCross(RegArgVector lhsVec, RegArgVector rhsVec) noexcept
{
#ifdef FRAO_AVX2
	// 0xc9 = swizzle to (y, z, x, w)
	// 0xd2 = swizzle to (z, x, y, w)
	WideVector lhsLeft	= _mm256_permute4x64_pd(lhsVec, 0xC9),
			   lhsRight = _mm256_permute4x64_pd(lhsVec, 0xD2),
			   rhsLeft	= _mm256_permute4x64_pd(rhsVec, 0xD2),
			   rhsRight = _mm256_permute4x64_pd(rhsVec, 0xC9);

#else
	WideVector lhsLeft	= vectorSwizzle(lhsVec, 1, 2, 0, 3),
			   lhsRight = vectorSwizzle(lhsVec, 2, 0, 1, 3),
			   rhsLeft	= vectorSwizzle(rhsVec, 2, 0, 1, 3),
			   rhsRight = vectorSwizzle(rhsVec, 1, 2, 0, 3);
#endif

	return vectorSubtract(vectorMultiply(lhsLeft, rhsLeft),
						  vectorMultiply(lhsRight, rhsRight));
}


// vector 3D length operations
WideVector FRAO_VECTORCALL
vector3DLengthSq(RegArgVector vector) noexcept
{
	return vector3DDot(vector, vector);
}
WideVector FRAO_VECTORCALL
vector3DLength(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	return _mm256_sqrt_pd(vector3DLengthSq(vector));
#else

	WideVector lengthSq = vector3DLengthSq(vector), result;

	result.m_Values[0] = result.m_Values[1] = result.m_Values[2] =
		result.m_Values[3] = sqrt(lengthSq.m_Values[0]);

	return result;
#endif
}
WideVector FRAO_VECTORCALL
vector3DNormalised(RegArgVector vector) noexcept
{
	return vectorDivide(vector, vector3DLength(vector));
}
//
WideVector FRAO_VECTORCALL
vector3DTransform(RegArgVector vector, RegArgMatrix matrix) noexcept
{
	WideVector rowOne[3] = {vector3DDot(vector, matrix.m_Tuples[0]),
							vector3DDot(vector, matrix.m_Tuples[1]),
							vector3DDot(vector, matrix.m_Tuples[2])};
	return vectorPermute(
		vectorPermute(rowOne[0], rowOne[1], 0, 4, 0, 4),
		vectorPermute(rowOne[2], vectorZero(), 0, 4, 0, 4), 0, 1, 4,
		5);
}
WideVector FRAO_VECTORCALL
matrix3DDeterminant(RegArgMatrix matrix) noexcept
{
	WideVector subDeterminants = IMPL::matrix3D_2DDeterminants(
				   matrix.m_Tuples[1], matrix.m_Tuples[2]),
			   signVector = vectorSet(1.0, -1.0, 1.0, -1.0);

	return vector3DDot(vectorMultiply(matrix.m_Tuples[0], signVector),
					   subDeterminants);
}
bool FRAO_VECTORCALL matrix3DInverse(WideMatrix&  result,
									  RegArgMatrix matrix) noexcept
{
	// define basic vectors, and find inverse determinant
	WideVector signVector			= vectorSet(1.0, -1.0, 1.0, 0.0),
			   signVectorNegative	= vectorSet(-1.0, 1.0, -1.0, 0.0),
			   idColFour			= vectorSet(0.0, 0.0, 0.0, 1.0),
			   subDeterminants_col1 = vectorMultiply(
				   IMPL::matrix3D_2DDeterminants(matrix.m_Tuples[1],
												 matrix.m_Tuples[2]),
				   signVector),
			   inverseDeterminant =
				   vectorDivide(vectorSplatOne(),
								vector3DDot(matrix.m_Tuples[0],
											subDeterminants_col1));

	if (vector3DIsInf(inverseDeterminant))
	{
		// if the inverse determinant was infinite, then we have no
		// solutions, since determinant was approx. 0
		return false;
	}

	// calculate other two columns, if we actually have an inverse
	WideVector subDeterminants_col2 = vectorMultiply(
				   IMPL::matrix3D_2DDeterminants(matrix.m_Tuples[0],
												 matrix.m_Tuples[2]),
				   signVectorNegative),
			   subDeterminants_col3 = vectorMultiply(
				   IMPL::matrix3D_2DDeterminants(matrix.m_Tuples[0],
												 matrix.m_Tuples[1]),
				   signVector);

	result = matrixSetByColumn(
		vectorMultiply(subDeterminants_col1, inverseDeterminant),
		vectorMultiply(subDeterminants_col2, inverseDeterminant),
		vectorMultiply(subDeterminants_col3, inverseDeterminant),
		idColFour);
	return true;
}
bool FRAO_VECTORCALL
matrix3DIsInvertible(RegArgMatrix matrix) noexcept
{
	// instead of comparing det(matrix) to 0, compare 1/det(matrix) to
	// inf, to check for values very close to 0, that are
	// (practically-speaking) uninvertible (due to lack of floating
	// point precision)
	return !vector3DIsInf(
		vectorDivide(vectorSplatOne(), matrix3DDeterminant(matrix)));
}

// 4D matrix implentation details
namespace IMPL
{
void FRAO_VECTORCALL matrix4D_2DDeterminants(
	RegArgVector rowOne, RegArgVector rowTwo, WideVector& outSetOne,
	WideVector& outSetTwo, WideVector& outSetThree) noexcept
{
	// prepare input rows for calculation
	WideVector swizzle_r1_1 = vectorSwizzle(rowOne, 1, 0, 0, 0),
			   swizzle_r1_2 = vectorSwizzle(rowOne, 2, 2, 1, 1),
			   swizzle_r1_3 = vectorSwizzle(rowOne, 3, 3, 3, 2),
			   swizzle_r2_1 = vectorSwizzle(rowTwo, 1, 0, 0, 0),
			   swizzle_r2_2 = vectorSwizzle(rowTwo, 2, 2, 1, 1),
			   swizzle_r2_3 = vectorSwizzle(rowTwo, 3, 3, 3, 2);

	// calculate 2D determinants
	outSetOne =
		vectorSubtract(vectorMultiply(swizzle_r1_2, swizzle_r2_3),
					   vectorMultiply(swizzle_r1_3, swizzle_r2_2)),
	outSetTwo =
		vectorSubtract(vectorMultiply(swizzle_r1_1, swizzle_r2_3),
					   vectorMultiply(swizzle_r1_3, swizzle_r2_1)),
	outSetThree =
		vectorSubtract(vectorMultiply(swizzle_r1_1, swizzle_r2_2),
					   vectorMultiply(swizzle_r1_2, swizzle_r2_1));
}
WideVector FRAO_VECTORCALL matrix4D_3DDeterminants(
	RegArgVector dets2DOne, RegArgVector dets2DTwo,
	RegArgVector dets2DThree, RegArgVector swizzleOne,
	RegArgVector swizzleTwo, RegArgVector swizzleThree) noexcept
{
	WideVector components[3] = {
		vectorMultiply(swizzleOne, dets2DOne),
		vectorMultiply(swizzleTwo, dets2DTwo),
		vectorMultiply(swizzleThree, dets2DThree)};

	return vectorSubtract(
		vectorAddition(components[0], components[2]), components[1]);
}
}  // namespace IMPL


// vector 4D specific maths operations
WideVector FRAO_VECTORCALL vector4DDot(RegArgVector lhsVec,
										RegArgVector rhsVec) noexcept
{
#ifdef FRAO_AVX
	WideVector intermediate = vectorMultiply(lhsVec, rhsVec);
	intermediate = _mm256_hadd_pd(intermediate, intermediate);
	WideVector reverseInter =
		_mm256_permute2f128_pd(intermediate, intermediate, 0b1);

	return _mm256_add_pd(intermediate, reverseInter);
#else

	WideVector intermediate = vectorMultiply(lhsVec, rhsVec);

	double res = intermediate.m_Values[0] + intermediate.m_Values[1]
				 + intermediate.m_Values[2]
				 + intermediate.m_Values[3];

	return WideVector(res, res, res, res);
#endif
}

// vector 4D length operations
WideVector FRAO_VECTORCALL
vector4DLengthSq(RegArgVector vector) noexcept
{
	return vector4DDot(vector, vector);
}
WideVector FRAO_VECTORCALL
vector4DLength(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	return _mm256_sqrt_pd(vector4DLengthSq(vector));
#else

	WideVector lengthSq = vector4DLengthSq(vector), result;

	result.m_Values[0] = result.m_Values[1] = result.m_Values[2] =
		result.m_Values[3] = sqrt(lengthSq.m_Values[0]);

	return result;
#endif
}
WideVector FRAO_VECTORCALL
vector4DNormalised(RegArgVector vector) noexcept
{
	return vectorDivide(vector, vector4DLength(vector));
}
//
WideVector FRAO_VECTORCALL
vector4DTransform(RegArgVector vector, RegArgMatrix matrix) noexcept
{
	WideVector rowOne[4] = {vector4DDot(vector, matrix.m_Tuples[0]),
							vector4DDot(vector, matrix.m_Tuples[1]),
							vector4DDot(vector, matrix.m_Tuples[2]),
							vector4DDot(vector, matrix.m_Tuples[3])};
	return vectorPermute(
		vectorPermute(rowOne[0], rowOne[1], 0, 4, 0, 4),
		vectorPermute(rowOne[2], rowOne[3], 0, 4, 0, 4), 0, 1, 4, 5);
}
WideVector FRAO_VECTORCALL
matrix4DDeterminant(RegArgMatrix matrix) noexcept
{
	// calculate 2D determinants
	WideVector subDeterminants2D[3];
	IMPL::matrix4D_2DDeterminants(
		matrix.m_Tuples[2], matrix.m_Tuples[3], subDeterminants2D[0],
		subDeterminants2D[1], subDeterminants2D[2]);

	// swizzle things into the order we need them
	WideVector elements[3] = {
		vectorSwizzle(matrix.m_Tuples[1], 1, 0, 0, 0),
		vectorSwizzle(matrix.m_Tuples[1], 2, 2, 1, 1),
		vectorSwizzle(matrix.m_Tuples[1], 3, 3, 3, 2)};

	// calculate 3D determinants, and prepare for final calculation
	WideVector signVector		 = vectorSet(1.0, -1.0, 1.0, -1.0),
			   subDeterminants3D = IMPL::matrix4D_3DDeterminants(
				   subDeterminants2D[0], subDeterminants2D[1],
				   subDeterminants2D[2], elements[0], elements[1],
				   elements[2]),
			   lhsProduct =
				   vectorMultiply(matrix.m_Tuples[0], signVector);

	return vector4DDot(lhsProduct, subDeterminants3D);
}
bool FRAO_VECTORCALL matrix4DInverse(WideMatrix&  result,
									  RegArgMatrix matrix) noexcept
{
	// calculate 2D determinants for 4D determinant calculation
	WideVector subDeterminants2D_cd[3];
	IMPL::matrix4D_2DDeterminants(
		matrix.m_Tuples[2], matrix.m_Tuples[3],
		subDeterminants2D_cd[0], subDeterminants2D_cd[1],
		subDeterminants2D_cd[2]);

	// swizzle things into the order we need them
	WideVector elementsOfB[3] = {
		vectorSwizzle(matrix.m_Tuples[1], 1, 0, 0, 0),
		vectorSwizzle(matrix.m_Tuples[1], 2, 2, 1, 1),
		vectorSwizzle(matrix.m_Tuples[1], 3, 3, 3, 2)};

	// calculate 3D determinants, and prepare for final calculation
	WideVector signVector = vectorSet(1.0, -1.0, 1.0, -1.0),
			   subDeterminants3D_col1 = vectorMultiply(
				   IMPL::matrix4D_3DDeterminants(
					   subDeterminants2D_cd[0],
					   subDeterminants2D_cd[1],
					   subDeterminants2D_cd[2], elementsOfB[0],
					   elementsOfB[1], elementsOfB[2]),
				   signVector),
			   inverseDeterminant =
				   vectorDivide(vectorSplatOne(),
								vector4DDot(matrix.m_Tuples[0],
											subDeterminants3D_col1));

	if (vector4DIsInf(inverseDeterminant))
	{
		// if the inverse determinant was infinite, then we have no
		// solutions, since determinant was approx. 0
		return false;
	}

	// calculate remaining 2D determinants
	WideVector subDeterminants2D_bd[3];
	WideVector subDeterminants2D_bc[3];
	IMPL::matrix4D_2DDeterminants(
		matrix.m_Tuples[1], matrix.m_Tuples[3],
		subDeterminants2D_bd[0], subDeterminants2D_bd[1],
		subDeterminants2D_bd[2]);
	IMPL::matrix4D_2DDeterminants(
		matrix.m_Tuples[1], matrix.m_Tuples[2],
		subDeterminants2D_bc[0], subDeterminants2D_bc[1],
		subDeterminants2D_bc[2]);

	// swizzle things into the order we need them
	WideVector elementsOfA[3] = {
		vectorSwizzle(matrix.m_Tuples[0], 1, 0, 0, 0),
		vectorSwizzle(matrix.m_Tuples[0], 2, 2, 1, 1),
		vectorSwizzle(matrix.m_Tuples[0], 3, 3, 3, 2)};

	// calculate 3D determinants, and prepare for final calculation
	WideVector signVectorNegative = vectorSet(-1.0, 1.0, -1.0, 1.0),
			   subDeterminants3D_col2 = vectorMultiply(
				   IMPL::matrix4D_3DDeterminants(
					   subDeterminants2D_cd[0],
					   subDeterminants2D_cd[1],
					   subDeterminants2D_cd[2], elementsOfA[0],
					   elementsOfA[1], elementsOfA[2]),
				   signVectorNegative),
			   subDeterminants3D_col3 = vectorMultiply(
				   IMPL::matrix4D_3DDeterminants(
					   subDeterminants2D_bd[0],
					   subDeterminants2D_bd[1],
					   subDeterminants2D_bd[2], elementsOfA[0],
					   elementsOfA[1], elementsOfA[2]),
				   signVector),
			   subDeterminants3D_col4 = vectorMultiply(
				   IMPL::matrix4D_3DDeterminants(
					   subDeterminants2D_bc[0],
					   subDeterminants2D_bc[1],
					   subDeterminants2D_bc[2], elementsOfA[0],
					   elementsOfA[1], elementsOfA[2]),
				   signVectorNegative);

	result = matrixSetByColumn(
		vectorMultiply(subDeterminants3D_col1, inverseDeterminant),
		vectorMultiply(subDeterminants3D_col2, inverseDeterminant),
		vectorMultiply(subDeterminants3D_col3, inverseDeterminant),
		vectorMultiply(subDeterminants3D_col4, inverseDeterminant));

	return true;
}
bool FRAO_VECTORCALL
matrix4DIsInvertible(RegArgMatrix matrix) noexcept
{
	// instead of comparing det(matrix) to 0, compare 1/det(matrix) to
	// inf, to check for values very close to 0, that are
	// (practically-speaking) uninvertible (due to lack of floating
	// point precision)
	return !vector4DIsInf(
		vectorDivide(vectorSplatOne(), matrix4DDeterminant(matrix)));
}
}  // namespace Maths
}  // namespace frao