//! \file
//!
//! \brief Implementation of data storage types, including those that
//! store vector and matrix data, and interactions with these types
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "DataStorageTypes.hpp"
#include <Nucleus_inc\Environment.hpp>	//for EMIT_WARNING
#include "VectorTypes.hpp"				//for complete vector types

namespace frao
{
namespace Maths
{
// loads of NarrowVector, NarrowIntVector, WideVector, and
// WideIntVector
NarrowVector loadFloat2(const Float2& data) noexcept
{
#ifdef FRAO_AVX
	NarrowVector xVec = _mm_load_ss(&data.m_Components.m_X),
				 yVec = _mm_load_ss(&data.m_Components.m_Y);

	return _mm_unpacklo_ps(xVec, yVec);
#else
	return NarrowVector(data.m_Components.m_X, data.m_Components.m_Y,
						.0, .0);
#endif
}
NarrowVector loadFloat3(const Float3& data) noexcept
{
#ifdef FRAO_AVX
	NarrowVector xVec = _mm_load_ss(&data.m_Components.m_X),
				 yVec = _mm_load_ss(&data.m_Components.m_Y),
				 zVec = _mm_broadcast_ss(&data.m_Components.m_Z),
				 // permute to (z, y, z, 0)
		endVec = _mm_unpacklo_ps(zVec, yVec);

	// permute to (x, y, z, 0)
	return _mm_move_ss(endVec, xVec);
#else

	return NarrowVector(data.m_Components.m_X, data.m_Components.m_Y,
						data.m_Components.m_Z, .0);
#endif
}
NarrowVector loadFloat4(const Float4& data) noexcept
{
#ifdef FRAO_AVX
	return _mm_loadu_ps(data.m_Members);
#else

	return NarrowVector(data.m_Components.m_X, data.m_Components.m_Y,
						data.m_Components.m_Z, data.m_Components.m_W);
#endif
}
NarrowIntVector loadSmallInt2(const SmallInt2& data) noexcept
{
#ifdef FRAO_AVX
	NarrowIntVector xVec = _mm_loadu_si32(&data.m_Components.m_X),
					yVec = _mm_loadu_si32(&data.m_Components.m_Y);

	return _mm_unpacklo_epi32(xVec, yVec);
#else
	return NarrowIntVector(data.m_Components.m_X,
						   data.m_Components.m_Y, 0, 0);
#endif
}
NarrowIntVector loadSmallInt3(const SmallInt3& data) noexcept
{
#ifdef FRAO_AVX
	NarrowIntVector xVec = _mm_loadu_si32(&data.m_Components.m_X),
					yVec = _mm_loadu_si32(&data.m_Components.m_Y),
					// permute to (x, y, 0, 0)
		startVec = _mm_unpacklo_epi32(xVec, yVec);

	// insert z for (x, y, z, 0)
	return _mm_insert_epi32(startVec, data.m_Components.m_Z, 2);
#else

	return NarrowIntVector(data.m_Components.m_X,
						   data.m_Components.m_Y,
						   data.m_Components.m_Z, 0);
#endif
}
NarrowIntVector loadSmallInt4(const SmallInt4& data) noexcept
{
#ifdef FRAO_AVX
	return _mm_loadu_si128(
		reinterpret_cast<const __m128i*>(data.m_Members));
#else

	return NarrowIntVector(
		data.m_Components.m_X, data.m_Components.m_Y,
		data.m_Components.m_Z, data.m_Components.m_W);
#endif
}
WideVector loadDouble2(const Double2& data) noexcept
{
#ifdef FRAO_AVX
	__m128d comb = _mm_loadu_pd(data.m_Members);

	// extend to (x, y, 0, 0)
	return _mm256_zextpd128_pd256(comb);
#else

	return WideVector(data.m_Components.m_X, data.m_Components.m_Y,
					  .0, .0);
#endif
}
WideVector loadDouble3(const Double3& data) noexcept
{
#ifdef FRAO_AVX
	__m128d zVec = _mm_load_sd(&data.m_Components.m_Z),
			// get (x, y)
		beginVec = _mm_loadu_pd(data.m_Members);

	// combine to (x, y, z, 0)
	return _mm256_set_m128d(zVec, beginVec);
#else

	return WideVector(data.m_Components.m_X, data.m_Components.m_Y,
					  data.m_Components.m_Z, .0);
#endif
}
WideVector loadDouble4(const Double4& data) noexcept
{
#ifdef FRAO_AVX
	return _mm256_loadu_pd(data.m_Members);
#else

	return WideVector(data.m_Components.m_X, data.m_Components.m_Y,
					  data.m_Components.m_Z, data.m_Components.m_W);
#endif
}
//
NarrowMatrix loadFloat2x2(const Float2x2& data) noexcept
{
	return NarrowMatrix(data.m_Tuples[0], data.m_Tuples[1]);
}
NarrowMatrix loadFloat3x3(const Float3x3& data) noexcept
{
	return NarrowMatrix(data.m_Tuples[0], data.m_Tuples[1],
						data.m_Tuples[2]);
}
NarrowMatrix loadFloat4x4(const Float4x4& data) noexcept
{
	return NarrowMatrix(data.m_Tuples[0], data.m_Tuples[1],
						data.m_Tuples[2], data.m_Tuples[3]);
}
//
WideMatrix loadDouble2x2(const Double2x2& data) noexcept
{
	return WideMatrix(data.m_Tuples[0], data.m_Tuples[1]);
}
WideMatrix loadDouble3x3(const Double3x3& data) noexcept
{
	return WideMatrix(data.m_Tuples[0], data.m_Tuples[1],
					  data.m_Tuples[2]);
}
WideMatrix loadDouble4x4(const Double4x4& data) noexcept
{
	return WideMatrix(data.m_Tuples[0], data.m_Tuples[1],
					  data.m_Tuples[2], data.m_Tuples[3]);
}
//
WideIntVector loadInteger2(const Integer2& data) noexcept
{
#ifdef FRAO_AVX2
	__m128i comb = _mm_loadu_si128(
		reinterpret_cast<const __m128i*>(data.m_Members));

	return _mm256_zextsi128_si256(comb);
#else

	return WideIntVector(data.m_Components.m_X, data.m_Components.m_Y,
						 0, 0);
#endif
}
WideIntVector loadInteger3(const Integer3& data) noexcept
{
#ifdef FRAO_AVX2
	__m128i zVec  = _mm_loadu_si64(&data.m_Components.m_Z),
			lower = _mm_loadu_si128(
				reinterpret_cast<const __m128i*>(data.m_Members));

	return _mm256_set_m128i(zVec, lower);
#else

	return WideIntVector(data.m_Components.m_X, data.m_Components.m_Y,
						 data.m_Components.m_Z, 0);
#endif
}
WideIntVector loadInteger4(const Integer4& data) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_loadu_si256(
		reinterpret_cast<const __m256i*>(data.m_Members));
#else

	return WideIntVector(data.m_Components.m_X, data.m_Components.m_Y,
						 data.m_Components.m_Z,
						 data.m_Components.m_W);
#endif
}

// stores of NarrowVector, NarrowIntVector, WideVector, and
// WideIntVector
void FRAO_VECTORCALL storeFloat2(Float2&	   store,
								  RegArgNrwVec data) noexcept
{
#ifdef FRAO_AVX
	_mm_store_ss(&store.m_Components.m_X, data);
	_mm_store_ss(&store.m_Components.m_Y, _mm_permute_ps(data, 0x1));
#else

	store.m_Components.m_X = data.m_Values[0];
	store.m_Components.m_Y = data.m_Values[1];
#endif
}
void FRAO_VECTORCALL storeFloat3(Float3&	   store,
								  RegArgNrwVec data) noexcept
{
#ifdef FRAO_AVX
	_mm_store_ss(&store.m_Components.m_X, data);
	_mm_store_ss(&store.m_Components.m_Y, _mm_permute_ps(data, 0x1));
	_mm_store_ss(&store.m_Components.m_Z, _mm_permute_ps(data, 0x2));
#else

	store.m_Components.m_X = data.m_Values[0];
	store.m_Components.m_Y = data.m_Values[1];
	store.m_Components.m_Z = data.m_Values[2];
#endif
}
void FRAO_VECTORCALL storeFloat4(Float4&	   store,
								  RegArgNrwVec data) noexcept
{
#ifdef FRAO_AVX
	_mm_storeu_ps(store.m_Members, data);
#else

	store.m_Components.m_X = data.m_Values[0];
	store.m_Components.m_Y = data.m_Values[1];
	store.m_Components.m_Z = data.m_Values[2];
	store.m_Components.m_W = data.m_Values[3];
#endif
}
void FRAO_VECTORCALL storeSmallInt2(SmallInt2&	  store,
									 RegIntNrwVec data) noexcept
{
#ifdef FRAO_AVX
	_mm_storeu_si64(store.m_Members, data);
#else

	store.m_Components.m_X = data.m_Values[0];
	store.m_Components.m_Y = data.m_Values[1];
#endif
}
void FRAO_VECTORCALL storeSmallInt3(SmallInt3&	  store,
									 RegIntNrwVec data) noexcept
{
#ifdef FRAO_AVX
	_mm_storeu_si64(store.m_Members, data);
	_mm_storeu_si32(store.m_Members + 2,
					_mm_srli_si128(data, 8));
#else

	store.m_Components.m_X = data.m_Values[0];
	store.m_Components.m_Y = data.m_Values[1];
	store.m_Components.m_Z = data.m_Values[2];
#endif
}
void FRAO_VECTORCALL storeSmallInt4(SmallInt4&	  store,
									 RegIntNrwVec data) noexcept
{
#ifdef FRAO_AVX
	_mm_storeu_si64(store.m_Members, data);
	_mm_storeu_si64(store.m_Members + 2, _mm_srli_si128(data, 8));
#else

	store.m_Components.m_X = data.m_Values[0];
	store.m_Components.m_Y = data.m_Values[1];
	store.m_Components.m_Z = data.m_Values[2];
	store.m_Components.m_W = data.m_Values[3];
#endif
}
void FRAO_VECTORCALL storeDouble2(Double2&		store,
								   RegArgVector data) noexcept
{
#ifdef FRAO_AVX
	__m128d lower = _mm256_castpd256_pd128(data);

	// NOTE: need to have member array here, like I thought?
	// MOVUPD for 4 latency, but requires an array to store to
	_mm_storeu_pd(store.m_Members, lower);

#else

	store.m_Components.m_X = data.m_Values[0];
	store.m_Components.m_Y = data.m_Values[1];
#endif
}
void FRAO_VECTORCALL storeDouble3(Double3&		store,
								   RegArgVector data) noexcept
{
#ifdef FRAO_AVX
	__m128d lower = _mm256_castpd256_pd128(data),
			upper = _mm256_extractf128_pd(data, 0x1);

	_mm_storeu_pd(store.m_Members, lower);
	_mm_store_sd(store.m_Members + 2, upper);
#else

	store.m_Components.m_X = data.m_Values[0];
	store.m_Components.m_Y = data.m_Values[1];
	store.m_Components.m_Z = data.m_Values[2];
#endif
}
void FRAO_VECTORCALL storeDouble4(Double4&		store,
								   RegArgVector data) noexcept
{
#ifdef FRAO_AVX
	_mm256_storeu_pd(store.m_Members, data);
#else

	store.m_Components.m_X = data.m_Values[0];
	store.m_Components.m_Y = data.m_Values[1];
	store.m_Components.m_Z = data.m_Values[2];
	store.m_Components.m_W = data.m_Values[3];
#endif
}
//
void FRAO_VECTORCALL storeFloat2x2(Float2x2&	  store,
									RegArgNrwMtrx data) noexcept
{
	storeFloat2(store.m_Tuples[0], data.m_Tuples[0]);
	storeFloat2(store.m_Tuples[1], data.m_Tuples[1]);
}
void FRAO_VECTORCALL storeFloat3x3(Float3x3&	  store,
									RegArgNrwMtrx data) noexcept
{
	storeFloat3(store.m_Tuples[0], data.m_Tuples[0]);
	storeFloat3(store.m_Tuples[1], data.m_Tuples[1]);
	storeFloat3(store.m_Tuples[2], data.m_Tuples[2]);
}
void FRAO_VECTORCALL storeFloat4x4(Float4x4&	  store,
									RegArgNrwMtrx data) noexcept
{
	storeFloat4(store.m_Tuples[0], data.m_Tuples[0]);
	storeFloat4(store.m_Tuples[1], data.m_Tuples[1]);
	storeFloat4(store.m_Tuples[2], data.m_Tuples[2]);
	storeFloat4(store.m_Tuples[3], data.m_Tuples[3]);
}
//
void FRAO_VECTORCALL storeDouble2x2(Double2x2&	  store,
									 RegArgMatrix data) noexcept
{
	storeDouble2(store.m_Tuples[0], data.m_Tuples[0]);
	storeDouble2(store.m_Tuples[1], data.m_Tuples[1]);
}
void FRAO_VECTORCALL storeDouble3x3(Double3x3&	  store,
									 RegArgMatrix data) noexcept
{
	storeDouble3(store.m_Tuples[0], data.m_Tuples[0]);
	storeDouble3(store.m_Tuples[1], data.m_Tuples[1]);
	storeDouble3(store.m_Tuples[2], data.m_Tuples[2]);
}
void FRAO_VECTORCALL storeDouble4x4(Double4x4&	  store,
									 RegArgMatrix data) noexcept
{
	storeDouble4(store.m_Tuples[0], data.m_Tuples[0]);
	storeDouble4(store.m_Tuples[1], data.m_Tuples[1]);
	storeDouble4(store.m_Tuples[2], data.m_Tuples[2]);
	storeDouble4(store.m_Tuples[3], data.m_Tuples[3]);
}
//
void FRAO_VECTORCALL storeInteger2(Integer2&	 store,
									RegIntVector data) noexcept
{
#ifdef FRAO_AVX2
	__m128i lower = _mm256_castsi256_si128(data);

	_mm_storeu_si128(reinterpret_cast<__m128i*>(store.m_Members),
					 lower);
#else

	store.m_Components.m_X = data.m_Values[0];
	store.m_Components.m_Y = data.m_Values[1];
#endif
}
void FRAO_VECTORCALL storeInteger3(Integer3&	 store,
									RegIntVector data) noexcept
{
#ifdef FRAO_AVX2
	__m128i lower = _mm256_castsi256_si128(data),
			upper = _mm256_extracti128_si256(data, 0x1);

	_mm_storeu_si128(reinterpret_cast<__m128i*>(store.m_Members),
					 lower);
	_mm_storeu_si64(store.m_Members + 2, upper);
#else

	store.m_Components.m_X = data.m_Values[0];
	store.m_Components.m_Y = data.m_Values[1];
	store.m_Components.m_Z = data.m_Values[2];
#endif
}
void FRAO_VECTORCALL storeInteger4(Integer4&	 store,
									RegIntVector data) noexcept
{
#ifdef FRAO_AVX2
	_mm256_storeu_si256(reinterpret_cast<__m256i*>(store.m_Members),
						data);
#else

	store.m_Components.m_X = data.m_Values[0];
	store.m_Components.m_Y = data.m_Values[1];
	store.m_Components.m_Z = data.m_Values[2];
	store.m_Components.m_W = data.m_Values[3];
#endif
}
}  // namespace Maths
}  // namespace frao