//! \file
//!
//! \brief Header of conversions between different types/types of data
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "Conversions.hpp"
#include <cmath>						//for std::floor/ceiling etc
#include <cstdint>						//for integer types
#include <Nucleus_inc\Environment.hpp>	//for EMIT_WARNING
#include "ComponentAccess.hpp"			//for getters/setters
#include "VectorTypes.hpp"				//for complete vector types

#ifndef FRAO_AVX
#include <cfenv>  //for std::fesetround
#endif
namespace frao
{
namespace Maths
{
// double / int bit pattern reinterpretation
WideVector FRAO_VECTORCALL
reinterpretToDouble(RegIntVector vector) noexcept
{
#if defined(FRAO_AVX2)
	return _mm256_castsi256_pd(vector);
#elif defined(FRAO_AVX)
	return _mm256_castsi256_pd(_mm256_loadu_si256(
		reinterpret_cast<const __m256i*>(vector.m_Values)));

#else

	return WideVector(
		*reinterpret_cast<const double*>(&vector.m_Values[0]),
		*reinterpret_cast<const double*>(&vector.m_Values[1]),
		*reinterpret_cast<const double*>(&vector.m_Values[2]),
		*reinterpret_cast<const double*>(&vector.m_Values[3]));
#endif
}
WideIntVector FRAO_VECTORCALL
reinterpretToInteger(RegArgVector vector) noexcept
{
#if defined(FRAO_AVX2)
	return _mm256_castpd_si256(vector);
#elif defined(FRAO_AVX)
	WideIntVector result;

	_mm256_storeu_pd(reinterpret_cast<double*>(result.m_Values),
					 vector);

	return result;
#else

	return WideIntVector(
		*reinterpret_cast<const int_least64_t*>(&vector.m_Values[0]),
		*reinterpret_cast<const int_least64_t*>(&vector.m_Values[1]),
		*reinterpret_cast<const int_least64_t*>(&vector.m_Values[2]),
		*reinterpret_cast<const int_least64_t*>(&vector.m_Values[3]));
#endif
}

// double/int conversions
WideVector FRAO_VECTORCALL
toDoubleVector(RegIntVector vector) noexcept
{
// There exists no instruction below AVX512 to actually convert int64
// and double data, so we do things using standard x64 single
// conversions. There will certainly be some bit-twiddly way to do
// this efficiently, but we have no current need for efficient
// conversion, so this is left as the naive method
#if defined(FRAO_AVX2)
	int_least64_t preConv[4];
	double		  postConv[4];

	_mm256_storeu_si256(reinterpret_cast<__m256i*>(preConv), vector);

	postConv[0] = static_cast<double>(preConv[0]);
	postConv[1] = static_cast<double>(preConv[1]);
	postConv[2] = static_cast<double>(preConv[2]);
	postConv[3] = static_cast<double>(preConv[3]);

	return _mm256_loadu_pd(postConv);

#elif defined(FRAO_AVX)
	double convVals[4] = {static_cast<double>(vector.m_Values[0]),
						  static_cast<double>(vector.m_Values[1]),
						  static_cast<double>(vector.m_Values[2]),
						  static_cast<double>(vector.m_Values[3])};

	return _mm256_loadu_pd(convVals);
#else

	return WideVector(static_cast<double>(vector.m_Values[0]),
					  static_cast<double>(vector.m_Values[1]),
					  static_cast<double>(vector.m_Values[2]),
					  static_cast<double>(vector.m_Values[3]));
#endif
}
// convert a double vector to an integer one
WideIntVector FRAO_VECTORCALL
toIntegerVector(RegArgVector vector) noexcept
{
// There exists no instruction below AVX512 to actually convert int64
// and double data, so we do things using standard x64 single
// conversions. There will certainly be some bit-twiddly way to do
// this efficiently, but we have no current need for efficient
// conversion, so this is left as the naive method
#if defined(FRAO_AVX2)
	double		  preConv[4];
	int_least64_t postConv[4];

	_mm256_storeu_pd(preConv, vector);

	postConv[0] = static_cast<int_least64_t>(preConv[0]);
	postConv[1] = static_cast<int_least64_t>(preConv[1]);
	postConv[2] = static_cast<int_least64_t>(preConv[2]);
	postConv[3] = static_cast<int_least64_t>(preConv[3]);

	return _mm256_loadu_si256(reinterpret_cast<__m256i*>(postConv));

#elif defined(FRAO_AVX)

	double convVals[4];
	_mm256_storeu_pd(convVals, vector);

	return WideIntVector(static_cast<int_least64_t>(convVals[0]),
						 static_cast<int_least64_t>(convVals[1]),
						 static_cast<int_least64_t>(convVals[2]),
						 static_cast<int_least64_t>(convVals[3]));
#else

	return WideIntVector(
		static_cast<int_least64_t>(vector.m_Values[0]),
		static_cast<int_least64_t>(vector.m_Values[1]),
		static_cast<int_least64_t>(vector.m_Values[2]),
		static_cast<int_least64_t>(vector.m_Values[3]));
#endif
}

// Wide/narrow conversions
NarrowVector FRAO_VECTORCALL
toNarrowVector(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	return _mm256_cvtpd_ps(vector);
#else

	// TODO: use _mm256_cvtpd_ps for this, on AVX
	return NarrowVector(static_cast<float>(vector.m_Values[0]),
						static_cast<float>(vector.m_Values[1]),
						static_cast<float>(vector.m_Values[2]),
						static_cast<float>(vector.m_Values[3]));
#endif
}
WideVector FRAO_VECTORCALL toWideVector(RegArgNrwVec vector) noexcept
{
#ifdef FRAO_AVX
	return _mm256_cvtps_pd(vector);
#else

	// TODO: use _mm256_cvtps_pd for this, on AVX
	return WideVector(static_cast<double>(vector.m_Values[0]),
					  static_cast<double>(vector.m_Values[1]),
					  static_cast<double>(vector.m_Values[2]),
					  static_cast<double>(vector.m_Values[3]));
#endif
}
NarrowIntVector FRAO_VECTORCALL
toSmallIntVector(RegIntVector vector) noexcept
{
#if defined(FRAO_AVX2)
	NarrowIntVector xyVec = _mm256_castsi256_si128(vector),
					zwVec = _mm256_extractf128_si256(vector, 0b1);

	xyVec = _mm_shuffle_epi32(xyVec, 0b1000'1000);
	zwVec = _mm_shuffle_epi32(zwVec, 0b1000'1000);

	return _mm_blend_epi32(xyVec, zwVec, 0b1100);
#elif defined(FRAO_AVX)

	// have to use vectorSet, because input is via an emulation type,
	// and output is via __m128i
	return vectorSet(static_cast<int_least32_t>(vector.m_Values[0]),
					 static_cast<int_least32_t>(vector.m_Values[1]),
					 static_cast<int_least32_t>(vector.m_Values[2]),
					 static_cast<int_least32_t>(vector.m_Values[3]));

#else

	return NarrowIntVector(
		static_cast<int_least32_t>(vector.m_Values[0]),
		static_cast<int_least32_t>(vector.m_Values[1]),
		static_cast<int_least32_t>(vector.m_Values[2]),
		static_cast<int_least32_t>(vector.m_Values[3]));
#endif
}
WideIntVector FRAO_VECTORCALL
toWideIntVector(RegIntNrwVec vector) noexcept
{
#if defined(FRAO_AVX2)
	return _mm256_cvtepi32_epi64(vector);
#elif defined(FRAO_AVX)

	// have to use vectorGetX/Y/Z/W, because input is via __m128i,
	// and output is via an emulation type
	return WideIntVector(
		static_cast<int_least64_t>(vectorGetX(vector)),
		static_cast<int_least64_t>(vectorGetY(vector)),
		static_cast<int_least64_t>(vectorGetZ(vector)),
		static_cast<int_least64_t>(vectorGetW(vector)));

#else

	return WideIntVector(
		static_cast<int_least64_t>(vector.m_Values[0]),
		static_cast<int_least64_t>(vector.m_Values[1]),
		static_cast<int_least64_t>(vector.m_Values[2]),
		static_cast<int_least64_t>(vector.m_Values[3]));
#endif
}
NarrowMatrix FRAO_VECTORCALL
toNarrowMatrix(RegArgMatrix matrix) noexcept
{
	return NarrowMatrix(toNarrowVector(matrix.m_Tuples[0]),
						toNarrowVector(matrix.m_Tuples[1]),
						toNarrowVector(matrix.m_Tuples[2]),
						toNarrowVector(matrix.m_Tuples[3]));
}
WideMatrix FRAO_VECTORCALL
toWideMatrix(RegArgNrwMtrx matrix) noexcept
{
	return WideMatrix(toWideVector(matrix.m_Tuples[0]),
					  toWideVector(matrix.m_Tuples[1]),
					  toWideVector(matrix.m_Tuples[2]),
					  toWideVector(matrix.m_Tuples[3]));
}

WideVector FRAO_VECTORCALL vectorRound(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	return _mm256_round_pd(vector, _MM_FROUND_TO_NEAREST_INT);
#else

	// make sure we have the correct rounding mode
	auto oldRoundMode = std::fegetround();
	std::fesetround(FE_TONEAREST);

	WideVector result = WideVector(
		std::rint(vector.m_Values[0]), std::rint(vector.m_Values[1]),
		std::rint(vector.m_Values[2]), std::rint(vector.m_Values[3]));

	// restore the previous rounding mode, now that the calculation is
	// complete
	std::fesetround(oldRoundMode);

	return result;
#endif
}
WideVector FRAO_VECTORCALL vectorFloor(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	return _mm256_round_pd(vector, _MM_FROUND_TO_NEG_INF);
#else

	return WideVector(std::floor(vector.m_Values[0]),
					  std::floor(vector.m_Values[1]),
					  std::floor(vector.m_Values[2]),
					  std::floor(vector.m_Values[3]));
#endif
}
WideVector FRAO_VECTORCALL
vectorCeiling(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	return _mm256_round_pd(vector, _MM_FROUND_TO_POS_INF);
#else

	return WideVector(
		std::ceil(vector.m_Values[0]), std::ceil(vector.m_Values[1]),
		std::ceil(vector.m_Values[2]), std::ceil(vector.m_Values[3]));
#endif
}
WideVector FRAO_VECTORCALL
vectorTruncate(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	return _mm256_round_pd(vector, _MM_FROUND_TO_ZERO);
#else

	return WideVector(std::trunc(vector.m_Values[0]),
					  std::trunc(vector.m_Values[1]),
					  std::trunc(vector.m_Values[2]),
					  std::trunc(vector.m_Values[3]));
#endif
}
}  // namespace Maths
}  // namespace frao