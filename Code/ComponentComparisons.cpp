//! \file
//!
//! \brief Header of comparisons between vectors
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <Nucleus_inc\BitUtilities.hpp>	//for set/extract bits in non-avx mode
#include "ComponentAccess.hpp"			//for vector replicate
#include "ComponentComparison.hpp"
#include "Conversions.hpp"	//for vector reinterpret
#include "SpecialVectors.hpp"  //for IMPL constexpr variables and vectorSplatInfinity
#include "VectorTypes.hpp"	//for complete vector types

namespace frao
{
namespace Maths
{
bool comparisonAllTrue(uint_least64_t record) noexcept
{
	return (record)
		   == reinterpret_cast<const uint_least64_t&>(
			   IMPL::g_TrueBits);
}
bool comparisonAnyTrue(uint_least64_t record) noexcept
{
	return record > 0;
}
bool comparisonAllFalse(uint_least64_t record) noexcept
{
	return record == 0;
}
bool comparisonAnyFalse(uint_least64_t record) noexcept
{
	return record < reinterpret_cast<const uint_least64_t&>(
			   IMPL::g_TrueBits);
}
bool comparisonMixedResult(uint_least64_t record) noexcept
{
	return (record < reinterpret_cast<const uint_least64_t&>(
				IMPL::g_TrueBits))
		   & (record > 0);
}

namespace IMPL
{
uint_least64_t createFullMask(uint_least64_t xRecord,
							  uint_least64_t yRecord,
							  uint_least64_t zRecord,
							  uint_least64_t wRecord) noexcept
{
	uint_least64_t lowRecord =
					   frao::SetBits(xRecord * 0xFFFF,
									 yRecord * 0xFFFF, 0xFFFF, 16),
				   highRecord =
					   frao::SetBits(zRecord * 0xFFFF,
									 wRecord * 0xFFFF, 0xFFFF, 16);

	return frao::SetBits(lowRecord & 0xFFFF'FFFF, highRecord,
						 0xFFFFFFFF, 32);
}
uint_least64_t FRAO_VECTORCALL
convRecord(RegIntVector source) noexcept
{
#if defined(FRAO_AVX2)
	uint_least64_t maskBits = static_cast<uint_least64_t>(
					   _mm256_movemask_epi8(source)),
				   components[4] = {
					   frao::ExtractBits(maskBits, 1, 0),
					   frao::ExtractBits(maskBits, 1, 8),
					   frao::ExtractBits(maskBits, 1, 16),
					   frao::ExtractBits(maskBits, 1, 24)};
#else
	uint_least64_t components[4] = {
		reinterpret_cast<const uint_least64_t*>(source.m_Values)[0]
			& 0b1,
		reinterpret_cast<const uint_least64_t*>(source.m_Values)[1]
			& 0b1,
		reinterpret_cast<const uint_least64_t*>(source.m_Values)[2]
			& 0b1,
		reinterpret_cast<const uint_least64_t*>(source.m_Values)[3]
			& 0b1};
#endif

	return createFullMask(components[0], components[1], components[2],
						  components[3]);
}
}  // namespace IMPL

WideIntVector FRAO_VECTORCALL
vectorLess(RegArgVector lhsVec, RegArgVector rhsVec) noexcept
{
#if defined(FRAO_AVX)
	return reinterpretToInteger(
		_mm256_cmp_pd(lhsVec, rhsVec, _CMP_LT_OS));
#else

	constexpr int_least64_t selection[2] = {0, IMPL::g_TrueBits};
	bool components[4] = {lhsVec.m_Values[0] < rhsVec.m_Values[0],
						  lhsVec.m_Values[1] < rhsVec.m_Values[1],
						  lhsVec.m_Values[2] < rhsVec.m_Values[2],
						  lhsVec.m_Values[3] < rhsVec.m_Values[3]};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorLess(RegIntVector lhsVec, RegIntVector rhsVec) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_cmpgt_epi64(rhsVec, lhsVec);
#else

	constexpr int_least64_t selection[2] = {0, IMPL::g_TrueBits};
	bool components[4] = {lhsVec.m_Values[0] < rhsVec.m_Values[0],
						  lhsVec.m_Values[1] < rhsVec.m_Values[1],
						  lhsVec.m_Values[2] < rhsVec.m_Values[2],
						  lhsVec.m_Values[3] < rhsVec.m_Values[3]};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorLessRecord(uint_least64_t& record, RegArgVector lhsVec,
				 RegArgVector rhsVec) noexcept
{
	WideIntVector result = vectorLess(lhsVec, rhsVec);

	record = IMPL::convRecord(result);
	return result;
}
WideIntVector FRAO_VECTORCALL
vectorLessRecord(uint_least64_t& record, RegIntVector lhsVec,
				 RegIntVector rhsVec) noexcept
{
	WideIntVector result = vectorLess(lhsVec, rhsVec);

	record = IMPL::convRecord(result);
	return result;
}

WideIntVector FRAO_VECTORCALL
vectorLessEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept
{
#if defined(FRAO_AVX)
	return reinterpretToInteger(
		_mm256_cmp_pd(lhsVec, rhsVec, _CMP_LE_OS));
#else

	constexpr int_least64_t selection[2] = {0, IMPL::g_TrueBits};
	bool components[4] = {lhsVec.m_Values[0] <= rhsVec.m_Values[0],
						  lhsVec.m_Values[1] <= rhsVec.m_Values[1],
						  lhsVec.m_Values[2] <= rhsVec.m_Values[2],
						  lhsVec.m_Values[3] <= rhsVec.m_Values[3]};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorLessEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept
{
#ifdef FRAO_AVX2
	// do effectively !(lhs > rhs), to find (lhs <= rhs)
	return _mm256_xor_si256(vectorSplatAllBits(),
							_mm256_cmpgt_epi64(lhsVec, rhsVec));
#else

	constexpr int_least64_t selection[2] = {0, IMPL::g_TrueBits};
	bool components[4] = {lhsVec.m_Values[0] <= rhsVec.m_Values[0],
						  lhsVec.m_Values[1] <= rhsVec.m_Values[1],
						  lhsVec.m_Values[2] <= rhsVec.m_Values[2],
						  lhsVec.m_Values[3] <= rhsVec.m_Values[3]};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorLessEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					  RegArgVector rhsVec) noexcept
{
	WideIntVector result = vectorLessEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(result);
	return result;
}
WideIntVector FRAO_VECTORCALL
vectorLessEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					  RegIntVector rhsVec) noexcept
{
	WideIntVector result = vectorLessEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(result);
	return result;
}

WideIntVector FRAO_VECTORCALL
vectorGreater(RegArgVector lhsVec, RegArgVector rhsVec) noexcept
{
#if defined(FRAO_AVX)
	return reinterpretToInteger(
		_mm256_cmp_pd(lhsVec, rhsVec, _CMP_GT_OS));
#else

	constexpr int_least64_t selection[2] = {0, IMPL::g_TrueBits};
	bool components[4] = {lhsVec.m_Values[0] > rhsVec.m_Values[0],
						  lhsVec.m_Values[1] > rhsVec.m_Values[1],
						  lhsVec.m_Values[2] > rhsVec.m_Values[2],
						  lhsVec.m_Values[3] > rhsVec.m_Values[3]};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorGreater(RegIntVector lhsVec, RegIntVector rhsVec) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_cmpgt_epi64(lhsVec, rhsVec);
#else

	constexpr int_least64_t selection[2] = {0, IMPL::g_TrueBits};
	bool components[4] = {lhsVec.m_Values[0] > rhsVec.m_Values[0],
						  lhsVec.m_Values[1] > rhsVec.m_Values[1],
						  lhsVec.m_Values[2] > rhsVec.m_Values[2],
						  lhsVec.m_Values[3] > rhsVec.m_Values[3]};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorGreaterRecord(uint_least64_t& record, RegArgVector lhsVec,
					RegArgVector rhsVec) noexcept
{
	WideIntVector result = vectorGreater(lhsVec, rhsVec);

	record = IMPL::convRecord(result);
	return result;
}
WideIntVector FRAO_VECTORCALL
vectorGreaterRecord(uint_least64_t& record, RegIntVector lhsVec,
					RegIntVector rhsVec) noexcept
{
	WideIntVector result = vectorGreater(lhsVec, rhsVec);

	record = IMPL::convRecord(result);
	return result;
}

WideIntVector FRAO_VECTORCALL
vectorGreaterEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept
{
#if defined(FRAO_AVX)
	return reinterpretToInteger(
		_mm256_cmp_pd(lhsVec, rhsVec, _CMP_GE_OS));
#else

	constexpr int_least64_t selection[2] = {0, IMPL::g_TrueBits};
	bool components[4] = {lhsVec.m_Values[0] >= rhsVec.m_Values[0],
						  lhsVec.m_Values[1] >= rhsVec.m_Values[1],
						  lhsVec.m_Values[2] >= rhsVec.m_Values[2],
						  lhsVec.m_Values[3] >= rhsVec.m_Values[3]};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorGreaterEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept
{
#ifdef FRAO_AVX2
	WideIntVector greater = _mm256_cmpgt_epi64(lhsVec, rhsVec),
				  equal	  = _mm256_cmpeq_epi64(lhsVec, rhsVec);

	return _mm256_or_si256(greater, equal);
#else

	constexpr int_least64_t selection[2] = {0, IMPL::g_TrueBits};
	bool components[4] = {lhsVec.m_Values[0] >= rhsVec.m_Values[0],
						  lhsVec.m_Values[1] >= rhsVec.m_Values[1],
						  lhsVec.m_Values[2] >= rhsVec.m_Values[2],
						  lhsVec.m_Values[3] >= rhsVec.m_Values[3]};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorGreaterEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
						 RegArgVector rhsVec) noexcept
{
	WideIntVector result = vectorGreaterEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(result);
	return result;
}
WideIntVector FRAO_VECTORCALL
vectorGreaterEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
						 RegIntVector rhsVec) noexcept
{
	WideIntVector result = vectorGreaterEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(result);
	return result;
}
WideIntVector FRAO_VECTORCALL
vectorEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept
{
#if defined(FRAO_AVX)
	return reinterpretToInteger(
		_mm256_cmp_pd(lhsVec, rhsVec, _CMP_EQ_OS));
#else

	constexpr int_least64_t selection[2] = {0, IMPL::g_TrueBits};
	bool components[4] = {lhsVec.m_Values[0] == rhsVec.m_Values[0],
						  lhsVec.m_Values[1] == rhsVec.m_Values[1],
						  lhsVec.m_Values[2] == rhsVec.m_Values[2],
						  lhsVec.m_Values[3] == rhsVec.m_Values[3]};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_cmpeq_epi64(lhsVec, rhsVec);
#else

	constexpr int_least64_t selection[2] = {0, IMPL::g_TrueBits};
	bool components[4] = {lhsVec.m_Values[0] == rhsVec.m_Values[0],
						  lhsVec.m_Values[1] == rhsVec.m_Values[1],
						  lhsVec.m_Values[2] == rhsVec.m_Values[2],
						  lhsVec.m_Values[3] == rhsVec.m_Values[3]};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
				  RegArgVector rhsVec) noexcept
{
	WideIntVector result = vectorEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(result);
	return result;
}
WideIntVector FRAO_VECTORCALL
vectorEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
				  RegIntVector rhsVec) noexcept
{
	WideIntVector result = vectorEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(result);
	return result;
}
WideIntVector FRAO_VECTORCALL
vectorNotEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept
{
#if defined(FRAO_AVX)
	// because of our __m256i emulation for the (non-AVX2) AVX feature
	// level, this should be quicker
	__m256d trueSplat = _mm256_broadcast_sd(
		reinterpret_cast<const double*>(&IMPL::g_TrueBits));

	// we could directly use _CMP_NEQ_OS instead, but that doesn't
	// handle NaNs the way we want, so we use _CMP_EQ_OS, and negate
	return reinterpretToInteger(_mm256_xor_pd(
		trueSplat, _mm256_cmp_pd(lhsVec, rhsVec, _CMP_EQ_OS)));
#else

	constexpr int_least64_t selection[2] = {0, IMPL::g_TrueBits};
	bool components[4] = {lhsVec.m_Values[0] != rhsVec.m_Values[0],
						  lhsVec.m_Values[1] != rhsVec.m_Values[1],
						  lhsVec.m_Values[2] != rhsVec.m_Values[2],
						  lhsVec.m_Values[3] != rhsVec.m_Values[3]};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorNotEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept
{
#ifdef FRAO_AVX2
	// effectively !(lhs == rhs)
	return _mm256_xor_si256(vectorSplatAllBits(),
							_mm256_cmpeq_epi64(lhsVec, rhsVec));
#else

	constexpr int_least64_t selection[2] = {0, IMPL::g_TrueBits};
	bool components[4] = {lhsVec.m_Values[0] != rhsVec.m_Values[0],
						  lhsVec.m_Values[1] != rhsVec.m_Values[1],
						  lhsVec.m_Values[2] != rhsVec.m_Values[2],
						  lhsVec.m_Values[3] != rhsVec.m_Values[3]};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorNotEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					 RegArgVector rhsVec) noexcept
{
	WideIntVector result = vectorNotEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(result);
	return result;
}
WideIntVector FRAO_VECTORCALL
vectorNotEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					 RegIntVector rhsVec) noexcept
{
	WideIntVector result = vectorNotEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(result);
	return result;
}
WideIntVector FRAO_VECTORCALL
vectorInBounds(RegArgVector checked, RegArgVector lowerBound,
			   RegArgVector upperBound) noexcept
{
#if defined(FRAO_AVX)
	WideVector lhsCheck =
				   _mm256_cmp_pd(checked, lowerBound, _CMP_GE_OS),
			   rhsCheck =
				   _mm256_cmp_pd(checked, upperBound, _CMP_LE_OS);

	return reinterpretToInteger(_mm256_and_pd(lhsCheck, rhsCheck));
#else

	constexpr int_least64_t selection[2]  = {0, IMPL::g_TrueBits};
	bool					components[4] = {
		   static_cast<bool>(
			   (checked.m_Values[0] >= lowerBound.m_Values[0])
			   & (checked.m_Values[0] <= upperBound.m_Values[0])),
		   static_cast<bool>(
			   (checked.m_Values[1] >= lowerBound.m_Values[1])
			   & (checked.m_Values[1] <= upperBound.m_Values[1])),
		   static_cast<bool>(
			   (checked.m_Values[2] >= lowerBound.m_Values[2])
			   & (checked.m_Values[2] <= upperBound.m_Values[2])),
		   static_cast<bool>(
			   (checked.m_Values[3] >= lowerBound.m_Values[3])
			   & (checked.m_Values[3] <= upperBound.m_Values[3]))};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorInBounds(RegIntVector checked, RegIntVector lowerBound,
			   RegIntVector upperBound) noexcept
{
#ifdef FRAO_AVX2
	// effectively, for each side of the bounds, check if 'checked' is
	// NOT on the (strict) outside of the bounds
	WideIntVector trueVec  = vectorSplatAllBits(),
				  lhsCheck = _mm256_xor_si256(
					  trueVec,
					  _mm256_cmpgt_epi64(lowerBound, checked)),
				  rhsCheck = _mm256_xor_si256(
					  trueVec,
					  _mm256_cmpgt_epi64(checked, upperBound));

	return _mm256_and_si256(lhsCheck, rhsCheck);
#else

	constexpr int_least64_t selection[2]  = {0, IMPL::g_TrueBits};
	bool					components[4] = {
		   static_cast<bool>(
			   (checked.m_Values[0] >= lowerBound.m_Values[0])
			   & (checked.m_Values[0] <= upperBound.m_Values[0])),
		   static_cast<bool>(
			   (checked.m_Values[1] >= lowerBound.m_Values[1])
			   & (checked.m_Values[1] <= upperBound.m_Values[1])),
		   static_cast<bool>(
			   (checked.m_Values[2] >= lowerBound.m_Values[2])
			   & (checked.m_Values[2] <= upperBound.m_Values[2])),
		   static_cast<bool>(
			   (checked.m_Values[3] >= lowerBound.m_Values[3])
			   & (checked.m_Values[3] <= upperBound.m_Values[3]))};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL vectorInBoundsRecord(
	uint_least64_t& record, RegArgVector checked,
	RegArgVector lowerBound, RegArgVector upperBound) noexcept
{
	WideIntVector result =
		vectorInBounds(checked, lowerBound, upperBound);

	record = IMPL::convRecord(result);
	return result;
}
WideIntVector FRAO_VECTORCALL vectorInBoundsRecord(
	uint_least64_t& record, RegIntVector checked,
	RegIntVector lowerBound, RegIntVector upperBound) noexcept
{
	WideIntVector result =
		vectorInBounds(checked, lowerBound, upperBound);

	record = IMPL::convRecord(result);
	return result;
}

WideIntVector FRAO_VECTORCALL
vectorIsNaN(RegArgVector vector) noexcept
{
#if defined(FRAO_AVX)
	// because of our __m256i emulation for the (non-AVX2) AVX feature
	// level, this should be quicker
	__m256d trueSplat = _mm256_broadcast_sd(
		reinterpret_cast<const double*>(&IMPL::g_TrueBits));

	// a NaN component will always compare unequal to itself, so
	// compare equal and negate (since NaN comp NaN will yield false,
	// regardless of the type of comparison)
	return reinterpretToInteger(_mm256_xor_pd(
		trueSplat, _mm256_cmp_pd(vector, vector, _CMP_EQ_OS)));
#else

	constexpr int_least64_t selection[2]  = {0, IMPL::g_TrueBits};
	bool					components[4] = {
		   isnan(vector.m_Values[0]), isnan(vector.m_Values[1]),
		   isnan(vector.m_Values[2]), isnan(vector.m_Values[3])};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL vectorIsNaNRecord(
	uint_least64_t& record, RegArgVector vector) noexcept
{
	WideIntVector result = vectorIsNaN(vector);

	record = IMPL::convRecord(result);
	return result;
}
WideIntVector FRAO_VECTORCALL
vectorIsInf(RegArgVector vector) noexcept
{
#if defined(FRAO_AVX)
	const WideVector nonSignBits =
		reinterpretToDouble(vectorReplicate(0x7FFF'FFFF'FFFF'FFFF));

	// mask away the sign bits, and then compare to infinity
	return reinterpretToInteger(
		_mm256_cmp_pd(_mm256_and_pd(vector, nonSignBits),
					  vectorSplatInfinity(), _CMP_EQ_OS));

#else

	constexpr int_least64_t selection[2]  = {0, IMPL::g_TrueBits};
	bool					components[4] = {
		   isinf(vector.m_Values[0]), isinf(vector.m_Values[1]),
		   isinf(vector.m_Values[2]), isinf(vector.m_Values[3])};

	return WideIntVector(
		selection[static_cast<int_least64_t>(components[0])],
		selection[static_cast<int_least64_t>(components[1])],
		selection[static_cast<int_least64_t>(components[2])],
		selection[static_cast<int_least64_t>(components[3])]);
#endif
}
WideIntVector FRAO_VECTORCALL vectorIsInfRecord(
	uint_least64_t& record, RegArgVector vector) noexcept
{
	WideIntVector result = vectorIsInf(vector);

	record = IMPL::convRecord(result);
	return result;
}
}  // namespace Maths
}  // namespace frao