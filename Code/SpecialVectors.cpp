//! \file
//!
//! \brief Implementation of special vectors, such as the zero vector
//! and the epsilon vector
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <Nucleus_inc\Environment.hpp>	//for EMIT_WARNING
#include "SpecialVectors.hpp"
#include "VectorTypes.hpp"	//for complete vector types
#include "ComponentAccess.hpp" //for vector replicate

namespace frao
{
namespace Maths
{
// vector constants
WideVector vectorZero() noexcept
{
#ifdef FRAO_AVX
	return _mm256_setzero_pd();
#else

	return WideVector(0., 0., 0., 0.);
#endif
}
WideIntVector vectorZeroInt() noexcept
{
#ifdef FRAO_AVX2
	return _mm256_setzero_si256();
#else

	return WideIntVector(0, 0, 0, 0);
#endif
}
WideIntVector vectorSplatAllBits() noexcept
{
	return vectorReplicate(IMPL::g_TrueBits);
}
//
WideVector vectorSplatOne() noexcept
{
	return vectorReplicate(1.);
}
WideIntVector vectorSplatOneInt() noexcept
{
	return vectorReplicate(1LL);
}
//
WideVector vectorSplatQNaN() noexcept
{
	return vectorReplicate(IMPL::g_QNaN);
}
WideVector vectorSplatSNaN() noexcept
{
	return vectorReplicate(IMPL::g_SNaN);
}
WideVector vectorSplatInfinity() noexcept
{
	return vectorReplicate(IMPL::g_Inf);
}
WideVector vectorSplatEpsilon() noexcept
{
	return vectorReplicate(IMPL::g_Epsilon);
}
}  // namespace Maths
}  // namespace frao