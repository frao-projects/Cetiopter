//! \file
//!
//! \brief Implementation of vector types, specifically those contain
//! double sized floating point data and calculations
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "VectorTypes.hpp"
#include "ComponentAccess.hpp"	 //for setters
#include "DataStorageTypes.hpp"	 //for load/store functions

namespace frao
{
namespace Maths
{
#ifndef FRAO_AVX

// NarrowVector functions
// private
NarrowVector::NarrowVector(float x, float y, float z,
						   float w) noexcept
	: m_Values{x, y, z, w}
{}
// public
NarrowVector::NarrowVector() noexcept : m_Values()
{}

// NarrowIntVector functions
// private
NarrowIntVector::NarrowIntVector(int_least32_t x, int_least32_t y,
								 int_least32_t z,
								 int_least32_t w) noexcept
	: m_Values{x, y, z, w}
{}
// public
NarrowIntVector::NarrowIntVector() noexcept : m_Values()
{}

// WideVector functions
// private
WideVector::WideVector(double x, double y, double z,
					   double w) noexcept
	: m_Values{x, y, z, w}
{}
// public
WideVector::WideVector() noexcept : m_Values()
{}

#endif

#ifndef FRAO_AVX2

// WideIntVector functions
// private
WideIntVector::WideIntVector(int_least64_t x, int_least64_t y,
							 int_least64_t z,
							 int_least64_t w) noexcept
	: m_Values{x, y, z, w}
{}
// public
WideIntVector::WideIntVector() noexcept : m_Values()
{}

#endif

// NarrowMatrix functions
// private
NarrowMatrix::NarrowMatrix(const Float2& v0,
						   const Float2& v1) noexcept
	: m_Tuples{loadFloat2(v0), loadFloat2(v1),
			   vectorSet(0.f, 0.f, 1.f, 0.f),
			   vectorSet(0.f, 0.f, 0.f, 1.f)}
{}
NarrowMatrix::NarrowMatrix(const Float3& v0, const Float3& v1,
						   const Float3& v2) noexcept
	: m_Tuples{loadFloat3(v0), loadFloat3(v1), loadFloat3(v2),
			   vectorSet(0.f, 0.f, 0.f, 1.f)}
{}
NarrowMatrix::NarrowMatrix(const Float4& v0, const Float4& v1,
						   const Float4& v2,
						   const Float4& v3) noexcept
	: m_Tuples{loadFloat4(v0), loadFloat4(v1), loadFloat4(v2),
			   loadFloat4(v3)}
{}
NarrowMatrix::NarrowMatrix(LateArgNrwVec v0, LateArgNrwVec v1,
						   LateArgNrwVec v2,
						   LateArgNrwVec v3) noexcept
	: m_Tuples{v0, v1, v2, v3}
{}
// public
NarrowMatrix::NarrowMatrix() noexcept : m_Tuples{}
{}

// WideMatrix functions
// private
WideMatrix::WideMatrix(const Double2& v0, const Double2& v1) noexcept
	: m_Tuples{loadDouble2(v0), loadDouble2(v1),
			   vectorSet(0., 0., 1., 0.), vectorSet(0., 0., 0., 1.)}
{}
WideMatrix::WideMatrix(const Double3& v0, const Double3& v1,
					   const Double3& v2) noexcept
	: m_Tuples{loadDouble3(v0), loadDouble3(v1), loadDouble3(v2),
			   vectorSet(0., 0., 0., 1.)}
{}
WideMatrix::WideMatrix(const Double4& v0, const Double4& v1,
					   const Double4& v2, const Double4& v3) noexcept
	: m_Tuples{loadDouble4(v0), loadDouble4(v1), loadDouble4(v2),
			   loadDouble4(v3)}
{}
WideMatrix::WideMatrix(LateArgVector v0, LateArgVector v1,
					   LateArgVector v2, LateArgVector v3) noexcept
	: m_Tuples{v0, v1, v2, v3}
{}
// public
WideMatrix::WideMatrix() noexcept : m_Tuples{}
{}

}  // namespace Maths
}  // namespace frao