//! \file
//!
//! \brief Implementation of per-component and per-vector/matrix
//! maths. That is, maths which is not specifically 2/3/4D
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "ComponentMaths.hpp"
#include <Nucleus_inc\Environment.hpp>	//for EMIT_WARNING
#include "ComponentAccess.hpp"			//for permut/getters/setters
#include "Conversions.hpp"		 //for converting to/from int vectors
#include "DimensionalMaths.hpp"	 //for vector4DDot
#include "SpecialVectors.hpp"	 //for vectorSplatAllBits
#include "VectorTypes.hpp"		 //for complete vector types

namespace frao
{
namespace Maths
{
// vector bit manipulation
NarrowIntVector FRAO_VECTORCALL
vectorAnd(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept
{
#ifdef FRAO_AVX
	return _mm_and_si128(lhsVec, rhsVec);
#else
	return NarrowIntVector(lhsVec.m_Values[0] & rhsVec.m_Values[0],
						   lhsVec.m_Values[1] & rhsVec.m_Values[1],
						   lhsVec.m_Values[2] & rhsVec.m_Values[2],
						   lhsVec.m_Values[3] & rhsVec.m_Values[3]);
#endif
}
WideIntVector FRAO_VECTORCALL vectorAnd(RegIntVector lhsVec,
										 RegIntVector rhsVec) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_and_si256(lhsVec, rhsVec);
#else
	return WideIntVector(lhsVec.m_Values[0] & rhsVec.m_Values[0],
						 lhsVec.m_Values[1] & rhsVec.m_Values[1],
						 lhsVec.m_Values[2] & rhsVec.m_Values[2],
						 lhsVec.m_Values[3] & rhsVec.m_Values[3]);
#endif
}
NarrowIntVector FRAO_VECTORCALL
vectorOr(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept
{
#ifdef FRAO_AVX
	return _mm_or_si128(lhsVec, rhsVec);
#else
	return NarrowIntVector(lhsVec.m_Values[0] | rhsVec.m_Values[0],
						   lhsVec.m_Values[1] | rhsVec.m_Values[1],
						   lhsVec.m_Values[2] | rhsVec.m_Values[2],
						   lhsVec.m_Values[3] | rhsVec.m_Values[3]);
#endif
}
WideIntVector FRAO_VECTORCALL vectorOr(RegIntVector lhsVec,
										RegIntVector rhsVec) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_or_si256(lhsVec, rhsVec);
#else
	return WideIntVector(lhsVec.m_Values[0] | rhsVec.m_Values[0],
						 lhsVec.m_Values[1] | rhsVec.m_Values[1],
						 lhsVec.m_Values[2] | rhsVec.m_Values[2],
						 lhsVec.m_Values[3] | rhsVec.m_Values[3]);
#endif
}
NarrowIntVector FRAO_VECTORCALL
vectorXor(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept
{
#ifdef FRAO_AVX
	return _mm_xor_si128(lhsVec, rhsVec);
#else
	return NarrowIntVector(lhsVec.m_Values[0] ^ rhsVec.m_Values[0],
						   lhsVec.m_Values[1] ^ rhsVec.m_Values[1],
						   lhsVec.m_Values[2] ^ rhsVec.m_Values[2],
						   lhsVec.m_Values[3] ^ rhsVec.m_Values[3]);
#endif
}
WideIntVector FRAO_VECTORCALL vectorXor(RegIntVector lhsVec,
										 RegIntVector rhsVec) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_xor_si256(lhsVec, rhsVec);
#else
	return WideIntVector(lhsVec.m_Values[0] ^ rhsVec.m_Values[0],
						 lhsVec.m_Values[1] ^ rhsVec.m_Values[1],
						 lhsVec.m_Values[2] ^ rhsVec.m_Values[2],
						 lhsVec.m_Values[3] ^ rhsVec.m_Values[3]);
#endif
}
NarrowIntVector FRAO_VECTORCALL
vectorNot(RegIntNrwVec vector) noexcept
{
#ifdef FRAO_AVX
	return _mm_xor_si128(toSmallIntVector(vectorSplatAllBits()),
						 vector);
#else
	return NarrowIntVector(~vector.m_Values[0], ~vector.m_Values[1],
						   ~vector.m_Values[2], ~vector.m_Values[3]);
#endif
}
WideIntVector FRAO_VECTORCALL vectorNot(RegIntVector vector) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_xor_si256(vectorSplatAllBits(), vector);
#else
	return WideIntVector(~vector.m_Values[0], ~vector.m_Values[1],
						 ~vector.m_Values[2], ~vector.m_Values[3]);
#endif
}

// wide vector manipulation
WideVector FRAO_VECTORCALL
vectorAddition(RegArgVector lhsVec, RegArgVector rhsVec) noexcept
{
#ifdef FRAO_AVX
	return _mm256_add_pd(lhsVec, rhsVec);
#else
	return WideVector(lhsVec.m_Values[0] + rhsVec.m_Values[0],
					  lhsVec.m_Values[1] + rhsVec.m_Values[1],
					  lhsVec.m_Values[2] + rhsVec.m_Values[2],
					  lhsVec.m_Values[3] + rhsVec.m_Values[3]);
#endif
}
NarrowIntVector FRAO_VECTORCALL
vectorAddition(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept
{
#ifdef FRAO_AVX
	return _mm_add_epi32(lhsVec, rhsVec);
#else
	return NarrowIntVector(lhsVec.m_Values[0] + rhsVec.m_Values[0],
						   lhsVec.m_Values[1] + rhsVec.m_Values[1],
						   lhsVec.m_Values[2] + rhsVec.m_Values[2],
						   lhsVec.m_Values[3] + rhsVec.m_Values[3]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorAddition(RegIntVector lhsVec, RegIntVector rhsVec) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_add_epi64(lhsVec, rhsVec);
#else
	return WideIntVector(lhsVec.m_Values[0] + rhsVec.m_Values[0],
						 lhsVec.m_Values[1] + rhsVec.m_Values[1],
						 lhsVec.m_Values[2] + rhsVec.m_Values[2],
						 lhsVec.m_Values[3] + rhsVec.m_Values[3]);
#endif
}
WideVector FRAO_VECTORCALL
vectorSubtract(RegArgVector lhsVec, RegArgVector rhsVec) noexcept
{
#ifdef FRAO_AVX
	return _mm256_sub_pd(lhsVec, rhsVec);
#else
	return WideVector(lhsVec.m_Values[0] - rhsVec.m_Values[0],
					  lhsVec.m_Values[1] - rhsVec.m_Values[1],
					  lhsVec.m_Values[2] - rhsVec.m_Values[2],
					  lhsVec.m_Values[3] - rhsVec.m_Values[3]);
#endif
}
NarrowIntVector FRAO_VECTORCALL
vectorSubtract(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept
{
#ifdef FRAO_AVX
	return _mm_sub_epi32(lhsVec, rhsVec);
#else
	return NarrowIntVector(lhsVec.m_Values[0] - rhsVec.m_Values[0],
						   lhsVec.m_Values[1] - rhsVec.m_Values[1],
						   lhsVec.m_Values[2] - rhsVec.m_Values[2],
						   lhsVec.m_Values[3] - rhsVec.m_Values[3]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorSubtract(RegIntVector lhsVec, RegIntVector rhsVec) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_sub_epi64(lhsVec, rhsVec);
#else
	return WideIntVector(lhsVec.m_Values[0] - rhsVec.m_Values[0],
						 lhsVec.m_Values[1] - rhsVec.m_Values[1],
						 lhsVec.m_Values[2] - rhsVec.m_Values[2],
						 lhsVec.m_Values[3] - rhsVec.m_Values[3]);
#endif
}
WideVector FRAO_VECTORCALL
vectorMultiply(RegArgVector lhsVec, RegArgVector rhsVec) noexcept
{
#ifdef FRAO_AVX
	return _mm256_mul_pd(lhsVec, rhsVec);
#else
	return WideVector(lhsVec.m_Values[0] * rhsVec.m_Values[0],
					  lhsVec.m_Values[1] * rhsVec.m_Values[1],
					  lhsVec.m_Values[2] * rhsVec.m_Values[2],
					  lhsVec.m_Values[3] * rhsVec.m_Values[3]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorMultiply(RegIntVector lhsVec, RegIntVector rhsVec) noexcept
{
#ifdef FRAO_AVX2
	// Need to do: (lo32_l*lo32_r) + (hi32_l*lo32_r) + (lo32_l*hi32_r)
	//
	// to do the high multiplies, we shift down the high, do the
	// multiply, and then shift the result back up to high, before
	// adding to the others
	//
	// In theory, we would also need to add hi32*hi32, but that will
	// always be larger than the 64 bits of the result
	WideIntVector multLoLo = _mm256_mul_epu32(lhsVec, rhsVec),
				  multHiLo = _mm256_slli_epi64(
					  _mm256_mul_epu32(_mm256_srli_epi64(lhsVec, 32),
									   rhsVec),
					  32),
				  multLoHi = _mm256_slli_epi64(
					  _mm256_mul_epu32(lhsVec,
									   _mm256_srli_epi64(rhsVec, 32)),
					  32);

	return _mm256_add_epi64(multLoHi,
							_mm256_add_epi64(multHiLo, multLoLo));
#else

	return WideIntVector(lhsVec.m_Values[0] * rhsVec.m_Values[0],
						 lhsVec.m_Values[1] * rhsVec.m_Values[1],
						 lhsVec.m_Values[2] * rhsVec.m_Values[2],
						 lhsVec.m_Values[3] * rhsVec.m_Values[3]);
#endif
}
WideVector FRAO_VECTORCALL vectorDivide(
	RegArgVector dividendVec, RegArgVector divisorVec) noexcept
{
#ifdef FRAO_AVX
	return _mm256_div_pd(dividendVec, divisorVec);
#else
	return WideVector(
		dividendVec.m_Values[0] / divisorVec.m_Values[0],
		dividendVec.m_Values[1] / divisorVec.m_Values[1],
		dividendVec.m_Values[2] / divisorVec.m_Values[2],
		dividendVec.m_Values[3] / divisorVec.m_Values[3]);
#endif
}
#if defined(FRAO_AVX2)
namespace
{
// static globals for use in the division algorithm
static const WideIntVector g_Mask2Digit =
	vectorReplicate(0xFFFF'FFFFLL);
static const WideIntVector g_Mask1Digit = vectorReplicate(0xFFFFLL);
static const WideIntVector g_Mask31Bit =
	vectorReplicate(0x7FFF'FFFFLL);
static const WideIntVector g_OneSplat	  = vectorSplatOneInt();
static const WideIntVector g_SixteenSplat = vectorReplicate(16LL);
static const WideIntVector g_ZeroVec	  = vectorZeroInt();

WideIntVector FRAO_VECTORCALL
findNum16bDigits(RegIntVector examinee) noexcept
{
	// Basically, determine the highest non-zero digit, by looking at
	// each prospective digit in turn, and seeing if any bits were
	// set. If they were, or they were for any previous digit, then
	// increment a digit counter. Thus, the digit counter will, at the
	// end, have the correct (meaningful) digit count

	WideIntVector upperMask = vectorReplicate(
					  (long long) 0xFFFF'0000'0000'0000),
				  numDigits, isSet, preSet;

	// top bit
	isSet = vectorAnd(
		g_OneSplat,
		vectorNotEqual(g_ZeroVec, vectorAnd(upperMask, examinee)));
	numDigits = isSet;

	// 2nd bit
	upperMask = _mm256_srli_epi64(upperMask, 16);
	preSet =
		vectorAnd(g_OneSplat, vectorNotEqual(g_ZeroVec, numDigits));
	isSet = vectorAnd(
		g_OneSplat,
		vectorNotEqual(g_ZeroVec, vectorAnd(upperMask, examinee)));
	numDigits = vectorAddition(numDigits, vectorOr(isSet, preSet));

	// 3rd bit
	upperMask = _mm256_srli_epi64(upperMask, 16);
	preSet =
		vectorAnd(g_OneSplat, vectorNotEqual(g_ZeroVec, numDigits));
	isSet = vectorAnd(
		g_OneSplat,
		vectorNotEqual(g_ZeroVec, vectorAnd(upperMask, examinee)));
	numDigits = vectorAddition(numDigits, vectorOr(isSet, preSet));

	// bottom bit
	upperMask = _mm256_srli_epi64(upperMask, 16);
	preSet =
		vectorAnd(g_OneSplat, vectorNotEqual(g_ZeroVec, numDigits));
	isSet = vectorAnd(
		g_OneSplat,
		vectorNotEqual(g_ZeroVec, vectorAnd(upperMask, examinee)));
	numDigits = vectorAddition(numDigits, vectorOr(isSet, preSet));

	return numDigits;
}  // namespace
// use double division to exactly divide integers of up to 48 bits
// (theoretically 52)
//
// NOTE: We cannot call this with numbers that will result in a
// inf/NaN result (such as dividing by 0), bc then the conversion
// back to int will fail. This should never happen with the division
// algorithm (for which this is written), though
WideIntVector FRAO_VECTORCALL
uint48Divide(RegIntVector dividend, RegIntVector divisor) noexcept
{
	WideVector dividendDouble = toDoubleVector(dividend),
			   divisorDouble  = toDoubleVector(divisor);

	return toIntegerVector(
		vectorDivide(dividendDouble, divisorDouble));
}
// function just to make it more clear what we're doing when we xor
// the operands
WideIntVector FRAO_VECTORCALL
unsignedLessThan(RegIntVector lhs, RegIntVector rhs) noexcept
{
	static const WideIntVector signBitMask =
		vectorReplicate((long long) 0x8000'0000'0000'0000);

	return vectorLess(vectorXor(signBitMask, lhs),
					  vectorXor(signBitMask, rhs));
}
// from 64bit and 32bit UNSIGNED input, returns three 32-bit vectors,
// which shall combine for the 96-bits, of the full unsigned result
void FRAO_VECTORCALL multU64xU32(RegIntVector	 in64,
								  RegIntVector	 in32,
								  WideIntVector &outHi,
								  WideIntVector &outMid,
								  WideIntVector &outLo) noexcept
{
	// We first calculate the high and low partial results,
	// and then we have to add the overlapping 32 bits, and
	// resolve any carries into the upper 32 bits
	WideIntVector loMult = _mm256_mul_epu32(in64, in32),
				  hiMult = _mm256_mul_epu32(
					  _mm256_srli_epi64(in64, 32), in32),
				  preMid = _mm256_add_epi64(
					  hiMult, _mm256_srli_epi64(loMult, 32));

	// ensure each 32-bit component has only its own bits
	outLo  = _mm256_and_si256(loMult, g_Mask2Digit);
	outMid = _mm256_and_si256(preMid, g_Mask2Digit);
	outHi =
		_mm256_and_si256(_mm256_srli_epi64(preMid, 32), g_Mask2Digit);
}
// from 2x64bitUNSIGNED input, returns two 64-bit vectors, which shall
// combine for the 128-bits, of the full unsigned result
void FRAO_VECTORCALL multU64ToU128(RegIntVector   lhs64,
									RegIntVector   rhs64,
									WideIntVector &outHi,
									WideIntVector &outLo) noexcept
{
	// Need to do: (lo32_l*lo32_r) + (hi32_l*lo32_r) + (lo32_l*hi32_r)
	// + (hi32_l*hi32_r)
	//
	// to do the high multiplies, we shift down the high, do the
	// multiply, and then shift the result back up to high, before
	// adding to the others, to get the high and low halves of the
	// full 128-bit result
	WideIntVector loLoMult = _mm256_mul_epu32(lhs64, rhs64),
				  loHiMult = _mm256_mul_epu32(
					  lhs64, _mm256_srli_epi64(rhs64, 32)),
				  hiLoMult = _mm256_mul_epu32(
					  _mm256_srli_epi64(lhs64, 32), rhs64),
				  hiHiMult =
					  _mm256_mul_epu32(_mm256_srli_epi64(lhs64, 32),
									   _mm256_srli_epi64(rhs64, 32));

	outLo = _mm256_add_epi64(
		loLoMult, _mm256_add_epi64(_mm256_slli_epi64(loHiMult, 32),
								   _mm256_slli_epi64(hiLoMult, 32)));
	outHi = _mm256_add_epi64(
		hiHiMult, _mm256_add_epi64(_mm256_srli_epi64(loHiMult, 32),
								   _mm256_srli_epi64(hiLoMult, 32)));
}
// returns 1 splat in component iff lessThan. Each input component
// should be 4x64 bit differences (max. )
WideIntVector FRAO_VECTORCALL
lessThan3Digit(RegIntVector diffHi, RegIntVector diffMid,
			   RegIntVector diffLo) noexcept
{
	WideIntVector loLess  = vectorLess(diffLo, g_ZeroVec),
				  midLess = vectorLess(diffMid, g_ZeroVec),
				  hiLess  = vectorLess(diffHi, g_ZeroVec),
				  result =
					  vectorSelect(g_ZeroVec, loLess,
								   vectorNotEqual(diffLo, g_ZeroVec));

	result = vectorSelect(result, midLess,
						  vectorNotEqual(diffMid, g_ZeroVec));
	result = vectorSelect(result, hiLess,
						  vectorNotEqual(diffHi, g_ZeroVec));

	return result;
}
// helper function for subtract93, to calculate an individual 31-bit
// digit
WideIntVector FRAO_VECTORCALL
calcSub31bDigit(RegIntVector digit, RegIntVector neg,
				WideIntVector &carry) noexcept
{
	static const WideIntVector minusRadix31 =
		vectorReplicate(0x8000'0000LL);

	// carry from previous digit, and then calculate new
	// carry, and transform this digit to the correct range,
	// using the information of whether this digit will carry.
	// We also transform the carry to 1/0, once we're done
	// selecting with it
	WideIntVector resDigit = _mm256_sub_epi32(digit, carry);
	carry				   = _mm256_cmpgt_epi32(g_ZeroVec, resDigit);

	resDigit = vectorSelect(resDigit,
							vectorXor(minusRadix31, resDigit), carry);
	carry	 = vectorXor(neg, vectorAnd(g_OneSplat, carry));

	resDigit =
		vectorSelect(resDigit, _mm256_sub_epi32(g_ZeroVec, resDigit),
					 _mm256_cmpgt_epi32(g_ZeroVec, resDigit));

	return resDigit;
}
// with the raw digit diffs, calculate the actual full-number
// difference. Each input component should be 4x32 bit differences, in
// the low half of each 64-bit component. The sign bit should be the
// 32nd bit, not the 64th
void FRAO_VECTORCALL subtract93(
	RegIntVector diffHi, RegIntVector diffMid, RegIntVector diffLo,
	WideIntVector &resHi, WideIntVector &resMid, WideIntVector &resLo,
	WideIntVector &negative) noexcept
{
	// with the raw digit diffs, calculate the actual full-number
	// difference
	WideIntVector carry = g_ZeroVec;

	// negative should be either 1 or 0, not 'splat all bits' or 0
	negative = vectorAnd(g_OneSplat,
						 lessThan3Digit(diffHi, diffMid, diffLo));

	// carry and calculate each 32-bit digit
	resLo  = calcSub31bDigit(diffLo, negative, carry);
	resMid = calcSub31bDigit(diffMid, negative, carry);
	resHi  = calcSub31bDigit(diffHi, negative, carry);

	runtime_assert(vector4DEqual(g_ZeroVec, carry),
				   "Should never have non-0 carry after last "
				   "digit calculation");
}
// converts an unsigned 3x32 number to 3x31. obviously requires the
// stored number to be less than 2^93, lo/mid < 2^32, and hi < 2^29
void FRAO_VECTORCALL convto3x31(WideIntVector &hi,
								 WideIntVector &mid,
								 WideIntVector &lo) noexcept
{
	static const WideIntVector mask29Bit =
								   vectorReplicate(0x1FFF'FFFFLL),
							   mask30Bit =
								   vectorReplicate(0x3FFF'FFFFLL);
	;

	// we dont check the size of hi, because there are
	// circumstances where we can underflow to an invalid number,
	// in paths that will be selected against
	runtime_assert(vector4DLessEqual(mid, g_Mask2Digit),
				   "The middle bits must not be greater than 32 bits "
				   "in actual size!");
	runtime_assert(vector4DLessEqual(lo, g_Mask2Digit),
				   "The low bits must not be greater than 32 bits in "
				   "actual size!");

	// order is important, to avoid reading from a variable after
	// we've written to it
	hi	= vectorOr(_mm256_slli_epi64(vectorAnd(mask29Bit, hi), 2),
				   _mm256_srli_epi64(mid, 30));
	mid = vectorOr(_mm256_slli_epi64(vectorAnd(mask30Bit, mid), 1),
				   _mm256_srli_epi64(lo, 31));
	lo	= vectorAnd(g_Mask31Bit, lo);
}
// converts a 3x31 number to an unsigned 3x32 number
void FRAO_VECTORCALL convto3x32(RegIntVector	hi31,
								 RegIntVector	mid31,
								 RegIntVector	lo31,
								 WideIntVector &hi32,
								 WideIntVector &mid32,
								 WideIntVector &lo32) noexcept
{
	runtime_assert(vector4DLessEqual(hi31, g_Mask31Bit),
				   "The high bits must not be greater than 31 "
				   "bits in actual size!");
	runtime_assert(vector4DLessEqual(mid31, g_Mask31Bit),
				   "The middle bits must not be greater than 31 bits "
				   "in actual size!");
	runtime_assert(vector4DLessEqual(lo31, g_Mask31Bit),
				   "The low bits must not be greater than 31 bits in "
				   "actual size!");

	lo32  = vectorAnd(g_Mask2Digit,
					  vectorOr(lo31, _mm256_slli_epi64(mid31, 31)));
	mid32 = vectorAnd(g_Mask2Digit,
					  vectorOr(_mm256_srli_epi64(mid31, 1),
							   _mm256_slli_epi64(hi31, 30)));
	hi32  = _mm256_srli_epi64(hi31, 2);
}
// helper function to make code more readable
//
// digitNum = diff + #divisor = #dividend - #iteration_so_far
//
// assumes that passed remainders are 3x32 unsigned
WideIntVector FRAO_VECTORCALL getRem3(RegIntVector remHi,
									   RegIntVector remMid,
									   RegIntVector remLo,
									   RegIntVector digitNum)
{
	static const WideIntVector mask3Digit =
		vectorReplicate(0xFFFF'FFFF'FFFF);

	// shift all (shifting in 0) by a given amount (depending on
	// digitNum), and then 'or' all together. 'bad' values will be
	// shifted to all zeroes.
	WideIntVector preshiftedMid = _mm256_slli_epi64(remMid, 32),
				  commonShift	= _mm256_mul_epu32(
						g_SixteenSplat,
						vectorSubtract(digitNum, g_OneSplat)),
				  loComp = _mm256_srlv_epi64(remLo, commonShift),
				  medComp =
					  _mm256_srlv_epi64(preshiftedMid, commonShift),
				  hiComp = _mm256_sllv_epi64(
					  remHi, _mm256_mul_epu32(
								 g_SixteenSplat,
								 vectorSubtract(vectorReplicate(5LL),
												digitNum)));

	//(0xFFFF'FFFF'FFFF & (lo >> (16*(d-1)))
	//	| ((mid << 32) >> (16*(d-1))) | (hi << (16*(5-d))))
	return vectorAnd(mask3Digit,
					 vectorOr(loComp, vectorOr(medComp, hiComp)));
}
// Helper function to make code more readable
//
// assumes 32 bit unsigned dividendHi/Mid/Lo, and 16-bit unsigned
// divisor. Divides 96-bit unsigned int by 16 bit one
void FRAO_VECTORCALL oneDigitDivide(RegIntVector	dividendHi,
									 RegIntVector	dividendMid,
									 RegIntVector	dividendLo,
									 RegIntVector	divisor,
									 WideIntVector &hiRes,
									 WideIntVector &mainRes,
									 WideIntVector &finalCarry)
{
	runtime_assert(vector4DLessEqual(divisor, g_Mask1Digit),
				   "The divisor must not be greater than 1 digit!");
	runtime_assert(vector4DLessEqual(dividendHi, g_Mask2Digit),
				   "The dividend must not be greater than 6 digits!");
	runtime_assert(vector4DLessEqual(dividendMid, g_Mask2Digit),
				   "The middle dividend component must not be "
				   "greater than 2 digits!");
	runtime_assert(vector4DLessEqual(dividendLo, g_Mask2Digit),
				   "The low dividend component must not be "
				   "greater than 2 digits!");

	// divide top [80b-95b] digit
	WideIntVector nextDividend = _mm256_srli_epi64(dividendHi, 16),
				  nextRes	   = uint48Divide(nextDividend, divisor),
				  product	   = _mm256_mul_epu32(nextRes, divisor),
				  carry = vectorSubtract(nextDividend, product);
	hiRes				= _mm256_slli_epi64(nextRes, 16);

	// divide 2nd top [64b-79b] digit
	nextDividend =
		vectorAddition(_mm256_slli_epi64(carry, 16),
					   vectorAnd(dividendHi, g_Mask1Digit));
	nextRes = uint48Divide(nextDividend, divisor);
	product = _mm256_mul_epu32(nextRes, divisor);
	carry	= vectorSubtract(nextDividend, product);
	hiRes	= vectorOr(hiRes, nextRes);

	// divide 3rd [48b-63b] digit
	nextDividend = vectorAddition(_mm256_slli_epi64(carry, 16),
								  _mm256_srli_epi64(dividendMid, 16));
	nextRes		 = uint48Divide(nextDividend, divisor);
	product		 = _mm256_mul_epu32(nextRes, divisor);
	carry		 = vectorSubtract(nextDividend, product);
	mainRes		 = _mm256_slli_epi64(nextRes, 48);

	// divide 4th [32b-47b] digit
	nextDividend =
		vectorAddition(_mm256_slli_epi64(carry, 16),
					   vectorAnd(dividendMid, g_Mask1Digit));
	nextRes = uint48Divide(nextDividend, divisor);
	product = _mm256_mul_epu32(nextRes, divisor);
	carry	= vectorSubtract(nextDividend, product);
	mainRes = vectorOr(mainRes, _mm256_slli_epi64(nextRes, 32));

	// divide 5th [16b-31b] digit
	nextDividend = vectorAddition(_mm256_slli_epi64(carry, 16),
								  _mm256_srli_epi64(dividendLo, 16));
	nextRes		 = uint48Divide(nextDividend, divisor);
	product		 = _mm256_mul_epu32(nextRes, divisor);
	carry		 = vectorSubtract(nextDividend, product);
	mainRes		 = vectorOr(mainRes, _mm256_slli_epi64(nextRes, 16));

	// divide 6th [0b-15b] digit
	nextDividend =
		vectorAddition(_mm256_slli_epi64(carry, 16),
					   vectorAnd(dividendLo, g_Mask1Digit));
	nextRes	   = uint48Divide(nextDividend, divisor);
	product	   = _mm256_mul_epu32(nextRes, divisor);
	finalCarry = vectorSubtract(nextDividend, product);
	mainRes	   = vectorOr(mainRes, nextRes);
}
}  // namespace
#endif
WideIntVector FRAO_VECTORCALL
vectorDivide(RegIntVector dividendVec, RegIntVector divisorVec,
			 WideIntVector *remRes) noexcept
{
#ifdef FRAO_AVX2
	// The algorithm assumes unsigned numbers, so ensure we have the
	// absolute values of them instead, and prepare the corrections
	// that will be necessary, once unsigned calculations are
	// complete.
	//
	// We also check for, and guard against, a zero divisor, by
	// setting the divisor to a dummy value, in that case
	const WideIntVector
		negDividend		= vectorSubtract(g_ZeroVec, dividendVec),
		negDivisor		= vectorSubtract(g_ZeroVec, divisorVec),
		haveNegDividend = vectorLess(dividendVec, g_ZeroVec),
		haveNegDivisor	= vectorLess(divisorVec, g_ZeroVec),
		haveZeroDivisor = vectorEqual(divisorVec, g_ZeroVec),
		unsignedDividend =
			vectorSelect(dividendVec, negDividend, haveNegDividend),
		unsignedDivisor = vectorSelect(
			vectorSelect(divisorVec, negDivisor, haveNegDivisor),
			g_OneSplat, haveZeroDivisor);

	// Here we correct for division by zero, by setting to a dummy
	// divisor, and later return specific values. Then, we calculate
	// the required shift, to get the leading divisor digit in the low
	// digit position
	const WideIntVector guardDivisor =
							vectorSelect(unsignedDivisor, g_OneSplat,
										 haveZeroDivisor),
						numDividendDigits =
							findNum16bDigits(unsignedDividend),
						numDivisorDigits =
							findNum16bDigits(guardDivisor);
	WideIntVector leadingDigitShift = _mm256_mul_epi32(
		g_SixteenSplat,
		_mm256_sub_epi64(numDivisorDigits, g_OneSplat));

	// We now normalise the divisor and dividend, by the same
	// factor, so that the dividend is as large as possible,
	// within 4 digits. The factor is determined by: radix /
	// (leading divisor digit + 1)
	static const WideIntVector radix16Splat =
		vectorReplicate(0x1'0000LL);
	const WideIntVector normalisationFactor = uint48Divide(
							radix16Splat,
							vectorAddition(
								g_OneSplat,
								vectorAnd(g_Mask1Digit,
										  _mm256_srlv_epi64(
											  guardDivisor,
											  leadingDigitShift)))),
						normalDivisor = vectorMultiply(
							guardDivisor, normalisationFactor);

	runtime_assert(vector4DLess(normalisationFactor, radix16Splat),
				   "The normalisation factor must not be greater "
				   "than one digit!");

	// perform dividend 64x32 multiplication, and collate results, for
	// normalised dividend
	WideIntVector remainderLo, remainderMid, remainderHi;
	multU64xU32(unsignedDividend, normalisationFactor, remainderHi,
				remainderMid, remainderLo);

	runtime_assert(
		vector4DLessEqual(remainderHi, g_Mask1Digit),
		"The normalised dividend must not be greater than 80 bits");

	// we now want the leading two digits of the divisor, rather than
	// one, so shift less (unless we only have a one digit divisor)
	leadingDigitShift = vectorSelect(
		g_ZeroVec, vectorSubtract(leadingDigitShift, g_SixteenSplat),
		vectorGreaterEqual(leadingDigitShift, g_SixteenSplat));

	// prepare remainder, by converting to required 3x31 format
	convto3x31(remainderHi, remainderMid, remainderLo);

	// determine how many digits should be in the answer, prepare
	// quotient and remainder variables, and set d2
	WideIntVector digitDifference = vectorSubtract(numDividendDigits,
												   numDivisorDigits),
				  divisor2 =
					  vectorAnd(g_Mask2Digit,
								_mm256_srlv_epi64(normalDivisor,
												  leadingDigitShift)),
				  quotient = g_ZeroVec, remainder3;

	for (; vector4DNotEqual(
			 g_ZeroVec, vectorLessEqual(g_ZeroVec, digitDifference));
		 digitDifference =
			 vectorSubtract(digitDifference, vectorSplatOneInt()))
	{
		// convert 31bit rem to required 32 bit
		WideIntVector hi32, mid32, lo32;
		convto3x32(remainderHi, remainderMid, remainderLo, hi32,
				   mid32, lo32);

		// prepare this iteration's rem3
		remainder3 = getRem3(
			hi32, mid32, lo32,
			vectorAddition(numDivisorDigits, digitDifference));

		// calculate guess at digit, and possible correction. Also
		// prepare the shift required for the result
		//
		// g_Mask1Digit == radix16 -1
		const WideIntVector likelyTrial =
								uint48Divide(remainder3, divisor2),
							trial = vectorSelect(
								likelyTrial, g_Mask1Digit,
								vectorLess(g_Mask1Digit,
										   likelyTrial)),
							smallerTrial =
								vectorSubtract(trial, g_OneSplat),
							digitDiffShift = _mm256_mul_epu32(
								g_SixteenSplat, digitDifference);

		// calculate possible divisor/digit products
		WideIntVector trialProductHi, trialProduct64Lo,
			smallTrialProductHi, smallTrialProduct64Lo;
		multU64ToU128(normalDivisor,
					  _mm256_sllv_epi64(trial, digitDiffShift),
					  trialProductHi, trialProduct64Lo);
		multU64ToU128(normalDivisor,
					  _mm256_sllv_epi64(smallerTrial, digitDiffShift),
					  smallTrialProductHi, smallTrialProduct64Lo);

		// assuming that hi component will be < 2^32 (and actually,
		// 2^16?), for all valid indices, split lower 64 bits into
		// middle and lower portions of the 96-bit number
		WideIntVector trialProductMid =
						  _mm256_srli_epi64(trialProduct64Lo, 32),
					  trialProductLo =
						  vectorAnd(trialProduct64Lo, g_Mask2Digit),
					  smallTrialProductMid = _mm256_srli_epi64(
						  smallTrialProduct64Lo, 32),
					  smallTrialProductLo = vectorAnd(
						  smallTrialProduct64Lo, g_Mask2Digit);

		// Next, convert trial products to 3x31
		convto3x31(trialProductHi, trialProductMid, trialProductLo);
		convto3x31(smallTrialProductHi, smallTrialProductMid,
				   smallTrialProductLo);

		// And then, calculate the raw difference between the
		// remainder and both options for the digit*divisor product
		WideIntVector remProductDiffHi = _mm256_sub_epi32(
						  remainderHi, trialProductHi),
					  remProductDiffMid = _mm256_sub_epi32(
						  remainderMid, trialProductMid),
					  remProductDiffLo = _mm256_sub_epi32(
						  remainderLo, trialProductLo);

		// Now, check whether we need the corrected or normal values,
		// and apply accordingly
		const WideIntVector remainderSmaller = lessThan3Digit(
			remProductDiffHi, remProductDiffMid, remProductDiffLo);

		remProductDiffHi = vectorSelect(
			remProductDiffHi,
			_mm256_sub_epi32(remainderHi, smallTrialProductHi),
			remainderSmaller);
		remProductDiffMid = vectorSelect(
			remProductDiffMid,
			_mm256_sub_epi32(remainderMid, smallTrialProductMid),
			remainderSmaller);
		remProductDiffLo = vectorSelect(
			remProductDiffLo,
			_mm256_sub_epi32(remainderLo, smallTrialProductLo),
			remainderSmaller);

		// calculate prospective remainders, and if we keep the
		// results of this iteration, for this element
		const WideIntVector keepResults =
			vectorLessEqual(g_ZeroVec, digitDifference);

		WideIntVector remTempHi, remTempMid, remTempLo, negDummy;
		subtract93(remProductDiffHi, remProductDiffMid,
				   remProductDiffLo, remTempHi, remTempMid, remTempLo,
				   negDummy);

		runtime_assert(vector4DLessEqual(g_ZeroVec, negDummy),
					   "remainder - product must never be <0 !");

		// set calculated digit, and update remainder for next
		// iteration, if our digit index is still in range
		quotient = vectorOr(
			quotient, _mm256_sllv_epi64(
						  vectorAnd(keepResults,
									vectorSelect(trial, smallerTrial,
												 remainderSmaller)),
						  digitDiffShift));

		// if element is still being worked on, update remainder
		remainderHi =
			vectorSelect(remainderHi, remTempHi, keepResults);
		remainderMid =
			vectorSelect(remainderMid, remTempMid, keepResults);
		remainderLo =
			vectorSelect(remainderLo, remTempLo, keepResults);
	}

	// convert 31bit rem to required 32 bit, and rescale remainder to
	// actual value, rather than normalised value
	WideIntVector hi32, mid32, lo32, overflowRem, finalRemainder,
		finalCarry;
	convto3x32(remainderHi, remainderMid, remainderLo, hi32, mid32,
			   lo32);
	oneDigitDivide(hi32, mid32, lo32, normalisationFactor,
				   overflowRem, finalRemainder, finalCarry);

	runtime_assert(vector4DEqual(overflowRem, g_ZeroVec),
				   "Final remainder must fit within 64 bits");
	runtime_assert(vector4DEqual(finalCarry, g_ZeroVec),
				   "Final remainder must be cleanly dividable by "
				   "normalisation factor, or something went wrong");

	// select for special cases (dividend < divisor, and divisor == 0)
	// here. We make sure not the alter the quotient when divisor ==
	// 0, so that the two cases are distinguishable. With a divisor of
	// 0, quotient should at this point be equal to the unsigned
	// dividend, although that is subject to negative correction,
	// below.
	const WideIntVector hasSmallDividend =
		unsignedLessThan(unsignedDividend, unsignedDivisor);
	quotient = vectorSelect(
		quotient, g_ZeroVec,
		vectorAnd(vectorNot(haveZeroDivisor), hasSmallDividend));
	finalRemainder =
		vectorSelect(finalRemainder, dividendVec, hasSmallDividend);

	// Correct for negative input.
	const WideIntVector negMagCorrection =
							vectorAnd(haveNegDividend,
									  vectorNotEqual(g_ZeroVec,
													 finalRemainder)),
						negSignCorrection = vectorXor(haveNegDividend,
													  haveNegDivisor);
	quotient =
		vectorSelect(quotient, vectorAddition(g_OneSplat, quotient),
					 negMagCorrection);
	finalRemainder =
		vectorSelect(finalRemainder,
					 vectorSubtract(unsignedDivisor, finalRemainder),
					 negMagCorrection);
	quotient =
		vectorSelect(quotient, vectorSubtract(g_ZeroVec, quotient),
					 negSignCorrection);

	if (remRes != nullptr)
	{
		*remRes = finalRemainder;
	}

	return quotient;
#else

	// since c++ truncates towards 0, rather than performing proper
	// euclidean division, we must correct for negative remainder,
	// after calculating the 'c++' remainder
	int_least64_t remValues[4] =
		{dividendVec.m_Values[0] % divisorVec.m_Values[0],
		 dividendVec.m_Values[1] % divisorVec.m_Values[1],
		 dividendVec.m_Values[2] % divisorVec.m_Values[2],
		 dividendVec.m_Values[3] % divisorVec.m_Values[3]},
				  divValues[4] = {dividendVec.m_Values[0]
									  / divisorVec.m_Values[0],
								  dividendVec.m_Values[1]
									  / divisorVec.m_Values[1],
								  dividendVec.m_Values[2]
									  / divisorVec.m_Values[2],
								  dividendVec.m_Values[3]
									  / divisorVec.m_Values[3]},
				  absValues[4] = {std::abs(divisorVec.m_Values[0]),
								  std::abs(divisorVec.m_Values[1]),
								  std::abs(divisorVec.m_Values[2]),
								  std::abs(divisorVec.m_Values[3])},
				  divSign[4]   = {(divValues[0] < 0) ? -1 : 1,
								  (divValues[1] < 0) ? -1 : 1,
								  (divValues[2] < 0) ? -1 : 1,
								  (divValues[3] < 0) ? -1 : 1};
	bool negCorrection[4] = {(remValues[0] < 0), (remValues[1] < 0),
							 (remValues[2] < 0), (remValues[3] < 0)};

	remValues[0] = negCorrection[0] ? (absValues[0] + remValues[0])
									: remValues[0];
	remValues[1] = negCorrection[1] ? (absValues[1] + remValues[1])
									: remValues[1];
	remValues[2] = negCorrection[2] ? (absValues[2] + remValues[2])
									: remValues[2];
	remValues[3] = negCorrection[3] ? (absValues[3] + remValues[3])
									: remValues[3];

	divValues[0] =
		negCorrection[0] ? (divValues[0] + divSign[0]) : divValues[0];
	divValues[1] =
		negCorrection[1] ? (divValues[1] + divSign[1]) : divValues[1];
	divValues[2] =
		negCorrection[2] ? (divValues[2] + divSign[2]) : divValues[2];
	divValues[3] =
		negCorrection[3] ? (divValues[3] + divSign[3]) : divValues[3];

	if (remRes != nullptr)
	{
		*remRes = WideIntVector(remValues[0], remValues[1],
								remValues[2], remValues[3]);
	}

	return WideIntVector(divValues[0], divValues[1], divValues[2],
						 divValues[3]);
#endif
}
WideIntVector FRAO_VECTORCALL vectorRemainder(
	RegIntVector dividendVec, RegIntVector divisorVec) noexcept
{
#ifdef FRAO_AVX2
	WideIntVector result;

	// we just use the divide function, which ends up with a free
	// remainder, once it's done
	vectorDivide(dividendVec, divisorVec, &result);

	return result;
#else
	// since c++ truncates towards 0, rather than performing proper
	// euclidean division, we must correct for negative remainder,
	// after calculating the 'c++' remainder
	int_least64_t remValues[4] =
		{dividendVec.m_Values[0] % divisorVec.m_Values[0],
		 dividendVec.m_Values[1] % divisorVec.m_Values[1],
		 dividendVec.m_Values[2] % divisorVec.m_Values[2],
		 dividendVec.m_Values[3] % divisorVec.m_Values[3]},
				  absValues[4] = {std::abs(divisorVec.m_Values[0]),
								  std::abs(divisorVec.m_Values[1]),
								  std::abs(divisorVec.m_Values[2]),
								  std::abs(divisorVec.m_Values[3])};
	bool negCorrection[4] = {(remValues[0] < 0), (remValues[1] < 0),
							 (remValues[2] < 0), (remValues[3] < 0)};

	remValues[0] = negCorrection[0] ? (absValues[0] + remValues[0])
									: remValues[0];
	remValues[1] = negCorrection[1] ? (absValues[1] + remValues[1])
									: remValues[1];
	remValues[2] = negCorrection[2] ? (absValues[2] + remValues[2])
									: remValues[2];
	remValues[3] = negCorrection[3] ? (absValues[3] + remValues[3])
									: remValues[3];
	return WideIntVector(remValues[0], remValues[1], remValues[2],
						 remValues[3]);
#endif
}

// wide vector maths operations
WideVector FRAO_VECTORCALL vectorScale(RegArgVector vector,
										double scaleFactor) noexcept
{
	return vectorMultiply(vector, vectorReplicate(scaleFactor));
}
WideVector FRAO_VECTORCALL vectorSqrt(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	return _mm256_sqrt_pd(vector);
#else
	return WideVector(
		sqrt(vector.m_Values[0]), sqrt(vector.m_Values[1]),
		sqrt(vector.m_Values[2]), sqrt(vector.m_Values[3]));
#endif
}
WideVector FRAO_VECTORCALL vectorNegate(RegArgVector vector) noexcept
{
	return vectorSubtract(vectorZero(), vector);
}

// basic matrix arithmetic
WideMatrix FRAO_VECTORCALL matrixAddition(
	RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept
{
	return WideMatrix(
		vectorAddition(lhsMatrix.m_Tuples[0], rhsMatrix.m_Tuples[0]),
		vectorAddition(lhsMatrix.m_Tuples[1], rhsMatrix.m_Tuples[1]),
		vectorAddition(lhsMatrix.m_Tuples[2], rhsMatrix.m_Tuples[2]),
		vectorAddition(lhsMatrix.m_Tuples[3], rhsMatrix.m_Tuples[3]));
}
WideMatrix FRAO_VECTORCALL matrixMultiply(
	RegArgMatrix multiplier, LateArgMatrix multiplicand) noexcept
{
	WideMatrix transpose = matrixTranspose(multiplier);
	WideVector resultRows[4];

	// do it for each row in turn
	for (uint_least64_t rowIndex = 0; rowIndex < 4; ++rowIndex)
	{
		WideVector nextMultiplicandRow =
			multiplicand.m_Tuples[rowIndex];

		WideVector rowOne[4] = {
			vector4DDot(nextMultiplicandRow, transpose.m_Tuples[0]),
			vector4DDot(nextMultiplicandRow, transpose.m_Tuples[1]),
			vector4DDot(nextMultiplicandRow, transpose.m_Tuples[2]),
			vector4DDot(nextMultiplicandRow, transpose.m_Tuples[3])};
		resultRows[rowIndex] = vectorPermute(
			vectorPermute(rowOne[0], rowOne[1], 0, 4, 0, 4),
			vectorPermute(rowOne[2], rowOne[3], 0, 4, 0, 4), 0, 1, 4,
			5);
	}

	return WideMatrix(resultRows[0], resultRows[1], resultRows[2],
					  resultRows[3]);
}
WideMatrix FRAO_VECTORCALL matrixScale(RegArgMatrix matrix,
										double scaleFactor) noexcept
{
	WideVector scaleVec = vectorReplicate(scaleFactor);

	return WideMatrix(vectorMultiply(matrix.m_Tuples[0], scaleVec),
					  vectorMultiply(matrix.m_Tuples[1], scaleVec),
					  vectorMultiply(matrix.m_Tuples[2], scaleVec),
					  vectorMultiply(matrix.m_Tuples[3], scaleVec));
}
WideMatrix FRAO_VECTORCALL
matrixTranspose(RegArgMatrix matrix) noexcept
{
#ifdef FRAO_AVX
	// collect x/z columns in two vecs, and y/w columns in two vecs
	WideVector xzColumnsLo = _mm256_unpacklo_pd(matrix.m_Tuples[0],
												matrix.m_Tuples[1]),
			   xzColumnsHi = _mm256_unpacklo_pd(matrix.m_Tuples[2],
												matrix.m_Tuples[3]),
			   ywColumnsLo = _mm256_unpackhi_pd(matrix.m_Tuples[0],
												matrix.m_Tuples[1]),
			   ywColumnsHi = _mm256_unpackhi_pd(matrix.m_Tuples[2],
												matrix.m_Tuples[3]);

	// have vectors now, of the form:
	//	xzLo - (x0, x1, z0, z1)
	//	xzHi = (x2, x3, z2, z3)
	//	ywLo = (y0, y1, w0, w1)
	//	ywHi = (y2, y3, w2, w3)

	// permute first two vecs to 1st/3rd rows
	// permute second two vecs to 2nd/4th rows

	// In THEORY: (zen1)
	// extract = 3/cycle, 1 lat
	// insert  = 2/cycle, 1 lat
	//	SO:
	//	x/y: cast (free) + insert; per row = 2xinsert = 1 lat
	//	z/w: extract + insert; per row = 2xextract + 2xinsert = 2 lat
	//	total: 3 cycles min

	WideVector row4 = _mm256_insertf128_pd(
				   ywColumnsHi,
				   _mm256_extractf128_pd(ywColumnsLo, 0b1), 0b0),
			   row3 = _mm256_insertf128_pd(
				   xzColumnsHi,
				   _mm256_extractf128_pd(xzColumnsLo, 0b1), 0b0),
			   row2 = _mm256_insertf128_pd(
				   ywColumnsLo, _mm256_castpd256_pd128(ywColumnsHi),
				   0b1),
			   row1 = _mm256_insertf128_pd(
				   xzColumnsLo, _mm256_castpd256_pd128(xzColumnsHi),
				   0b1);

	return WideMatrix(row1, row2, row3, row4);
#else

	return WideMatrix(vectorSet(vectorGetX(matrix.m_Tuples[0]),
								vectorGetX(matrix.m_Tuples[1]),
								vectorGetX(matrix.m_Tuples[2]),
								vectorGetX(matrix.m_Tuples[3])),
					  vectorSet(vectorGetY(matrix.m_Tuples[0]),
								vectorGetY(matrix.m_Tuples[1]),
								vectorGetY(matrix.m_Tuples[2]),
								vectorGetY(matrix.m_Tuples[3])),
					  vectorSet(vectorGetZ(matrix.m_Tuples[0]),
								vectorGetZ(matrix.m_Tuples[1]),
								vectorGetZ(matrix.m_Tuples[2]),
								vectorGetZ(matrix.m_Tuples[3])),
					  vectorSet(vectorGetW(matrix.m_Tuples[0]),
								vectorGetW(matrix.m_Tuples[1]),
								vectorGetW(matrix.m_Tuples[2]),
								vectorGetW(matrix.m_Tuples[3])));
#endif
}

namespace IMPL
{
void scalarSinCos(double &sinResult, double &cosResult,
				  double angle) noexcept
{
	// NOTE: this function is a placeholder for when we calculate sin
	// and cos ourselves, which will result in savings when we reuse
	// parts of the calculations
	sinResult = sin(angle);
	cosResult = cos(angle);
}
}  // namespace IMPL

namespace
{
static const WideIntVector g_MantissaMask =
	vectorReplicate(0xF'FFFF'FFFF'FFFFLL);
static const WideIntVector g_ExponentBias = vectorReplicate(1023LL);
static const WideIntVector g_ExponentMask = vectorReplicate(0x7FFLL);
static const WideIntVector g_MinusOneExponent =
	vectorReplicate(0x3FE0'0000'0000'0000LL);
static const WideVector g_SqrtTwo = vectorSqrt(vectorReplicate(2.0));
static const WideVector g_LnTwo =
	vectorReplicate(0.693'147'180'559'945'309);
static const WideVector g_Log10Two =
	vectorReplicate(0.301'029'995'663'981'195);
static const WideVector g_TwoPI = vectorReplicate(2.0 * ::frao::PI_D);
static const WideVector g_PIVec = vectorReplicate(::frao::PI_D);
static const WideVector g_HalfPI =
	vectorReplicate(::frao::PI_D / 2.0);
static const WideVector g_Half	  = vectorReplicate(.5);
static const WideVector g_Quarter = vectorReplicate(.25);
static const WideVector g_Four	  = vectorReplicate(4.);

#ifdef FRAO_AVX
WideVector FRAO_VECTORCALL
oddPolynomial3Power(RegArgVector arg, RegArgVector coeff0,
					RegArgVector coeff1, RegArgVector coeff2) noexcept
{
	// 1. Precalculate powers of the argument.
	// need a^2, a^4
	WideVector argSquare = vectorMultiply(arg, arg),
			   argPow4	 = vectorMultiply(argSquare, argSquare);

	// calculate the coefficientless part of each term
	WideVector var0 = arg, var1 = vectorMultiply(arg, argSquare),
			   var2 = vectorMultiply(arg, argPow4);

	// calculate each full term
	var0 = vectorMultiply(var0, coeff0);
	var1 = vectorMultiply(var1, coeff1);
	var2 = vectorMultiply(var2, coeff2);

	// 2. calculate from smallest term (highest power) to highest, to
	// preserve accuracy. Could calculate more terms simultaneously,
	// at the cost of reduced accuracy
	return vectorAddition(var0, vectorAddition(var1, var2));
}
WideVector FRAO_VECTORCALL oddPolynomial8Power(
	RegArgVector arg, RegArgVector coeff0, RegArgVector coeff1,
	RegArgVector coeff2, RegArgVector coeff3, RegArgVector coeff4,
	LateArgVector coeff5, LateArgVector coeff6,
	LateArgVector coeff7) noexcept
{
	// 1. Precalculate powers of the argument.
	// need a, a^3, a^5, a^7, a^9, a^11, a^13, a^15, so square arg
	// repeatedly to prepare x^2, x^4, x^8. We do this so that we can
	// calulate the coefficientless part without a long dependency
	// chain, from one term to the next
	WideVector argSquare = vectorMultiply(arg, arg),
			   argPow4	 = vectorMultiply(argSquare, argSquare),
			   argPow8	 = vectorMultiply(argPow4, argPow4);

	// calculate the coefficientless part of each term
	WideVector var0 = arg, var1 = vectorMultiply(arg, argSquare),
			   var2 = vectorMultiply(arg, argPow4),
			   var3 = vectorMultiply(
				   arg, vectorMultiply(argSquare, argPow4)),
			   var4 = vectorMultiply(arg, argPow8),
			   var5 = vectorMultiply(
				   arg, vectorMultiply(argSquare, argPow8)),
			   var6 = vectorMultiply(
				   arg, vectorMultiply(argPow4, argPow8)),
			   var7 =
				   vectorMultiply(vectorMultiply(arg, argSquare),
								  vectorMultiply(argPow4, argPow8));

	// calculate each full term
	var0 = vectorMultiply(var0, coeff0);
	var1 = vectorMultiply(var1, coeff1);
	var2 = vectorMultiply(var2, coeff2);
	var3 = vectorMultiply(var3, coeff3);
	var4 = vectorMultiply(var4, coeff4);
	var5 = vectorMultiply(var5, coeff5);
	var6 = vectorMultiply(var6, coeff6);
	var7 = vectorMultiply(var7, coeff7);

	// 2. calculate from smallest term (highest power) to highest, to
	// preserve accuracy. Could calculate more terms simultaneously,
	// at the cost of reduced accuracy
	return vectorAddition(
		var0,
		vectorAddition(
			var1,
			vectorAddition(
				var2,
				vectorAddition(
					var3, vectorAddition(
							  var4, vectorAddition(
										var5, vectorAddition(
												  var6, var7)))))));
}
WideVector FRAO_VECTORCALL evenPolynomial3Power(
	RegArgVector arg, RegArgVector coeff0, RegArgVector coeff1,
	RegArgVector coeff2) noexcept
{
	// 1. Precalculate powers of the argument.
	// need a^2, a^4, a^6
	WideVector argSquare = vectorMultiply(arg, arg),
			   argPow4	 = vectorMultiply(argSquare, argSquare);

	// calculate each term
	WideVector var0 = coeff0,
			   var1 = vectorMultiply(coeff1, argSquare),
			   var2 = vectorMultiply(coeff2, argPow4);

	// 2. calculate from smallest term (highest power) to highest, to
	// preserve accuracy. Could calculate more terms simultaneously,
	// at the cost of reduced accuracy
	return vectorAddition(var0, vectorAddition(var1, var2));
}
WideVector FRAO_VECTORCALL evenPolynomial4Power(
	RegArgVector arg, RegArgVector coeff0, RegArgVector coeff1,
	RegArgVector coeff2, RegArgVector coeff3) noexcept
{
	// 1. Precalculate powers of the argument.
	// need a^2, a^4, a^6
	WideVector argSquare = vectorMultiply(arg, arg),
			   argPow4	 = vectorMultiply(argSquare, argSquare),
			   argPow6	 = vectorMultiply(argSquare, argPow4);

	// calculate each term
	WideVector var0 = coeff0,
			   var1 = vectorMultiply(coeff1, argSquare),
			   var2 = vectorMultiply(coeff2, argPow4),
			   var3 = vectorMultiply(coeff3, argPow6);

	// 2. calculate from smallest term (highest power) to highest, to
	// preserve accuracy. Could calculate more terms simultaneously,
	// at the cost of reduced accuracy
	return vectorAddition(
		var0, vectorAddition(var1, vectorAddition(var2, var3)));
}
WideVector FRAO_VECTORCALL evenPolynomial9Power(
	RegArgVector arg, RegArgVector coeff0, RegArgVector coeff1,
	RegArgVector coeff2, RegArgVector coeff3, RegArgVector coeff4,
	LateArgVector coeff5, LateArgVector coeff6, LateArgVector coeff7,
	LateArgVector coeff8) noexcept
{
	// 1. Precalculate powers of the argument.
	// need a^2, a^4, a^6, a^8, a^10, a^12, a^14, a^16.
	WideVector argSquare = vectorMultiply(arg, arg),
			   argPow4	 = vectorMultiply(argSquare, argSquare),
			   argPow6	 = vectorMultiply(argSquare, argPow4),
			   argPow8	 = vectorMultiply(argPow4, argPow4),
			   argPow10	 = vectorMultiply(argSquare, argPow8),
			   argPow12	 = vectorMultiply(argPow4, argPow8),
			   argPow14	 = vectorMultiply(argPow6, argPow8),
			   argPow16	 = vectorMultiply(argPow8, argPow8);

	// calculate each term
	WideVector var0 = coeff0,
			   var1 = vectorMultiply(coeff1, argSquare),
			   var2 = vectorMultiply(coeff2, argPow4),
			   var3 = vectorMultiply(coeff3, argPow6),
			   var4 = vectorMultiply(coeff4, argPow8),
			   var5 = vectorMultiply(coeff5, argPow10),
			   var6 = vectorMultiply(coeff6, argPow12),
			   var7 = vectorMultiply(coeff7, argPow14),
			   var8 = vectorMultiply(coeff8, argPow16);

	// 2. calculate from smallest term (highest power) to highest, to
	// preserve accuracy. Could calculate more terms simultaneously,
	// at the cost of reduced accuracy
	return vectorAddition(
		var0,
		vectorAddition(
			var1,
			vectorAddition(
				var2,
				vectorAddition(
					var3,
					vectorAddition(
						var4,
						vectorAddition(
							var5, vectorAddition(
									  var6, vectorAddition(
												var7, var8))))))));
}
#endif
template<int ShiftVal>
WideIntVector FRAO_VECTORCALL
vectorShiftLeft(RegIntVector vector) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_slli_epi64(vector, ShiftVal);
#else
	return vectorSet(vectorGetX(vector) << ShiftVal,
					 vectorGetY(vector) << ShiftVal,
					 vectorGetZ(vector) << ShiftVal,
					 vectorGetW(vector) << ShiftVal);
#endif
}
template<int ShiftVal>
WideIntVector FRAO_VECTORCALL
vectorShiftRight(RegIntVector vector) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_srli_epi64(vector, ShiftVal);
#else
	return vectorSet(vectorGetX(vector) >> ShiftVal,
					 vectorGetY(vector) >> ShiftVal,
					 vectorGetZ(vector) >> ShiftVal,
					 vectorGetW(vector) >> ShiftVal);
#endif
}
}  // namespace

// wide vector trig operations
WideVector FRAO_VECTORCALL vectorSin(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	static const WideVector s_Coeffs[8] = {
		vectorReplicate(1.5707'96326'79489'50978),
		vectorReplicate(-.64596'40975'06173'16602),
		vectorReplicate(.0'79692'62624'51428'611),
		vectorReplicate(-.00'46817'54128'87387'128),
		vectorReplicate(.000'16044'11632'74341'52),
		vectorReplicate(-0.00000'35988'02475'1212),
		vectorReplicate(.00'00000'56877'69395'25),
		vectorReplicate(-.0000'00000'64348'41839'4)};

	// 1. range reduction to [0, 2PI] (actually [0, 1], with 1 meaning
	// 2PI). Based on the fact that frac/2PI = start/2PI -
	// floor(start/2PI)
	//
	// the deduced arg in [0, 1] will have lost precision, for large
	// values of the starting vector. In cases where the starting
	// vector is very large, a better function, with a better starting
	// translation, should be used
	WideVector scaledArg	  = vectorDivide(vector, g_TwoPI),
			   argTranslation = vectorFloor(scaledArg),
			   reducedRangeArg =
				   vectorSubtract(scaledArg, argTranslation);

	// 2. transformation to [0, pi], conditionally for [pi, 2pi],
	// using that sin(x+pi)=-sin(x). Given our range reduction to [0,
	// 1], this means we subtract 0.5 if the value lies above that,
	// and note the need to negate later
	WideIntVector argInNegHalf =
		vectorGreater(reducedRangeArg, g_Half);
	reducedRangeArg = vectorSelect(
		reducedRangeArg, vectorSubtract(reducedRangeArg, g_Half),
		argInNegHalf);

	// 3. transformation to [0, pi/2], conditionally for [pi/2, pi],
	// using that sin(x + pi/2) = cos(x) = sin(pi/2 - x). Given our
	// range reduction to [0, 0.5], this means we subtract the arg
	// from 0.5, if above 0.25
	reducedRangeArg = vectorSelect(
		reducedRangeArg, vectorSubtract(g_Half, reducedRangeArg),
		vectorGreater(reducedRangeArg, g_Quarter));

	// 4. Multiply argument in [0, 0.25], by 4, to have arg in
	// range [0, 1], as polynomial function expects (or rather, as
	// the coefficents do)
	reducedRangeArg = vectorMultiply(g_Four, reducedRangeArg);

	// 5. Call polynomial approximation function, with correct
	// coefficients.
	WideVector result =
		oddPolynomial8Power(reducedRangeArg, s_Coeffs[0], s_Coeffs[1],
							s_Coeffs[2], s_Coeffs[3], s_Coeffs[4],
							s_Coeffs[5], s_Coeffs[6], s_Coeffs[7]);

	// 6. Do any post-calc re-tranformations, and return
	return vectorSelect(result, vectorNegate(result), argInNegHalf);
#else

	return WideVector(
		sin(vector.m_Values[0]), sin(vector.m_Values[1]),
		sin(vector.m_Values[2]), sin(vector.m_Values[3]));
#endif
}
WideVector FRAO_VECTORCALL vectorCos(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	static const WideVector s_Coeffs[9] = {
		vectorReplicate(.99999'99999'99999'99608'97),
		vectorReplicate(-.49999'99999'99999'74308'584),
		vectorReplicate(.0'41666'66666'66638'87959'16),
		vectorReplicate(-.00'13888'88888'87731'72115'1),
		vectorReplicate(.0000'24801'58727'74439'38629),
		vectorReplicate(-.0'00000'27557'31639'35346'178),
		vectorReplicate(.000'00000'20876'56196'01122'53),
		vectorReplicate(-.00000'00000'11462'90489'93344),
		vectorReplicate(.000'00000'00000'46090'07376'9)};

	// 1. range reduction to [0, 2PI] (actually [0, 1], with 1 meaning
	// 2PI). Based on the fact that frac/2PI = start/2PI -
	// floor(start/2PI)
	//
	// the deduced arg in [0, 1] will have lost precision, for large
	// values of the starting vector. In cases where the starting
	// vector is very large, a better function, with a better starting
	// translation, should be used
	WideVector scaledArg	  = vectorDivide(vector, g_TwoPI),
			   argTranslation = vectorFloor(scaledArg),
			   reducedRangeArg =
				   vectorSubtract(scaledArg, argTranslation);

	// 2. transformation to [0, pi], conditionally for [pi, 2pi],
	// using that cos(x+pi)= -cos(x). Given our range reduction to [0,
	// 1], this means we subtract 0.5 if the value lies above that,
	// and note the need to negate later
	WideIntVector argInNegHalf =
		vectorGreater(reducedRangeArg, g_Half);
	reducedRangeArg = vectorSelect(
		reducedRangeArg, vectorSubtract(reducedRangeArg, g_Half),
		argInNegHalf);

	// 3. transformation to [0, pi/2], conditionally for [pi/2, pi],
	// using that cos(x + pi/2) = -sin(x) = -cos(pi/2 - x). Given our
	// range reduction to [0, 0.5], this means we subtract the arg
	// from 0.5, if above 0.25, and note the need to negate later
	WideIntVector argInNegQuarter =
		vectorGreater(reducedRangeArg, g_Quarter);
	reducedRangeArg = vectorSelect(
		reducedRangeArg, vectorSubtract(g_Half, reducedRangeArg),
		argInNegQuarter);

	// 4. Multiply argument in [0, 0.25], by 2PI, to have arg in
	// range [0, PI/2], as polynomial function expects (or rather, as
	// the coefficents do)
	reducedRangeArg = vectorMultiply(g_TwoPI, reducedRangeArg);

	// 5. Call polynomial approximation function, with correct
	// coefficients.
	WideVector result = evenPolynomial9Power(
		reducedRangeArg, s_Coeffs[0], s_Coeffs[1], s_Coeffs[2],
		s_Coeffs[3], s_Coeffs[4], s_Coeffs[5], s_Coeffs[6],
		s_Coeffs[7], s_Coeffs[8]);

	// 6. Do any post-calc re-tranformations, and return
	return vectorSelect(result, vectorNegate(result),
						vectorXor(argInNegHalf, argInNegQuarter));
#else

	return WideVector(
		cos(vector.m_Values[0]), cos(vector.m_Values[1]),
		cos(vector.m_Values[2]), cos(vector.m_Values[3]));
#endif
}
WideVector FRAO_VECTORCALL vectorTan(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	static const WideVector
		s_DividendCoeffs[4] =
			{vectorReplicate(-16045.33119'55921'87943'92686'1),
			 vectorReplicate(1269.7'54837'65808'28837'86072),
			 vectorReplicate(-17.135'18551'48861'10932'101),
			 vectorReplicate(.0'28208'77297'16551'03151'4)},
		s_DivisorCoeffs[4] = {
			vectorReplicate(-20429.55018'66006'97853'11414'2),
			vectorReplicate(5817.3'59955'46556'86739'03419),
			vectorReplicate(-181.49'31035'40890'45993'4575),
			vectorReplicate(1.)};

	// 1. range reduction to [-PI/2, PI/2] (actually [-1/2, 1/2], with
	// 1/2 meaning PI/2). Based on the fact that frac/PI = start/PI -
	// round(start/2PI)
	//
	// the deduced arg in [-1/2, 1/2] will have lost precision, for
	// large values of the starting vector. In cases where the
	// starting vector is very large, a better function, with a better
	// starting translation, should be used
	WideVector scaledArg = vectorDivide(
				   vector, vectorReplicate(::frao::PI_D)),
			   argTranslation = vectorRound(scaledArg),
			   reducedRangeArg =
				   vectorSubtract(scaledArg, argTranslation);

	// 2. transformation to [0, pi/2], conditionally for [-pi/2, 0],
	// using that tan(-x)= -tan(x). This means we negate if the value
	// lies below 0, and note the need to negate the result later
	WideIntVector argInNegHalf =
		vectorLess(reducedRangeArg, vectorZero());
	reducedRangeArg = vectorSelect(
		reducedRangeArg, vectorNegate(reducedRangeArg), argInNegHalf);

	// 3. transformation to [0, pi/4], conditionally for [pi/4, pi/2],
	// using that tan(pi/2 - x) = cot(x) = 1/tan(x). Given our range
	// reduction to [0, 0.5], this means we subtract the arg from 0.5,
	// if above 0.25, and note the need to invert later
	WideIntVector argInTopQuarter =
		vectorGreater(reducedRangeArg, g_Quarter);
	reducedRangeArg = vectorSelect(
		reducedRangeArg, vectorSubtract(g_Half, reducedRangeArg),
		argInTopQuarter);

	// 4. Multiply argument in [0, 0.25], by 4, to have arg in
	// range [0, 1], as polynomial function expects (or rather, as
	// the coefficents do)
	reducedRangeArg = vectorMultiply(g_Four, reducedRangeArg);

	// 5. Calculate the divisor, and most of the dividend, using even4
	// polynomials, and then multiply the dividend by the arg again,
	// to correct it to its real value
	WideVector dividend = evenPolynomial4Power(
				   reducedRangeArg, s_DividendCoeffs[0],
				   s_DividendCoeffs[1], s_DividendCoeffs[2],
				   s_DividendCoeffs[3]),
			   divisor = evenPolynomial4Power(
				   reducedRangeArg, s_DivisorCoeffs[0],
				   s_DivisorCoeffs[1], s_DivisorCoeffs[2],
				   s_DivisorCoeffs[3]);
	dividend = vectorMultiply(reducedRangeArg, dividend);

	// 6. With the dividend and divisor of the result, we then select
	// either perform the division as normal, or perform the inverse
	// (ie: calc 1/tan instead), if that is required. It will be
	// required if we had a 'top half' argument. That is, an argument
	// with original magnitude in the range (PI/4, PI/2)
	WideVector result = vectorSelect(vectorDivide(dividend, divisor),
									 vectorDivide(divisor, dividend),
									 argInTopQuarter);

	// 7. Do any post-calc negation, and return
	return vectorSelect(result, vectorNegate(result), argInNegHalf);

#else

	return WideVector(
		tan(vector.m_Values[0]), tan(vector.m_Values[1]),
		tan(vector.m_Values[2]), tan(vector.m_Values[3]));
#endif
}

// wide vector reverse trig operations
WideVector FRAO_VECTORCALL vectorArcSin(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	static const WideVector s_MinusOne =
								vectorNegate(vectorSplatOne()),
							s_TwoVector		 = vectorReplicate(2.0),
							s_ChoiceFraction = vectorDivide(
								vectorSqrt(s_TwoVector), s_TwoVector);

	// clamp vector to [-1, 1]
	WideVector clampedVec =
		vectorSelect(vector, vectorSplatOne(),
					 vectorNot(vectorLess(vector, vectorSplatOne())));
	clampedVec = vectorSelect(clampedVec, s_MinusOne,
							  vectorLess(vector, s_MinusOne));

	// prepare |x|, sgn(x), sqrt(1-x^2)
	WideIntVector isNegative   = vectorLess(clampedVec, vectorZero());
	WideVector	  magnitudeVec = vectorSelect(
					  clampedVec, vectorNegate(clampedVec), isNegative),
			   signVec = vectorSelect(vectorSplatOne(), s_MinusOne,
									  isNegative),
			   argFunc = vectorSqrt(vectorSubtract(
				   vectorSplatOne(),
				   vectorMultiply(clampedVec, clampedVec)));

	// work out which option for calculation should be more accurate
	// [arctan(x/argFunc) or
	// sgn(x)(PI/2-arctan(argFunc/magnitude(x)))], and then perform
	// the calculation
	WideIntVector doOptionTwo =
		vectorGreater(magnitudeVec, s_ChoiceFraction);
	WideVector result = vectorArcTan(vectorSelect(
		vectorDivide(clampedVec, argFunc),
		vectorDivide(argFunc, magnitudeVec), doOptionTwo));

	// calculate prospective option 2 correction
	WideVector correction =
		vectorMultiply(signVec, vectorSubtract(g_HalfPI, result));

	// return either correct form of result
	return vectorSelect(result, correction, doOptionTwo);

#else

	return WideVector(
		asin(vector.m_Values[0]), asin(vector.m_Values[1]),
		asin(vector.m_Values[2]), asin(vector.m_Values[3]));
#endif
}
WideVector FRAO_VECTORCALL vectorArcCos(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	// arccos = pi/2 - arcsin(vector)
	return vectorSubtract(g_HalfPI, vectorArcSin(vector));
#else

	return WideVector(
		acos(vector.m_Values[0]), acos(vector.m_Values[1]),
		acos(vector.m_Values[2]), acos(vector.m_Values[3]));
#endif
}
WideVector FRAO_VECTORCALL vectorArcTan(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	// Coefficients for the approximation polynomial
	static const WideVector s_Coeffs[8] = {
		vectorReplicate(.99999'99999'99999'99072'43),
		vectorReplicate(-.33333'33333'33303'24024'2),
		vectorReplicate(.19999'99999'83973'5188),
		vectorReplicate(-.14285'71396'02658'521),
		vectorReplicate(.11111'07860'64198'1),
		vectorReplicate(-.0'90891'41250'1871),
		vectorReplicate(.0'76387'43526'387),
		vectorReplicate(-.0'58096'31913)};

	// Here we store vectors of the segmentation points, that divide
	// the our range, and each range's related coefficient.
	//
	// To find the correct index, one finds the largest value of
	// s_PartitionPoints (Xi), that is smaller than the argument.
	// Then, the index of that point, is the index of
	// s_InvCentrePoints(xi^-1), s_InvSqCentrePoints(xi^-2), and
	// s_ArcTanCentrePoints(arctan(xi)), that should be used to
	// calculate the argument translation to the range of the
	// polynomial
	static const WideVector s_PartitionPoints = vectorSet(
								0.19891'23673'79658'00691'16,
								0.66817'86379'19298'91999'78,
								1.4966'05762'66548'90176'01,
								5.0273'39492'12584'81045'15),
							s_InvCentrePoints = vectorSet(
								2.4142'13562'37309'50488'02, 1.,
								0.41421'35623'73095'04880'17, 0.),
							s_InvSqCentrePoints = vectorSet(
								5.8284'27124'74619'00976'03, 1.,
								0.17157'28752'53809'90239'66, 0.),
							s_ArcTanCentrePoints =
								vectorSet(::frao::PI_D / 8.0,
										  ::frao::PI_D / 4.0,
										  3.0 * (::frao::PI_D / 8.0),
										  ::frao::PI_D / 2.0);

	// just to allow increment/ decrement of narrow int vectors
	static const NarrowIntVector s_SmallOneSplat =
		vectorSet(1, 1, 1, 1);

	// take the magnitude of the argument, making a note of the
	// original sign
	WideIntVector negArg = vectorLess(vector, vectorZero());
	WideVector	  magArg =
		vectorSelect(vector, vectorNegate(vector), negArg);

	// compare argument magnitude to partition division points,
	// sequentially, to determine which partition each argument lies
	// in
	NarrowIntVector partitionNum = toSmallIntVector(vectorZeroInt());
	partitionNum				 = vectorSelect(
		partitionNum, vectorAddition(s_SmallOneSplat, partitionNum),
		toSmallIntVector(
			vectorGreater(magArg, vectorSplatX(s_PartitionPoints))));
	partitionNum = vectorSelect(
		partitionNum, vectorAddition(s_SmallOneSplat, partitionNum),
		toSmallIntVector(
			vectorGreater(magArg, vectorSplatY(s_PartitionPoints))));
	partitionNum = vectorSelect(
		partitionNum, vectorAddition(s_SmallOneSplat, partitionNum),
		toSmallIntVector(
			vectorGreater(magArg, vectorSplatZ(s_PartitionPoints))));
	partitionNum = vectorSelect(
		partitionNum, vectorAddition(s_SmallOneSplat, partitionNum),
		toSmallIntVector(
			vectorGreater(magArg, vectorSplatW(s_PartitionPoints))));

	// obtain correct values of 1/xi, 1/xi^2, arctan(xi), to translate
	// arctan argument into range [0, tan(PI/16)], for the polynomial
	// approximation.
	//
	// translation is pArg = xi^-1 - (xi^-2 + 1)/(xi^-1 + x)
	NarrowIntVector partitionIndex =
		vectorSubtract(partitionNum, s_SmallOneSplat);
	WideVector ourInvPt =
				   vectorSwizzle(s_InvCentrePoints, partitionIndex),
			   ourInvSqPt =
				   vectorSwizzle(s_InvSqCentrePoints, partitionIndex),
			   ourOffsetPt = vectorSwizzle(s_ArcTanCentrePoints,
										   partitionIndex),
			   transArg	   = vectorSubtract(
					  ourInvPt,
					  vectorDivide(
						  vectorAddition(vectorSplatOne(), ourInvSqPt),
						  vectorAddition(ourInvPt, magArg)));

	// check whether we needed to translate the argument, then
	// calculate the polynomial, and add any necessary translation to
	// it
	WideIntVector translationNeeded =
		vectorGreater(toWideIntVector(partitionNum), vectorZeroInt());
	WideVector polynomialArg =
				   vectorSelect(magArg, transArg, translationNeeded),
			   polynomialRes = oddPolynomial8Power(
				   polynomialArg, s_Coeffs[0], s_Coeffs[1],
				   s_Coeffs[2], s_Coeffs[3], s_Coeffs[4], s_Coeffs[5],
				   s_Coeffs[6], s_Coeffs[7]),
			   result = vectorSelect(
				   polynomialRes,
				   vectorAddition(ourOffsetPt, polynomialRes),
				   translationNeeded);

	// if we need to correct for negative input, do so, then return
	return vectorSelect(result, vectorNegate(result), negArg);
#else

	return WideVector(
		atan(vector.m_Values[0]), atan(vector.m_Values[1]),
		atan(vector.m_Values[2]), atan(vector.m_Values[3]));
#endif
}
WideVector FRAO_VECTORCALL
vectorArcTan2(RegArgVector topVec, RegArgVector bottomVec) noexcept
{
#ifdef FRAO_AVX
	// check for special cases, in the numerator and denominator of
	// the argument
	WideIntVector argsAreInf  = vectorAnd(vectorIsInf(topVec),
										  vectorIsInf(bottomVec)),
				  negTop	  = vectorLess(topVec, vectorZero()),
				  negBottom	  = vectorLess(bottomVec, vectorZero()),
				  oneArgIsNeg = vectorXor(negTop, negBottom);

	// calculate basic argument
	WideVector argument = vectorDivide(topVec, bottomVec);

	// shouldn't need to correct for argument == +-inf, since arctan
	// should naturally result in +-PI/2 in that case, as required

	// correct for (|y|, |x| == inf) => sgn(y). sign of x will be
	// corrected for later
	argument = vectorSelect(argument, vectorSplatOne(), argsAreInf);
	argument = vectorSelect(argument, vectorNegate(argument),
							vectorAnd(oneArgIsNeg, argsAreInf));

	// Calculate basic artan, in range [-PI/2, PI/2]
	WideVector narrowRes = vectorArcTan(argument);

	// if the top and bottom vecs imply that the result should lie
	// outside of [-PI/2, PI/2], then we need to +-PI to a result in
	// the range [-PI, PI]
	WideVector correctionSize =
				   vectorSelect(vectorZero(), g_PIVec, negBottom),
			   correction =
				   vectorSelect(correctionSize,
								vectorNegate(correctionSize), negTop),
			   result = vectorAddition(narrowRes, correction);

	return result;
#else

	return WideVector(
		atan2(topVec.m_Values[0], bottomVec.m_Values[0]),
		atan2(topVec.m_Values[1], bottomVec.m_Values[1]),
		atan2(topVec.m_Values[2], bottomVec.m_Values[2]),
		atan2(topVec.m_Values[3], bottomVec.m_Values[3]));
#endif
}

// wide vector special maths operations
WideVector FRAO_VECTORCALL vectorExp(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	// just use equivalent approx. of 2^x, by converting argument
	return vectorExp2(vectorDivide(vector, g_LnTwo));

#else

	return WideVector(
		exp(vector.m_Values[0]), exp(vector.m_Values[1]),
		exp(vector.m_Values[2]), exp(vector.m_Values[3]));
#endif
}
WideVector FRAO_VECTORCALL vectorExp2(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	// Coefficients for the approximation polynomial
	static const WideVector s_OddCoeffs[3] =
		{vectorReplicate(1513.9'06799'05433'89158'94328),
		 vectorReplicate(20.202'06565'12869'27227'886),
		 vectorReplicate(0.0'23093'34775'37502'33624)},
							s_EvenCoeffs[3] = {
								vectorReplicate(
									4368.2'11662'72755'84984'96814),
								vectorReplicate(
									233.18'42114'27481'62379'0295),
								vectorReplicate(1.0)};

	// first, check if the argument is negative, or infinite
	WideIntVector argIsNeg = vectorLess(vector, vectorZero()),
				  argIsInf = vectorIsInf(vector);

	// get the positive form of the argument, and divide the argument
	// into integer and fractional (in range [0, 1) ) arguments. Then,
	// divide the fraction in two, for the argument to the
	// approximation function
	WideVector magnitudeArg = vectorAbs(vector),
			   integerArg	= vectorFloor(magnitudeArg),
			   fractionArg = vectorSubtract(magnitudeArg, integerArg);

	// NOTE: the way this function procedes is essentially:
	// 2^x = (2^n)(2^t/2)(2^m), where n + t + m = x. t either 0
	// or 1/2, n an integer, m in range [0,1/2)

	// next, use integer arithmetic to divide off the 'multiple of
	// two' portion of the number. With that, calculate an exponent
	// multiplier, for later [eq. to
	// reinterpret<double>(((integerArgIntVec & 7FF) << 52) + 1023)].
	// Then, determine if the integer was odd, for later
	// multiplication
	WideIntVector integerArgIntVec	= toIntegerVector(integerArg),
				  extraExponentBits = vectorAddition(
					  vectorShiftLeft<52>(g_ExponentBias),
					  vectorShiftLeft<52>(vectorAnd(
						  g_ExponentMask, integerArgIntVec))),
				  doSqrtScale = vectorGreater(fractionArg, g_Half);

	// check if the exponent was too large to fit a double, in which
	// case we return inf
	argIsInf = vectorOr(
		argIsInf, vectorGreater(integerArgIntVec, g_ExponentBias));

	// Calculate the result multiplier, approximation polynomial
	// result, and final scale factor
	WideVector approximationArg = vectorSelect(
				   fractionArg, vectorSubtract(fractionArg, g_Half),
				   doSqrtScale),
			   extraExponentMultiplier =
				   reinterpretToDouble(extraExponentBits),
			   oddComponent = oddPolynomial3Power(
				   approximationArg, s_OddCoeffs[0], s_OddCoeffs[1],
				   s_OddCoeffs[2]),
			   evenComponent = evenPolynomial3Power(
				   approximationArg, s_EvenCoeffs[0], s_EvenCoeffs[1],
				   s_EvenCoeffs[2]),
			   result = vectorDivide(
				   vectorAddition(evenComponent, oddComponent),
				   vectorSubtract(evenComponent, oddComponent)),
			   finalScaleFactor = vectorSelect(
				   extraExponentMultiplier,
				   vectorMultiply(g_SqrtTwo, extraExponentMultiplier),
				   doSqrtScale);

	// scale by square root, if necessary
	result = vectorMultiply(finalScaleFactor, result);

	// set to inf, if exponential of result was too large, or argument
	// was infinite
	result = vectorSelect(result, vectorSplatInfinity(), argIsInf);

	// return result, or reciprocal, if original argument was negative
	return vectorSelect(
		result, vectorDivide(vectorSplatOne(), result), argIsNeg);
#else

	return WideVector(
		exp2(vector.m_Values[0]), exp2(vector.m_Values[1]),
		exp2(vector.m_Values[2]), exp2(vector.m_Values[3]));
#endif
}
WideVector FRAO_VECTORCALL vectorLog(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	// Coefficients for the approximation polynomial
	static const WideVector
		s_TopCoeffs[3]	  = {vectorReplicate(
								 -90.174'69166'20405'36328'986),
							 vectorReplicate(93.463'90064'28585'38247'4),
							 vectorReplicate(-18.327'87037'2215'93212)},
		s_BottomCoeffs[4] = {
			vectorReplicate(-45.087'34583'10203'05748'486),
			vectorReplicate(61.761'06559'84713'02843),
			vectorReplicate(-20.733'48789'55139'39345),
			vectorReplicate(1.0)};

	// first, we have to check for arg <= 0, arg==inf, recording when
	// it is, and setting the argument to a sensible value instead.
	// Also prepare a vector of potential replacement return values,
	// for inf, <=0 arguments
	WideIntVector outOfRange = vectorNot(
					  vectorGreaterEqual(vector, vectorZero())),
				  argEqualsZero = vectorEqual(vector, vectorZero()),
				  argInfinite	= vectorIsInf(vector),
				  resultNeedsReplacement =
					  vectorOr(argEqualsZero,
							   vectorOr(outOfRange, argInfinite));
	WideVector replacementArgs = vectorSelect(
				   vectorSelect(vectorSplatInfinity(),
								vectorSplatQNaN(), outOfRange),
				   vectorNegate(vectorSplatInfinity()),
				   argEqualsZero),
			   usedArg =
				   vectorSelect(vector, vectorSplatOne(), outOfRange);

	// NOTE: the way this function procedes is essentially:
	// ln(x)=ln(2)*(n-1/2)+ln(t), with t in the range [1/sqrt(2.0),
	// sqrt(2.0))

	// Get the value of the exponent, and rescale the argument to
	// [0.5, 1)
	WideIntVector argBits = reinterpretToInteger(usedArg),
				  mantissaBits =
					  vectorOr(g_MinusOneExponent,
							   vectorAnd(g_MantissaMask, argBits)),
				  exponentValue = vectorSubtract(
					  vectorAnd(g_ExponentMask,
								vectorShiftRight<52>(argBits)),
					  g_ExponentBias);

	// take the fractional part of the argument, between [0.5, 1), and
	// rescale again, to [1/sqrt(2.0), sqrt(2.0)). Then, take the main
	// (exponent) part of the argument, and calculate it's
	// contribution to the rest.
	//
	// NOTE: the calculated expression for main is main = ln(2)*(n
	// -1/2), but we also have to add one to the exponent, to account
	// for resclaing the mantissa to [0.5, 1)
	WideVector fraction = vectorMultiply(
				   g_SqrtTwo, reinterpretToDouble(mantissaBits)),
			   main = vectorMultiply(
				   g_LnTwo,
				   vectorAddition(g_Half,
								  toDoubleVector(exponentValue)));

	// We now use the polynomial approximation of ln(fraction), in the
	// range [1/sqrt(2.0), sqrt(2.0)), to compute the rest of the
	// result. We then compute the full result.
	WideVector polynomialArg = vectorDivide(
				   vectorSubtract(fraction, vectorSplatOne()),
				   vectorAddition(vectorSplatOne(), fraction)),
			   restTop = oddPolynomial3Power(
				   polynomialArg, s_TopCoeffs[0], s_TopCoeffs[1],
				   s_TopCoeffs[2]),
			   restBottom = evenPolynomial4Power(
				   polynomialArg, s_BottomCoeffs[0],
				   s_BottomCoeffs[1], s_BottomCoeffs[2],
				   s_BottomCoeffs[3]),
			   rest = vectorDivide(restTop, restBottom);

	// Compute the full result
	WideVector result = vectorAddition(rest, main);

	// finally, set NaN for any faulty input, inf for inf input, and
	// -inf for 0 input
	result =
		vectorSelect(result, replacementArgs, resultNeedsReplacement);
	return result;
#else

	return WideVector(
		log(vector.m_Values[0]), log(vector.m_Values[1]),
		log(vector.m_Values[2]), log(vector.m_Values[3]));
#endif
}
WideVector FRAO_VECTORCALL vectorLog2(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	// Coefficients for the approximation polynomial
	static const WideVector
		s_TopCoeffs[3]	  = {vectorReplicate(
								 -39.162'36848'49248'00917'336),
							 vectorReplicate(40.590'85433'87185'67070'3),
							 vectorReplicate(-7.9596'92793'73355'6788)},
		s_BottomCoeffs[4] = {
			vectorReplicate(-45.087'34293'98638'65937'012),
			vectorReplicate(61.761'06236'94362'48610'5),
			vectorReplicate(-20.733'48719'71240'8126),
			vectorReplicate(1.0)};

	// first, we have to check for arg <= 0, arg==inf, recording when
	// it is, and setting the argument to a sensible value instead.
	// Also prepare a vector of potential replacement return values,
	// for inf, <=0 arguments
	WideIntVector outOfRange = vectorNot(
					  vectorGreaterEqual(vector, vectorZero())),
				  argEqualsZero = vectorEqual(vector, vectorZero()),
				  argInfinite	= vectorIsInf(vector),
				  resultNeedsReplacement =
					  vectorOr(argEqualsZero,
							   vectorOr(outOfRange, argInfinite));
	WideVector replacementArgs = vectorSelect(
				   vectorSelect(vectorSplatInfinity(),
								vectorSplatQNaN(), outOfRange),
				   vectorNegate(vectorSplatInfinity()),
				   argEqualsZero),
			   usedArg =
				   vectorSelect(vector, vectorSplatOne(), outOfRange);

	// NOTE: the way this function procedes is essentially:
	// log2(x)=log10(t)/log10(2)-1/2 + n, with t in the range
	// [1/sqrt(2.0), sqrt(2.0))

	// Get the value of the exponent, and rescale the argument to
	// [0.5, 1)
	WideIntVector argBits = reinterpretToInteger(usedArg),
				  mantissaBits =
					  vectorOr(g_MinusOneExponent,
							   vectorAnd(g_MantissaMask, argBits)),
				  exponentValue = vectorSubtract(
					  vectorAnd(g_ExponentMask,
								vectorShiftRight<52>(argBits)),
					  g_ExponentBias);

	// take the fractional part of the argument, between [0.5, 1), and
	// rescale again, to [1/sqrt(2.0), sqrt(2.0)). Then, take the main
	// (exponent) part of the argument, and calculate it's
	// contribution to the rest.
	//
	// NOTE: the calculated expression for main is main = log10(2)*(n
	// -1/2), but we also have to add one to the exponent, to account
	// for resclaing the mantissa to [0.5, 1)
	WideVector fraction = vectorMultiply(
				   g_SqrtTwo, reinterpretToDouble(mantissaBits)),
			   main = vectorAddition(g_Half,
									 toDoubleVector(exponentValue));

	// We now use the polynomial approximation of log10(fraction), in
	// the range [1/sqrt(2.0), sqrt(2.0)), to compute the rest of the
	// result. We then compute the full result.
	WideVector polynomialArg = vectorDivide(
				   vectorSubtract(fraction, vectorSplatOne()),
				   vectorAddition(vectorSplatOne(), fraction)),
			   restTop = oddPolynomial3Power(
				   polynomialArg, s_TopCoeffs[0], s_TopCoeffs[1],
				   s_TopCoeffs[2]),
			   restBottom = evenPolynomial4Power(
				   polynomialArg, s_BottomCoeffs[0],
				   s_BottomCoeffs[1], s_BottomCoeffs[2],
				   s_BottomCoeffs[3]),
			   rest = vectorDivide(vectorDivide(restTop, restBottom),
								   g_Log10Two);

	// Compute the full result
	WideVector result = vectorAddition(rest, main);

	// finally, set NaN for any faulty input, inf for inf input, and
	// -inf for 0 input
	result =
		vectorSelect(result, replacementArgs, resultNeedsReplacement);
	return result;
#else

	return WideVector(
		log2(vector.m_Values[0]), log2(vector.m_Values[1]),
		log2(vector.m_Values[2]), log2(vector.m_Values[3]));
#endif
}
WideVector FRAO_VECTORCALL vectorLog10(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	// Coefficients for the approximation polynomial
	static const WideVector
		s_TopCoeffs[3]	  = {vectorReplicate(
								 -39.162'36848'49248'00917'336),
							 vectorReplicate(40.590'85433'87185'67070'3),
							 vectorReplicate(-7.9596'92793'73355'6788)},
		s_BottomCoeffs[4] = {
			vectorReplicate(-45.087'34293'98638'65937'012),
			vectorReplicate(61.761'06236'94362'48610'5),
			vectorReplicate(-20.733'48719'71240'8126),
			vectorReplicate(1.0)};

	// first, we have to check for arg <= 0, arg==inf, recording when
	// it is, and setting the argument to a sensible value instead.
	// Also prepare a vector of potential replacement return values,
	// for inf, <=0 arguments
	WideIntVector outOfRange = vectorNot(
					  vectorGreaterEqual(vector, vectorZero())),
				  argEqualsZero = vectorEqual(vector, vectorZero()),
				  argInfinite	= vectorIsInf(vector),
				  resultNeedsReplacement =
					  vectorOr(argEqualsZero,
							   vectorOr(outOfRange, argInfinite));
	WideVector replacementArgs = vectorSelect(
				   vectorSelect(vectorSplatInfinity(),
								vectorSplatQNaN(), outOfRange),
				   vectorNegate(vectorSplatInfinity()),
				   argEqualsZero),
			   usedArg =
				   vectorSelect(vector, vectorSplatOne(), outOfRange);

	// NOTE: the way this function procedes is essentially:
	// log10(x)=log10(2)*(n-1/2)+log10(t), with t in the range
	// [1/sqrt(2.0), sqrt(2.0))

	// Get the value of the exponent, and rescale the argument to
	// [0.5, 1)
	WideIntVector argBits = reinterpretToInteger(usedArg),
				  mantissaBits =
					  vectorOr(g_MinusOneExponent,
							   vectorAnd(g_MantissaMask, argBits)),
				  exponentValue = vectorSubtract(
					  vectorAnd(g_ExponentMask,
								vectorShiftRight<52>(argBits)),
					  g_ExponentBias);

	// take the fractional part of the argument, between [0.5, 1), and
	// rescale again, to [1/sqrt(2.0), sqrt(2.0)). Then, take the main
	// (exponent) part of the argument, and calculate it's
	// contribution to the rest.
	//
	// NOTE: the calculated expression for main is main = log10(2)*(n
	// -1/2), but we also have to add one to the exponent, to account
	// for resclaing the mantissa to [0.5, 1)
	WideVector fraction = vectorMultiply(
				   g_SqrtTwo, reinterpretToDouble(mantissaBits)),
			   main = vectorMultiply(
				   g_Log10Two,
				   vectorAddition(g_Half,
								  toDoubleVector(exponentValue)));

	// We now use the polynomial approximation of log10(fraction), in
	// the range [1/sqrt(2.0), sqrt(2.0)), to compute the rest of the
	// result. We then compute the full result.
	WideVector polynomialArg = vectorDivide(
				   vectorSubtract(fraction, vectorSplatOne()),
				   vectorAddition(vectorSplatOne(), fraction)),
			   restTop = oddPolynomial3Power(
				   polynomialArg, s_TopCoeffs[0], s_TopCoeffs[1],
				   s_TopCoeffs[2]),
			   restBottom = evenPolynomial4Power(
				   polynomialArg, s_BottomCoeffs[0],
				   s_BottomCoeffs[1], s_BottomCoeffs[2],
				   s_BottomCoeffs[3]),
			   rest = vectorDivide(restTop, restBottom);

	// Compute the full result
	WideVector result = vectorAddition(rest, main);

	// finally, set NaN for any faulty input, inf for inf input, and
	// -inf for 0 input
	result =
		vectorSelect(result, replacementArgs, resultNeedsReplacement);
	return result;
#else

	return WideVector(
		log10(vector.m_Values[0]), log10(vector.m_Values[1]),
		log10(vector.m_Values[2]), log10(vector.m_Values[3]));
#endif
}
//
WideVector FRAO_VECTORCALL vectorPow(
	RegArgVector baseVector, RegArgVector exponentVector) noexcept
{
#ifdef FRAO_AVX
	// round exponent to integer, in case we need an integer exponent
	// (which we probably won't)
	WideVector intExponent = vectorRound(exponentVector);

	// check for base < 0, and whether the integer exponent is odd
	WideIntVector negBase = vectorLess(baseVector, vectorZero()),
				  haveOddExponent = vectorNotEqual(
					  vectorZeroInt(),
					  vectorAnd(vectorSplatOneInt(),
								toIntegerVector(intExponent)));

	// use exp2(exponent*log2(base)), assuming positive base
	WideVector usedExponent =
				   vectorSelect(exponentVector, intExponent, negBase),
			   negBaseCorrection = vectorSelect(
				   vectorSplatOne(), vectorNegate(vectorSplatOne()),
				   vectorAnd(negBase, haveOddExponent)),
			   magBase = vectorAbs(baseVector),
			   logBase = vectorLog2(magBase),
			   exp2Arg = vectorMultiply(logBase, usedExponent),
			   result  = vectorExp2(exp2Arg);

	// correct for any negative base
	result = vectorMultiply(negBaseCorrection, result);

	return result;
#else

	return WideVector(
		pow(baseVector.m_Values[0], exponentVector.m_Values[0]),
		pow(baseVector.m_Values[1], exponentVector.m_Values[1]),
		pow(baseVector.m_Values[2], exponentVector.m_Values[2]),
		pow(baseVector.m_Values[3], exponentVector.m_Values[3]));
#endif
}
//
WideVector FRAO_VECTORCALL vectorAbs(RegArgVector vector) noexcept
{
	// compute the bitwise AND of v and -v. ie: (v & -v), to remove
	// the sign bit
	return reinterpretToDouble(
		vectorAnd(reinterpretToInteger(vector),
				  reinterpretToInteger(vectorNegate(vector))));
}
WideVector FRAO_VECTORCALL vectorMax(RegArgVector lhsVec,
									  RegArgVector rhsVec) noexcept
{
#ifdef FRAO_AVX
	return _mm256_max_pd(lhsVec, rhsVec);
#else

	return WideVector(fmax(lhsVec.m_Values[0], rhsVec.m_Values[0]),
					  fmax(lhsVec.m_Values[1], rhsVec.m_Values[1]),
					  fmax(lhsVec.m_Values[2], rhsVec.m_Values[2]),
					  fmax(lhsVec.m_Values[3], rhsVec.m_Values[3]));
#endif
}
WideVector FRAO_VECTORCALL vectorMin(RegArgVector lhsVec,
									  RegArgVector rhsVec) noexcept
{
#ifdef FRAO_AVX
	return _mm256_min_pd(lhsVec, rhsVec);
#else

	return WideVector(fmin(lhsVec.m_Values[0], rhsVec.m_Values[0]),
					  fmin(lhsVec.m_Values[1], rhsVec.m_Values[1]),
					  fmin(lhsVec.m_Values[2], rhsVec.m_Values[2]),
					  fmin(lhsVec.m_Values[3], rhsVec.m_Values[3]));
#endif
}
}  // namespace Maths
}  // namespace frao