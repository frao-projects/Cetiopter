//! \file
//!
//! \brief Implementation of vector component-wise access/reordering
//! functions
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "ComponentAccess.hpp"
#include <Nucleus_inc\BitUtilities.hpp>	//for set/extract bits in non-avx mode
#include "ComponentMaths.hpp"			//for vectorAnd, etc
#include "Conversions.hpp"	   //for converting to/from int vectors
#include "SpecialVectors.hpp"  //for IMPL constexpr variables
#include "VectorTypes.hpp"	   //for complete vector types

namespace frao
{
namespace Maths
{
// vector getter /setters
float FRAO_VECTORCALL vectorGetByIndex(
	RegArgNrwVec vector, uint_least64_t elementIndex) noexcept
{
#ifdef FRAO_AVX
	const int_least64_t convIndex =
		static_cast<int_least64_t>(elementIndex & 0b11);

	return _mm_cvtss_f32(
		_mm_permutevar_ps(vector, _mm_cvtsi64_si128(convIndex)));
#else

	return vector.m_Values[elementIndex & 0b11];
#endif
}
int_least32_t FRAO_VECTORCALL vectorGetByIndex(
	RegIntNrwVec vector, uint_least64_t elementIndex) noexcept
{
#ifdef FRAO_AVX
	const int_least64_t convIndex =
		static_cast<int_least64_t>(elementIndex & 0b11);

	// there's not really any support for __m128i variable permutes,
	// so pretend the data is a float, and use the AVX float permute
	return _mm_cvtsi128_si32(_mm_castps_si128(_mm_permutevar_ps(
		_mm_castsi128_ps(vector), _mm_cvtsi64_si128(convIndex))));
#else

	return vector.m_Values[elementIndex & 0b11];
#endif
}
double FRAO_VECTORCALL vectorGetByIndex(
	RegArgVector vector, uint_least64_t elementIndex) noexcept
{
#ifdef FRAO_AVX
	int convIndex = static_cast<int>(elementIndex & 0b11);

	// determine how to select the element we want, in terms of which
	// half of the vector to select, and which quarter of the half to
	// select. For the quarter, we want bits [63:0] -> 1 iff we want
	// element 0 or 2, and bits [127: 64] -> 1 otherwise. For the
	// half, we set all bits to 1 if we want the upper half
	int_least64_t topQuarterChosen = -1LL * (convIndex & 0b1);
	__m128i		  quarterSelect	   = _mm_unpacklo_epi64(
				 _mm_cvtsi64_si128(topQuarterChosen),
				 _mm_cvtsi64_si128(topQuarterChosen ^ -1LL)),
			blendMask  = _mm_cvtsi64_si128(-1LL * (convIndex >> 1)),
			halfSelect = _mm_unpacklo_epi64(blendMask, blendMask);

	// Use permutevar to select 1st/2nd double in each half, and put
	// at position of 1st in each half. Then, get the topHalf, by
	// permuting it down with permute2f
	__m256d bottomHalf = _mm256_permutevar_pd(
				vector,
				_mm256_setr_m128i(quarterSelect, quarterSelect)),
			topHalf =
				_mm256_permute2f128_pd(bottomHalf, bottomHalf, 0x1);

	// with both halves, select the half needed with blendv, and
	// return the element we've put first
	return _mm256_cvtsd_f64(
		_mm256_blendv_pd(bottomHalf, topHalf,
						 _mm256_castsi256_pd(_mm256_setr_m128i(
							 halfSelect, halfSelect))));
#else

	return vector.m_Values[elementIndex & 0b11];
#endif
}
int_least64_t FRAO_VECTORCALL vectorGetByIndex(
	RegIntVector vector, uint_least64_t elementIndex) noexcept
{
#ifdef FRAO_AVX2
	// create a 256bit vector, with the lower two 32-bit elements as
	// (low-> high): [index*2, index*2 +1], which can be used to
	// select the required 64 bit element
	int_least64_t convIndex =
		2 * static_cast<int_least64_t>(elementIndex & 0b11);
	__m256i indices = _mm256_zextsi128_si256(
		_mm_cvtsi64_si128(convIndex + ((convIndex + 1) << 32)));

	return _mm_cvtsi128_si64(_mm256_castsi256_si128(
		_mm256_permutevar8x32_epi32(vector, indices)));
#else

	return vector.m_Values[elementIndex & 0b11];
#endif
}
NarrowVector FRAO_VECTORCALL
vectorSetByIndex(RegArgNrwVec vector, float number,
				 uint_least64_t elementIndex) noexcept
{
#ifdef FRAO_AVX
	// create a number with one of the 15th, 31st, 47th, or 63rd bits
	// set, depending on whether we want element 0, 1, 2, 3
	int_least64_t offset		 = 15 + ((elementIndex & 0b11) * 16),
				  preSelectorNum = 1LL << offset;

	// We convert the number to a vector with one of the 31st,
	// 63rd, 95th, or 127th bits set
	__m128i selector = _mm_unpacklo_epi16(
		_mm_setzero_si128(), _mm_cvtsi64_si128(preSelectorNum));

	// we cast the vector to the correct type, and it selects the
	// correct element to replace
	return _mm_blendv_ps(vector, _mm_broadcast_ss(&number),
						 _mm_castsi128_ps(selector));
#else

	NarrowVector result(vector);
	result.m_Values[elementIndex & 0b11] = number;

	return result;
#endif
}
NarrowIntVector FRAO_VECTORCALL
vectorSetByIndex(RegIntNrwVec vector, int_least32_t number,
				 uint_least64_t elementIndex) noexcept
{
#ifdef FRAO_AVX
	// create a number with one of the 15th, 31st, 47th, or 63rd bits
	// set, depending on whether we want element 0, 1, 2, 3
	int_least64_t offset		 = (elementIndex & 0b11) * 16,
				  preSelectorNum = 0xFFFFULL << offset;

	// In order to create a selector, to replace the correct index
	// number, we convert the preSelectorNum to a vector with the 32
	// bits up to one of the 31st, 63rd, 95th, or 127th bits set
	__m128i halfSelector = _mm_cvtsi64_si128(preSelectorNum),
			selector = _mm_unpacklo_epi16(halfSelector, halfSelector),
			numberToSet =
				_mm_shuffle_epi32(_mm_loadu_si32(&number), 0b0);

	// We replace the number at the specified index only
	return _mm_blendv_epi8(vector, numberToSet, selector);
#else

	NarrowIntVector result(vector);
	result.m_Values[elementIndex & 0b11] = number;

	return result;
#endif
}
WideVector FRAO_VECTORCALL
vectorSetByIndex(RegArgVector vector, double number,
				 uint_least64_t elementIndex) noexcept
{
#ifdef FRAO_AVX
	int_least64_t convIndex =
					  static_cast<int_least64_t>(elementIndex & 0b11),
				  halfIndex = (convIndex >> 1LL) & 0b1,
				  preMask	= 0xFFFF'FFFFLL
							<< (32LL * (convIndex & 0b1));

	// will set all bits to 1, for element we want
	__m128i lowerMask =
				_mm_cvtsi64_si128(preMask * (halfIndex ^ 0b1)),
			upperMask = _mm_cvtsi64_si128(preMask * halfIndex);
	__m256i mask =
		_mm256_set_m128i(_mm_unpacklo_epi32(upperMask, upperMask),
						 _mm_unpacklo_epi32(lowerMask, lowerMask));

	return _mm256_blendv_pd(vector, _mm256_broadcast_sd(&number),
							_mm256_castsi256_pd(mask));
#else

	WideVector result(vector);
	result.m_Values[elementIndex & 0b11] = number;

	return result;
#endif
}
WideIntVector FRAO_VECTORCALL
vectorSetByIndex(RegIntVector vector, int_least64_t number,
				 uint_least64_t elementIndex) noexcept
{
#ifdef FRAO_AVX2
	int_least64_t convIndex =
					  static_cast<int_least64_t>(elementIndex & 0b11),
				  halfIndex = (convIndex >> 1LL) & 0b1,
				  preMask	= 0xFFFF'FFFFLL
							<< (32LL * (convIndex & 0b1));

	// will set all bits to 1, for element we want
	__m128i lowerMask =
				_mm_cvtsi64_si128(preMask * (halfIndex ^ 0b1)),
			upperMask = _mm_cvtsi64_si128(preMask * halfIndex);
	__m256i mask =
		_mm256_set_m128i(_mm_unpacklo_epi32(upperMask, upperMask),
						 _mm_unpacklo_epi32(lowerMask, lowerMask));

	return _mm256_blendv_epi8(
		vector, _mm256_broadcastq_epi64(_mm_cvtsi64_si128(number)),
		mask);
#else

	WideIntVector result(vector);
	result.m_Values[elementIndex & 0b11] = number;

	return result;
#endif
}
//
float FRAO_VECTORCALL vectorGetX(RegArgNrwVec vector) noexcept
{
#ifdef FRAO_AVX
	return _mm_cvtss_f32(vector);
#else

	return vector.m_Values[0];
#endif
}
int_least32_t FRAO_VECTORCALL
vectorGetX(RegIntNrwVec vector) noexcept
{
#ifdef FRAO_AVX
	return _mm_cvtsi128_si32(vector);
#else

	return vector.m_Values[0];
#endif
}
double FRAO_VECTORCALL vectorGetX(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	return _mm256_cvtsd_f64(vector);
#else

	return vector.m_Values[0];
#endif
}
int_least64_t FRAO_VECTORCALL
vectorGetX(RegIntVector vector) noexcept
{
#ifdef FRAO_AVX2
	return _mm_cvtsi128_si64(_mm256_castsi256_si128(vector));
#else

	return vector.m_Values[0];
#endif
}
NarrowVector FRAO_VECTORCALL vectorSetX(RegArgNrwVec vector,
										 float		  number) noexcept
{
#ifdef FRAO_AVX
	return _mm_blend_ps(vector, _mm_broadcast_ss(&number), 0x1);
#else

	return NarrowVector(number, vector.m_Values[1],
						vector.m_Values[2], vector.m_Values[3]);
#endif
}
NarrowIntVector FRAO_VECTORCALL
vectorSetX(RegIntNrwVec vector, int_least32_t number) noexcept
{
#ifdef FRAO_AVX
	return _mm_insert_epi32(vector, number, 0b00);
#else

	return NarrowIntVector(number, vector.m_Values[1],
						   vector.m_Values[2], vector.m_Values[3]);
#endif
}
WideVector FRAO_VECTORCALL vectorSetX(RegArgVector vector,
									   double		number) noexcept
{
#ifdef FRAO_AVX
	return _mm256_blend_pd(vector, _mm256_broadcast_sd(&number), 0x1);
#else

	return WideVector(number, vector.m_Values[1], vector.m_Values[2],
					  vector.m_Values[3]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorSetX(RegIntVector vector, int_least64_t number) noexcept
{
#ifdef FRAO_AVX2
	// we have to select by 32 bit elements, bc there's no 64 bit
	// integer blend
	return _mm256_blend_epi32(
		vector, _mm256_zextsi128_si256(_mm_cvtsi64_si128(number)),
		0b11);
#else

	return WideIntVector(number, vector.m_Values[1],
						 vector.m_Values[2], vector.m_Values[3]);
#endif
}
//
float FRAO_VECTORCALL vectorGetY(RegArgNrwVec vector) noexcept
{
#ifdef FRAO_AVX
	return _mm_cvtss_f32(_mm_permute_ps(vector, 0x1));
#else

	return vector.m_Values[1];
#endif
}
int_least32_t FRAO_VECTORCALL
vectorGetY(RegIntNrwVec vector) noexcept
{
#ifdef FRAO_AVX
	return _mm_extract_epi32(vector, 0b01);
#else

	return vector.m_Values[1];
#endif
}
double FRAO_VECTORCALL vectorGetY(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX

	return _mm_cvtsd_f64(
		_mm_permute_pd(_mm256_extractf128_pd(vector, 0x0), 0x1));
#else

	return vector.m_Values[1];
#endif
}
int_least64_t FRAO_VECTORCALL
vectorGetY(RegIntVector vector) noexcept
{
#ifdef FRAO_AVX2
	return _mm_cvtsi128_si64(_mm256_castsi256_si128(
		_mm256_permute4x64_epi64(vector, 0x1)));
#else

	return vector.m_Values[1];
#endif
}
NarrowVector FRAO_VECTORCALL vectorSetY(RegArgNrwVec vector,
										 float		  number) noexcept
{
#ifdef FRAO_AVX
	return _mm_blend_ps(vector, _mm_broadcast_ss(&number), 0x2);
#else

	return NarrowVector(vector.m_Values[0], number,
						vector.m_Values[2], vector.m_Values[3]);
#endif
}
NarrowIntVector FRAO_VECTORCALL
vectorSetY(RegIntNrwVec vector, int_least32_t number) noexcept
{
#ifdef FRAO_AVX
	return _mm_insert_epi32(vector, number, 0b01);
#else

	return NarrowIntVector(vector.m_Values[0], number,
						   vector.m_Values[2], vector.m_Values[3]);
#endif
}
WideVector FRAO_VECTORCALL vectorSetY(RegArgVector vector,
									   double		number) noexcept
{
#ifdef FRAO_AVX
	return _mm256_blend_pd(vector, _mm256_broadcast_sd(&number), 0x2);
#else

	return WideVector(vector.m_Values[0], number, vector.m_Values[2],
					  vector.m_Values[3]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorSetY(RegIntVector vector, int_least64_t number) noexcept
{
#ifdef FRAO_AVX2
	// we have to select by 32 bit elements, bc there's no 64 bit
	// integer blend
	return _mm256_blend_epi32(
		vector, _mm256_broadcastq_epi64(_mm_cvtsi64_si128(number)),
		0b1100);
#else

	return WideIntVector(vector.m_Values[0], number,
						 vector.m_Values[2], vector.m_Values[3]);
#endif
}
//
float FRAO_VECTORCALL vectorGetZ(RegArgNrwVec vector) noexcept
{
#ifdef FRAO_AVX
	return _mm_cvtss_f32(_mm_permute_ps(vector, 0x2));
#else

	return vector.m_Values[2];
#endif
}
int_least32_t FRAO_VECTORCALL
vectorGetZ(RegIntNrwVec vector) noexcept
{
#ifdef FRAO_AVX
	return _mm_extract_epi32(vector, 0b10);
#else

	return vector.m_Values[2];
#endif
}
double FRAO_VECTORCALL vectorGetZ(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	return _mm_cvtsd_f64(
		_mm_permute_pd(_mm256_extractf128_pd(vector, 0x1), 0x0));
#else

	return vector.m_Values[2];
#endif
}
int_least64_t FRAO_VECTORCALL
vectorGetZ(RegIntVector vector) noexcept
{
#ifdef FRAO_AVX2
	return _mm_cvtsi128_si64(_mm256_castsi256_si128(
		_mm256_permute4x64_epi64(vector, 0x2)));
#else

	return vector.m_Values[2];
#endif
}
NarrowVector FRAO_VECTORCALL vectorSetZ(RegArgNrwVec vector,
										 float		  number) noexcept
{
#ifdef FRAO_AVX
	return _mm_blend_ps(vector, _mm_broadcast_ss(&number), 0x4);
#else

	return NarrowVector(vector.m_Values[0], vector.m_Values[1],
						number, vector.m_Values[3]);
#endif
}
NarrowIntVector FRAO_VECTORCALL
vectorSetZ(RegIntNrwVec vector, int_least32_t number) noexcept
{
#ifdef FRAO_AVX
	return _mm_insert_epi32(vector, number, 0b10);
#else

	return NarrowIntVector(vector.m_Values[0], vector.m_Values[1],
						   number, vector.m_Values[3]);
#endif
}
WideVector FRAO_VECTORCALL vectorSetZ(RegArgVector vector,
									   double		number) noexcept
{
#ifdef FRAO_AVX
	return _mm256_blend_pd(vector, _mm256_broadcast_sd(&number), 0x4);
#else

	return WideVector(vector.m_Values[0], vector.m_Values[1], number,
					  vector.m_Values[3]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorSetZ(RegIntVector vector, int_least64_t number) noexcept
{
#ifdef FRAO_AVX2
	// we have to select by 32 bit elements, bc there's no 64 bit
	// integer blend
	return _mm256_blend_epi32(
		vector, _mm256_broadcastq_epi64(_mm_cvtsi64_si128(number)),
		0b110000);
#else

	return WideIntVector(vector.m_Values[0], vector.m_Values[1],
						 number, vector.m_Values[3]);
#endif
}
//
float FRAO_VECTORCALL vectorGetW(RegArgNrwVec vector) noexcept
{
#ifdef FRAO_AVX
	return _mm_cvtss_f32(_mm_permute_ps(vector, 0x3));
#else

	return vector.m_Values[3];
#endif
}
int_least32_t FRAO_VECTORCALL
vectorGetW(RegIntNrwVec vector) noexcept
{
#ifdef FRAO_AVX
	return _mm_extract_epi32(vector, 0b11);
#else

	return vector.m_Values[3];
#endif
}
double FRAO_VECTORCALL vectorGetW(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	return _mm_cvtsd_f64(
		_mm_permute_pd(_mm256_extractf128_pd(vector, 0x1), 0x1));
#else

	return vector.m_Values[3];
#endif
}
int_least64_t FRAO_VECTORCALL
vectorGetW(RegIntVector vector) noexcept
{
#ifdef FRAO_AVX2
	return _mm_cvtsi128_si64(_mm256_castsi256_si128(
		_mm256_permute4x64_epi64(vector, 0x3)));
#else

	return vector.m_Values[3];
#endif
}
NarrowVector FRAO_VECTORCALL vectorSetW(RegArgNrwVec vector,
										 float		  number) noexcept
{
#ifdef FRAO_AVX
	return _mm_blend_ps(vector, _mm_broadcast_ss(&number), 0x8);
#else

	return NarrowVector(vector.m_Values[0], vector.m_Values[1],
						vector.m_Values[2], number);
#endif
}
NarrowIntVector FRAO_VECTORCALL
vectorSetW(RegIntNrwVec vector, int_least32_t number) noexcept
{
#ifdef FRAO_AVX
	return _mm_insert_epi32(vector, number, 0b11);
#else

	return NarrowIntVector(vector.m_Values[0], vector.m_Values[1],
						   vector.m_Values[2], number);
#endif
}
WideVector FRAO_VECTORCALL vectorSetW(RegArgVector vector,
									   double		number) noexcept
{
#ifdef FRAO_AVX
	return _mm256_blend_pd(vector, _mm256_broadcast_sd(&number), 0x8);
#else

	return WideVector(vector.m_Values[0], vector.m_Values[1],
					  vector.m_Values[2], number);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorSetW(RegIntVector vector, int_least64_t number) noexcept
{
#ifdef FRAO_AVX2
	// we have to select by 32 bit elements, bc there's no 64 bit
	// integer blend
	return _mm256_blend_epi32(
		vector, _mm256_broadcastq_epi64(_mm_cvtsi64_si128(number)),
		0b1100'0000);
#else

	return WideIntVector(vector.m_Values[0], vector.m_Values[1],
						 vector.m_Values[2], number);
#endif
}
//
NarrowVector FRAO_VECTORCALL vectorSet(float x, float y, float z,
										float w) noexcept
{
#ifdef FRAO_AVX
	NarrowVector xyVec = _mm_unpacklo_ps(_mm_broadcast_ss(&x),
										 _mm_broadcast_ss(&y)),
				 zwVec = _mm_unpacklo_ps(_mm_broadcast_ss(&z),
										 _mm_broadcast_ss(&w));

	return _mm_movelh_ps(xyVec, zwVec);
#else

	return NarrowVector(x, y, z, w);
#endif
}
NarrowIntVector FRAO_VECTORCALL vectorSet(int_least32_t x,
										   int_least32_t y,
										   int_least32_t z,
										   int_least32_t w) noexcept
{
#ifdef FRAO_AVX
	NarrowIntVector xyVec = _mm_unpacklo_epi32(_mm_loadu_si32(&x),
											   _mm_loadu_si32(&y)),
					zwVec = _mm_unpacklo_epi32(_mm_loadu_si32(&z),
											   _mm_loadu_si32(&w));

	return _mm_unpacklo_epi64(xyVec, zwVec);
#else

	return NarrowIntVector(x, y, z, w);
#endif
}
WideVector FRAO_VECTORCALL vectorSet(double x, double y, double z,
									  double w) noexcept
{
#ifdef FRAO_AVX
	WideVector xyVec = _mm256_unpacklo_pd(_mm256_broadcast_sd(&x),
										  _mm256_broadcast_sd(&y));
	__m128d zwVec = _mm_unpacklo_pd(_mm_load_sd(&z), _mm_load_sd(&w));

	return _mm256_insertf128_pd(xyVec, zwVec, 0x1);
#else

	return WideVector(x, y, z, w);
#endif
}
WideIntVector FRAO_VECTORCALL vectorSet(int_least64_t x,
										 int_least64_t y,
										 int_least64_t z,
										 int_least64_t w) noexcept
{
#ifdef FRAO_AVX2
	__m128i xyVec = _mm_unpacklo_epi64(_mm_cvtsi64_si128(x),
									   _mm_cvtsi64_si128(y)),
			zwVec = _mm_unpacklo_epi64(_mm_cvtsi64_si128(z),
									   _mm_cvtsi64_si128(w));

	return _mm256_inserti128_si256(_mm256_zextsi128_si256(xyVec),
								   zwVec, 0x1);
#else

	return WideIntVector(x, y, z, w);
#endif
}

// matrix getter /setters
WideVector FRAO_VECTORCALL
matrixGetRow(RegArgMatrix matrix, uint_least64_t index) noexcept
{
	// mask to prevent indices > 3
	return matrix.m_Tuples[index & 0b11];
}
WideMatrix FRAO_VECTORCALL
matrixSetRow(RegArgMatrix matrix, RegArgVector row,
			 uint_least64_t index) noexcept
{
	WideMatrix result(matrix);
	result.m_Tuples[index] = row;

	return result;
}
WideMatrix FRAO_VECTORCALL
matrixSetColumn(RegArgMatrix matrix, RegArgVector column,
				uint_least64_t index) noexcept
{
	return WideMatrix(vectorSetByIndex(matrix.m_Tuples[0],
									   vectorGetX(column), index),
					  vectorSetByIndex(matrix.m_Tuples[1],
									   vectorGetY(column), index),
					  vectorSetByIndex(matrix.m_Tuples[2],
									   vectorGetZ(column), index),
					  vectorSetByIndex(matrix.m_Tuples[3],
									   vectorGetW(column), index));
}

WideMatrix FRAO_VECTORCALL matrixSet(
	double r1_c1, double r1_c2, double r1_c3, double r1_c4,
	double r2_c1, double r2_c2, double r2_c3, double r2_c4,
	double r3_c1, double r3_c2, double r3_c3, double r3_c4,
	double r4_c1, double r4_c2, double r4_c3, double r4_c4) noexcept
{
	return WideMatrix(vectorSet(r1_c1, r1_c2, r1_c3, r1_c4),
					  vectorSet(r2_c1, r2_c2, r2_c3, r2_c4),
					  vectorSet(r3_c1, r3_c2, r3_c3, r3_c4),
					  vectorSet(r4_c1, r4_c2, r4_c3, r4_c4));
}
WideMatrix FRAO_VECTORCALL matrixSet(RegArgVector row1,
									  RegArgVector row2,
									  RegArgVector row3,
									  RegArgVector row4) noexcept
{
	return WideMatrix(row1, row2, row3, row4);
}
WideMatrix FRAO_VECTORCALL
matrixSetByColumn(RegArgVector column1, RegArgVector column2,
				  RegArgVector column3, RegArgVector column4) noexcept
{
	WideMatrix result = matrixSet(column1, column2, column3, column4);

	return matrixTranspose(result);
}

// vector replicate/splat functions
WideVector vectorReplicate(double value) noexcept
{
#ifdef FRAO_AVX
	return _mm256_broadcast_sd(&value);
#else

	return WideVector(value, value, value, value);
#endif
}
WideIntVector vectorReplicate(int_least64_t value) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_broadcastq_epi64(_mm_cvtsi64_si128(value));
#else

	return WideIntVector(value, value, value, value);
#endif
}
//
WideVector FRAO_VECTORCALL vectorSplatX(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	__m128d lanePermuted =
		_mm256_castpd256_pd128(_mm256_permute_pd(vector, 0b0));

	return _mm256_set_m128d(lanePermuted, lanePermuted);
#else

	return WideVector(vector.m_Values[0], vector.m_Values[0],
					  vector.m_Values[0], vector.m_Values[0]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorSplatX(RegIntVector vector) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_broadcastq_epi64(_mm256_castsi256_si128(vector));
#else

	return WideIntVector(vector.m_Values[0], vector.m_Values[0],
						 vector.m_Values[0], vector.m_Values[0]);
#endif
}
//
WideVector FRAO_VECTORCALL vectorSplatY(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	__m128d lanePermuted =
		_mm256_castpd256_pd128(_mm256_permute_pd(vector, 0b11));

	return _mm256_set_m128d(lanePermuted, lanePermuted);
#else

	return WideVector(vector.m_Values[1], vector.m_Values[1],
					  vector.m_Values[1], vector.m_Values[1]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorSplatY(RegIntVector vector) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_permute4x64_epi64(vector, 0b0101'0101);
#else

	return WideIntVector(vector.m_Values[1], vector.m_Values[1],
						 vector.m_Values[1], vector.m_Values[1]);
#endif
}
//
WideVector FRAO_VECTORCALL vectorSplatZ(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	__m128d lanePermuted =
		_mm256_extractf128_pd(_mm256_permute_pd(vector, 0b0), 0b1);

	return _mm256_set_m128d(lanePermuted, lanePermuted);
#else

	return WideVector(vector.m_Values[2], vector.m_Values[2],
					  vector.m_Values[2], vector.m_Values[2]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorSplatZ(RegIntVector vector) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_permute4x64_epi64(vector, 0b1010'1010);
#else

	return WideIntVector(vector.m_Values[2], vector.m_Values[2],
						 vector.m_Values[2], vector.m_Values[2]);
#endif
}
//
WideVector FRAO_VECTORCALL vectorSplatW(RegArgVector vector) noexcept
{
#ifdef FRAO_AVX
	__m128d lanePermuted =
		_mm256_extractf128_pd(_mm256_permute_pd(vector, 0b1111), 0b1);

	return _mm256_set_m128d(lanePermuted, lanePermuted);
#else

	return WideVector(vector.m_Values[3], vector.m_Values[3],
					  vector.m_Values[3], vector.m_Values[3]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorSplatW(RegIntVector vector) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_permute4x64_epi64(vector, 0b1111'1111);
#else

	return WideIntVector(vector.m_Values[3], vector.m_Values[3],
						 vector.m_Values[3], vector.m_Values[3]);
#endif
}


// permutation functions
NarrowIntVector FRAO_VECTORCALL
vectorSelect(RegIntNrwVec lhsVector, RegIntNrwVec rhsVector,
			 RegIntNrwVec control) noexcept
{
#ifdef FRAO_AVX
	return _mm_blendv_epi8(lhsVector, rhsVector, control);
#else

	// if a bit in control is set, take the bit from rhs. If not, take
	// the bit from lhs. That is, lhsPart = lhs & ~control; rhsPart =
	// rhs & control
	NarrowIntVector lhsPart(vectorAnd(lhsVector, vectorNot(control))),
		rhsPart(vectorAnd(rhsVector, control));

	// combine the individual selection halves into a whole. That is,
	// result = lhsPart | rhsPart. Or, in detail, result = (lhs &
	// ~control) | (rhs & control)
	return vectorOr(lhsPart, rhsPart);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorSelect(RegIntVector lhsVector, RegIntVector rhsVector,
			 RegIntVector control) noexcept
{
#ifdef FRAO_AVX2
	return _mm256_blendv_epi8(lhsVector, rhsVector, control);
#else

	// if a bit in control is set, take the bit from rhs. If not, take
	// the bit from lhs. That is, lhsPart = lhs & ~control; rhsPart =
	// rhs & control
	WideIntVector lhsPart(vectorAnd(lhsVector, vectorNot(control))),
		rhsPart(vectorAnd(rhsVector, control));

	// combine the individual selection halves into a whole. That is,
	// result = lhsPart | rhsPart. Or, in detail, result = (lhs &
	// ~control) | (rhs & control)
	return vectorOr(lhsPart, rhsPart);
#endif
}
WideVector FRAO_VECTORCALL
vectorSelect(RegArgVector lhsVector, RegArgVector rhsVector,
			 RegIntVector control) noexcept
{
#ifdef FRAO_AVX
	return _mm256_blendv_pd(lhsVector, rhsVector,
							reinterpretToDouble(control));
#else

	return reinterpretToDouble(
		vectorSelect(reinterpretToInteger(lhsVector),
					 reinterpretToInteger(rhsVector), control));
#endif
}
WideIntVector vectorSelectControl(bool useXRhs, bool useYRhs,
								  bool useZRhs, bool useWRhs) noexcept
{
#ifdef FRAO_AVX2
	WideIntVector result =
		vectorSet(static_cast<int_least64_t>(useXRhs),
				  static_cast<int_least64_t>(useYRhs),
				  static_cast<int_least64_t>(useZRhs),
				  static_cast<int_least64_t>(useWRhs));

	return vectorEqual(result, vectorSplatOneInt());
#else

	constexpr int_least64_t selection[2] = {0, IMPL::g_TrueBits};

	return WideIntVector(
		selection[static_cast<int_least32_t>(useXRhs)],
		selection[static_cast<int_least32_t>(useYRhs)],
		selection[static_cast<int_least32_t>(useZRhs)],
		selection[static_cast<int_least32_t>(useWRhs)]);
#endif
}

WideVector FRAO_VECTORCALL
vectorSwizzle(RegArgVector vector, RegIntNrwVec choices) noexcept
{
#ifdef FRAO_AVX
	static const NarrowIntVector s_OneSplat = vectorSet(1, 1, 1, 1),
								 s_TrueVec =
									 vectorSet(-1, -1, -1, -1),
								 s_XorVec = vectorSet(0, 0, 1, 1);

	NarrowIntVector bit1Choices = _mm_mullo_epi32(
						s_TrueVec,
						_mm_and_si128(s_OneSplat, choices)),
					bit2Choices = _mm_mullo_epi32(
						s_TrueVec,
						_mm_xor_si128(
							s_XorVec,
							_mm_and_si128(
								s_OneSplat,
								_mm_srli_epi32(choices, 0b1))));

	// Set all bits of a given element 1 iff the corresponding choice
	// element: quarterChoices = {x[0], y[0], z[0], w[0]} AND
	// laneChoices = {x[1], y[1], NOT z[1], NOT w[1]}
	// is non-zero
	__m256i quarterChoices = _mm256_set_m128i(
				_mm_unpackhi_epi32(bit1Choices, bit1Choices),
				_mm_unpacklo_epi32(bit1Choices, bit1Choices)),
			laneChoices = _mm256_set_m128i(
				_mm_unpackhi_epi32(bit2Choices, bit2Choices),
				_mm_unpacklo_epi32(bit2Choices, bit2Choices));

	// get a high/low swapped vector, so that we can select, for each
	// lane, the two high/low candidates for the final position, based
	// on the low bit of the input index
	WideVector vecConjugate =
				   _mm256_permute2f128_pd(vector, vector, 0b1),
			   laneCandidate1 =
				   _mm256_permutevar_pd(vector, quarterChoices),
			   laneCandidate2 =
				   _mm256_permutevar_pd(vecConjugate, quarterChoices);

	// then, select between the lane candidates, based on whether the
	// 2nd lowest bit of the index is set
	return _mm256_blendv_pd(laneCandidate1, laneCandidate2,
							_mm256_castsi256_pd(laneChoices));
#else

	return WideVector(
		vector.m_Values[ExtractBits(choices.m_Values[0], 2, 0)],
		vector.m_Values[ExtractBits(choices.m_Values[1], 2, 0)],
		vector.m_Values[ExtractBits(choices.m_Values[2], 2, 0)],
		vector.m_Values[ExtractBits(choices.m_Values[3], 2, 0)]);
#endif
}
WideVector FRAO_VECTORCALL vectorSwizzle(
	RegArgVector vector, uint_least8_t xChoice, uint_least8_t yChoice,
	uint_least8_t zChoice, uint_least8_t wChoice) noexcept
{
#ifdef FRAO_AVX
	return vectorSwizzle(
		vector,
		vectorSet((int_least32_t) xChoice, (int_least32_t) yChoice,
				  (int_least32_t) zChoice, (int_least32_t) wChoice));
#else

	return WideVector(vector.m_Values[ExtractBits(xChoice, 2, 0)],
					  vector.m_Values[ExtractBits(yChoice, 2, 0)],
					  vector.m_Values[ExtractBits(zChoice, 2, 0)],
					  vector.m_Values[ExtractBits(wChoice, 2, 0)]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorSwizzle(RegIntVector vector, RegIntNrwVec choices) noexcept
{
#ifdef FRAO_AVX2
	static const NarrowIntVector s_Mask = _mm_broadcastd_epi32(
									 _mm_cvtsi32_si128(0b11)),
								 s_Offset = vectorSet(0, 1, 0, 1);

	// prepare half of each 256-bit choice vector, in each of
	// xyChoices and zwChoices. Shift is instead of a 16x16->32 bit
	// multiply, by 2
	NarrowIntVector maskedChoices = _mm_and_si128(choices, s_Mask),
					doubleChoices = _mm_slli_epi32(maskedChoices, 1),
					xyChoices	  = _mm_unpacklo_epi32(doubleChoices,
												   doubleChoices),
					zwChoices	  = _mm_unpackhi_epi32(doubleChoices,
												   doubleChoices);

	// finish half preparations
	xyChoices = _mm_add_epi32(xyChoices, s_Offset);
	zwChoices = _mm_add_epi32(zwChoices, s_Offset);

	// permutevar8x32 with [index, index + 1] in [lo, hi] parts of
	// each 64 bit idx element
	return _mm256_permutevar8x32_epi32(
		vector, _mm256_set_m128i(zwChoices, xyChoices));
#else

	return WideIntVector(
		vector.m_Values[ExtractBits(vectorGetX(choices), 2, 0)],
		vector.m_Values[ExtractBits(vectorGetY(choices), 2, 0)],
		vector.m_Values[ExtractBits(vectorGetZ(choices), 2, 0)],
		vector.m_Values[ExtractBits(vectorGetW(choices), 2, 0)]);
#endif
}
WideIntVector FRAO_VECTORCALL vectorSwizzle(
	RegIntVector vector, uint_least8_t xChoice, uint_least8_t yChoice,
	uint_least8_t zChoice, uint_least8_t wChoice) noexcept
{
#ifdef FRAO_AVX2
	return vectorSwizzle(
		vector,
		vectorSet((int_least32_t) xChoice, (int_least32_t) yChoice,
				  (int_least32_t) zChoice, (int_least32_t) wChoice));
#else

	return WideIntVector(vector.m_Values[ExtractBits(xChoice, 2, 0)],
						 vector.m_Values[ExtractBits(yChoice, 2, 0)],
						 vector.m_Values[ExtractBits(zChoice, 2, 0)],
						 vector.m_Values[ExtractBits(wChoice, 2, 0)]);
#endif
}
WideVector FRAO_VECTORCALL
vectorPermute(RegArgVector lhsVector, RegArgVector rhsVector,
			  RegIntNrwVec choices) noexcept
{
#ifdef FRAO_AVX
	static const NarrowIntVector s_OneSplat = vectorSet(1, 1, 1, 1),
								 s_TrueVec =
									 vectorSet(-1, -1, -1, -1),
								 s_XorVec = vectorSet(0, 0, 1, 1);

	// turn choices {x, y}, {z, w} into masks (0 or all 1s) of:
	// {x[0], y[0], z[0], w[0]} and
	// {x[1], y[1], NOT z[1], NOT w[1]} and
	// {x[2], y[2], z[2], w[2]}
	NarrowIntVector
		bit1Choices = _mm_mullo_epi32(
			s_TrueVec, _mm_and_si128(s_OneSplat, choices)),
		bit2Choices = _mm_mullo_epi32(
			s_TrueVec,
			_mm_xor_si128(s_XorVec,
						  _mm_and_si128(s_OneSplat,
										_mm_srli_epi32(choices, 1)))),
		bit3Choices = _mm_mullo_epi32(
			s_TrueVec,
			_mm_and_si128(s_OneSplat, _mm_srli_epi32(choices, 2)));

	// Set all bits of a given element 1 iff the corresponding choice
	// element: quarterChoices = {x[0], y[0], z[0], w[0]} AND
	// laneChoices = {x[1], y[1], NOT z[1], NOT w[1]}
	// is non-zero
	__m256i quarterChoices = _mm256_set_m128i(
				_mm_unpackhi_epi32(bit1Choices, bit1Choices),
				_mm_unpacklo_epi32(bit1Choices, bit1Choices)),
			laneChoices = _mm256_set_m128i(
				_mm_unpackhi_epi32(bit2Choices, bit2Choices),
				_mm_unpacklo_epi32(bit2Choices, bit2Choices)),
			vecChoices = _mm256_set_m128i(
				_mm_unpackhi_epi32(bit3Choices, bit3Choices),
				_mm_unpacklo_epi32(bit3Choices, bit3Choices));

	// get a high/low swapped vector, for both left and right hand
	// side, so that we can select, for each lane and vector, the two
	// high/low candidates for the final position, based on the low
	// bit of the input index
	WideVector lhsConjugate =
				   _mm256_permute2f128_pd(lhsVector, lhsVector, 0b1),
			   rhsConjugate =
				   _mm256_permute2f128_pd(rhsVector, rhsVector, 0b1),
			   lhsLaneCand1 =
				   _mm256_permutevar_pd(lhsVector, quarterChoices),
			   rhsLaneCand1 =
				   _mm256_permutevar_pd(rhsVector, quarterChoices),
			   lhsLaneCand2 =
				   _mm256_permutevar_pd(lhsConjugate, quarterChoices),
			   rhsLaneCand2 =
				   _mm256_permutevar_pd(rhsConjugate, quarterChoices),
			   lhsSwizzle =
				   _mm256_blendv_pd(lhsLaneCand1, lhsLaneCand2,
									_mm256_castsi256_pd(laneChoices)),
			   rhsSwizzle =
				   _mm256_blendv_pd(rhsLaneCand1, rhsLaneCand2,
									_mm256_castsi256_pd(laneChoices));

	// once we have the candidates for each swizzled vector, we can
	// pick between them, for the final permute
	return _mm256_blendv_pd(lhsSwizzle, rhsSwizzle,
							_mm256_castsi256_pd(vecChoices));
#else
	const double* proxyArray[2] = {lhsVector.m_Values,
								   rhsVector.m_Values};

	return WideVector(
		proxyArray[ExtractBits(choices.m_Values[0], 1, 2)]
				  [ExtractBits(choices.m_Values[0], 2, 0)],
		proxyArray[ExtractBits(choices.m_Values[1], 1, 2)]
				  [ExtractBits(choices.m_Values[1], 2, 0)],
		proxyArray[ExtractBits(choices.m_Values[2], 1, 2)]
				  [ExtractBits(choices.m_Values[2], 2, 0)],
		proxyArray[ExtractBits(choices.m_Values[3], 1, 2)]
				  [ExtractBits(choices.m_Values[3], 2, 0)]);
#endif
}
WideVector FRAO_VECTORCALL
vectorPermute(RegArgVector lhsVector, RegArgVector rhsVector,
			  uint_least8_t xChoice, uint_least8_t yChoice,
			  uint_least8_t zChoice, uint_least8_t wChoice) noexcept
{
#ifdef FRAO_AVX
	return vectorPermute(
		lhsVector, rhsVector,
		vectorSet((int_least32_t) xChoice, (int_least32_t) yChoice,
				  (int_least32_t) zChoice, (int_least32_t) wChoice));
#else

	const double* proxyArray[2] = {lhsVector.m_Values,
								   rhsVector.m_Values};

	return WideVector(proxyArray[ExtractBits(xChoice, 1, 2)]
								[ExtractBits(xChoice, 2, 0)],
					  proxyArray[ExtractBits(yChoice, 1, 2)]
								[ExtractBits(yChoice, 2, 0)],
					  proxyArray[ExtractBits(zChoice, 1, 2)]
								[ExtractBits(zChoice, 2, 0)],
					  proxyArray[ExtractBits(wChoice, 1, 2)]
								[ExtractBits(wChoice, 2, 0)]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorPermute(RegIntVector lhsVector, RegIntVector rhsVector,
			  RegIntNrwVec choices) noexcept
{
#ifdef FRAO_AVX2
	static const WideIntVector s_OneSplat = _mm256_broadcastd_epi32(
								   _mm_cvtsi32_si128(1)),
							   s_TrueSplat = vectorSplatAllBits(),
							   s_ElemMask  = _mm256_broadcastd_epi32(
									_mm_cvtsi32_si128(0b11)),
							   s_AltOnes =
								   _mm256_slli_epi64(s_OneSplat, 32);

	// need vectors of the form:
	// elem permute: {2*x_elem, 2*x_elem + 1, 2*y_elem, 2*y_elem +
	// 1,...}
	// and
	// vec blend: {x_vec*-1LL, y_vec*-1LL, z_vec*-1LL, w_vec*-1LL}
	__m256i choices64	  = toWideIntVector(choices),
			repeatChoices = _mm256_or_si256(
				choices64, _mm256_slli_epi64(choices64, 32)),
			elemChoices = _mm256_add_epi32(
				s_AltOnes,
				_mm256_slli_epi32(
					_mm256_and_si256(s_ElemMask, repeatChoices), 1)),
			vecChoiceMask = _mm256_mullo_epi32(
				s_TrueSplat,
				_mm256_and_si256(
					s_OneSplat, _mm256_srli_epi32(repeatChoices, 2)));

	// permutevar8x32 with [elemindex, elemindex + 1] in [lo, hi]
	// parts of each 64 bit idx element, to keep low and high parts of
	// 64-bit elements together
	__m256i lhsChoices =
				_mm256_permutevar8x32_epi32(lhsVector, elemChoices),
			rhsChoices =
				_mm256_permutevar8x32_epi32(rhsVector, elemChoices);

	// pick between the presented lhs and rhs options , based on
	// whether a rhs or lhs element was wanted
	return _mm256_blendv_epi8(lhsChoices, rhsChoices, vecChoiceMask);
#else

	const int_least64_t* proxyArray[2] = {lhsVector.m_Values,
										  rhsVector.m_Values};

	return WideIntVector(
		proxyArray[ExtractBits(vectorGetX(choices), 1, 2)]
				  [ExtractBits(vectorGetX(choices), 2, 0)],
		proxyArray[ExtractBits(vectorGetY(choices), 1, 2)]
				  [ExtractBits(vectorGetY(choices), 2, 0)],
		proxyArray[ExtractBits(vectorGetZ(choices), 1, 2)]
				  [ExtractBits(vectorGetZ(choices), 2, 0)],
		proxyArray[ExtractBits(vectorGetW(choices), 1, 2)]
				  [ExtractBits(vectorGetW(choices), 2, 0)]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorPermute(RegIntVector lhsVector, RegIntVector rhsVector,
			  uint_least8_t xChoice, uint_least8_t yChoice,
			  uint_least8_t zChoice, uint_least8_t wChoice) noexcept
{
#ifdef FRAO_AVX2
	return vectorPermute(
		lhsVector, rhsVector,
		vectorSet((int_least32_t) xChoice, (int_least32_t) yChoice,
				  (int_least32_t) zChoice, (int_least32_t) wChoice));
#else

	const int_least64_t* proxyArray[2] = {lhsVector.m_Values,
										  rhsVector.m_Values};

	return WideIntVector(proxyArray[ExtractBits(xChoice, 1, 2)]
								   [ExtractBits(xChoice, 2, 0)],
						 proxyArray[ExtractBits(yChoice, 1, 2)]
								   [ExtractBits(yChoice, 2, 0)],
						 proxyArray[ExtractBits(zChoice, 1, 2)]
								   [ExtractBits(zChoice, 2, 0)],
						 proxyArray[ExtractBits(wChoice, 1, 2)]
								   [ExtractBits(wChoice, 2, 0)]);
#endif
}
WideVector FRAO_VECTORCALL vectorRotateLeft(
	RegArgVector vector, uint_least8_t numElements) noexcept
{
#ifdef FRAO_AVX
	// given that a left rotation is equivalent to a swizzle by
	// (original index + num) % 4, this what we effectively do
	return vectorSwizzle(
		vector, (numElements) &0b11, (1 + numElements) & 0b11,
		(2 + numElements) & 0b11, (3 + numElements) & 0b11);
#else

	switch (ExtractBits(numElements, 2, 0))
	{
		case 3:
			return WideVector(vector.m_Values[3], vector.m_Values[0],
							  vector.m_Values[1], vector.m_Values[2]);
		case 2:
			return WideVector(vector.m_Values[2], vector.m_Values[3],
							  vector.m_Values[0], vector.m_Values[1]);
		case 1:
			return WideVector(vector.m_Values[1], vector.m_Values[2],
							  vector.m_Values[3], vector.m_Values[0]);
		default:
			return vector;
	}
#endif
}
WideIntVector FRAO_VECTORCALL vectorRotateLeft(
	RegIntVector vector, uint_least8_t numElements) noexcept
{
#ifdef FRAO_AVX2
	// given that a left rotation is equivalent to a swizzle by
	// (original index + num) % 4, this what we effectively do
	return vectorSwizzle(
		vector, (numElements) &0b11, (1 + numElements) & 0b11,
		(2 + numElements) & 0b11, (3 + numElements) & 0b11);
#else

	switch (ExtractBits(numElements, 2, 0))
	{
		case 3:
			return WideIntVector(
				vector.m_Values[3], vector.m_Values[0],
				vector.m_Values[1], vector.m_Values[2]);
		case 2:
			return WideIntVector(
				vector.m_Values[2], vector.m_Values[3],
				vector.m_Values[0], vector.m_Values[1]);
		case 1:
			return WideIntVector(
				vector.m_Values[1], vector.m_Values[2],
				vector.m_Values[3], vector.m_Values[0]);
		default:
			return vector;
	}
#endif
}
WideVector FRAO_VECTORCALL vectorRotateRight(
	RegArgVector vector, uint_least8_t numElements) noexcept
{
#ifdef FRAO_AVX
	// given that a right rotation is equivalent to a swizzle by
	// (original index - num) % 4, this what we effectively do
	return vectorSwizzle(
		vector, (-numElements) & 0b11, (1 - numElements) & 0b11,
		(2 - numElements) & 0b11, (3 - numElements) & 0b11);
#else

	switch (ExtractBits(numElements, 2, 0))
	{
		case 3:
			return WideVector(vector.m_Values[1], vector.m_Values[2],
							  vector.m_Values[3], vector.m_Values[0]);
		case 2:
			return WideVector(vector.m_Values[2], vector.m_Values[3],
							  vector.m_Values[0], vector.m_Values[1]);
		case 1:
			return WideVector(vector.m_Values[3], vector.m_Values[0],
							  vector.m_Values[1], vector.m_Values[2]);
		default:
			return vector;
	}
#endif
}
WideIntVector FRAO_VECTORCALL vectorRotateRight(
	RegIntVector vector, uint_least8_t numElements) noexcept
{
#ifdef FRAO_AVX2
	// given that a right rotation is equivalent to a swizzle by
	// (original index - num) % 4, this what we effectively do
	return vectorSwizzle(
		vector, (-numElements) & 0b11, (1 - numElements) & 0b11,
		(2 - numElements) & 0b11, (3 - numElements) & 0b11);
#else

	switch (ExtractBits(numElements, 2, 0))
	{
		case 3:
			return WideIntVector(
				vector.m_Values[1], vector.m_Values[2],
				vector.m_Values[3], vector.m_Values[0]);
		case 2:
			return WideIntVector(
				vector.m_Values[2], vector.m_Values[3],
				vector.m_Values[0], vector.m_Values[1]);
		case 1:
			return WideIntVector(
				vector.m_Values[3], vector.m_Values[0],
				vector.m_Values[1], vector.m_Values[2]);
		default:
			return vector;
	}
#endif
}

WideVector FRAO_VECTORCALL
vectorMergeXY(RegArgVector lhsVector, RegArgVector rhsVector) noexcept
{
#ifdef FRAO_AVX
	__m128d lowLhs = _mm256_castpd256_pd128(lhsVector),
			lowRhs = _mm256_castpd256_pd128(rhsVector),
			xMerge = _mm_unpacklo_pd(lowLhs, lowRhs),
			yMerge = _mm_unpackhi_pd(lowLhs, lowRhs);

	return _mm256_set_m128d(yMerge, xMerge);
#else

	return WideVector(lhsVector.m_Values[0], rhsVector.m_Values[0],
					  lhsVector.m_Values[1], rhsVector.m_Values[1]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorMergeXY(RegIntVector lhsVector, RegIntVector rhsVector) noexcept
{
#ifdef FRAO_AVX2
	__m128i lowLhs = _mm256_castsi256_si128(lhsVector),
			lowRhs = _mm256_castsi256_si128(rhsVector),
			xMerge = _mm_unpacklo_epi64(lowLhs, lowRhs),
			yMerge = _mm_unpackhi_epi64(lowLhs, lowRhs);

	return _mm256_set_m128i(yMerge, xMerge);
#else

	return WideIntVector(lhsVector.m_Values[0], rhsVector.m_Values[0],
						 lhsVector.m_Values[1],
						 rhsVector.m_Values[1]);
#endif
}
WideVector FRAO_VECTORCALL
vectorMergeZW(RegArgVector lhsVector, RegArgVector rhsVector) noexcept
{
#ifdef FRAO_AVX
	__m128d highLhs = _mm256_extractf128_pd(lhsVector, 0b1),
			highRhs = _mm256_extractf128_pd(rhsVector, 0b1),
			zMerge	= _mm_unpacklo_pd(highLhs, highRhs),
			wMerge	= _mm_unpackhi_pd(highLhs, highRhs);

	return _mm256_set_m128d(wMerge, zMerge);
#else

	return WideVector(lhsVector.m_Values[2], rhsVector.m_Values[2],
					  lhsVector.m_Values[3], rhsVector.m_Values[3]);
#endif
}
WideIntVector FRAO_VECTORCALL
vectorMergeZW(RegIntVector lhsVector, RegIntVector rhsVector) noexcept
{
#ifdef FRAO_AVX2
	__m128i highLhs = _mm256_extracti128_si256(lhsVector, 0b1),
			highRhs = _mm256_extracti128_si256(rhsVector, 0b1),
			zMerge	= _mm_unpacklo_epi64(highLhs, highRhs),
			wMerge	= _mm_unpackhi_epi64(highLhs, highRhs);

	return _mm256_set_m128i(wMerge, zMerge);
#else

	return WideIntVector(lhsVector.m_Values[2], rhsVector.m_Values[2],
						 lhsVector.m_Values[3],
						 rhsVector.m_Values[3]);
#endif
}
}  // namespace Maths
}  // namespace frao