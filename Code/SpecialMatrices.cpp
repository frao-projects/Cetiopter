//! \file
//!
//! \brief Header of special matrices, such as the identity and
//! perspective matrices
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "SpecialMatrices.hpp"
#include <Nucleus_inc\Environment.hpp>	//for EMIT_WARNING
#include "ComponentAccess.hpp"			//for getter/setters
#include "ComponentMaths.hpp"			//for vectorSin etc
#include "DimensionalMaths.hpp"			//for vector3DNormalised
#include "SpecialVectors.hpp"			//for vectorZero
#include "VectorTypes.hpp"				//for complete vector types

namespace frao
{
namespace Maths
{
WideMatrix matrixIdentity() noexcept
{
	return matrixSet(1., 0., 0., 0., 0., 1., 0., 0., 0., 0., 1., 0.,
					 0., 0., 0., 1.);
}

// matrix creation functions
WideMatrix FRAO_VECTORCALL
matrix2DTranslation(RegArgVector move) noexcept
{
	return matrixSet(1., 0., 0., vectorGetX(move), 0., 1., 0.,
					 vectorGetY(move), 0., 0., 1., 0., 0., 0., 0.,
					 1.);
}
WideMatrix FRAO_VECTORCALL matrix2DTranslation(double xMove,
												double yMove) noexcept
{
	return matrixSet(1., 0., 0., xMove, 0., 1., 0., yMove, 0., 0., 1.,
					 0., 0., 0., 0., 1.);
}
WideMatrix FRAO_VECTORCALL
matrix3DTranslation(RegArgVector move) noexcept
{
	return matrixSet(1., 0., 0., vectorGetX(move), 0., 1., 0.,
					 vectorGetY(move), 0., 0., 1., vectorGetZ(move),
					 0., 0., 0., 1.);
}
WideMatrix FRAO_VECTORCALL matrix3DTranslation(double xMove,
												double yMove,
												double zMove) noexcept
{
	return matrixSet(1., 0., 0., xMove, 0., 1., 0., yMove, 0., 0., 1.,
					 zMove, 0., 0., 0., 1.);
}
//
WideMatrix FRAO_VECTORCALL
matrix2DScaling(RegArgVector scaling) noexcept
{
	WideMatrix result = matrixIdentity();

	result.m_Tuples[0] = vectorMultiply(result.m_Tuples[0], scaling);
	result.m_Tuples[1] = vectorMultiply(result.m_Tuples[1], scaling);

	return result;
}
WideMatrix FRAO_VECTORCALL matrix2DScaling(double xScaling,
											double yScaling) noexcept
{
	return matrixSet(xScaling, 0., 0., 0., 0., yScaling, 0., 0., 0.,
					 0., 1., 0., 0., 0., 0., 1.);
}
WideMatrix FRAO_VECTORCALL
matrix3DScaling(RegArgVector scaling) noexcept
{
	WideMatrix result = matrixIdentity();

	result.m_Tuples[0] = vectorMultiply(result.m_Tuples[0], scaling);
	result.m_Tuples[1] = vectorMultiply(result.m_Tuples[1], scaling);
	result.m_Tuples[2] = vectorMultiply(result.m_Tuples[2], scaling);

	return result;
}
WideMatrix FRAO_VECTORCALL matrix3DScaling(double xScaling,
											double yScaling,
											double zScaling) noexcept
{
	return matrixSet(xScaling, 0., 0., 0., 0., yScaling, 0., 0., 0.,
					 0., zScaling, 0., 0., 0., 0., 1.);
}
//
WideMatrix matrix2DRotationRH(double angle) noexcept
{
	double sinAngle, cosAngle;
	IMPL::scalarSinCos(sinAngle, cosAngle, angle);

	return matrixSet(cosAngle, -sinAngle, 0., 0., sinAngle, cosAngle,
					 0., 0., 0., 0., 1., 0., 0., 0., 0., 1.);
}
WideMatrix matrix2DRotationLH(double angle) noexcept
{
	return matrix2DRotationRH(-angle);
}
WideMatrix matrix3DRotationYawRH(double angle) noexcept
{
	double sinAngle, cosAngle;
	IMPL::scalarSinCos(sinAngle, cosAngle, angle);

	return matrixSet(cosAngle, -sinAngle, 0., 0., sinAngle, cosAngle,
					 0., 0., 0., 0., 1., 0., 0., 0., 0., 1.);
}
WideMatrix matrix3DRotationYawLH(double angle) noexcept
{
	return matrix3DRotationYawRH(-angle);
}
WideMatrix matrix3DRotationPitchRH(double angle) noexcept
{
	double sinAngle, cosAngle;
	IMPL::scalarSinCos(sinAngle, cosAngle, angle);

	return matrixSet(1., 0., 0., 0., 0., cosAngle, -sinAngle, 0., 0.,
					 sinAngle, cosAngle, 0., 0., 0., 0., 1.);
}
WideMatrix matrix3DRotationPitchLH(double angle) noexcept
{
	return matrix3DRotationPitchRH(-angle);
}
WideMatrix matrix3DRotationRollRH(double angle) noexcept
{
	double sinAngle, cosAngle;
	IMPL::scalarSinCos(sinAngle, cosAngle, angle);

	return matrixSet(cosAngle, 0., sinAngle, 0., 0., 1., 0., 0.,
					 -sinAngle, 0., cosAngle, 0., 0., 0., 0., 1.);
}
WideMatrix matrix3DRotationRollLH(double angle) noexcept
{
	return matrix3DRotationRollRH(-angle);
}
//
WideMatrix matrix3DRotationRH(double pitch, double roll,
							  double yaw) noexcept
{
	return matrix3DRotationRHFromVector(
		vectorSet(pitch, roll, yaw, 0.));
}
WideMatrix FRAO_VECTORCALL
matrix3DRotationRHFromVector(RegArgVector rotAngles) noexcept
{
	// have angles in form, as denoted in notes, of
	// ([B]eta, [G]amma, [A]lpha, ?)
	//
	// we then acquire sin_v = (sin(B), sin(G), sin(A), 0), by
	// applying sin (and permuting), and cos_v = (cos(B), cos(G),
	// cos(A), 1) by applying cos (and permuting)
	WideVector sinAngles	= vectorSin(rotAngles),
			   cosAngles	= vectorCos(rotAngles),
			   negSinAngles = vectorNegate(sinAngles),
			   // Then, we get the following through one (1) permute
			   // of those two (three technically) vectors:
			   //
			   // v0=(sin A, cos A, 1, 1?) = permute<2, 6, 7,
			   // 7>(sin_v, cos_v),
			   //
			   // v1=(cos A, -sin A, 0, 0) = permute<6, 2, 3,
			   // 3>(negate(sin_v), cos_v),
			   //
			   // v2=(sin B, sin B, cos B, 0) = permute<0, 0, 4,
			   // 3>(sin_v, cos_v),
			   //
			   // v3=(cos B, cos B, -sin B, 0) = permute<4, 4, 0,
			   // 3>(negate(sin_v), cos_v)
		componentVectors[4] =
			{vectorPermute(sinAngles, cosAngles, 2, 6, 7, 7),
			 vectorPermute(negSinAngles, cosAngles, 6, 2, 3, 3),
			 vectorPermute(sinAngles, cosAngles, 0, 0, 4, 3),
			 vectorPermute(negSinAngles, cosAngles, 4, 4, 0, 3)},
			   // pre-compute v0*v2
		compoundComponent =
			vectorMultiply(componentVectors[0], componentVectors[2]);

	// Then compute:
	// row[0] = sin G*v0*v2 + cos G*v1
	// row[1] = v3*v0
	// row[2] = cos G*v0*v2 - sin G*v1
	// row[3] = Identity_Last_Row
	WideMatrix result  = matrixIdentity();
	result.m_Tuples[0] = vectorAddition(
		vectorMultiply(vectorSplatY(sinAngles), compoundComponent),
		vectorMultiply(vectorSplatY(cosAngles), componentVectors[1]));
	result.m_Tuples[1] =
		vectorMultiply(componentVectors[0], componentVectors[3]);
	result.m_Tuples[2] = vectorSubtract(
		vectorMultiply(vectorSplatY(cosAngles), compoundComponent),
		vectorMultiply(vectorSplatY(sinAngles), componentVectors[1]));

	return result;
}
WideMatrix matrix3DRotationLH(double pitch, double roll,
							  double yaw) noexcept
{
	return matrix3DRotationLHFromVector(
		vectorSet(pitch, roll, yaw, 0.));
}
WideMatrix FRAO_VECTORCALL
matrix3DRotationLHFromVector(RegArgVector rotAngles) noexcept
{
	// have angles in form, as denoted in notes, of
	// ([B]eta, [G]amma, [A]lpha, ?)
	//
	// we then acquire sin_v = (sin(B), sin(G), sin(A), 0), by
	// applying sin (and permuting), and cos_v = (cos(B), cos(G),
	// cos(A), 1) by applying cos (and permuting)
	WideVector sinAngles = vectorPermute(vectorSin(rotAngles),
										 vectorZero(), 0, 1, 2, 4),
			   cosAngles =
				   vectorPermute(vectorCos(rotAngles),
								 vectorSplatOne(), 0, 1, 2, 4),
			   negSinAngles = vectorNegate(sinAngles),
			   // Then, we get the following through one (1) permute
			   // of those two (three technically) vectors:
			   //
			   // v0=(sin B, cos B, sin B, 0) = permute<0, 4, 0,
			   // 3>(sin_v, cos_v),
			   //
			   // v1=(cos G, 0, -sin G, 0) = permute<5, 3, 1,
			   // 3>(negate(sin_v), cos_v),
			   //
			   // v2=(sin G, 1, cos G, 1?) = permute<1, 7, 5,
			   // 7>(sin_v, cos_v),
			   //
			   // v3=(cos B, -sin B, cos B, 0) = permute<4, 0, 4,
			   // 3>(negate(sin_v), cos_v)
		componentVectors[4] =
			{vectorPermute(sinAngles, cosAngles, 0, 4, 0, 3),
			 vectorPermute(negSinAngles, cosAngles, 5, 3, 1, 3),
			 vectorPermute(sinAngles, cosAngles, 1, 7, 5, 7),
			 vectorPermute(negSinAngles, cosAngles, 4, 0, 4, 3)},
			   // pre-compute v0*v2
		compoundComponent =
			vectorMultiply(componentVectors[0], componentVectors[2]);

	// Then compute:
	// row[0] = sin A*v0*v2 + cos A*v1
	// row[1] = cos A*v0*v2 - sin A*v1
	// row[2] = v3*v2
	// row[3] = Identity_Last_Row
	WideMatrix result  = matrixIdentity();
	result.m_Tuples[0] = vectorAddition(
		vectorMultiply(vectorSplatZ(sinAngles), compoundComponent),
		vectorMultiply(vectorSplatZ(cosAngles), componentVectors[1]));
	result.m_Tuples[1] = vectorSubtract(
		vectorMultiply(vectorSplatZ(cosAngles), compoundComponent),
		vectorMultiply(vectorSplatZ(sinAngles), componentVectors[1]));
	result.m_Tuples[2] =
		vectorMultiply(componentVectors[2], componentVectors[3]);

	return result;
}
//
namespace IMPL
{
// passing the only element (the skew symmetric matrix, which is right
// handed in the normal configuration, and whose transpose is used for
// the left handed calculation) that is left or right handed about the
// calulation, as well as a normalised axis of the form (x, y, z, 0),
// calculation a rotation matrix about the given axis
WideMatrix calculateCommonRotationAboutAxis(
	RegArgMatrix skewSymmetric, WideVector normalisedAxis,
	double angle) noexcept
{
	// 1a. completed in the caller
	// b. Calculate sin and cos of angle, and 1-cos
	double sinAngle, cosAngle, oneMinusCosAngle;
	IMPL::scalarSinCos(sinAngle, cosAngle, angle);
	oneMinusCosAngle = 1.0 - cosAngle;

	// c. calculate axis*axis^T (outer product) matrix
	WideMatrix outerProduct = matrixSet(
		vectorMultiply(normalisedAxis, vectorSplatX(normalisedAxis)),
		vectorMultiply(normalisedAxis, vectorSplatY(normalisedAxis)),
		vectorMultiply(normalisedAxis, vectorSplatZ(normalisedAxis)),
		vectorZero());

	// 2a. Calculate cos * identity matrix, for 1st of final sum
	// matrices. last row will be unaltered from 4D identity matrix
	WideMatrix finalSum[3];
	finalSum[0] = matrix3DScaling(vectorReplicate(cosAngle));

	// 2b. multiply outer product matrix by 1-cos, for 2nd of final
	// product matrices
	finalSum[1] = matrixScale(outerProduct, oneMinusCosAngle);

	// 2c. multiply sin by skew symmetric matrix, for 3rd of final sum
	// matrices
	finalSum[2] = matrixScale(skewSymmetric, sinAngle);

	// 3. add final sum matrices, return result;
	return matrixAddition(finalSum[0],
						  matrixAddition(finalSum[1], finalSum[2]));
}
}  // namespace IMPL
//
WideMatrix matrix3DRotationRHAboutAxis(double axisX, double axisY,
									   double axisZ,
									   double angle) noexcept
{
	return matrix3DRotationRHAboutAxis(
		vectorSet(axisX, axisY, axisZ, 0.), angle);
}
WideMatrix FRAO_VECTORCALL
matrix3DRotationRHAboutAxis(RegArgVector axis, double angle) noexcept
{
	// preparation: ensure we have a normalised axis of the form (x,
	// y, z, 0)
	WideVector usedAxis = vectorPermute(vector3DNormalised(axis),
										vectorZero(), 0, 1, 2, 4);

	// 1a. Calculate right-handed skew-symmetric matrix
	WideMatrix skewSymmetric = matrixSet(
		vectorPermute(usedAxis, vectorNegate(usedAxis), 3, 6, 1, 3),
		vectorPermute(usedAxis, vectorNegate(usedAxis), 2, 3, 4, 3),
		vectorPermute(usedAxis, vectorNegate(usedAxis), 5, 0, 3, 3),
		vectorZero());

	// 1b->3, calculated in the IMPL function. return the result,
	// which is the right-handed rotation matrix desired
	return IMPL::calculateCommonRotationAboutAxis(skewSymmetric,
												  usedAxis, angle);
}
WideMatrix matrix3DRotationLHAboutAxis(double axisX, double axisY,
									   double axisZ,
									   double angle) noexcept
{
	return matrix3DRotationLHAboutAxis(
		vectorSet(axisX, axisY, axisZ, 0.), angle);
}
WideMatrix FRAO_VECTORCALL
matrix3DRotationLHAboutAxis(RegArgVector axis, double angle) noexcept
{
	// preparation: ensure we have a normalised axis of the form (x,
	// y, z, 0)
	WideVector usedAxis = vectorPermute(vector3DNormalised(axis),
										vectorZero(), 0, 1, 2, 4);

	// 1a. Calculate left-handed skew-symmetric matrix
	WideMatrix skewSymmetric = matrixSet(
		vectorPermute(usedAxis, vectorNegate(usedAxis), 3, 2, 5, 3),
		vectorPermute(usedAxis, vectorNegate(usedAxis), 6, 3, 0, 3),
		vectorPermute(usedAxis, vectorNegate(usedAxis), 1, 4, 3, 3),
		vectorZero());

	// 1b->3, calculated in the IMPL function. return the result,
	// which is the left-handed rotation matrix desired
	return IMPL::calculateCommonRotationAboutAxis(skewSymmetric,
												  usedAxis, angle);
}
//
WideMatrix matrix3DPerspectiveCameraRH(double viewWidth,
									   double viewHeight,
									   double viewNear,
									   double viewFar,
									   bool	  negateZaxis) noexcept
{
	double doubleNear	 = 2.0 * viewNear,
		   distanceRatio = viewFar / (viewFar - viewNear);

	// clang-format off
	return matrixSet(
		doubleNear / viewWidth,		0.,		0.,		0.,
		0.,		0., 	doubleNear / viewHeight,	0.,
		0.,		(negateZaxis ? -distanceRatio : distanceRatio),
											0.,		-viewNear * distanceRatio,
		0.,		(negateZaxis ? -1. : 1.),	0.,		0.);
	// clang-format on
}
WideMatrix matrix3DPerspectiveFOVCameraRH(double verticalFOV,
										  double aspectRatio,
										  double viewNear,
										  double viewFar,
										  bool	 negateZaxis) noexcept
{
	double cosFOV, sinFOV;
	IMPL::scalarSinCos(sinFOV, cosFOV, verticalFOV);

	double cotFOV		 = cosFOV / sinFOV,
		   distanceRatio = viewFar / (viewFar - viewNear);

	// clang-format off
	return matrixSet(
		cotFOV / aspectRatio,	0.,				0.,			0.,
		0.,			0.,							cotFOV,		0.,
		0.,			(negateZaxis ? -distanceRatio : distanceRatio),
												0.,			-viewNear * distanceRatio,
		0.,			(negateZaxis ? -1. : 1.),	0.,			0.);
	// clang-format on
}
WideMatrix matrix3DOrthographicCameraRH(double viewWidth,
										double viewHeight,
										double viewNear,
										double viewFar,
										bool   negateZaxis) noexcept
{
	double distanceRatio = 1. / (viewFar - viewNear);

	// clang-format off
	return matrixSet(
		2. / viewWidth,	0.,				0.,					0.,
		0.,				0., 			2. / viewHeight,	0.,
		0.,				(negateZaxis ? -distanceRatio : distanceRatio),
										0.,					-viewNear * distanceRatio,
		0.,				0.,				0.,					1.);
	// clang-format on
}
WideMatrix matrix3DPerspectiveCameraLH(double viewWidth,
									   double viewHeight,
									   double viewNear,
									   double viewFar,
									   bool	  negateZaxis) noexcept
{
	double doubleNear	 = 2.0 * viewNear,
		   distanceRatio = viewFar / (viewFar - viewNear);

	// clang-format off
	return matrixSet(
		doubleNear / viewWidth,		0.,		0.,		0.,
		0.,		doubleNear / viewHeight,	0.,		0.,
		0.,		0.,		(negateZaxis ? -distanceRatio : distanceRatio),
													-viewNear * distanceRatio,
		0.,		0.,		(negateZaxis ? -1. : 1.),	0.);
	// clang-format on
}
WideMatrix matrix3DPerspectiveFOVCameraLH(double verticalFOV,
										  double aspectRatio,
										  double viewNear,
										  double viewFar,
										  bool	 negateZaxis) noexcept
{
	double cosFOV, sinFOV;
	IMPL::scalarSinCos(sinFOV, cosFOV, verticalFOV);

	double cotFOV		 = cosFOV / sinFOV,
		   distanceRatio = viewFar / (viewFar - viewNear);

	// clang-format off
	return matrixSet(
		cotFOV / aspectRatio,	0.,		0.,		0.,
		0.,			cotFOV,		0.,				0.,
		0.,			0.,			(negateZaxis ? -distanceRatio : distanceRatio),
												-viewNear * distanceRatio,
		0.,			0.,			(negateZaxis ? -1. : 1.),
												0.);
	// clang-format on
}
WideMatrix matrix3DOrthographicCameraLH(double viewWidth,
										double viewHeight,
										double viewNear,
										double viewFar,
										bool   negateZaxis) noexcept
{
	double distanceRatio = 1. / (viewFar - viewNear);

	// clang-format off
	return matrixSet(
		2. / viewWidth,	0.,				0.,			0.,
		0.,				2. / viewHeight,	0.,		0.,
		0.,				0.,				(negateZaxis ? -distanceRatio : distanceRatio),
													-viewNear * distanceRatio,
		0.,				0.,				0.,			1.);
	// clang-format on
}
}  // namespace Maths
}  // namespace frao