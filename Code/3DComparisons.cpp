//! \file
//!
//! \brief Implementation of 3D comparisons between, and on, vectors
//! and matrices
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "3DComparisons.hpp"
#include <Nucleus_inc\BitUtilities.hpp>	//for set/extract bits in non-avx mode
#include "ComponentComparison.hpp"
#include "SpecialVectors.hpp"  //for IMPL constexpr variables
#include "VectorTypes.hpp"	   //for complete vector types

namespace frao
{
namespace Maths
{
namespace IMPL
{
uint_least64_t create3DMask(uint_least64_t xRecord,
							uint_least64_t yRecord,
							uint_least64_t zRecord) noexcept
{
	uint_least64_t lowRecord =
					   frao::SetBits(xRecord * 0xFFFF,
									 yRecord * 0xFFFF, 0xFFFF, 16),
				   highRecord =
					   frao::SetBits(zRecord * 0xFFFF,
									 zRecord * 0xFFFF, 0xFFFF, 16);

	return frao::SetBits(lowRecord & 0xFFFF'FFFF, highRecord,
						 0xFFFFFFFF, 32);
}
uint_least64_t FRAO_VECTORCALL
conv3DRecord(RegIntVector source) noexcept
{
#if defined(FRAO_AVX2)
	uint_least64_t maskBits = static_cast<uint_least64_t>(
					   _mm256_movemask_epi8(source)),
				   components[3] = {
					   frao::ExtractBits(maskBits, 1, 0),
					   frao::ExtractBits(maskBits, 1, 8),
					   frao::ExtractBits(maskBits, 1, 16)};
#else
	uint_least64_t components[3] = {
		reinterpret_cast<const uint_least64_t*>(source.m_Values)[0]
			& 0b1,
		reinterpret_cast<const uint_least64_t*>(source.m_Values)[1]
			& 0b1,
		reinterpret_cast<const uint_least64_t*>(source.m_Values)[2]
			& 0b1};
#endif

	return create3DMask(components[0], components[1], components[2]);
}

bool FRAO_VECTORCALL
result3DAllTrue(RegIntVector fullResult) noexcept
{
#ifdef FRAO_AVX2
	int_least32_t mask = _mm256_movemask_epi8(fullResult);

	// eg: all bits up to z coord are set
	return (mask & 0xFF'FFFF) == 0xFF'FFFF;
#else
	return (fullResult.m_Values[0] & fullResult.m_Values[1]
			& fullResult.m_Values[2])
		   == IMPL::g_TrueBits;
#endif
}
bool FRAO_VECTORCALL
result3DAnyTrue(RegIntVector fullResult) noexcept
{
#ifdef FRAO_AVX2
	int_least32_t mask = _mm256_movemask_epi8(fullResult);

	// eg: any bits up to z coord are set
	return (mask & 0xFF'FFFF) != 0;
#else
	return (fullResult.m_Values[0] | fullResult.m_Values[1]
			| fullResult.m_Values[2])
		   == IMPL::g_TrueBits;
#endif
}
}  // namespace IMPL

// vector 3D comparisons
bool FRAO_VECTORCALL vector3DLess(RegArgVector lhsVec,
								   RegArgVector rhsVec) noexcept
{
	return IMPL::result3DAllTrue(vectorLess(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector3DLess(RegIntVector lhsVec,
								   RegIntVector rhsVec) noexcept
{
	return IMPL::result3DAllTrue(vectorLess(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector3DLessRecord(uint_least64_t& record,
										 RegArgVector	 lhsVec,
										 RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorLess(lhsVec, rhsVec);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAllTrue(fullResult);
}
bool FRAO_VECTORCALL vector3DLessRecord(uint_least64_t& record,
										 RegIntVector	 lhsVec,
										 RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorLess(lhsVec, rhsVec);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector3DLessEqual(RegArgVector lhsVec,
										RegArgVector rhsVec) noexcept
{
	return IMPL::result3DAllTrue(vectorLessEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector3DLessEqual(RegIntVector lhsVec,
										RegIntVector rhsVec) noexcept
{
	return IMPL::result3DAllTrue(vectorLessEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL
vector3DLessEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
						RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorLessEqual(lhsVec, rhsVec);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAllTrue(fullResult);
}
bool FRAO_VECTORCALL
vector3DLessEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
						RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorLessEqual(lhsVec, rhsVec);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector3DGreater(RegArgVector lhsVec,
									  RegArgVector rhsVec) noexcept
{
	return IMPL::result3DAllTrue(vectorGreater(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector3DGreater(RegIntVector lhsVec,
									  RegIntVector rhsVec) noexcept
{
	return IMPL::result3DAllTrue(vectorGreater(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL
vector3DGreaterRecord(uint_least64_t& record, RegArgVector lhsVec,
					  RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorGreater(lhsVec, rhsVec);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAllTrue(fullResult);
}
bool FRAO_VECTORCALL
vector3DGreaterRecord(uint_least64_t& record, RegIntVector lhsVec,
					  RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorGreater(lhsVec, rhsVec);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector3DGreaterEqual(
	RegArgVector lhsVec, RegArgVector rhsVec) noexcept
{
	return IMPL::result3DAllTrue(vectorGreaterEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector3DGreaterEqual(
	RegIntVector lhsVec, RegIntVector rhsVec) noexcept
{
	return IMPL::result3DAllTrue(vectorGreaterEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector3DGreaterEqualRecord(
	uint_least64_t& record, RegArgVector lhsVec,
	RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorGreaterEqual(lhsVec, rhsVec);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAllTrue(fullResult);
}
bool FRAO_VECTORCALL vector3DGreaterEqualRecord(
	uint_least64_t& record, RegIntVector lhsVec,
	RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorGreaterEqual(lhsVec, rhsVec);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector3DEqual(RegArgVector lhsVec,
									RegArgVector rhsVec) noexcept
{
	return IMPL::result3DAllTrue(vectorEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector3DEqual(RegIntVector lhsVec,
									RegIntVector rhsVec) noexcept
{
	return IMPL::result3DAllTrue(vectorEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL
vector3DEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorEqual(lhsVec, rhsVec);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAllTrue(fullResult);
}
bool FRAO_VECTORCALL
vector3DEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorEqual(lhsVec, rhsVec);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector3DNotEqual(RegArgVector lhsVec,
									   RegArgVector rhsVec) noexcept
{
	return IMPL::result3DAnyTrue(vectorNotEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector3DNotEqual(RegIntVector lhsVec,
									   RegIntVector rhsVec) noexcept
{
	return IMPL::result3DAnyTrue(vectorNotEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL
vector3DNotEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					   RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorNotEqual(lhsVec, rhsVec);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAnyTrue(fullResult);
}
bool FRAO_VECTORCALL
vector3DNotEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					   RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorNotEqual(lhsVec, rhsVec);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAnyTrue(fullResult);
}
//
bool FRAO_VECTORCALL
vector3DInBounds(RegArgVector checked, RegArgVector lowerBound,
				 RegArgVector upperBound) noexcept
{
	return IMPL::result3DAllTrue(
		vectorInBounds(checked, lowerBound, upperBound));
}
bool FRAO_VECTORCALL
vector3DInBounds(RegIntVector checked, RegIntVector lowerBound,
				 RegIntVector upperBound) noexcept
{
	return IMPL::result3DAllTrue(
		vectorInBounds(checked, lowerBound, upperBound));
}
bool FRAO_VECTORCALL vector3DInBoundsRecord(
	uint_least64_t& record, RegArgVector checked,
	RegArgVector lowerBound, RegArgVector upperBound) noexcept
{
	WideIntVector fullResult =
		vectorInBounds(checked, lowerBound, upperBound);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAllTrue(fullResult);
}
bool FRAO_VECTORCALL vector3DInBoundsRecord(
	uint_least64_t& record, RegIntVector checked,
	RegIntVector lowerBound, RegIntVector upperBound) noexcept
{
	WideIntVector fullResult =
		vectorInBounds(checked, lowerBound, upperBound);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector3DIsNaN(RegArgVector vector) noexcept
{
	return IMPL::result3DAnyTrue(vectorIsNaN(vector));
}
bool FRAO_VECTORCALL vector3DIsNaNRecord(
	uint_least64_t& record, RegArgVector vector) noexcept
{
	WideIntVector fullResult = vectorIsNaN(vector);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAnyTrue(fullResult);
}
bool FRAO_VECTORCALL vector3DIsInf(RegArgVector vector) noexcept
{
	return IMPL::result3DAnyTrue(vectorIsInf(vector));
}
bool FRAO_VECTORCALL vector3DIsInfRecord(
	uint_least64_t& record, RegArgVector vector) noexcept
{
	WideIntVector fullResult = vectorIsInf(vector);

	record = IMPL::conv3DRecord(fullResult);
	return IMPL::result3DAnyTrue(fullResult);
}

// 3D matrix comparison functions
bool FRAO_VECTORCALL matrix3DEqual(RegArgMatrix  lhsMatrix,
									LateArgMatrix rhsMatrix) noexcept
{
	// two matrices are equal iff all of their elements are, and hence
	// if all of their rows are
	return (
		vector3DEqual(lhsMatrix.m_Tuples[0], rhsMatrix.m_Tuples[0])
		& vector3DEqual(lhsMatrix.m_Tuples[1], rhsMatrix.m_Tuples[1])
		& vector3DEqual(lhsMatrix.m_Tuples[2],
						rhsMatrix.m_Tuples[2]));
}
bool FRAO_VECTORCALL matrix3DNotEqual(
	RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept
{
	// two matrices are not equal iff any of their elements are, and
	// hence if any of their rows are
	return (
		vector3DNotEqual(lhsMatrix.m_Tuples[0], rhsMatrix.m_Tuples[0])
		| vector3DNotEqual(lhsMatrix.m_Tuples[1],
						   rhsMatrix.m_Tuples[1])
		| vector3DNotEqual(lhsMatrix.m_Tuples[2],
						   rhsMatrix.m_Tuples[2]));
}
//
bool FRAO_VECTORCALL matrix3DIsNaN(RegArgMatrix matrix) noexcept
{
	// a matrix is NaN iff any of its elements are, and hence if any
	// of its rows are
	return (vector3DIsNaN(matrix.m_Tuples[0])
			| vector3DIsNaN(matrix.m_Tuples[1])
			| vector3DIsNaN(matrix.m_Tuples[2]));
}
bool FRAO_VECTORCALL matrix3DIsInf(RegArgMatrix matrix) noexcept
{
	// a matrix is Infinite iff any of its elements are, and hence if
	// any of its rows are
	return (vector3DIsInf(matrix.m_Tuples[0])
			| vector3DIsInf(matrix.m_Tuples[1])
			| vector3DIsInf(matrix.m_Tuples[2]));
}
}  // namespace Maths
}  // namespace frao