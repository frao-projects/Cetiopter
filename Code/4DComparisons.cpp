//! \file
//!
//! \brief Implementation of 4D comparisons between, and on, vectors
//! and matrices
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "4DComparisons.hpp"
#include <Nucleus_inc\Environment.hpp>	//for EMIT_WARNING
#include "ComponentComparison.hpp"
#include "SpecialVectors.hpp"  //for IMPL constexpr variables
#include "VectorTypes.hpp"	   //for complete vector types

namespace frao
{
namespace Maths
{
namespace IMPL
{
bool FRAO_VECTORCALL
result4DAllTrue(RegIntVector fullResult) noexcept
{
#ifdef FRAO_AVX2
	int_least32_t mask = _mm256_movemask_epi8(fullResult);

	// eg: all bits are set
	return mask == -1;
#else
	return (fullResult.m_Values[0] & fullResult.m_Values[1]
			& fullResult.m_Values[2] & fullResult.m_Values[3])
		   == IMPL::g_TrueBits;
#endif
}
bool FRAO_VECTORCALL
result4DAnyTrue(RegIntVector fullResult) noexcept
{
#ifdef FRAO_AVX2
	int_least32_t mask = _mm256_movemask_epi8(fullResult);

	// eg: any bits are set
	return mask != 0;
#else
	return (fullResult.m_Values[0] | fullResult.m_Values[1]
			| fullResult.m_Values[2] | fullResult.m_Values[3])
		   == IMPL::g_TrueBits;
#endif
}
}  // namespace IMPL

// vector 4D comparisons
bool FRAO_VECTORCALL vector4DLess(RegArgVector lhsVec,
								   RegArgVector rhsVec) noexcept
{
	return IMPL::result4DAllTrue(vectorLess(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector4DLess(RegIntVector lhsVec,
								   RegIntVector rhsVec) noexcept
{
	return IMPL::result4DAllTrue(vectorLess(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector4DLessRecord(uint_least64_t& record,
										 RegArgVector	 lhsVec,
										 RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorLess(lhsVec, rhsVec);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAllTrue(fullResult);
}
bool FRAO_VECTORCALL vector4DLessRecord(uint_least64_t& record,
										 RegIntVector	 lhsVec,
										 RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorLess(lhsVec, rhsVec);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector4DLessEqual(RegArgVector lhsVec,
										RegArgVector rhsVec) noexcept
{
	return IMPL::result4DAllTrue(vectorLessEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector4DLessEqual(RegIntVector lhsVec,
										RegIntVector rhsVec) noexcept
{
	return IMPL::result4DAllTrue(vectorLessEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL
vector4DLessEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
						RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorLessEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAllTrue(fullResult);
}
bool FRAO_VECTORCALL
vector4DLessEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
						RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorLessEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector4DGreater(RegArgVector lhsVec,
									  RegArgVector rhsVec) noexcept
{
	return IMPL::result4DAllTrue(vectorGreater(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector4DGreater(RegIntVector lhsVec,
									  RegIntVector rhsVec) noexcept
{
	return IMPL::result4DAllTrue(vectorGreater(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL
vector4DGreaterRecord(uint_least64_t& record, RegArgVector lhsVec,
					  RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorGreater(lhsVec, rhsVec);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAllTrue(fullResult);
}
bool FRAO_VECTORCALL
vector4DGreaterRecord(uint_least64_t& record, RegIntVector lhsVec,
					  RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorGreater(lhsVec, rhsVec);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector4DGreaterEqual(
	RegArgVector lhsVec, RegArgVector rhsVec) noexcept
{
	return IMPL::result4DAllTrue(vectorGreaterEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector4DGreaterEqual(
	RegIntVector lhsVec, RegIntVector rhsVec) noexcept
{
	return IMPL::result4DAllTrue(vectorGreaterEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector4DGreaterEqualRecord(
	uint_least64_t& record, RegArgVector lhsVec,
	RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorGreaterEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAllTrue(fullResult);
}
bool FRAO_VECTORCALL vector4DGreaterEqualRecord(
	uint_least64_t& record, RegIntVector lhsVec,
	RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorGreaterEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector4DEqual(RegArgVector lhsVec,
									RegArgVector rhsVec) noexcept
{
	return IMPL::result4DAllTrue(vectorEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector4DEqual(RegIntVector lhsVec,
									RegIntVector rhsVec) noexcept
{
	return IMPL::result4DAllTrue(vectorEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL
vector4DEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAllTrue(fullResult);
}
bool FRAO_VECTORCALL
vector4DEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector4DNotEqual(RegArgVector lhsVec,
									   RegArgVector rhsVec) noexcept
{
	return IMPL::result4DAnyTrue(vectorNotEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL vector4DNotEqual(RegIntVector lhsVec,
									   RegIntVector rhsVec) noexcept
{
	return IMPL::result4DAnyTrue(vectorNotEqual(lhsVec, rhsVec));
}
bool FRAO_VECTORCALL
vector4DNotEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					   RegArgVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorNotEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAnyTrue(fullResult);
}
bool FRAO_VECTORCALL
vector4DNotEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					   RegIntVector rhsVec) noexcept
{
	WideIntVector fullResult = vectorNotEqual(lhsVec, rhsVec);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAnyTrue(fullResult);
}
//
bool FRAO_VECTORCALL
vector4DInBounds(RegArgVector checked, RegArgVector lowerBound,
				 RegArgVector upperBound) noexcept
{
	return IMPL::result4DAllTrue(
		vectorInBounds(checked, lowerBound, upperBound));
}
bool FRAO_VECTORCALL
vector4DInBounds(RegIntVector checked, RegIntVector lowerBound,
				 RegIntVector upperBound) noexcept
{
	return IMPL::result4DAllTrue(
		vectorInBounds(checked, lowerBound, upperBound));
}
bool FRAO_VECTORCALL vector4DInBoundsRecord(
	uint_least64_t& record, RegArgVector checked,
	RegArgVector lowerBound, RegArgVector upperBound) noexcept
{
	WideIntVector fullResult =
		vectorInBounds(checked, lowerBound, upperBound);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAllTrue(fullResult);
}
bool FRAO_VECTORCALL vector4DInBoundsRecord(
	uint_least64_t& record, RegIntVector checked,
	RegIntVector lowerBound, RegIntVector upperBound) noexcept
{
	WideIntVector fullResult =
		vectorInBounds(checked, lowerBound, upperBound);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAllTrue(fullResult);
}
//
bool FRAO_VECTORCALL vector4DIsNaN(RegArgVector vector) noexcept
{
	return IMPL::result4DAnyTrue(vectorIsNaN(vector));
}
bool FRAO_VECTORCALL vector4DIsNaNRecord(
	uint_least64_t& record, RegArgVector vector) noexcept
{
	WideIntVector fullResult = vectorIsNaN(vector);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAnyTrue(fullResult);
}
bool FRAO_VECTORCALL vector4DIsInf(RegArgVector vector) noexcept
{
	return IMPL::result4DAnyTrue(vectorIsInf(vector));
}
bool FRAO_VECTORCALL vector4DIsInfRecord(
	uint_least64_t& record, RegArgVector vector) noexcept
{
	WideIntVector fullResult = vectorIsInf(vector);

	record = IMPL::convRecord(fullResult);
	return IMPL::result4DAnyTrue(fullResult);
}

// 4D matrix comparison functions
bool FRAO_VECTORCALL matrix4DEqual(RegArgMatrix  lhsMatrix,
									LateArgMatrix rhsMatrix) noexcept
{
	// two matrices are equal iff all of their elements are, and hence
	// if all of their rows are
	return (
		vector4DEqual(lhsMatrix.m_Tuples[0], rhsMatrix.m_Tuples[0])
		& vector4DEqual(lhsMatrix.m_Tuples[1], rhsMatrix.m_Tuples[1])
		& vector4DEqual(lhsMatrix.m_Tuples[2], rhsMatrix.m_Tuples[2])
		& vector4DEqual(lhsMatrix.m_Tuples[3],
						rhsMatrix.m_Tuples[3]));
}
bool FRAO_VECTORCALL matrix4DNotEqual(
	RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept
{
	// two matrices are not equal iff any of their elements are, and
	// hence if any of their rows are
	return (
		vector4DNotEqual(lhsMatrix.m_Tuples[0], rhsMatrix.m_Tuples[0])
		| vector4DNotEqual(lhsMatrix.m_Tuples[1],
						   rhsMatrix.m_Tuples[1])
		| vector4DNotEqual(lhsMatrix.m_Tuples[2],
						   rhsMatrix.m_Tuples[2])
		| vector4DNotEqual(lhsMatrix.m_Tuples[3],
						   rhsMatrix.m_Tuples[3]));
}
//
bool FRAO_VECTORCALL matrix4DIsNaN(RegArgMatrix matrix) noexcept
{
	// a matrix is NaN iff any of its elements are, and hence if any
	// of its rows are
	return (vector4DIsNaN(matrix.m_Tuples[0])
			| vector4DIsNaN(matrix.m_Tuples[1])
			| vector4DIsNaN(matrix.m_Tuples[2])
			| vector4DIsNaN(matrix.m_Tuples[3]));
}
bool FRAO_VECTORCALL matrix4DIsInf(RegArgMatrix matrix) noexcept
{
	// a matrix is Infinite iff any of its elements are, and hence if
	// any of its rows are
	return (vector4DIsInf(matrix.m_Tuples[0])
			| vector4DIsInf(matrix.m_Tuples[1])
			| vector4DIsInf(matrix.m_Tuples[2])
			| vector4DIsInf(matrix.m_Tuples[3]));
}
}  // namespace Maths
}  // namespace frao