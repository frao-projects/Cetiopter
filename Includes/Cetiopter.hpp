#ifndef FRAO_CETIOPTER_TOPINCL
#define FRAO_CETIOPTER_TOPINCL

//! \file
//!
//! \brief Header of entire Cetiopter api
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "APIDefines.hpp"
#include "2DComparisons.hpp"
#include "3DComparisons.hpp"
#include "4DComparisons.hpp"
#include "ComponentAccess.hpp"
#include "ComponentComparison.hpp"
#include "ComponentMaths.hpp"
#include "Conversions.hpp"
#include "DataStorageTypes.hpp"
#include "DimensionalMaths.hpp"
#include "SpecialMatrices.hpp"
#include "SpecialVectors.hpp"
#include "VectorTypes.hpp"
#include "Version/VersionIncl.hpp"

#endif