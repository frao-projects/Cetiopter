#ifndef FRAO_CETIOPTER_3D_COMPARISONS
#define FRAO_CETIOPTER_3D_COMPARISONS

//! \file
//!
//! \brief Header of 3D comparisons between, and on, vectors and
//! matrices
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <cstdint>	//for integer types
#include "APIDefines.hpp"

namespace frao
{
namespace Maths
{
namespace IMPL
{
uint_least64_t FRAO_VECTORCALL
conv3DRecord(RegIntVector source) noexcept;
bool FRAO_VECTORCALL
result3DAllTrue(RegIntVector fullResult) noexcept;
bool FRAO_VECTORCALL
result3DAnyTrue(RegIntVector fullResult) noexcept;
}  // namespace IMPL

//! \defgroup Vector3DComparisons Vector 3D comparisons
//!
//! \brief Comparison functions that operate under the assumption that
//! the underlying data is 3D
//!
//! @{

//! \brief Compare two double vectors component by component, checking
//! if the left hand side was less than the right hand side for all 3D
//! components
//!
//! \returns True iff for all of x, y, and z, left hand side is less
//! than right hand side
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DLess(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, checking
//! if the left hand side was less than the right hand side for all 3D
//! components
//!
//! \returns True iff for all of x, y, and z, left hand side is less
//! than right hand side
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the less-than sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the less-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DLess(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was less than the right hand side for all 3D
//! components
//!
//! \returns True iff for all of x, y, and z, left hand side is less
//! than right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DLessRecord(uint_least64_t& record, RegArgVector lhsVec,
				   RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, recording
//! the result to an int64 variable, and checking if the left hand
//! side was less than the right hand side for all 3D components
//!
//! \returns True iff for all of x, y, and z, left hand side is less
//! than right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the less-than sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the less-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DLessRecord(uint_least64_t& record, RegIntVector lhsVec,
				   RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, checking
//! if the left hand side was less than or equal to the right hand
//! side for all 3D components
//!
//! \returns True iff for all of x, y, and z, left hand side is less
//! than or equal to right hand side
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DLessEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, checking
//! if the left hand side was less than or equal to the right hand
//! side for all 3D components
//!
//! \returns True iff for all of x, y, and z, left hand side is less
//! than or equal to right hand side
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DLessEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was less than or equal to the right hand side for
//! all 3D components
//!
//! \returns True iff for all of x, y, and z, left hand side is less
//! than or equal to right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DLessEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
						RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, recording
//! the result to an int64 variable, and checking if the left hand
//! side was less than or equal to the right hand side for all 3D
//! components
//!
//! \returns True iff for all of x, y, and z, left hand side is less
//! than or equal to right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on
//! the left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on
//! the right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DLessEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
						RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, checking
//! if the left hand side was greater than the right hand side for all
//! 3D components
//!
//! \returns True iff for all of x, y, and z, left hand side is
//! greater than right hand side
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the greater-than sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the greater-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DGreater(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, checking if
//! the left hand side was greater than the right hand side for all 3D
//! components
//!
//! \returns True iff for all of x, y, and z, left hand side is
//! greater than right hand side
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the greater-than sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the greater-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DGreater(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was greater than the right hand side for all 3D
//! components
//!
//! \returns True iff for all of x, y, and z, left hand side is
//! greater than right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the greater-than sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the greater-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DGreaterRecord(uint_least64_t& record, RegArgVector lhsVec,
					  RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, recording
//! the result to an int64 variable, and checking if the left hand
//! side was greater than the right hand side for all 3D components
//!
//! \returns True iff for all of x, y, and z, left hand side is
//! greater than right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the greater-than sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the greater-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DGreaterRecord(uint_least64_t& record, RegIntVector lhsVec,
					  RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, checking
//! if the left hand side was greater than or equal to the right hand
//! side for all 3D components
//!
//! \returns True iff for all of x, y, and z, left hand side is
//! greater than or equal to right hand side
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DGreaterEqual(
	RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, checking if
//! the left hand side was greater than or equal to the right hand
//! side for all 3D components
//!
//! \returns True iff for all of x, y, and z, left hand side is
//! greater than or equal to right hand side
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the greater-than-or-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the greater-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DGreaterEqual(
	RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was greater than or equal to the right hand side
//! for all 3D components
//!
//! \returns True iff for all of x, y, and z, left hand side is
//! greater than or equal to right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the greater-than-or-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the greater-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DGreaterEqualRecord(
	uint_least64_t& record, RegArgVector lhsVec,
	RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was greater than or equal to the right hand side
//! for all 3D components
//!
//! \returns True iff for all of x, y, and z, left hand side is
//! greater than or equal to right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on
//! the left hand side (lhs) of the greater-than-or-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on
//! the right hand side (rhs) of the greater-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DGreaterEqualRecord(
	uint_least64_t& record, RegIntVector lhsVec,
	RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, checking
//! if the left hand side was equal to the right hand side for all 3D
//! components
//!
//! \returns True iff for all of x, y, and z, left hand side is equal
//! to right hand side
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, checking if
//! the left hand side was equal to the right hand side for all 3D
//! components
//!
//! \returns True iff for all of x, y, and z, left hand side is equal
//! to right hand side
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was equal to the right hand side for all 3D
//! components
//!
//! \returns True iff for all of x, y, and z, left hand side is equal
//! to right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, recording
//! the result to an int64 variable, and checking if the left hand
//! side was equal to the right hand side for all 3D components
//!
//! \returns True iff for all of x, y, and z, left hand side is equal
//! to right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on
//! the left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on
//! the right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, checking
//! if the left hand side was not equal to the right hand side for all
//! 3D components
//!
//! \returns True iff for all of x, y, and z, left hand side is not
//! equal to right hand side
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the not-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the not-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DNotEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, checking if
//! the left hand side was not equal to the right hand side for all 3D
//! components
//!
//! \returns True iff for all of x, y, and z, left hand side is not
//! equal to right hand side
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the not-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the not-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DNotEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was not equal to the right hand side for all 3D
//! components
//!
//! \returns True iff for all of x, y, and z, left hand side is not
//! equal to right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the not-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the not-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DNotEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					   RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was not equal to the right hand side for all 3D
//! components
//!
//! \returns True iff for all of x, y, and z, left hand side is not
//! equal to right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on
//! the left hand side (lhs) of the not-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on
//! the right hand side (rhs) of the not-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DNotEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					   RegIntVector rhsVec) noexcept;

//! \brief Compare three double vectors component by component,
//! checking if for the corresponding operand components, the checked
//! vector was greater than or equal to the lower bound, and less than
//! or equal to the higher bound for all 3D components
//!
//! \returns True iff for all of x, y, and z, the corresponding
//! checked component was inclusively within the specified bounds.
//!
//! \param[in] checked The double vector of components to compare
//! against both bounds
//!
//! \param[in] lowerBound The double vector of components to against
//! the checked component, to determine whether the checked component
//! is greater than or equal to the corresponding component of
//! lowerBound
//!
//! \param[in] upperBound The double vector of components to against
//! the checked component, to determine whether the checked component
//! is less than or equal to the corresponding component of upperBound
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DInBounds(RegArgVector checked, RegArgVector lowerBound,
				 RegArgVector upperBound) noexcept;
//! \brief Compare three int vectors component by component,
//! checking if for the corresponding operand components, the checked
//! vector was greater than or equal to the lower bound, and less than
//! or equal to the higher bound for all 3D components
//!
//! \returns True iff for all of x, y, and z, the corresponding
//! checked component was inclusively within the specified bounds.
//!
//! \param[in] checked The int vector of components to compare
//! against both bounds
//!
//! \param[in] lowerBound The int vector of components to against
//! the checked component, to determine whether the checked component
//! is greater than or equal to the corresponding component of
//! lowerBound
//!
//! \param[in] upperBound The int vector of components to against
//! the checked component, to determine whether the checked component
//! is less than or equal to the corresponding component of upperBound
// bounds
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DInBounds(RegIntVector checked, RegIntVector lowerBound,
				 RegIntVector upperBound) noexcept;
//! \brief Compare three double vectors component by component,
//! recording the result to an int64 variable, checking if for the
//! corresponding operand components, the checked vector was greater
//! than or equal to the lower bound, and less than or equal to the
//! higher bound for all 3D components
//!
//! \returns True iff for all of x, y, and z, the corresponding
//! checked component was inclusively within the specified bounds.
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] checked The double vector of components to compare
//! against both bounds
//!
//! \param[in] lowerBound The double vector of components to against
//! the checked component, to determine whether the checked component
//! is greater than or equal to the corresponding component of
//! lowerBound
//!
//! \param[in] upperBound The double vector of components to against
//! the checked component, to determine whether the checked component
//! is less than or equal to the corresponding component of upperBound
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DInBoundsRecord(
	uint_least64_t& record, RegArgVector checked,
	RegArgVector lowerBound, RegArgVector upperBound) noexcept;
//! \brief Compare three int vectors component by component,
//! recording the result to an int64 variable, checking if for the
//! corresponding operand components, the checked vector was greater
//! than or equal to the lower bound, and less than or equal to the
//! higher bound for all 3D components
//!
//! \returns True iff for all of x, y, and z, the corresponding
//! checked component was inclusively within the specified bounds.
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] checked The int vector of components to compare against
//! both bounds
//!
//! \param[in] lowerBound The int vector of components to against the
//! checked component, to determine whether the checked component is
//! greater than or equal to the corresponding component of lowerBound
//!
//! \param[in] upperBound The int vector of components to against the
//! checked component, to determine whether the checked component is
//! less than or equal to the corresponding component of upperBound
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DInBoundsRecord(
	uint_least64_t& record, RegIntVector checked,
	RegIntVector lowerBound, RegIntVector upperBound) noexcept;

//! \brief Check each 3D component of a vector for NaN, and return
//! true iff any of them are
//!
//! \returns True iff any of the x, y, z components was a type of NaN
//!
//! \param[in] vector The vector whose components should be checked vs
//! NaN
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DIsNaN(RegArgVector vector) noexcept;
//! \brief Check each 3D component of a vector for NaN, return true
//! iff any of them are, and record the result to an int
//!
//! \returns True iff any of the x, y, z components was a type of NaN
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] vector The vector whose components should be checked vs
//! NaN
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DIsNaNRecord(
	uint_least64_t& record, RegArgVector vector) noexcept;

//! \brief Check each 3D component of a vector for infinities, and
//! return true iff any of them are
//!
//! \returns True iff any of the x, y, z components was a type of inf
//!
//! \param[in] vector The vector whose components should be checked vs
//! inf
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector3DIsInf(RegArgVector vector) noexcept;
//! \brief Check each 3D component of a vector for infinities, return
//! true iff any of them are, and record the result to an int
//!
//! \returns True iff any of the x, y, z components was a type of inf
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] vector The vector whose components should be checked vs
//! inf
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DIsInfRecord(
	uint_least64_t& record, RegArgVector vector) noexcept;

//! @}

//! \defgroup Matrix3DComparisons Matrix 3D comparisons
//!
//! \brief Comparison functions that operate on matrices under the
//! assumption that the underlying data is 3D
//!
//! @{

//! \brief Compare two 3x3 matrices component by component, checking
//! if the left hand side was equal to the right hand side
//!
//! \returns True iff for all x, y, and z, from the three rows, left
//! hand side is equal to right hand side
//!
//! \param[in] lhsMatrix The double vector of components to compare on
//! the left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsMatrix The double vector of components to compare on
//! the right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL matrix3DEqual(
	RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept;
//! \brief Compare two 3x3 matrices component by component, checking
//! if the left hand side was not equal to the right hand side
//!
//! \returns True iff for all x, y, and z, from the three rows, left
//! hand side is not equal to right hand side
//!
//! \param[in] lhsMatrix The double vector of components to compare on
//! the left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsMatrix The double vector of components to compare on
//! the right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL matrix3DNotEqual(
	RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept;

//! \brief Check each component of a 3x3 matrix for NaN, and return
//! true iff any of them are
//!
//! \returns True iff any component was a type of NaN
//!
//! \param[in] matrix The matrix whose components should be checked vs
//! NaN
CETIOPTER_LIB_API bool FRAO_VECTORCALL
matrix3DIsNaN(RegArgMatrix matrix) noexcept;
//! \brief Check each component of a 3x3 matrix for infinities, and
//! return true iff any of them are
//!
//! \returns True iff any component was a type of inf
//!
//! \param[in] matrix The matrix whose components should be checked vs
//! inf
CETIOPTER_LIB_API bool FRAO_VECTORCALL
matrix3DIsInf(RegArgMatrix matrix) noexcept;

//! @}
}  // namespace Maths
}  // namespace frao

#endif