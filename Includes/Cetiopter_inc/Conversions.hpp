#ifndef FRAO_CETIOPTER_CONVERSIONS
#define FRAO_CETIOPTER_CONVERSIONS

//! \file
//!
//! \brief Header of conversions between different types/types of data
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "APIDefines.hpp"

namespace frao
{
namespace Maths
{
//! \defgroup VectorConversion Vector conversion functions
//!
//! \brief Functions that convert a vector or matrix to some other
//! form, or rounding
//!
//! @{

//! \brief Reinterpret an int vector as a double one
//!
//! \details Takes the bit pattern of an int vector, and keeping the
//! it unchanged, turns it into a double one
//!
//! \returns A double vector with the same bit pattern as the input
//! vector
//!
//! \param[in] vector The input vector whose bit pattern should be
//! made into a double vector
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
reinterpretToDouble(RegIntVector vector) noexcept;
//! \brief Reinterpret double vector as an int one
//!
//! \details Takes the bit pattern of a double vector, and keeping the
//! it unchanged, turns it into an int one
//!
//! \returns An int vector with the same bit pattern as the input
//! vector
//!
//! \param[in] vector The input vector whose bit pattern should be
//! made into an int vector
// reinterpret a double vector as an integer one
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
reinterpretToInteger(RegArgVector vector) noexcept;

//! \brief Convert a vector of ints to a vector of doubles
//!
//! \note Bit pattern will not be preserved, since conversion will be
//! logical, not a reinterpretation
//!
//! \returns A double vector with each component converted from the
//! corresponding int vector component
//!
//! \param[in] vector The input vector, whose components should be
//! converted to double
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
toDoubleVector(RegIntVector vector) noexcept;
//! \brief Convert a vector of doubles to a vector of ints
//!
//! \note Bit pattern will not be preserved, since conversion will be
//! logical, not a reinterpretation
//!
//! \returns An int vector with each component converted from the
//! corresponding double vector component
//!
//! \param[in] vector The input vector, whose components should be
//! converted to int
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
toIntegerVector(RegArgVector vector) noexcept;

//! \brief Convert a WideVector (double-precision) to a narrow one
//! (single-precision)
//!
//! \returns A vector of floating points
//!
//! \param[in] vector The vector of doubles to narrow to floats
CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
toNarrowVector(RegArgVector vector) noexcept;
//! \brief Convert a narrow vector (single-precision) to a wide one
//! (double-precision)
//!
//! \returns A vector of doubles
//!
//! \param[in] vector The vector of floats to widen to doubles
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
toWideVector(RegArgNrwVec vector) noexcept;

//! \brief Convert an int64 vector to an int32 one
//!
//! \returns A vector of int32s
//!
//! \param[in] vector The vector of int64s to narrow to int32s
CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
toSmallIntVector(RegIntVector vector) noexcept;
//! \brief Convert an in32 vector to an in464 one
//!
//! \returns A vector of int64s
//!
//! \param[in] vector The vector of int32s to widen to int64s
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
toWideIntVector(RegIntNrwVec vector) noexcept;

//! \brief Convert a WideVector (double-precision) to a narrow one
//! (single-precision)
//!
//! \returns A matrix of floats
//!
//! \param[in] matrix The matrix of doubles to narrow to floats
CETIOPTER_LIB_API NarrowMatrix FRAO_VECTORCALL
toNarrowMatrix(RegArgMatrix matrix) noexcept;
//! \brief Convert a narrow vector (single-precision) to a wide one
//! (double-precision)
//!
//! \returns A matrix of doubles
//!
//! \param[in] matrix The matrix of floats to widen to doubles
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
toWideMatrix(RegArgNrwMtrx matrix) noexcept;

//! \brief For each component of the vector, round away from zero,
//! with halves towards even (bankers round)
//!
//! \returns A vector containing rounded (away from zero, towards
//! even) components
//!
//! \param[in] vector The vector that should be rounded
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorRound(RegArgVector vector) noexcept;
//! \brief For each component of the vector, round towards -inf (floor
//! function)
//!
//! \returns A vector containing floored (rounded towards -inf)
//! components
//!
//! \param[in] vector The vector that should be floored
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorFloor(RegArgVector vector) noexcept;
//! \brief For each component of the vector, round towards +inf
//! (ceiling function)
//!
//! \returns A vector that has had its components subjected to the
//! ceiling function (rounded towards +inf) components
//!
//! \param[in] vector The vector that should be subjected to the
//! ciling function
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorCeiling(RegArgVector vector) noexcept;
//! \brief For each component of the vector, round towards 0
//! (truncate)
//!
//! \returns A vector containing truncated (rounded towards 0)
//! components
//!
//! \param[in] vector The vector that should be truncated
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorTruncate(RegArgVector vector) noexcept;


//! @}
}  // namespace Maths
}  // namespace frao

#endif