#ifndef FRAO_CETIOPTER_SPECIAL_VECTORS
#define FRAO_CETIOPTER_SPECIAL_VECTORS

//! \file
//!
//! \brief Header of special vectors, such as the zero vector and the
//! epsilon vector
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <cstdint>	// for integer types
#include <limits>	//for std::numeric_limits
#include "APIDefines.hpp"

namespace frao
{
namespace Maths
{
namespace IMPL
{
// used for returning true values, or for when we need all bits set
constexpr static const int_least64_t g_TrueBits =
	0xFFFF'FFFF'FFFF'FFFF;
constexpr static const double g_QNaN =
	std::numeric_limits<double>::quiet_NaN();
constexpr static const double g_SNaN =
	std::numeric_limits<double>::signaling_NaN();
constexpr static const double g_Inf =
	std::numeric_limits<double>::infinity();
constexpr static const double g_Epsilon =
	std::numeric_limits<double>::epsilon();
}  // namespace IMPL

//! \defgroup VectorConstants Vector constant functions
//!
//! \brief Functions that hold constants replicated
//! across every component
//!
//! @{

//! \brief Gets the zero (double) vector.
//!
//! \returns The zero vector, which is a vector with 0.0 in every
//! component
CETIOPTER_LIB_API WideVector vectorZero() noexcept;
//! \brief Gets the zero (int) vector.
//!
//! \returns The zero vector, which is a vector with 0 in every
//! component
CETIOPTER_LIB_API WideIntVector vectorZeroInt() noexcept;
//! \brief Gets a vector with all bits set.
//!
//! \returns An int vector in which every constituent bit (**not
//! component**) is set to 1
CETIOPTER_LIB_API WideIntVector vectorSplatAllBits() noexcept;

//! \brief Gets a double vector with the value 1 replicated across all
//! components
//!
//! \returns A vector with each of the four double components holding
//! the value 1.0
CETIOPTER_LIB_API WideVector vectorSplatOne() noexcept;
//! \brief Gets an integer vector with the value 1 replicated across
//! all components
//!
//! \returns A vector with each of the four integer components holding
//! the value 1
CETIOPTER_LIB_API WideIntVector vectorSplatOneInt() noexcept;

//! \brief Gets a vector with a quiet NaN replicated across all
//! components
//!
//! \returns A vector with a quiet NaN for each component
CETIOPTER_LIB_API WideVector vectorSplatQNaN() noexcept;
//! \brief Gets a vector with a signalling NaN replicated across all
//! components
//!
//! \returns A vector with a signalling NaN for each component
CETIOPTER_LIB_API WideVector vectorSplatSNaN() noexcept;
//! \brief Gets a vector with the double +infinity representation
//! replicated across all components
//!
//! \returns A vector with +infinity for each component
CETIOPTER_LIB_API WideVector vectorSplatInfinity() noexcept;
//! \brief Get a vector with the standard epsilon replicated across
//! all components
//!
//! \warning Standard epsilon represents the difference between 1.0
//! and the next representable value, and is not a universal
//! floating-point error-checking value. Use epsilons always with care
//!
//! \returns A vector with standard epsilonm for each component
CETIOPTER_LIB_API WideVector vectorSplatEpsilon() noexcept;

//! @}

}  // namespace Maths
}  // namespace frao

#endif