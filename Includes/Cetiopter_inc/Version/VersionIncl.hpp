#ifndef FRAO_CETIOPTER_VERSION_INCLUDE
#define FRAO_CETIOPTER_VERSION_INCLUDE

//! \file
//!
//! \brief Header of version retreival functions, for this
//! project. Stores constexpr variables, and a function,
//! that encode the current Cetiopter version
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights
//! reserved. Full licence available in 'LICENCE' file, in
//! the root source code folder of this project

#include <Nucleus_inc\Environment.hpp>
#include "APIDefines.hpp"
#include "CetiopterVersion.h"

namespace frao
{
inline namespace Environment
{
//! \brief version number, major component. a in "a.i.s.p.d"
constexpr const small_nat8 g_CetiopterVersionMajor = VERSION_MAJOR;
//! \brief version number, minor component. i in "a.i.s.p.d"
constexpr const small_nat16 g_CetiopterVersionMinor = VERSION_MINOR;
//! \brief version number, status component. s in "a.i.s.p.d"
constexpr const char g_CetiopterVersionStatus[3] = VERSION_STATUS;
//! \brief version number, patch component. p in "a.i.s.p.d"
constexpr const small_nat16 g_CetiopterVersionPatch = VERSION_PATCH;
//! \brief version number, development build component. d in
//! "a.i.s.p.d"
constexpr const small_nat16 g_CetiopterVersionBuild =
	VERSION_DEV_BUILD;

//! \brief Get the Cetiopter API version, for this build
//!
//! \returns A version information class, that holds the
//! version data, for this build
CETIOPTER_LIB_API constexpr VersionInfo getCetiopterVersion() noexcept
{
	return VersionInfo(
		g_CetiopterVersionMajor, g_CetiopterVersionMinor,
		g_CetiopterVersionPatch, g_CetiopterVersionBuild, g_CetiopterVersionStatus);
}
}  // namespace Environment
}  // namespace frao

//This will undef the macros we just used, second time around
#include "CetiopterVersion.h"

#endif
