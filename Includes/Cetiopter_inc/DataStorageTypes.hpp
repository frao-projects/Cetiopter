#ifndef FRAO_CETIOPTER_STORAGE_TYPES
#define FRAO_CETIOPTER_STORAGE_TYPES

//! \file
//!
//! \brief Header of data storage types, including those that store
//! vector and matrix data, and interactions with these types
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <cstdint>	// for integer types
#include "APIDefines.hpp"

namespace frao
{
namespace Maths
{
//! \defgroup VectorDataStores Vector data storage
//!
//! \brief Types used to store data, that can be efficiently loaded to
//! Wide and WideInt vectors, for work
//!
//! @{

//! \brief 2 component float data store.
struct CETIOPTER_LIB_API Float2
{
	union
	{
		//! \brief The x, and y components of the 2 component
		//! data store
		float m_Members[2];

		struct Components
		{
			//! \brief The x component of the 2 component data store
			float m_X;
			//! \brief The y component of the 2 component data store
			float m_Y;
		} m_Components;
	};
};

//! \brief 3 component float data store.
struct CETIOPTER_LIB_API Float3
{
	union
	{
		//! \brief The x, y, and z components of the 3 component data
		//! store
		float m_Members[3];

		struct Components
		{
			//! \brief The x component of the 3 component data store
			float m_X;
			//! \brief The y component of the 3 component data store
			float m_Y;
			//! \brief The z component of the 3 component data store
			float m_Z;
		} m_Components;
	};
};

//! \brief 4 component float data store.
struct CETIOPTER_LIB_API Float4
{
	union
	{
		//! \brief The x, y, z, and w components of the 4 component
		//! data store
		float m_Members[4];

		struct Components
		{
			//! \brief The x component of the 4 component data store
			float m_X;
			//! \brief The y component of the 4 component data store
			float m_Y;
			//! \brief The z component of the 4 component data store
			float m_Z;
			//! \brief The w component of the 4 component data store
			float m_W;
		} m_Components;
	};
};

//! \brief 2 component int32 data store.
struct CETIOPTER_LIB_API SmallInt2
{
	union
	{
		//! \brief The x, and y components of the 2 component
		//! data store
		int_least32_t m_Members[2];

		struct Components
		{
			//! \brief The x component of the 2 component data store
			int_least32_t m_X;
			//! \brief The y component of the 2 component data store
			int_least32_t m_Y;
		} m_Components;
	};
};

//! \brief 3 component int32 data store.
struct CETIOPTER_LIB_API SmallInt3
{
	union
	{
		//! \brief The x, y, and z components of the 3 component data
		//! store
		int_least32_t m_Members[3];

		struct Components
		{
			//! \brief The x component of the 3 component data store
			int_least32_t m_X;
			//! \brief The y component of the 3 component data store
			int_least32_t m_Y;
			//! \brief The z component of the 3 component data store
			int_least32_t m_Z;
		} m_Components;
	};
};

//! \brief 4 component int32 data store.
struct CETIOPTER_LIB_API SmallInt4
{
	union
	{
		//! \brief The x, y, z, and w components of the 4 component
		//! data store
		int_least32_t m_Members[4];

		struct Components
		{
			//! \brief The x component of the 4 component data store
			int_least32_t m_X;
			//! \brief The y component of the 4 component data store
			int_least32_t m_Y;
			//! \brief The z component of the 4 component data store
			int_least32_t m_Z;
			//! \brief The w component of the 4 component data store
			int_least32_t m_W;
		} m_Components;
	};
};

//! \brief 2 component double data store.
struct CETIOPTER_LIB_API Double2
{
	union
	{
		//! \brief The x, and y components of the 2 component data
		//! store
		double m_Members[2];

		struct Components
		{
			//! \brief The x component of the 2 component data store
			double m_X;
			//! \brief The y component of the 2 component data store
			double m_Y;
		} m_Components;
	};
};

//! \brief 3 component double data store.
struct CETIOPTER_LIB_API Double3
{
	union
	{
		//! \brief The x, y, and z components of the 3 component data
		//! store
		double m_Members[3];

		struct Components
		{
			//! \brief The x component of the 3 component data store
			double m_X;
			//! \brief The y component of the 3 component data store
			double m_Y;
			//! \brief The z component of the 3 component data store
			double m_Z;
		} m_Components;
	};
};

//! \brief 4 component double data store.
struct CETIOPTER_LIB_API Double4
{
	union
	{
		//! \brief The x, y, z, and w components of the 4 component
		//! data store
		double m_Members[4];

		struct Components
		{
			//! \brief The x component of the 4 component data store
			double m_X;
			//! \brief The y component of the 4 component data store
			double m_Y;
			//! \brief The z component of the 4 component data store
			double m_Z;
			//! \brief The w component of the 4 component data store
			double m_W;
		} m_Components;
	};
};

//! \brief 4 component, 2 row float data store.
struct CETIOPTER_LIB_API Float2x2
{
	//! \brief The array of two two-component data stores
	Float2 m_Tuples[2];
};

//! \brief 9 component, 3 row float data store.
struct CETIOPTER_LIB_API Float3x3
{
	//! \brief The array of three three-component data stores
	Float3 m_Tuples[3];
};

//! \brief 16 component, 4 row float data store.
struct CETIOPTER_LIB_API Float4x4
{
	//! \brief The array of four four-component data stores
	Float4 m_Tuples[4];
};

//! \brief 4 component, 2 row double data store.
struct CETIOPTER_LIB_API Double2x2
{
	//! \brief The array of two two-component data stores
	Double2 m_Tuples[2];
};

//! \brief 9 component, 3 row double data store.
struct CETIOPTER_LIB_API Double3x3
{
	//! \brief The array of three three-component data stores
	Double3 m_Tuples[3];
};

//! \brief 16 component, 4 row double data store.
struct CETIOPTER_LIB_API Double4x4
{
	//! \brief The array of four four-component data stores
	Double4 m_Tuples[4];
};

//! \brief 2 component int data store.
struct CETIOPTER_LIB_API Integer2
{
	union
	{
		//! \brief The x, and y components of the 2 component data
		//! store
		int_least64_t m_Members[2];

		struct Components
		{
			//! \brief The x component of the 2 component data store
			int_least64_t m_X;
			//! \brief The y component of the 2 component data store
			int_least64_t m_Y;
		} m_Components;
	};
};

//! \brief 3 component int data store.
struct CETIOPTER_LIB_API Integer3
{
	union
	{
		//! \brief The x, y, and z components of the 3 component data
		//! store
		int_least64_t m_Members[3];

		struct Components
		{
			//! \brief The x component of the 3 component data store
			int_least64_t m_X;
			//! \brief The y component of the 3 component data store
			int_least64_t m_Y;
			//! \brief The z component of the 3 component data store
			int_least64_t m_Z;
		} m_Components;
	};
};

//! \brief 4 component int data store.
struct CETIOPTER_LIB_API Integer4
{
	union
	{
		//! \brief The x, y, z, and w components of the 4 component
		//! data store
		int_least64_t m_Members[4];

		struct Components
		{
			//! \brief The x component of the 4 component data store
			int_least64_t m_X;
			//! \brief The y component of the 4 component data store
			int_least64_t m_Y;
			//! \brief The z component of the 4 component data store
			int_least64_t m_Z;
			//! \brief The w component of the 4 component data store
			int_least64_t m_W;
		} m_Components;
	};
};

//! @}

//! \defgroup VectorLoads Vector load functions
//!
//! \brief Functions used to store vector registers to data types
//!
//! @{

//! \brief load a 2D float data store into a form capable of doing
//! work
//!
//! \returns A narrow vector capable of doing useful work
//!
//! \param[in] data A 2D float data store
CETIOPTER_LIB_API NarrowVector loadFloat2(const Float2& data) noexcept;
//! \brief load a 3D float data store into a form capable of doing
//! work
//!
//! \returns A narrow vector capable of doing useful work
//!
//! \param[in] data A 3D float data store
CETIOPTER_LIB_API NarrowVector loadFloat3(const Float3& data) noexcept;
//! \brief load a 4D float data store into a form capable of doing
//! work
//!
//! \returns A narrow vector capable of doing useful work
//!
//! \param[in] data A 4D float data store
CETIOPTER_LIB_API NarrowVector loadFloat4(const Float4& data) noexcept;
//! \brief load a 2D int32 data store into a form capable of doing
//! work
//!
//! \returns A narrow vector capable of doing useful work
//!
//! \param[in] data A 2D int32 data store
CETIOPTER_LIB_API NarrowIntVector
loadSmallInt2(const SmallInt2& data) noexcept;
//! \brief load a 3D int32 data store into a form capable of doing
//! work
//!
//! \returns A narrow vector capable of doing useful work
//!
//! \param[in] data A 3D int32 data store
CETIOPTER_LIB_API NarrowIntVector
loadSmallInt3(const SmallInt3& data) noexcept;
//! \brief load a 4D int32 data store into a form capable of doing
//! work
//!
//! \returns A narrow vector capable of doing useful work
//!
//! \param[in] data A 4D int32 data store
CETIOPTER_LIB_API NarrowIntVector
loadSmallInt4(const SmallInt4& data) noexcept;
//! \brief load a 2D double data store into a form capable of doing
//! work
//!
//! \returns A wide vector capable of doing useful work
//!
//! \param[in] data A 2D double data store
CETIOPTER_LIB_API WideVector loadDouble2(const Double2& data) noexcept;
//! \brief load a 3D double data store into a form capable of doing
//! work
//!
//! \returns A wide vector capable of doing useful work
//!
//! \param[in] data A 3D double double data store
CETIOPTER_LIB_API WideVector loadDouble3(const Double3& data) noexcept;
//! \brief load a 4D double data store into a form capable of doing
//! work
//!
//! \returns A wide vector capable of doing useful work
//!
//! \param[in] data A 4D double data store
CETIOPTER_LIB_API WideVector loadDouble4(const Double4& data) noexcept;

//! \brief load a 2x2D float data store into a form capable of doing
//! work
//!
//! \returns A narrow matrix capable of doing useful work
//!
//! \param[in] data A 2x2D float data store
CETIOPTER_LIB_API NarrowMatrix
loadFloat2x2(const Float2x2& data) noexcept;
//! \brief load a 3x3D float data store into a form capable of doing
//! work
//!
//! \returns A narrow matrix capable of doing useful work
//!
//! \param[in] data A 3x3D float data store
CETIOPTER_LIB_API NarrowMatrix
loadFloat3x3(const Float3x3& data) noexcept;
//! \brief load a 4x4D float data store into a form capable of doing
//! work
//!
//! \returns A narrow matrix capable of doing useful work
//!
//! \param[in] data A 4x4D float data store
CETIOPTER_LIB_API NarrowMatrix
loadFloat4x4(const Float4x4& data) noexcept;

//! \brief load a 2x2D double data store into a form capable of doing
//! work
//!
//! \returns A wide matrix capable of doing useful work
//!
//! \param[in] data A 2x2D double data store
CETIOPTER_LIB_API WideMatrix
loadDouble2x2(const Double2x2& data) noexcept;
//! \brief load a 3x3D double data store into a form capable of doing
//! work
//!
//! \returns A wide matrix capable of doing useful work
//!
//! \param[in] data A 3x3D double data store
CETIOPTER_LIB_API WideMatrix
loadDouble3x3(const Double3x3& data) noexcept;
//! \brief load a 4x4D double data store into a form capable of doing
//! work
//!
//! \returns A wide matrix capable of doing useful work
//!
//! \param[in] data A 4x4D double data store
CETIOPTER_LIB_API WideMatrix
loadDouble4x4(const Double4x4& data) noexcept;

//! \brief load a 2D integer data store into a form capable of doing
//! work
//!
//! \returns A wide vector capable of doing useful work
//!
//! \param[in] data A 2D integer data store
CETIOPTER_LIB_API WideIntVector
loadInteger2(const Integer2& data) noexcept;
//! \brief load a 3D integer data store into a form capable of doing
//! work
//!
//! \returns A wide vector capable of doing useful work
//!
//! \param[in] data A 3D integer data store
CETIOPTER_LIB_API WideIntVector
loadInteger3(const Integer3& data) noexcept;
//! \brief load a 4D integer data store into a form capable of doing
//! work
//!
//! \returns A wide vector capable of doing useful work
//!
//! \param[in] data A 4D integer data store
CETIOPTER_LIB_API WideIntVector
loadInteger4(const Integer4& data) noexcept;

//! @}

//! \defgroup VectorStores Vector store functions
//!
//! \brief Functions used to store vector registers to data types
//!
//! @{

//! \brief Store the result of calculation(s) to a 2D float data store
//!
//! \param[out] store The 2D float data store to receive the vector
//! data
//!
//! \param[in] data The vector data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeFloat2(Float2& store, RegArgNrwVec data) noexcept;
//! \brief Store the result of calculation(s) to a 3D float data store
//!
//! \param[out] store The 3D float data store to receive the vector
//! data
//!
//! \param[in] data The vector data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeFloat3(Float3& store, RegArgNrwVec data) noexcept;
//! \brief Store the result of calculation(s) to a 4D float data store
//!
//! \param[out] store The 4D float data store to receive the vector
//! data
//!
//! \param[in] data The vector data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeFloat4(Float4& store, RegArgNrwVec data) noexcept;
//! \brief Store the result of calculation(s) to a 2D int32 data store
//!
//! \param[out] store The 2D int32 data store to receive the vector
//! data
//!
//! \param[in] data The vector data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeSmallInt2(SmallInt2& store, RegIntNrwVec data) noexcept;
//! \brief Store the result of calculation(s) to a 3D int32 data store
//!
//! \param[out] store The 3D int32 data store to receive the vector
//! data
//!
//! \param[in] data The vector data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeSmallInt3(SmallInt3& store, RegIntNrwVec data) noexcept;
//! \brief Store the result of calculation(s) to a 4D int32 data store
//!
//! \param[out] store The 4D int32 data store to receive the vector
//! data
//!
//! \param[in] data The vector data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeSmallInt4(SmallInt4& store, RegIntNrwVec data) noexcept;
//! \brief Store the result of calculation(s) to a 2D double data
//! store
//!
//! \param[out] store The 2D double data store to receive the vector
//! data
//!
//! \param[in] data The vector data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeDouble2(Double2& store, RegArgVector data) noexcept;
//! \brief Store the result of calculation(s) to a 3D double data
//! store
//!
//! \param[out] store The 3D double data store to receive the vector
//! data
//!
//! \param[in] data The vector data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeDouble3(Double3& store, RegArgVector data) noexcept;
//! \brief Store the result of calculation(s) to a 4D double data
//! store
//!
//! \param[out] store The 4D double data store to receive the vector
//! data
//!
//! \param[in] data The vector data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeDouble4(Double4& store, RegArgVector data) noexcept;

//! \brief Store the result of calculation(s) to a 2x2D float data
//! store
//!
//! \param[out] store The 2x2D float data store to receive the matrix
//! data
//!
//! \param[in] data The matrix data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeFloat2x2(Float2x2& store, RegArgNrwMtrx data) noexcept;
//! \brief Store the result of calculation(s) to a 3x3D float data
//! store
//!
//! \param[out] store The 3x3D float data store to receive the matrix
//! data
//!
//! \param[in] data The matrix data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeFloat3x3(Float3x3& store, RegArgNrwMtrx data) noexcept;
//! \brief Store the result of calculation(s) to a 4x4D float data
//! store
//!
//! \param[out] store The 4x4D float data store to receive the matrix
//! data
//!
//! \param[in] data The matrix data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeFloat4x4(Float4x4& store, RegArgNrwMtrx data) noexcept;

//! \brief Store the result of calculation(s) to a 2x2D double data
//! store
//!
//! \param[out] store The 2x2D double data store to receive the matrix
//! data
//!
//! \param[in] data The matrix data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeDouble2x2(Double2x2& store, RegArgMatrix data) noexcept;
//! \brief Store the result of calculation(s) to a 3x3D double data
//! store
//!
//! \param[out] store The 3x3D double data store to receive the matrix
//! data
//!
//! \param[in] data The matrix data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeDouble3x3(Double3x3& store, RegArgMatrix data) noexcept;
//! \brief Store the result of calculation(s) to a 4x4D double data
//! store
//!
//! \param[out] store The 4x4D double data store to receive the matrix
//! data
//!
//! \param[in] data The matrix data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeDouble4x4(Double4x4& store, RegArgMatrix data) noexcept;

//! \brief Store the result of calculation(s) to a 2D integer data
//! store
//!
//! \param[out] store The 2D integer data store to receive the vector
//! data
//!
//! \param[in] data The vector data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeInteger2(Integer2& store, RegIntVector data) noexcept;
//! \brief Store the result of calculation(s) to a 3D integer data
//! store
//!
//! \param[out] store The 3D integer data store to receive the vector
//! data
//!
//! \param[in] data The vector data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeInteger3(Integer3& store, RegIntVector data) noexcept;
//! \brief Store the result of calculation(s) to a 4D integer data
//! store
//!
//! \param[out] store The 4D integer data store to receive the vector
//! data
//!
//! \param[in] data The vector data to be stored.
CETIOPTER_LIB_API void FRAO_VECTORCALL
storeInteger4(Integer4& store, RegIntVector data) noexcept;

//! @}
}  // namespace Maths
}  // namespace frao

#endif