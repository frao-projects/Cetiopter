#ifndef FRAO_CETIOPTER_COMPONENT_ACCESS
#define FRAO_CETIOPTER_COMPONENT_ACCESS

//! \file
//!
//! \brief Header of vector component-wise access/reordering functions
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <cstdint>		   //for integer types
#include "APIDefines.hpp"  //for AVX/2 switching, etc

namespace frao
{
namespace Maths
{
//! \defgroup VectorGettersSetters Vector getters/setters
//!
//! \brief Functions that get or set a whole or a part of the vector
//!
//! @{

//! \brief Get the specified component of a float vector.
//!
//! \details Get the float stored at the specified location within
//! the float vector. Index is treated as an offset from the first
//! (x) element, and thus is within the range [0, 3]. Indices will be
//! masked such that all indices are within this range.
//!
//! \returns The float stored at the location within the vector
//!
//! \param[in] vector The float vector from which to get the float
//!
//! \param[in] elementIndex The index of the location to retreive,
//! within the vector. Will be masked to <= 3
CETIOPTER_LIB_API float FRAO_VECTORCALL vectorGetByIndex(
	RegArgNrwVec vector, uint_least64_t elementIndex) noexcept;
//! \brief Get the specified component of an int32 vector.
//!
//! \details Get the int32 stored at the specified location within
//! the int32 vector. Index is treated as an offset from the first
//! (x) element, and thus is within the range [0, 3]. Indices will be
//! masked such that all indices are within this range.
//!
//! \returns The int32 stored at the location within the vector
//!
//! \param[in] vector The int32 vector from which to get the int32
//!
//! \param[in] elementIndex The index of the location to retreive,
//! within the vector. Will be masked to <= 3
CETIOPTER_LIB_API int_least32_t FRAO_VECTORCALL vectorGetByIndex(
	RegIntNrwVec vector, uint_least64_t elementIndex) noexcept;
//! \brief Get the specified component of a double vector.
//!
//! \details Get the double stored at the specified location within
//! the double vector. Index is treated as an offset from the first
//! (x) element, and thus is within the range [0, 3]. Indices will be
//! masked such that all indices are within this range.
//!
//! \returns The double stored at the location within the vector
//!
//! \param[in] vector The double vector from which to get the double
//!
//! \param[in] elementIndex The index of the location to retreive,
//! within the vector. Will be masked to <= 3
CETIOPTER_LIB_API double FRAO_VECTORCALL vectorGetByIndex(
	RegArgVector vector, uint_least64_t elementIndex) noexcept;
//! \brief Get the specified component of a int vector.
//!
//! \details Get the int stored at the specified location within the
//! int vector. Index is treated as an offset from the first (x)
//! element, and thus is within the range [0, 3]. Indices will be
//! masked such that all indices are within this range.
//!
//! \returns The int stored at the location within the vector
//!
//! \param[in] vector The int vector from which to get the int
//!
//! \param[in] elementIndex The index of the location to retreive,
//! within the vector. Will be masked to <= 3
CETIOPTER_LIB_API int_least64_t FRAO_VECTORCALL vectorGetByIndex(
	RegIntVector vector, uint_least64_t elementIndex) noexcept;
//! \brief Set the specified component of a float vector.
//!
//! \details Set the float stored at the specified location within
//! the float vector. Index is treated as an offset from the first
//! (x) element, and thus is within the range [0, 3]. Indices will be
//! masked such that all indices are within this range.
//!
//! \returns The new float vector, with the specified component
//! suitably altered
//!
//! \param[in] vector The float vector to alter
//!
//! \param[in] number The float to place at the specified location
//! within the vector.
//!
//! \param[in] elementIndex The index of the location to set, within
//! the vector. Will be masked to <= 3
CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
vectorSetByIndex(RegArgNrwVec vector, float number,
				 uint_least64_t elementIndex) noexcept;
//! \brief Set the specified component of an int32 vector.
//!
//! \details Set the int32 stored at the specified location within
//! the int32 vector. Index is treated as an offset from the first
//! (x) element, and thus is within the range [0, 3]. Indices will be
//! masked such that all indices are within this range.
//!
//! \returns The new int32 vector, with the specified component
//! suitably altered
//!
//! \param[in] vector The int32 vector to alter
//!
//! \param[in] number The int32 to place at the specified location
//! within the vector.
//!
//! \param[in] elementIndex The index of the location to set, within
//! the vector. Will be masked to <= 3
CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
vectorSetByIndex(RegIntNrwVec vector, int_least32_t number,
				 uint_least64_t elementIndex) noexcept;
//! \brief Set the specified component of a double vector.
//!
//! \details Set the double stored at the specified location within
//! the double vector. Index is treated as an offset from the first
//! (x) element, and thus is within the range [0, 3]. Indices will be
//! masked such that all indices are within this range.
//!
//! \returns The new double vector, with the specified component
//! suitably altered
//!
//! \param[in] vector The double vector to alter
//!
//! \param[in] number The double to place at the specified location
//! within the vector.
//!
//! \param[in] elementIndex The index of the location to set, within
//! the vector. Will be masked to <= 3
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSetByIndex(RegArgVector vector, double number,
				 uint_least64_t elementIndex) noexcept;
//! \brief Get the specified component of a int vector.
//!
//! \details Get the int stored at the specified location within the
//! int vector. Index is treated as an offset from the first (x)
//! element, and thus is within the range [0, 3]. Indices will be
//! masked such that all indices are within this range.
//!
//! \returns The new int vector, with the specified component suitably
//! altered
//!
//! \param[in] vector The int vector to alter
//!
//! \param[in] number The int to place at the specified location
//! within the vector.
//!
//! \param[in] elementIndex The index of the location to set, within
//! the vector. Will be masked to <= 3
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorSetByIndex(RegIntVector vector, int_least64_t number,
				 uint_least64_t elementIndex) noexcept;

//! \brief Get the x component of a float vector
//!
//! \details Get the component, denoted x, located at the start of the
//! vector. The first element.
//!
//! \returns A float with the value of the X component of the given
//! float vector
//!
//! \param[in] vector The vector from which to get the x component
CETIOPTER_LIB_API float FRAO_VECTORCALL
vectorGetX(RegArgNrwVec vector) noexcept;
//! \brief Get the x component of an int32 vector
//!
//! \details Get the component, denoted x, located at the start of the
//! vector. The first element.
//!
//! \returns A int32 with the value of the X component of the given
//! int32 vector
//!
//! \param[in] vector The vector from which to get the x component
CETIOPTER_LIB_API int_least32_t FRAO_VECTORCALL
vectorGetX(RegIntNrwVec vector) noexcept;
//! \brief Get the x component of a double vector
//!
//! \details Get the component, denoted x, located at the start of the
//! vector. The first element.
//!
//! \returns A double with the value of the X component of the given
//! double vector
//!
//! \param[in] vector The vector from which to get the x component
CETIOPTER_LIB_API double FRAO_VECTORCALL
vectorGetX(RegArgVector vector) noexcept;
//! \brief Get the x component of an int vector
//!
//! \details Get the component, denoted x, located at the start of the
//! vector. The first element.
//!
//! \returns An int with the value of the X component of the given int
//! vector
//!
//! \param[in] vector The vector from which to get the x component
CETIOPTER_LIB_API int_least64_t FRAO_VECTORCALL
vectorGetX(RegIntVector vector) noexcept;
//! \brief Set the x component of a float vector
//!
//! \details Set the component, denoted x, located at the start of the
//! vector. The first element.
//!
//! \returns A float vector with the given value for the X component
//!
//! \param[in] vector The float vector to alter
//!
//! \param[in] number The float value to set for the x component of
//! the given vector
CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
vectorSetX(RegArgNrwVec vector, float number) noexcept;
//! \brief Set the x component of an int32 vector
//!
//! \details Set the component, denoted x, located at the end of the
//! vector. The fourth element.
//!
//! \returns A int32 vector with the given value for the x component
//!
//! \param[in] vector The int32 vector to alter
//!
//! \param[in] number The int32 value to set for the x component of
//! the given vector
CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
vectorSetX(RegIntNrwVec vector, int_least32_t number) noexcept;
//! \brief Set the x component of a double vector
//!
//! \details Set the component, denoted x, located at the start of the
//! vector. The first element.
//!
//! \returns A double vector with the given value for the X component
//!
//! \param[in] vector The double vector to alter
//!
//! \param[in] number The double value to set for the x component of
//! the given vector
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSetX(RegArgVector vector, double number) noexcept;
//! \brief Set the x component of an int vector
//!
//! \details Set the component, denoted x, located at the start of the
//! vector. The first element.
//!
//! \returns An int vector with the given value for the X component
//!
//! \param[in] vector The int vector to alter
//!
//! \param[in] number The int value to set for the x component of the
//! given vector
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorSetX(RegIntVector vector, int_least64_t number) noexcept;

//! \brief Get the y component of a float vector
//!
//! \details Get the component, denoted y, located immediately after
//! the start of the vector. The second element.
//!
//! \returns A float with the value of the y component of the given
//! float vector
//!
//! \param[in] vector The vector from which to get the y component
CETIOPTER_LIB_API float FRAO_VECTORCALL
vectorGetY(RegArgNrwVec vector) noexcept;
//! \brief Get the y component of an int32 vector
//!
//! \details Get the component, denoted y, located at the start of the
//! vector. The first element.
//!
//! \returns A int32 with the value of the y component of the given
//! int32 vector
//!
//! \param[in] vector The vector from which to get the y component
CETIOPTER_LIB_API int_least32_t FRAO_VECTORCALL
vectorGetY(RegIntNrwVec vector) noexcept;
//! \brief Get the y component of a double vector
//!
//! \details Get the component, denoted y, located immediately after
//! the start of the vector. The second element.
//!
//! \returns A double with the value of the y component of the given
//! double vector
//!
//! \param[in] vector The vector from which to get the y component
CETIOPTER_LIB_API double FRAO_VECTORCALL
vectorGetY(RegArgVector vector) noexcept;
//! \brief Get the y component of an int vector
//!
//! \details Get the component, denoted y, located immediately after
//! the start of the vector. The second element.
//!
//! \returns An int with the value of the y component of the given int
//! vector
//!
//! \param[in] vector The vector from which to get the y component
CETIOPTER_LIB_API int_least64_t FRAO_VECTORCALL
vectorGetY(RegIntVector vector) noexcept;
//! \brief Set the y component of a float vector
//!
//! \details Set the component, denoted y, located immediately after
//! the start of the vector. The second element.
//!
//! \returns A float vector with the given value for the y component
//!
//! \param[in] vector The float vector to alter
//!
//! \param[in] number The float value to set for the y component of
//! the given vector
CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
vectorSetY(RegArgNrwVec vector, float number) noexcept;
//! \brief Set the y component of an int32 vector
//!
//! \details Set the component, denoted y, located at the end of the
//! vector. The fourth element.
//!
//! \returns A int32 vector with the given value for the y component
//!
//! \param[in] vector The int32 vector to alter
//!
//! \param[in] number The int32 value to set for the y component of
//! the given vector
CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
vectorSetY(RegIntNrwVec vector, int_least32_t number) noexcept;
//! \brief Set the y component of a double vector
//!
//! \details Set the component, denoted y, located immediately after
//! the start of the vector. The second element.
//!
//! \returns A double vector with the given value for the y component
//!
//! \param[in] vector The double vector to alter
//!
//! \param[in] number The double value to set for the y component of
//! the given vector
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSetY(RegArgVector vector, double number) noexcept;
//! \brief Set the y component of an int vector
//!
//! \details Set the component, denoted y, located immediately after
//! the start of the vector. The second element.
//!
//! \returns An int vector with the given value for the y component
//!
//! \param[in] vector The int vector to alter
//!
//! \param[in] number The int value to set for the y component of the
//! given vector
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorSetY(RegIntVector vector, int_least64_t number) noexcept;

//! \brief Get the z component of a float vector
//!
//! \details Get the component, denoted z, located penultimately in
//! the vector. The third element.
//!
//! \returns A float with the value of the z component of the given
//! float vector
//!
//! \param[in] vector The vector from which to get the z component
CETIOPTER_LIB_API float FRAO_VECTORCALL
vectorGetZ(RegArgNrwVec vector) noexcept;
//! \brief Get the z component of an int32 vector
//!
//! \details Get the component, denoted z, located at the start of the
//! vector. The first element.
//!
//! \returns A int32 with the value of the z component of the given
//! int32 vector
//!
//! \param[in] vector The vector from which to get the z component
CETIOPTER_LIB_API int_least32_t FRAO_VECTORCALL
vectorGetZ(RegIntNrwVec vector) noexcept;
//! \brief Get the z component of a double vector
//!
//! \details Get the component, denoted z, located penultimately in
//! the vector. The third element.
//!
//! \returns A double with the value of the z component of the given
//! double vector
//!
//! \param[in] vector The vector from which to get the z component
CETIOPTER_LIB_API double FRAO_VECTORCALL
vectorGetZ(RegArgVector vector) noexcept;
//! \brief Get the z component of an int vector
//!
//! \details Get the component, denoted z, located penultimately in
//! the vector. The third element.
//!
//! \returns An int with the value of the z component of the given int
//! vector
//!
//! \param[in] vector The vector from which to get the z component
CETIOPTER_LIB_API int_least64_t FRAO_VECTORCALL
vectorGetZ(RegIntVector vector) noexcept;
//! \brief Set the z component of a float vector
//!
//! \details Set the component, denoted z, located penultimately in
//! the vector. The third element.
//!
//! \returns A float vector with the given value for the z component
//!
//! \param[in] vector The float vector to alter
//!
//! \param[in] number The float value to set for the z component of
//! the given vector
CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
vectorSetZ(RegArgNrwVec vector, float number) noexcept;
//! \brief Set the z component of an int32 vector
//!
//! \details Set the component, denoted z, located at the end of the
//! vector. The fourth element.
//!
//! \returns A int32 vector with the given value for the z component
//!
//! \param[in] vector The int32 vector to alter
//!
//! \param[in] number The int32 value to set for the z component of
//! the given vector
CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
vectorSetZ(RegIntNrwVec vector, int_least32_t number) noexcept;
//! \brief Set the z component of a double vector
//!
//! \details Set the component, denoted z, located penultimately in
//! the vector. The third element.
//!
//! \returns A double vector with the given value for the z component
//!
//! \param[in] vector The double vector to alter
//!
//! \param[in] number The double value to set for the z component of
//! the given vector
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSetZ(RegArgVector vector, double number) noexcept;
//! \brief Set the z component of an int vector
//!
//! \details Set the component, denoted z, located penultimately in
//! the vector. The third element.
//!
//! \returns An int vector with the given value for the z component
//!
//! \param[in] vector The int vector to alter
//!
//! \param[in] number The int value to set for the z component of the
//! given vector
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorSetZ(RegIntVector vector, int_least64_t number) noexcept;

//! \brief Get the w component of a float vector
//!
//! \details Get the component, denoted w, located at the end of the
//! vector. The first element.
//!
//! \returns A float with the value of the w component of the given
//! float vector
//!
//! \param[in] vector The vector from which to get the w component
CETIOPTER_LIB_API float FRAO_VECTORCALL
vectorGetW(RegArgNrwVec vector) noexcept;
//! \brief Get the w component of an int32 vector
//!
//! \details Get the component, denoted w, located at the start of the
//! vector. The first element.
//!
//! \returns A int32 with the value of the w component of the given
//! int32 vector
//!
//! \param[in] vector The vector from which to get the w component
CETIOPTER_LIB_API int_least32_t FRAO_VECTORCALL
vectorGetW(RegIntNrwVec vector) noexcept;
//! \brief Get the w component of a double vector
//!
//! \details Get the component, denoted w, located at the end of the
//! vector. The first element.
//!
//! \returns A double with the value of the w component of the given
//! double vector
//!
//! \param[in] vector The vector from which to get the w component
CETIOPTER_LIB_API double FRAO_VECTORCALL
vectorGetW(RegArgVector vector) noexcept;
//! \brief Get the w component of an int vector
//!
//! \details Get the component, denoted w, located at the end of the
//! vector. The first element.
//!
//! \returns An int with the value of the w component of the given int
//! vector
//!
//! \param[in] vector The vector from which to get the w component
CETIOPTER_LIB_API int_least64_t FRAO_VECTORCALL
vectorGetW(RegIntVector vector) noexcept;
//! \brief Set the w component of a float vector
//!
//! \details Set the component, denoted w, located at the end of the
//! vector. The fourth element.
//!
//! \returns A float vector with the given value for the w component
//!
//! \param[in] vector The float vector to alter
//!
//! \param[in] number The float value to set for the w component of
//! the given vector
CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
vectorSetW(RegArgNrwVec vector, float number) noexcept;
//! \brief Set the w component of an int32 vector
//!
//! \details Set the component, denoted w, located at the end of the
//! vector. The fourth element.
//!
//! \returns A int32 vector with the given value for the w component
//!
//! \param[in] vector The int32 vector to alter
//!
//! \param[in] number The int32 value to set for the w component of
//! the given vector
CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
vectorSetW(RegIntNrwVec vector, int_least32_t number) noexcept;
//! \brief Set the w component of a double vector
//!
//! \details Set the component, denoted w, located at the end of the
//! vector. The fourth element.
//!
//! \returns A double vector with the given value for the w component
//!
//! \param[in] vector The double vector to alter
//!
//! \param[in] number The double value to set for the w component of
//! the given vector
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSetW(RegArgVector vector, double number) noexcept;
//! \brief Set the w component of an int vector
//!
//! \details Set the component, denoted w, located at the end of the
//! vector. The fourth element.
//!
//! \returns An int vector with the given value for the w component
//!
//! \param[in] vector The int vector to alter
//!
//! \param[in] number The int value to set for the w component of the
//! given vector
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorSetW(RegIntVector vector, int_least64_t number) noexcept;

//! \brief Set all components of a float vector
//!
//! \returns A float vector with the specified components
//!
//! \param[in] x The float to set as the first component of the
//! resulting vector
//!
//! \param[in] y The float to set as the second component of the
//! resulting vector
//!
//! \param[in] z The float to set as the third component of the
//! resulting vector
//!
//! \param[in] w The float to set as the fourth component of the
//! resulting vector
CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
vectorSet(float x, float y, float z, float w) noexcept;
//! \brief Set all components of an int32 vector
//!
//! \returns A int32 vector with the specified components
//!
//! \param[in] x The int32 to set as the first component of the
//! resulting vector
//!
//! \param[in] y The int32 to set as the second component of the
//! resulting vector
//!
//! \param[in] z The int32 to set as the third component of the
//! resulting vector
//!
//! \param[in] w The int32 to set as the fourth component of the
//! resulting vector
CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
vectorSet(int_least32_t x, int_least32_t y, int_least32_t z,
		  int_least32_t w) noexcept;
//! \brief Set all components of a double vector
//!
//! \returns A double vector with the specified components
//!
//! \param[in] x The double to set as the first component of the
//! resulting vector
//!
//! \param[in] y The double to set as the second component of the
//! resulting vector
//!
//! \param[in] z The double to set as the third component of the
//! resulting vector
//!
//! \param[in] w The double to set as the fourth component of the
//! resulting vector
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSet(double x, double y, double z, double w) noexcept;
//! \brief Set all components of an int vector
//!
//! \returns An int vector with the specified components
//!
//! \param[in] x The int to set as the first component of the
//! resulting vector
//!
//! \param[in] y The int to set as the second component of the
//! resulting vector
//!
//! \param[in] z The int to set as the third component of the
//! resulting vector
//!
//! \param[in] w The int to set as the fourth component of the
//! resulting vector
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorSet(int_least64_t x, int_least64_t y, int_least64_t z,
		  int_least64_t w) noexcept;

//! @}

//! \defgroup MatrixGettersSetters Matrix getters/setters
//!
//! \brief Functions that get or set a whole or a part of the matrix
//!
//! @{

//! \brief Get The specified row of the given matrix
//!
//! \details Get the row stored at the specified location within the
//! matrix. Index is treated as an offset from the first row, and thus
//! is within the range [0, 3]. Indices will be masked such that all
//! indices are within this range.
//!
//! \returns A vector with with components equal to that of the
//! specified row
//!
//! \param[in] matrix The matrix from which to extract the specified
//! row
//!
//! \param[in] index The index, in [0, 3], of the row to extract. The
//! top row is numbered: 0, and the bottom: 3. Index will be masked to
//! the valid [0, 3] range
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
matrixGetRow(RegArgMatrix matrix, uint_least64_t index) noexcept;
//! \brief Set The specified row of the given matrix
//!
//! \details Set the row stored at the specified location within the
//! matrix. Index is treated as an offset from the first row, and thus
//! is within the range [0, 3]. Indices will be masked such that all
//! indices are within this range.
//!
//! \returns A matrix based on the input matrix, with the specified
//! row replaced.
//!
//! \param[in] matrix The matrix which should be altered
//!
//! \param[in] row The row that should replace the specified one in
//! the matrix
//!
//! \param[in] index The index, in [0, 3], of the row to replace. The
//! top row is numbered: 0, and the bottom: 3. Index will be masked to
//! the valid [0, 3] range
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrixSetRow(RegArgMatrix matrix, RegArgVector row,
			 uint_least64_t index) noexcept;
//! \brief Set The specified column of the given matrix
//!
//! \details Set the column stored at the specified location within
//! the matrix. Index is treated as an offset from the first column,
//! and thus is within the range [0, 3]. Indices will be masked such
//! that all indices are within this range.
//!
//! \returns A matrix based on the input matrix, with the specified
//! column replaced.
//!
//! \param[in] matrix The matrix which should be altered
//!
//! \param[in] column The column that should replace the specified one
//! in the matrix
//!
//! \param[in] index The index, in [0, 3], of the column to replace.
//! The top column is numbered: 0, and the bottom: 3. Index will be
//! masked to the valid [0, 3] range
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrixSetColumn(RegArgMatrix matrix, RegArgVector column,
				uint_least64_t index) noexcept;

//! \brief Create a matrix based on the given elements
//!
//! \returns A matrix with the specified components
//!
//! \param[in] r1_c1 The value to place at row 1, column 1 of the
//! matrix
//!
//! \param[in] r1_c2 The value to place at row 1, column 2 of the
//! matrix
//!
//! \param[in] r1_c3 The value to place at row 1, column 3 of the
//! matrix
//!
//! \param[in] r1_c4 The value to place at row 1, column 4 of the
//! matrix
//!
//! \param[in] r2_c1 The value to place at row 2, column 1 of the
//! matrix
//!
//! \param[in] r2_c2 The value to place at row 2, column 2 of the
//! matrix
//!
//! \param[in] r2_c3 The value to place at row 2, column 3 of the
//! matrix
//!
//! \param[in] r2_c4 The value to place at row 2, column 4 of the
//! matrix
//!
//! \param[in] r3_c1 The value to place at row 3, column 1 of the
//! matrix
//!
//! \param[in] r3_c2 The value to place at row 3, column 2 of the
//! matrix
//!
//! \param[in] r3_c3 The value to place at row 3, column 3 of the
//! matrix
//!
//! \param[in] r3_c4 The value to place at row 3, column 4 of the
//! matrix
//!
//! \param[in] r4_c1 The value to place at row 4, column 1 of the
//! matrix
//!
//! \param[in] r4_c2 The value to place at row 4, column 2 of the
//! matrix
//!
//! \param[in] r4_c3 The value to place at row 4, column 3 of the
//! matrix
//!
//! \param[in] r4_c4 The value to place at row 4, column 4 of the
//! matrix
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL matrixSet(
	double r1_c1, double r1_c2, double r1_c3, double r1_c4,
	double r2_c1, double r2_c2, double r2_c3, double r2_c4,
	double r3_c1, double r3_c2, double r3_c3, double r3_c4,
	double r4_c1, double r4_c2, double r4_c3, double r4_c4) noexcept;
//! \brief Create a matrix based on the given rows
//!
//! \returns A matrix with the specified rows
//!
//! \param[in] row1 The row to have as the first row of the matrix
//!
//! \param[in] row2 The row to have as the second row of the matrix
//!
//! \param[in] row3 The row to have as the third row of the matrix
//!
//! \param[in] row4 The row to have as the fourth row of the matrix
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrixSet(RegArgVector row1, RegArgVector row2, RegArgVector row3,
		  RegArgVector row4) noexcept;
//! \brief Create a matrix based on the given columns
//!
//! \returns A matrix with the specified columns
//!
//! \param[in] column1 The column to have as the first column of the
//! matrix
//!
//! \param[in] column2 The column to have as the second column of the
//! matrix
//!
//! \param[in] column3 The column to have as the third column of the
//! matrix
//!
//! \param[in] column4 The column to have as the fourth column of the
//! matrix
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL matrixSetByColumn(
	RegArgVector column1, RegArgVector column2, RegArgVector column3,
	RegArgVector column4) noexcept;

//! @}

//! \defgroup VectorReorderFunctions Vector reorder functions
//!
//! \brief Functions that rearrange values among components of a
//! vector
//!
//! @{

//! \brief Gets a double vector with each component the value
//! specified
//!
//! \returns A double vector, with each component equal to the
//! specified value
//!
//! \param[in] value The double to replicate across the vector
CETIOPTER_LIB_API WideVector vectorReplicate(double value) noexcept;
//! \brief Gets an int vector with each component the value specified
//!
//! \returns An int vector, with each component equal to the specified
//! value
//!
//! \param[in] value The int to replicate across the vector
CETIOPTER_LIB_API WideIntVector
vectorReplicate(int_least64_t value) noexcept;

//! \brief Replicate the x component of the vector across all
//! components of the result
//!
//! \returns A double vector with every component equal to the x
//! component of the input
//!
//! \param[in] vector The double vector whose x component should be
//! replicated
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSplatX(RegArgVector vector) noexcept;
//! \brief Replicate the x component of the vector across all
//! components of the result
//!
//! \returns An int vector with every component equal to the x
//! component of the input
//!
//! \param[in] vector The int vector whose x component should be
//! replicated
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorSplatX(RegIntVector vector) noexcept;

//! \brief Replicate the y component of the vector across all
//! components of the result
//!
//! \returns A double vector with every component equal to the y
//! component of the input
//!
//! \param[in] vector The double vector whose y component should be
//! replicated
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSplatY(RegArgVector vector) noexcept;
//! \brief Replicate the y component of the vector across all
//! components of the result
//!
//! \returns An int vector with every component equal to the y
//! component of the input
//!
//! \param[in] vector The int vector whose y component should be
//! replicated
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorSplatY(RegIntVector vector) noexcept;

//! \brief Replicate the z component of the vector across all
//! components of the result
//!
//! \returns A double vector with every component equal to the z
//! component of the input
//!
//! \param[in] vector The double vector whose z component should be
//! replicated
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSplatZ(RegArgVector vector) noexcept;
//! \brief Replicate the z component of the vector across all
//! components of the result
//!
//! \returns An int vector with every component equal to the z
//! component of the input
//!
//! \param[in] vector The int vector whose z component should be
//! replicated
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorSplatZ(RegIntVector vector) noexcept;

//! \brief Replicate the w component of the vector across all
//! components of the result
//!
//! \returns A double vector with every component equal to the w
//! component of the input
//!
//! \param[in] vector The double vector whose w component should be
//! replicated
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSplatW(RegArgVector vector) noexcept;
//! \brief Replicate the w component of the vector across all
//! components of the result
//!
//! \returns An int vector with every component equal to the w
//! component of the input
//!
//! \param[in] vector The int vector whose w component should be
//! replicated
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorSplatW(RegIntVector vector) noexcept;

//! \brief Contruct a result vector consisting of bits from the lhs
//! and rhs small int vectors, as specified by the control vector
//!
//! \details The control vector will be checked, with 1 bits meaning
//! the corresponding bit is taken from the rhs vector, and a 0
//! meaning that the bit is taken from the lhs. Conceptually
//! equivalent to "result = (lhs & ~control) | (rhs & control)"
//!
//! \returns A small int vector whose bits will be as selected by the
//! control vector
//!
//! \param[in] lhsVector The lhs small int vector, whose bits will be
//! selected by a 0 bit in the control vector
//!
//! \param[in] rhsVector The rhs small int vector, whose bits will be
//! selected by a 1 bit in the control vector
//!
//! \param[in] control The control vector, whose bits control whether
//! the corresponding bit in result is from lhsVector or rhsVector
CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
vectorSelect(RegIntNrwVec lhsVector, RegIntNrwVec rhsVector,
			 RegIntNrwVec control) noexcept;
//! \brief Contruct a result vector consisting of bits from the lhs
//! and rhs int vectors, as specified by the control vector
//!
//! \details The control vector will be checked, with 1 bits meaning
//! the corresponding bit is taken from the rhs vector, and a 0
//! meaning that the bit is taken from the lhs. Conceptually
//! equivalent to "result = (lhs & ~control) | (rhs & control)"
//!
//! \returns An int vector whose bits will be as selected by the
//! control vector
//!
//! \param[in] lhsVector The lhs int vector, whose bits will be
//! selected by a 0 bit in the control vector
//!
//! \param[in] rhsVector The rhs int vector, whose bits will be
//! selected by a 1 bit in the control vector
//!
//! \param[in] control The control vector, whose bits control whether
//! the corresponding bit in result is from lhsVector or rhsVector
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorSelect(RegIntVector lhsVector, RegIntVector rhsVector,
			 RegIntVector control) noexcept;
//! \brief Contruct a result vector consisting of bits from the lhs
//! and rhs double vectors, as specified by the control vector
//!
//! \details The control vector will be checked, with 1 bits meaning
//! the corresponding bit is taken from the rhs vector, and a 0
//! meaning that the bit is taken from the lhs. Conceptually
//! equivalent to "result = (lhs & ~control) | (rhs & control)"
//!
//! \returns An double vector whose bits will be as selected by the
//! control vector
//!
//! \param[in] lhsVector The lhs double vector, whose bits will be
//! selected by a 0 bit in the control vector
//!
//! \param[in] rhsVector The rhs double vector, whose bits will be
//! selected by a 1 bit in the control vector
//!
//! \param[in] control The control vector, whose bits control whether
//! the corresponding bit in result is from lhsVector or rhsVector
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSelect(RegArgVector lhsVector, RegArgVector rhsVector,
			 RegIntVector control) noexcept;
//! \brief Construct a per-component selection vector. eg. for use in
//! vectorSelect
//!
//! \details Will create a control vector, with every bit of a
//! component set to true, if the corresponding parameter is, in the
//! input to this function. eg: calling this function with (true,
//! false, true, false) will result in all x and z bits being set to
//! 1, and all y and w bits being set to 0
//!
//! \returns A suitably formatted control vector, for use in functions
//! eg. vectorSelect
//!
//! \param[in] useXRhs Should be set to true if the rhs vector's x
//! component should be selected, and set to false for the lhsVector's
//! x component
//!
//! \param[in] useYRhs Should be set to true if the rhs vector's y
//! component should be selected, and set to false for the lhsVector's
//! y component
//!
//! \param[in] useZRhs Should be set to true if the rhs vector's z
//! component should be selected, and set to false for the lhsVector's
//! z component
//!
//! \param[in] useWRhs Should be set to true if the rhs vector's w
//! component should be selected, and set to false for the lhsVector's
//! w component
CETIOPTER_LIB_API WideIntVector vectorSelectControl(
	bool useXRhs, bool useYRhs, bool useZRhs, bool useWRhs) noexcept;
//! \brief Permute a single double vector, rearranging the elements as
//! required
//!
//! \details Passed int vector parameters should be numbers in the
//! range [0, 3], s.t [0] refers to x, [1] refers to y, etc. eg: (vec,
//! 2, 3, 0, 1) would result in a vector of the form (vec.z, vec.w,
//! vec.x, vec.y)
//!
//! \returns A suitably rearranged version of the input vector
//!
//! \param[in] vector The double vector to rearrange
//!
//! \param[in] choices Contains indices in the range [0, 3], to select
//! which component of the input becomes the component, in the place
//! of the result. For example, input of (3, 0, 0, 0) would result in
//! a vector of (vector.w, vector.x, vector.x, vector.x)
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSwizzle(RegArgVector vector, RegIntNrwVec choices) noexcept;
//! \brief Permute a single double vector, rearranging the elements as
//! required
//!
//! \details Passed non-vector parameters should be numbers in the
//! range [0, 3], s.t [0] refers to x, [1] refers to y, etc. eg: (vec,
//! 2, 3, 0, 1) would result in a vector of the form (vec.z, vec.w,
//! vec.x, vec.y)
//!
//! \returns A suitably rearranged version of the input vector
//!
//! \param[in] vector The double vector to rearrange
//!
//! \param[in] xChoice An index in the range [0, 3], to select which
//! component of the input becomes the x component of the result
//!
//! \param[in] yChoice An index in the range [0, 3], to select which
//! component of the input becomes the y component of the result
//!
//! \param[in] zChoice An index in the range [0, 3], to select which
//! component of the input becomes the z component of the result
//!
//! \param[in] wChoice An index in the range [0, 3], to select which
//! component of the input becomes the w component of the result
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL vectorSwizzle(
	RegArgVector vector, uint_least8_t xChoice, uint_least8_t yChoice,
	uint_least8_t zChoice, uint_least8_t wChoice) noexcept;
//! \brief Permute a single int vector, rearranging the elements as
//! required
//!
//! \details Passed small int vector parameters should be numbers in
//! the range [0, 3], s.t [0] refers to x, [1] refers to y, etc. eg:
//! (vec, 2, 3, 0, 1) would result in a vector of the form (vec.z,
//! vec.w, vec.x, vec.y)
//!
//! \returns A suitably rearranged version of the input vector
//!
//! \param[in] vector The int vector to rearrange
//!
//! \param[in] choices Contains indices in the range [0, 3], to select
//! which component of the input becomes the component, in the place
//! of the result. For example, input of (3, 0, 0, 0) would result in
//! a vector of (vector.w, vector.x, vector.x, vector.x)
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorSwizzle(RegIntVector vector, RegIntNrwVec choices) noexcept;
//! \brief Permute a single int vector, rearranging the elements as
//! required
//!
//! \details Passed non-vector parameters should be numbers in the
//! range [0, 3], s.t [0] refers to x, [1] refers to y, etc. eg: (vec,
//! 2, 3, 0, 1) would result in a vector of the form (vec.z, vec.w,
//! vec.x, vec.y)
//!
//! \returns A suitably rearranged version of the input vector
//!
//! \param[in] vector The int vector to rearrange
//!
//! \param[in] xChoice An index in the range [0, 3], to select which
//! component of the input becomes the x component of the result
//!
//! \param[in] yChoice An index in the range [0, 3], to select which
//! component of the input becomes the y component of the result
//!
//! \param[in] zChoice An index in the range [0, 3], to select which
//! component of the input becomes the z component of the result
//!
//! \param[in] wChoice An index in the range [0, 3], to select which
//! component of the input becomes the w component of the result
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL vectorSwizzle(
	RegIntVector vector, uint_least8_t xChoice, uint_least8_t yChoice,
	uint_least8_t zChoice, uint_least8_t wChoice) noexcept;

//! \brief Permute two double vectors, rearranging the elements as
//! required
//!
//! \details Passed non-vector parameters should be number in the
//! range [0, 7], s.t [0-3] refers to x-w in lhs, [4-7] refers to x-w
//! in rhs, etc. eg: (vec, 7, 5, 2, 0) would result in a vector of
//! the form (rhs.w, rhs.y, lhs.z, lhs.x)
//!
//! \returns A suitably rearranged combination of the input vectors
//!
//! \param[in] lhsVector The left hand side double vector to rearrange
//!
//! \param[in] rhsVector The right hand side double vector to
//! rearrange
//!
//! \param[in] choices Contains indices in the range [0, 7], to select
//! which component of which input becomes the x component of the
//! result. [0, 3] for lhs components, [4, 7] for rhs components
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorPermute(RegArgVector lhsVector, RegArgVector rhsVector,
			  RegIntNrwVec choices) noexcept;
//! \brief Permute two double vectors, rearranging the elements as
//! required
//!
//! \details Passed non-vector parameters should be number in the
//! range [0, 7], s.t [0-3] refers to x-w in lhs, [4-7] refers to x-w
//! in rhs, etc. eg: (vec, 7, 5, 2, 0) would result in a vector of
//! the form (rhs.w, rhs.y, lhs.z, lhs.x)
//!
//! \returns A suitably rearranged combination of the input vectors
//!
//! \param[in] lhsVector The left hand side double vector to rearrange
//!
//! \param[in] rhsVector The right hand side double vector to
//! rearrange
//!
//! \param[in] xChoice An index in the range [0, 7], to select which
//! component of which input becomes the x component of the result.
//! [0, 3] for lhs components, [4, 7] for rhs components
//!
//! \param[in] yChoice An index in the range [0, 7], to select which
//! component of which input becomes the y component of the result.
//! [0, 3] for lhs components, [4, 7] for rhs components
//!
//! \param[in] zChoice An index in the range [0, 7], to select which
//! component of which input becomes the z component of the result.
//! [0, 3] for lhs components, [4, 7] for rhs components
//!
//! \param[in] wChoice An index in the range [0, 7], to select which
//! component of which input becomes the w component of the result.
//! [0, 3] for lhs components, [4, 7] for rhs components
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorPermute(RegArgVector lhsVector, RegArgVector rhsVector,
			  uint_least8_t xChoice, uint_least8_t yChoice,
			  uint_least8_t zChoice, uint_least8_t wChoice) noexcept;
//! \brief Permute two int vectors, rearranging the elements as
//! required
//!
//! \details Passed small int parameters should be number in the
//! range [0, 7], s.t [0-3] refers to x-w in lhs, [4-7] refers to x-w
//! in rhs, etc. eg: (vec, 7, 5, 2, 0) would result in a vector of
//! the form (rhs.w, rhs.y, lhs.z, lhs.x)
//!
//! \returns A suitably rearranged combination of the input vectors
//!
//! \param[in] lhsVector The left hand side int vector to rearrange
//!
//! \param[in] rhsVector The right hand side int vector to
//! rearrange
//!
//! \param[in] choices Contains indices in the range [0, 7], to select
//! which component of which input becomes the x component of the
//! result. [0, 3] for lhs components, [4, 7] for rhs components
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorPermute(RegIntVector lhsVector, RegIntVector rhsVector,
			  RegIntNrwVec choices) noexcept;
//! \brief Permute two int vectors, rearranging the elements as
//! required
//!
//! \details Passed non-vector parameters should be number in the
//! range [0, 7], s.t [0-3] refers to x-w in lhs, [4-7] refers to x-w
//! in rhs, etc. eg: (vec, 7, 5, 2, 0) would result in a vector of
//! the form (rhs.w, rhs.y, lhs.z, lhs.x)
//!
//! \returns A suitably rearranged combination of the input vectors
//!
//! \param[in] lhsVector The left hand side int vector to rearrange
//!
//! \param[in] rhsVector The right hand side int vector to rearrange
//!
//! \param[in] xChoice An index in the range [0, 7], to select which
//! component of which input becomes the x component of the result.
//! [0, 3] for lhs components, [4, 7] for rhs components
//!
//! \param[in] yChoice An index in the range [0, 7], to select which
//! component of which input becomes the y component of the result.
//! [0, 3] for lhs components, [4, 7] for rhs components
//!
//! \param[in] zChoice An index in the range [0, 7], to select which
//! component of which input becomes the z component of the result.
//! [0, 3] for lhs components, [4, 7] for rhs components
//!
//! \param[in] wChoice An index in the range [0, 7], to select which
//! component of which input becomes the w component of the result.
//! [0, 3] for lhs components, [4, 7] for rhs components
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorPermute(RegIntVector lhsVector, RegIntVector rhsVector,
			  uint_least8_t xChoice, uint_least8_t yChoice,
			  uint_least8_t zChoice, uint_least8_t wChoice) noexcept;

//! \brief Move each element in a double vector 'left', in order,
//! wrapping end around
//!
//! \details Move each element left, with left meaning towards the
//! lower index. So a vector (x, y, z, w) if moved left 1, becomes (y,
//! z, w, x). ie: y -> x, z -> y, w -> z, x -> w
//!
//! \returns The result vector, suitably rotated
//!
//! \param[in] vector The double vector to rotate
//!
//! \param[in] numElements The number of 'places' by which to move a
//! component. So a vector (x, y, z, w) if moved left 1, becomes (y,
//! z, w, x). Will be masked to the range [0, 3]
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL vectorRotateLeft(
	RegArgVector vector, uint_least8_t numElements) noexcept;
//! \brief Move each element in an int vector 'left', in order,
//! wrapping end around
//!
//! \details Move each element left, with left meaning towards the
//! lower index. So a vector (x, y, z, w) if moved left 1, becomes (y,
//! z, w, x). ie: y -> x, z -> y, w -> z, x -> w
//!
//! \returns The result vector, suitably rotated
//!
//! \param[in] vector The int vector to rotate
//!
//! \param[in] numElements The number of 'places' by which to move a
//! component. So a vector (x, y, z, w) if moved left 1, becomes (y,
//! z, w, x). Will be masked to the range [0, 3]
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL vectorRotateLeft(
	RegIntVector vector, uint_least8_t numElements) noexcept;

//! \brief Move each element in a double vector 'right', in order,
//! wrapping end around
//!
//! \details Move each element right, with right meaning towards the
//! higher index. So a vector (x, y, z, w) if moved right 1, becomes
//! (w, x, y, z). ie: x -> y, y -> z, z -> w, w -> x
//!
//! \returns The result vector, suitably rotated
//!
//! \param[in] vector The double vector to rotate
//!
//! \param[in] numElements The number of 'places' by which to move a
//! component. So a vector (x, y, z, w) if moved right 1, becomes (w,
//! x, y, z). Will be masked to the range [0, 3]
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL vectorRotateRight(
	RegArgVector vector, uint_least8_t numElements) noexcept;
//! \brief Move each element in an int vector 'right', in order,
//! wrapping end around
//!
//! \details Move each element right, with right meaning towards the
//! higher index. So a vector (x, y, z, w) if moved right 1, becomes
//! (w, x, y, z). ie: x -> y, y -> z, z -> w, w -> x
//!
//! \returns The result vector, suitably rotated
//!
//! \param[in] vector The int vector to rotate
//!
//! \param[in] numElements The number of 'places' by which to move a
//! component. So a vector (x, y, z, w) if moved right 1, becomes (w,
//! x, y, z). Will be masked to the range [0, 3]
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL vectorRotateRight(
	RegIntVector vector, uint_least8_t numElements) noexcept;

//! \brief Merge the x and y components of two double vectors into
//! one, lhs component first.
//!
//! \returns A suitably merged vector, of the form (lhs.x, rhs.x,
//! lhs.y, rhs.y)
//!
//! \param[in] lhsVector The left hand side vector to merge
//!
//! \param[in] rhsVector The right hand side vector to merge
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL vectorMergeXY(
	RegArgVector lhsVector, RegArgVector rhsVector) noexcept;
//! \brief Merge the x and y components of two int vectors into one,
//! lhs component first.
//!
//! \returns A suitably merged vector, of the form (lhs.x, rhs.x,
//! lhs.y, rhs.y)
//!
//! \param[in] lhsVector The left hand side vector to merge
//!
//! \param[in] rhsVector The right hand side vector to merge
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL vectorMergeXY(
	RegIntVector lhsVector, RegIntVector rhsVector) noexcept;
//! \brief Merge the z and w components of two double vectors into
//! one, lhs component first.
//!
//! \returns A suitably merged vector, of the form (lhs.z, rhs.z,
//! lhs.w, rhs.w)
//!
//! \param[in] lhsVector The left hand side vector to merge
//!
//! \param[in] rhsVector The right hand side vector to merge
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL vectorMergeZW(
	RegArgVector lhsVector, RegArgVector rhsVector) noexcept;
//! \brief Merge the z and w components of two int vectors into one,
//! lhs component first.
//!
//! \returns A suitably merged vector, of the form (lhs.z, rhs.z,
//! lhs.w, rhs.w)
//!
//! \param[in] lhsVector The left hand side vector to merge
//!
//! \param[in] rhsVector The right hand side vector to merge
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL vectorMergeZW(
	RegIntVector lhsVector, RegIntVector rhsVector) noexcept;

//! @}

}  // namespace Maths
}  // namespace frao

#endif