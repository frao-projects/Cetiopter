#ifndef FRAO_CETIOPTER_4D_COMPARISONS
#define FRAO_CETIOPTER_4D_COMPARISONS

//! \file
//!
//! \brief Header of 4D comparisons between, and on, vectors and
//! matrices
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <cstdint>	//for integer types
#include "APIDefines.hpp"

namespace frao
{
namespace Maths
{
namespace IMPL
{
bool FRAO_VECTORCALL
result4DAllTrue(RegIntVector fullResult) noexcept;
bool FRAO_VECTORCALL
result4DAnyTrue(RegIntVector fullResult) noexcept;
}  // namespace IMPL

//! \defgroup Vector4DComparisons Vector 4D comparisons
//!
//! \brief Comparison functions that operate under the assumption that
//! the underlying data is 4D
//!
//! @{

//! \brief Compare two double vectors component by component, checking
//! if the left hand side was less than the right hand side for all 4D
//! components
//!
//! \returns True iff for all components, left hand side is less
//! than right hand side
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DLess(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, checking
//! if the left hand side was less than the right hand side for all 4D
//! components
//!
//! \returns True iff for all components, left hand side is less than
//! right hand side
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the less-than sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the less-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DLess(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was less than the right hand side for all 4D
//! components
//!
//! \returns True iff for all components, left hand side is less than
//! right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DLessRecord(uint_least64_t& record, RegArgVector lhsVec,
				   RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, recording
//! the result to an int64 variable, and checking if the left hand
//! side was less than the right hand side for all 4D components
//!
//! \returns True iff for all components, left hand side is less than
//! right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the less-than sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the less-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DLessRecord(uint_least64_t& record, RegIntVector lhsVec,
				   RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, checking
//! if the left hand side was less than or equal to the right hand
//! side for all 4D components
//!
//! \returns True iff for all components, left hand side is less than
//! or equal to right hand side
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DLessEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, checking
//! if the left hand side was less than or equal to the right hand
//! side for all 4D components
//!
//! \returns True iff for all components, left hand side is less than
//! or equal to right hand side
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DLessEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was less than or equal to the right hand side for
//! all 4D components
//!
//! \returns True iff for all components, left hand side is less than
//! or equal to right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DLessEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
						RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, recording
//! the result to an int64 variable, and checking if the left hand
//! side was less than or equal to the right hand side for all 4D
//! components
//!
//! \returns True iff for all components, left hand side is less than
//! or equal to right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on
//! the left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on
//! the right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DLessEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
						RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, checking
//! if the left hand side was greater than the right hand side for all
//! 4D components
//!
//! \returns True iff for all components, left hand side is greater
//! than right hand side
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the greater-than sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the greater-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DGreater(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, checking if
//! the left hand side was greater than the right hand side for all 4D
//! components
//!
//! \returns True iff for all components, left hand side is greater
//! than right hand side
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the greater-than sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the greater-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DGreater(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was greater than the right hand side for all 4D
//! components
//!
//! \returns True iff for all components, left hand side is greater
//! than right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the greater-than sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the greater-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DGreaterRecord(uint_least64_t& record, RegArgVector lhsVec,
					  RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, recording
//! the result to an int64 variable, and checking if the left hand
//! side was greater than the right hand side for all 4D components
//!
//! \returns True iff for all components, left hand side is greater
//! than right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the greater-than sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the greater-than sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DGreaterRecord(uint_least64_t& record, RegIntVector lhsVec,
					  RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, checking
//! if the left hand side was greater than or equal to the right hand
//! side for all 4D components
//!
//! \returns True iff for all components, left hand side is greater
//! than or equal to right hand side
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DGreaterEqual(
	RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, checking if
//! the left hand side was greater than or equal to the right hand
//! side for all 4D components
//!
//! \returns True iff for all components, left hand side is greater
//! than or equal to right hand side
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the greater-than-or-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the greater-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DGreaterEqual(
	RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was greater than or equal to the right hand side
//! for all 4D components
//!
//! \returns True iff for all components, left hand side is greater
//! than or equal to right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the greater-than-or-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the greater-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DGreaterEqualRecord(
	uint_least64_t& record, RegArgVector lhsVec,
	RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, recording
//! the result to an int64 variable, and checking if the left hand
//! side was greater than or equal to the right hand side for all 4D
//! components
//!
//! \returns True iff for all components, left hand side is greater
//! than or equal to right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on
//! the left hand side (lhs) of the greater-than-or-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on
//! the right hand side (rhs) of the greater-than-or-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DGreaterEqualRecord(
	uint_least64_t& record, RegIntVector lhsVec,
	RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, checking
//! if the left hand side was equal to the right hand side for all 4D
//! components
//!
//! \returns True iff for all components, left hand side is equal to
//! right hand side
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, checking if
//! the left hand side was equal to the right hand side for all 4D
//! components
//!
//! \returns True iff for all components, left hand side is equal to
//! right hand side
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was equal to the right hand side for all 4D
//! components
//!
//! \returns True iff for all components, left hand side is equal to
//! right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, recording
//! the result to an int64 variable, and checking if the left hand
//! side was equal to the right hand side for all 4D components
//!
//! \returns True iff for all components, left hand side is equal to
//! right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on
//! the left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on
//! the right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, checking
//! if the left hand side was not equal to the right hand side for all
//! 4D components
//!
//! \returns True iff for all components, left hand side is not equal
//! to right hand side
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the not-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the not-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DNotEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
// returns whether every 4D lhs component is not equal to its rhs
// counterpart
//! \brief Compare two int vectors component by component, checking if
//! the left hand side was not equal to the right hand side for all 4D
//! components
//!
//! \returns True iff for all components, left hand side is not equal
//! to right hand side
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the not-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the not-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DNotEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was not equal to the right hand side for all 4D
//! components
//!
//! \returns True iff for all components, left hand side is not equal
//! to right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the not-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the not-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DNotEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					   RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component,
//! recording the result to an int64 variable, and checking if the
//! left hand side was not equal to the right hand side for all 4D
//! components
//!
//! \returns True iff for all components, left hand side is not equal
//! to right hand side
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on
//! the left hand side (lhs) of the not-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on
//! the right hand side (rhs) of the not-equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DNotEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					   RegIntVector rhsVec) noexcept;

//! \brief Compare three double vectors component by component,
//! checking if for the corresponding operand components, the checked
//! vector was greater than or equal to the lower bound, and less than
//! or equal to the higher bound for all 4D components
//!
//! \returns True iff for all components, the corresponding checked
//! component was inclusively within the specified bounds.
//!
//! \param[in] checked The double vector of components to compare
//! against both bounds
//!
//! \param[in] lowerBound The double vector of components to against
//! the checked component, to determine whether the checked component
//! is greater than or equal to the corresponding component of
//! lowerBound
//!
//! \param[in] upperBound The double vector of components to against
//! the checked component, to determine whether the checked component
//! is less than or equal to the corresponding component of upperBound
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DInBounds(RegArgVector checked, RegArgVector lowerBound,
				 RegArgVector upperBound) noexcept;
//! \brief Compare three int vectors component by component,
//! checking if for the corresponding operand components, the checked
//! vector was greater than or equal to the lower bound, and less than
//! or equal to the higher bound for all 4D components
//!
//! \returns True iff for all components, the corresponding checked
//! component was inclusively within the specified bounds.
//!
//! \param[in] checked The int vector of components to compare
//! against both bounds
//!
//! \param[in] lowerBound The int vector of components to against
//! the checked component, to determine whether the checked component
//! is greater than or equal to the corresponding component of
//! lowerBound
//!
//! \param[in] upperBound The int vector of components to against
//! the checked component, to determine whether the checked component
//! is less than or equal to the corresponding component of upperBound
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DInBounds(RegIntVector checked, RegIntVector lowerBound,
				 RegIntVector upperBound) noexcept;
//! \brief Compare three double vectors component by component,
//! recording the result to an int64 variable, checking if for the
//! corresponding operand components, the checked vector was greater
//! than or equal to the lower bound, and less than or equal to the
//! higher bound for all 4D components
//!
//! \returns True iff for all components, the corresponding checked
//! component was inclusively within the specified bounds.
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] checked The double vector of components to compare
//! against both bounds
//!
//! \param[in] lowerBound The double vector of components to against
//! the checked component, to determine whether the checked component
//! is greater than or equal to the corresponding component of
//! lowerBound
//!
//! \param[in] upperBound The double vector of components to against
//! the checked component, to determine whether the checked component
//! is less than or equal to the corresponding component of upperBound
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DInBoundsRecord(
	uint_least64_t& record, RegArgVector checked,
	RegArgVector lowerBound, RegArgVector upperBound) noexcept;
//! \brief Compare three int vectors component by component,
//! recording the result to an int64 variable, checking if for the
//! corresponding operand components, the checked vector was greater
//! than or equal to the lower bound, and less than or equal to the
//! higher bound for all 4D components
//!
//! \returns True iff for all components, the corresponding checked
//! component was inclusively within the specified bounds.
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] checked The int vector of components to compare against
//! both bounds
//!
//! \param[in] lowerBound The int vector of components to against the
//! checked component, to determine whether the checked component is
//! greater than or equal to the corresponding component of lowerBound
//!
//! \param[in] upperBound The int vector of components to against the
//! checked component, to determine whether the checked component is
//! less than or equal to the corresponding component of upperBound
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DInBoundsRecord(
	uint_least64_t& record, RegIntVector checked,
	RegIntVector lowerBound, RegIntVector upperBound) noexcept;

//! \brief Check each 4D component of a vector for NaN, and return
//! true iff any of them are
//!
//! \returns True iff any of the components was a type of NaN
//!
//! \param[in] vector The vector whose components should be checked vs
//! NaN
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DIsNaN(RegArgVector vector) noexcept;
//! \brief Check each 4D component of a vector for NaN, return true
//! iff any of them are, and record the result to an int
//!
//! \returns True iff any of the components was a type of NaN
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] vector The vector whose components should be checked vs
//! NaN
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DIsNaNRecord(
	uint_least64_t& record, RegArgVector vector) noexcept;

//! \brief Check each 4D component of a vector for infinities, and
//! return true iff any of them are
//!
//! \returns True iff any of the components was a type of inf
//!
//! \param[in] vector The vector whose components should be checked vs
//! inf
CETIOPTER_LIB_API bool FRAO_VECTORCALL
vector4DIsInf(RegArgVector vector) noexcept;
//! \brief Check each 4D component of a vector for infinities, return
//! true iff any of them are, and record the result to an int
//!
//! \returns True iff any of the components was a type of inf
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] vector The vector whose components should be checked vs
//! inf
CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DIsInfRecord(
	uint_least64_t& record, RegArgVector vector) noexcept;

//! @}

//! \defgroup Matrix4DComparisons Matrix 4D comparisons
//!
//! \brief Comparison functions that operate on matrices under the
//! assumption that the underlying data is 4D
//!
//! @{

//! \brief Compare two 4x4 matrices component by component, checking
//! if the left hand side was equal to the right hand side
//!
//! \returns True iff for all components, left hand side is equal to
//! right hand side
//!
//! \param[in] lhsMatrix The double vector of components to compare on
//! the left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsMatrix The double vector of components to compare on
//! the right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL matrix4DEqual(
	RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept;
//! \brief Compare two 4x4 matrices component by component, checking
//! if the left hand side was not equal to the right hand side
//!
//! \returns True iff for all components, left hand side is not equal
//! to right hand side
//!
//! \param[in] lhsMatrix The double vector of components to compare on
//! the left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsMatrix The double vector of components to compare on
//! the right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API bool FRAO_VECTORCALL matrix4DNotEqual(
	RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept;

//! \brief Check each component of a 4x4 matrix for NaN, and return
//! true iff any of them are
//!
//! \returns True iff any component was a type of NaN
//!
//! \param[in] matrix The matrix whose components should be checked vs
//! NaN
CETIOPTER_LIB_API bool FRAO_VECTORCALL
matrix4DIsNaN(RegArgMatrix matrix) noexcept;
//! \brief Check each component of a 4x4 matrix for infinities, and
//! return true iff any of them are
//!
//! \returns True iff any component was a type of inf
//!
//! \param[in] matrix The matrix whose components should be checked vs
//! inf
CETIOPTER_LIB_API bool FRAO_VECTORCALL
matrix4DIsInf(RegArgMatrix matrix) noexcept;

//! @}
}  // namespace Maths
}  // namespace frao

#endif