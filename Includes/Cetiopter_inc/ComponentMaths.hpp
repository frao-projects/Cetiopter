#ifndef FRAO_CETIOPTER_COMPONENT_MATHS
#define FRAO_CETIOPTER_COMPONENT_MATHS

//! \file
//!
//! \brief Header of per-component and per-vector/matrix maths. That
//! is, maths which is not specifically 2/3/4D
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "APIDefines.hpp"

namespace frao
{
namespace Maths
{
//! \defgroup VectorBitFunctions Vector bit functions
//!
//! \brief Functions that operate on vectors per-bit
//!
//! @{

//! \brief Compute the bitwise AND of two small int vectors
//!
//! \returns A suitably-combined small int vector, the result of an
//! AND operation on the two operands
//!
//! \param[in] lhsVec The first of the two small int vector operands
//! to AND
//!
//! \param[in] rhsVec The second of the two small int vector operands
//! to AND
CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
vectorAnd(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept;
//! \brief Compute the bitwise AND of two int vectors
//!
//! \returns A suitably-combined int vector, the result of an AND
//! operation on the two operands
//!
//! \param[in] lhsVec The first of the two int vector operands to AND
//!
//! \param[in] rhsVec The second of the two int vector operands to AND
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorAnd(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;

//! \brief Compute the bitwise OR of two small int vectors
//!
//! \returns A suitably-combined small int vector, the result of an OR
//! operation on the two operands
//!
//! \param[in] lhsVec The first of the two small int vector operands
//! to OR
//!
//! \param[in] rhsVec The second of the two small int vector operands
//! to OR
CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
vectorOr(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept;
//! \brief Compute the bitwise OR of two int vectors
//!
//! \returns A suitably-combined int vector, the result of an OR
//! operation on the two operands
//!
//! \param[in] lhsVec The first of the two int vector operands to OR
//!
//! \param[in] rhsVec The second of the two int vector operands to OR
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorOr(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;

//! \brief Compute the bitwise XOR of two small int vectors
//!
//! \returns A suitably-combined small int vector, the result of an
//! XOR operation on the two operands
//!
//! \param[in] lhsVec The first of the two small int vector operands
//! to XOR
//!
//! \param[in] rhsVec The second of the two small int vector operands
//! to XOR
CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
vectorXor(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept;
//! \brief Compute the bitwise XOR of two int vectors
//!
//! \returns A suitably-combined int vector, the result of an XOR
//! operation on the two operands
//!
//! \param[in] lhsVec The first of the two int vector operands to XOR
//!
//! \param[in] rhsVec The second of the two int vector operands to XOR
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorXor(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;

//! \brief Compute the bitwise NOT of a small int vectors
//!
//! \returns A small int vector, the result of a NOT
//! operation on the operand
//!
//! \param[in] vector The small int vector operand to NOT
CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
vectorNot(RegIntNrwVec vector) noexcept;
//! \brief Compute the bitwise NOT of an int vectors
//!
//! \returns An int vector, the result of a NOT
//! operation on the operand
//!
//! \param[in] vector The int vector operand to NOT
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorNot(RegIntVector vector) noexcept;

//! @}

//! \defgroup VectorArithmetic Vector Arithmetic
//!
//! \brief Functions that concern basic vector maths operations
//!
//! @{

//! \brief Add two double vectors, lhs+rhs, component by component
//!
//! \returns A double vector, each of whose components are equal to
//! the sum of the respective operand components
//!
//! \param[in] lhsVec The first double vector operand
//!
//! \param[in] rhsVec The second double vector operand
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorAddition(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Add two small int vectors, lhs+rhs, component by component
//!
//! \returns A small int vector, each of whose components are equal to
//! the sum of the respective operand components
//!
//! \param[in] lhsVec The first small int vector operand
//!
//! \param[in] rhsVec The second small int vector operand
CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
vectorAddition(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept;
//! \brief Add two int vectors, lhs+rhs, component by component
//!
//! \returns An int vector, each of whose components are equal to
//! the sum of the respective operand components
//!
//! \param[in] lhsVec The first int vector operand
//!
//! \param[in] rhsVec The second int vector operand
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorAddition(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Subtract two double vectors, lhs-rhs, component by
//! component
//!
//! \returns A double vector, each of whose components are equal to
//! the difference of the respective operand components
//!
//! \param[in] lhsVec The lhs double vector operand. A vector of the
//! components conceptually to the left of the subtraction sign. The
//! amounts to be subtracted FROM
//!
//! \param[in] rhsVec The rhs double vector operand. A vector of the
//! components conceptually to the right of the subtraction sign. The
//! amounts TO BE subtracted
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSubtract(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Subtract two small int vectors, lhs-rhs, component by
//! component
//!
//! \returns A small int vector, each of whose components are equal to
//! the difference of the respective operand components
//!
//! \param[in] lhsVec The lhs small int vector operand. A vector of
//! the components conceptually to the left of the subtraction sign.
//! The amounts to be subtracted FROM
//!
//! \param[in] rhsVec The rhs small int vector operand. A vector of
//! the components conceptually to the right of the subtraction sign.
//! The amounts TO BE subtracted
CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
vectorSubtract(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept;
//! \brief Subtract two int vectors, lhs-rhs, component by
//! component
//!
//! \returns An int vector, each of whose components are equal to
//! the difference of the respective operand components
//!
//! \param[in] lhsVec The lhs int vector operand. A vector of the
//! components conceptually to the left of the subtraction sign. The
//! amounts to be subtracted FROM
//!
//! \param[in] rhsVec The rhs int vector operand. A  vector of the
//! components conceptually to the right of the subtraction sign. The
//! amounts TO BE subtracted
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorSubtract(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
// multiply two vectors, lhs*rhs, component by component
//! \brief Multiply two double vectors, lhs*rhs, component by
//! component
//!
//! \returns A double vector, each of whose components are equal to
//! the product of the respective operand components
//!
//! \param[in] lhsVec The first double vector operand
//!
//! \param[in] rhsVec The second double vector operand
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorMultiply(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Multiply two int vectors, lhs*rhs, component by component
//!
//! \returns An int vector, each of whose components are equal to
//! the product of the respective operand components
//!
//! \param[in] lhsVec The first int vector operand
//!
//! \param[in] rhsVec The second int vector operand
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorMultiply(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Divide two double vectors, lhs/rhs, component by
//! component
//!
//! \returns A double vector, each of whose components are equal to
//! the quotient of the respective operand components
//!
//! \param[in] dividendVec The lhs double vector operand. The
//! dividend. A vector of the components conceptually to the left of
//! the division sign. The amounts TO BE divided
//!
//! \param[in] divisorVec The rhs double vector operand. The divisor.
//! A vector of the components conceptually to the right of the
//! division sign. The amounts to divide BY
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL vectorDivide(
	RegArgVector dividendVec, RegArgVector divisorVec) noexcept;
//! \brief Divide two int vectors, lhs/rhs, component by
//! component
//!
//! \returns An int vector, each of whose components are equal to
//! the quotient of the respective operand components
//!
//! \param[in] dividendVec The lhs int vector operand. The dividend. A
//! vector of the components conceptually to the left of the division
//! sign. The amounts TO BE divided
//!
//! \param[in] divisorVec The rhs int vector operand. The divisor. A
//! vector of the components conceptually to the right of the division
//! sign. The amounts to divide BY
//!
//! \param[out] remRes The remainder, after the division has been
//! performed. To receive this, pass a valid ptr, and to ignore this,
//! pass nullptr.
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorDivide(RegIntVector dividendVec, RegIntVector divisorVec,
			 WideIntVector* remRes) noexcept;
//! \brief Calculate the per-component remaining value after an
//! integer division of the respective vectors
//!
//! \returns An int vector containing the remainders, after division
//! of the respective operand components
//!
//! \param[in] lhsVec The lhs int vector operand. The dividend of the
//! division to examine. A vector of the components conceptually to
//! the left of the division sign. The amounts TO BE divided, in the
//! examined division.
//!
//! \param[in] rhsVec The lhs int vector operand. The divisor of the
//! division to examine. A vector of the components conceptually to
//! the right of the division sign. The amounts to divide BY, in the
//! examined division.
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL vectorRemainder(
	RegIntVector dividendVec, RegIntVector divisorVec) noexcept;

//! \brief Scale each component of the specified vector, by the given
//! amount
//!
//! \returns A suitably multiplied version of the vector operand
//!
//! \param[in] vector The vector operand, whose components will each
//! by multiplied by the scale factor
//!
//! \param[in] scaleFactor The amount by which each vector component
//! should be multiplied
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorScale(RegArgVector vector, double scaleFactor) noexcept;
//! \brief Compute the square root of each component of the given
//! vector
//!
//! \returns A double vector containing the square roots of each
//! component of the operand
//!
//! \param[in] vector A double vector containing the doubles whose
//! square root should be found
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSqrt(RegArgVector vector) noexcept;
//! \brief For each component in the vector, multiply by -1
//!
//! \returns A operand vector, with each component negated
//!
//! \param[in] vector The vector to negate
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorNegate(RegArgVector vector) noexcept;

//! @}

//! \defgroup MatrixArithmetic Matrix Arthmetic
//!
//! \brief Functions that concern basic matrix maths operations
//!
//! @{

//! \brief Add two matrices, lhs+rhs, component by component
//!
//! \returns A matrix, each of whose components are equal to the sum
//! of the respective operand components
//!
//! \param[in] lhsMatrix The first matrix operand
//!
//! \param[in] rhsMatrix The second matrix operand
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL matrixAddition(
	RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept;
//! \brief Mutiply two matrices, lhs*rhs, by normal premultiplied
//! matrix multiplication
//!
//! \note This will premultiply the matrices in question. That is, if
//! we denote the first operand A, and the second B, we will arrive at
//! the product BA
//!
//! \returns A matrix, each of whose components are equal to the sum
//! of the respective operand components
//!
//! \param[in] multiplier The first matrix operand, the matrix to
//! multiply BY
//!
//! \param[in] multiplicand The second matrix operand, the matrix that
//! shall be multiplied
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL matrixMultiply(
	RegArgMatrix multiplier, LateArgMatrix multiplicand) noexcept;
//! \brief Scale each component of the specified matrix, by the given
//! amount
//!
//! \returns A suitably multiplied version of the matrix operand
//!
//! \param[in] matrix The matrix operand, whose components will each
//! by multiplied by the scale factor
//!
//! \param[in] scaleFactor The amount by which each matrix component
//! should be multiplied
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrixScale(RegArgMatrix matrix, double scaleFactor) noexcept;
//! \brief Transposes a matrix, swapping columns with equivalently
//! indexed rows
//!
//! \returns The operand matrix, transposed
//!
//! \param[in] matrix The matrix to transpose
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrixTranspose(RegArgMatrix matrix) noexcept;

//! @}

namespace IMPL
{
// calculate sin and cos of an angle at the same time
void scalarSinCos(double& sinResult, double& cosResult,
				  double angle) noexcept;
}  // namespace IMPL


//! \defgroup VectorTrigFunctions Vector trigonometry
//!
//! \brief Functions that concern trigonometry calculations
//!
//! @{

//! \brief Get the sine of every component in the vector
//!
//! \details Should be accurate for arguments in range [0, 2PI] to at
//! least 1x10^-15. That is, inaccuracies will, in theory, be less
//! than 1x10^-15. More specifically, inaccuracies in testing were of
//! the order [e, 2e), with e = machine epsilon at 1.0
//! = 2.22...x10^-16
//!
//! \warning Not suitable for very large angles, precision will be
//! pregressively lost, as one moves away from [0, 2PI]. The result
//! will cease to have any meaning, at (I think) 2PI*(2^52)
//!
//! \returns A vector containing the sine of each component of the
//! passed vector
//!
//! \param[in] vector A vector containing the components to take the
//! sine of. Each component is interpreted as an angle in radians
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorSin(RegArgVector vector) noexcept;
//! \brief Get the cosine of every component in the vector
//!
//! \details Should be accurate for arguments in range [0, 2PI] to at
//! least 1x10^-15. That is, inaccuracies will, in theory, be less
//! than 1x10^-15. More specifically, inaccuracies in testing were of
//! the order [e, 2e), with e = machine epsilon at 1.0
//! = 2.22...x10^-16
//!
//! \warning Not suitable for very large angles, precision will be
//! pregressively lost, as one moves away from [0, 2PI]. The result
//! will cease to have any meaning, at (I think) 2PI*(2^52)
//!
//! \returns A vector containing the cosine of each component of the
//! passed vector
//!
//! \param[in] vector A vector containing the components to take the
//! cosine of. Each component is interpreted as an angle in radians
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorCos(RegArgVector vector) noexcept;
//! \brief Get the tangent of every component in the vector
//!
//! \details Should be accurate for arguments in range (-PI/2, PI/2)
//! to at least 1x10^-15, except for regions near PI/2. That is,
//! inaccuracies will, in theory, and for most of the range, be less
//! than 1x10^-15. More specifically, inaccuracies in testing of most
//! places were of the order [e, 2e), with e = machine epsilon at 1.0
//! = 2.22...x10^-16. After around 17PI/40 = 0.425*PI ~ 1.34,
//! inaccuracies progressively increase (exponentially I think, so at
//! first the increase in inaccuracy is small), up to PI/2, where they
//! will be theoretically infinite
//!
//! \warning Not suitable for very large angles, precision will be
//! pregressively lost, as one moves away from (-PI/2, PI/2). The
//! result will cease to have any meaning, at (I think) PI*(2^52)
//!
//! \returns A vector containing the tangent of each component of the
//! passed vector
//!
//! \param[in] vector A vector containing the components to take the
//! tangent of. Each component is interpreted as an angle in radians
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorTan(RegArgVector vector) noexcept;

//! \brief Get the principle value of the arcsine of every component
//! in the vector
//!
//! \returns A vector containing the arcsine of each component of the
//! passed vector, between [0, pi]
//!
//! \param[in] vector A vector containing the components to find the
//! principle arcsine of. Each component should be in the range [-1,
//! 1]
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorArcSin(RegArgVector vector) noexcept;
//! \brief Get the principle value of the arccosine of every component
//! in the vector
//!
//! \returns A vector containing the arccosine of each component of
//! the passed vector, between [0, pi]
//!
//! \param[in] vector A vector containing the components to find the
//! principle arccosine of. Each component should be in the range [-1,
//! 1]
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorArcCos(RegArgVector vector) noexcept;
//! \brief Get the principle value of the arctangent of every
//! component in the vector
//!
//! \returns A vector containing the arctangent of each component of
//! the passed vector, between [-pi/2, pi/2]
//!
//! \param[in] vector A vector containing the components to find the
//! principle arctangent of.
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorArcTan(RegArgVector vector) noexcept;
//! \brief Get the principle value of the arctangent for every
//! top/bottom component pair, for the top and bottom input vectors
//!
//! \returns A vector containing the arctangent of each component of
//! the passed vector, between [-pi, pi]
//!
//! \param[in] topVec A vector containing the top components, for
//! atan(top/bottom), to find the principle arctangent of.
//!
//! \param[in] bottomVec A vector containing the bottom components,
//! for atan(top/bottom), to find the principle arctangent of.
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorArcTan2(RegArgVector topVec, RegArgVector bottomVec) noexcept;

//! @}

//! \defgroup VectorSpecialFunctions Vector special functions
//!
//! \brief Well-known functions like exp, log, abs
//!
//! @{

//! \brief For each component in the vector, calculate e to the power
//! of the argument
//!
//! \details While for most small results, the accuracy should be good
//! (ie: within about 2e or so), for larger results, the inaccuracy
//! will steadily increase, up to about 50e (inaccuracy of 1x10^-14
//! times result) at 3x10^158
//!
//! \returns A vector whose components equal e to the power of the
//! corresponding argument component
//!
//! \param[in] vector The vector whose components should be used as
//! powers of e
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorExp(RegArgVector vector) noexcept;
//! \brief For each component in the vector, calculate 2 to the power
//! of the argument
//!
//! \returns A vector whose components equal 2 to the power of the
//! corresponding argument component
//!
//! \param[in] vector The vector whose components should be used as
//! powers of 2
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorExp2(RegArgVector vector) noexcept;
//! \brief For each component in the vector, calculate the natural
//! logarithm of the argument
//!
//! \returns A vector whose components equal log base e of the
//! corresponding argument component
//!
//! \param[in] vector The vector for whose components log base e
//! should be taken
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorLog(RegArgVector vector) noexcept;
//! \brief For each component in the vector, calculate the logarithm
//! base 2 of the argument
//!
//! \returns A vector whose components equal log base 2 of the
//! corresponding argument component
//!
//! \param[in] vector The vector for whose components log base 2
//! should be taken
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorLog2(RegArgVector vector) noexcept;
//! \brief For each component in the vector, calculate the logarithm
//! base 10 of the argument
//!
//! \returns A vector whose components equal log base 10 of the
//! corresponding argument component
//!
//! \param[in] vector The vector for whose components log base 10
//! should be taken
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorLog10(RegArgVector vector) noexcept;

//! \brief Raise the base vector to the power of the exponent vector,
//! component by component
//!
//! \returns A double vector containing the results of each component
//! power operation
//!
//! \param[in] baseVector A double vector of the amounts to be raised
//! to respective powers
//!
//! \param[in] exponentVector A double vector of the powers that shall
//! be appplied to the respective base vector components
//!
//! \note Will round exponentVector to nearest integer, if base is
//! negative
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL vectorPow(
	RegArgVector baseVector, RegArgVector exponentVector) noexcept;

//! \brief For each component in the vector, calculate the (unsigned)
//! magnitude
//!
//! \returns A vector containing the magnitudes of each component of
//! the input
//!
//! \param[in] vector The vector of components whose absolute value
//! should be taken
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorAbs(RegArgVector vector) noexcept;
//! \brief Compare the two vectors, component-by-component, and find
//! the maximum of each component pair
//!
//! \returns A vector containing the larger of each component pair
//!
//! \param[in] lhsVec The left hand side (lhs) input, each of whose
//! components will be compaared against the corresponding rhs
//! components
//!
//! \param[in] rhsVec The right hand side (rhs) input, each of whose
//! components will be compaared against the corresponding lhs
//! components
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorMax(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare the two vectors, component-by-component, and find
//! the minimum of each component pair
//!
//! \returns A vector containing the smaller of each component pair
//!
//! \param[in] lhsVec The left hand side (lhs) input, each of whose
//! components will be compaared against the corresponding rhs
//! components
//!
//! \param[in] rhsVec The right hand side (rhs) input, each of whose
//! components will be compaared against the corresponding lhs
//! components
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vectorMin(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;

//! @}
}  // namespace Maths
}  // namespace frao

#endif