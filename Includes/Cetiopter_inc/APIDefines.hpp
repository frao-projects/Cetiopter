#ifndef FRAO_CETIOPTER_API_DEFINES
#define FRAO_CETIOPTER_API_DEFINES

//! \file
//!
//! \brief Header of api definitions, such as those that switch
//! between AVX/2 compilation modes, or dynamic vs static library
//! compilation
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <immintrin.h>	//for AVX types

//! \brief Namespace for all frao-series projects
namespace frao
{
//! \brief Sub-namespace for mathematic abstractions, and functions
namespace Maths
{
// we put conditional defines here, behind a symbol that only our
// documentation will define, so that it can understand the
// documentation for optional flags/defines
#if defined(FOR_DOXYGEN)

//! \def FRAO_AVX2
//!
//! \brief AVX2 mode defines and constexpr variables
//!
//! \note Defined iff AVX2 supported

//! \var bool g_HasAVX2
//!
//! \brief AVX2 mode defines and constexpr variables
//!
//! \note Set to true iff AVX2 supported

#define FRAO_AVX2

//! \def FRAO_AVX
//!
//! \brief AVX mode defines and constexpr variables
//!
//! \note Defined iff AVX supported

//! \var bool g_HasAVX
//!
//! \brief AVX mode defines and constexpr variables
//!
//! \note Set to true iff AVX supported

#define FRAO_AVX


#endif

#if defined(__AVX2__)
//	AVX 2 mode defines and constexpr variables

#define FRAO_AVX2
#define FRAO_AVX

constexpr static const bool g_HasAVX2 = true;
constexpr static const bool g_HasAVX  = true;

#elif defined(__AVX__)
//	AVX mode defines and constexpr variables

#define FRAO_AVX

constexpr static const bool g_HasAVX2 = false;
constexpr static const bool g_HasAVX  = true;

#else
//	non-AVX mode defines and constexpr variables

constexpr static const bool g_HasAVX2 = false;
constexpr static const bool g_HasAVX  = false;

#endif

//! \brief Designates that something is part of the library's API, and
//! should be exported
#ifdef CETIOPTER_EXPORT
#define CETIOPTER_LIB_API __declspec(dllexport)
#elif CETIOPTER_IMPORT
#define CETIOPTER_LIB_API __declspec(dllimport)
#else
#define CETIOPTER_LIB_API
#endif


// forward decalaration of WideVector so typedefs can use it
#if defined(FRAO_AVX) && !defined(FOR_DOXYGEN)
//! \brief In AVX mode, we use the __m128 register, instead of our
//! emulation class
using NarrowVector = __m128;
//! \brief In AVX mode, we use the __m128i register, instead of our
//! emulation class
using NarrowIntVector = __m128i;

//! \brief In AVX mode, we use the __m256d register, instead of our
//! emulation class
using WideVector = __m256d;
#else
class alignas(16) NarrowVector;
class alignas(16) NarrowIntVector;
class alignas(32) WideVector;
#endif

// forward declaration of WideIntVector so typedefs can use it

#if defined(FRAO_AVX2) && !defined(FOR_DOXYGGN)
//! \brief In AVX mode, we use the __m256i register, instead of our
//! emulation class
using WideIntVector = __m256i;
#else
class alignas(32) WideIntVector;
#endif

// forward declaration of NarrowMatrix so typedefs can use it
class alignas(16) NarrowMatrix;
// forward declaration of WideMatrix so typedefs can use it
class alignas(32) WideMatrix;

//! \defgroup VectorCallingConventions SIMD calling conventions
//!
//! \brief Things for functions that (can, if AVX is enabled) take
//! SIMD vectors
//!
//! @{

// we define this here, so that we can later conditionally compile
// with/without it

//! \brief If we have access to AVX, will expand to __vectorcall, to
//! optimally pass up to 6 SIMD registers. If we don't have access to
//! AVX, will expand to __fastcall
#ifdef FRAO_AVX
#define FRAO_VECTORCALL __vectorcall
#else
#define FRAO_VECTORCALL __fastcall
#endif

// These are so that we can pass the first 6 vector arguments in
// XMM/YMM registers, once we start to use __m128 __m256

//! \brief typedef referring to float SIMD vector, that should be
//! used for the first 6 SIMD 'slots' in a function, to pass
//! parameters efficiently under AVX
using RegArgNrwVec = const NarrowVector;
//! \brief typedef referring to int32 SIMD vector, that should be
//! used for the first 6 SIMD 'slots' in a function, to pass
//! parameters efficiently under AVX
using RegIntNrwVec = const NarrowIntVector;
//! \brief typedef referring to double SIMD vector, that should be
//! used for the first 6 SIMD 'slots' in a function, to pass
//! parameters efficiently under AVX
using RegArgVector = const WideVector;
//! \brief typedef referring to integer SIMD vector, that should be
//! used for the first 6 SIMD 'slots' in a function, to pass
//! parameters efficiently under AVX
using RegIntVector = const WideIntVector;
//! \brief typedef referring to 4xSIMD float matrix, that should be
//! used for the first 6 SIMD 'slots' in a function, to pass
//! parameters efficiently under AVX
//!
//! \warning This uses 4 of the 6 SIMD 'slots', so should only be used
//! once per function, and only if <= 2 slots have already been used!
using RegArgNrwMtrx = const NarrowMatrix;
//! \brief typedef referring to 4xSIMD double matrix, that should be
//! used for the first 6 SIMD 'slots' in a function, to pass
//! parameters efficiently under AVX
//!
//! \warning This uses 4 of the 6 SIMD 'slots', so should only be used
//! once per function, and only if <= 2 slots have already been used!
using RegArgMatrix = const WideMatrix;

// This is for any arguments that are past the first 6 that can be
// loaded into XMM/YMM registers

//! \brief typedef referring to float SIMD vector, that should be
//! used for parameters past the the 6 SIMD 'slots'
using LateArgNrwVec = const NarrowVector&;
//! \brief typedef referring to int32 SIMD vector, that should be
//! used for parameters past the the 6 SIMD 'slots'
using LateIntNrwVec = const NarrowVector&;
//! \brief typedef referring to double SIMD vector, that should be
//! used for parameters past the the 6 SIMD 'slots'
using LateArgVector = const WideVector&;
//! \brief typedef referring to integer SIMD vector, that should be
//! used for parameters past the the 6 SIMD 'slots'
using LateIntVector = const WideIntVector&;
//! \brief typedef referring to 4xSIMD float matrix, that should be
//! used for parameters past the the 6 SIMD 'slots'
//!
//! \note Since matrices use 4 SIMD 'slots', this must be used from
//! the **second** matrix onwards, even in the absence of passed
//! vectors
using LateArgNrwMtrx = const NarrowMatrix&;
//! \brief typedef referring to 4xSIMD double matrix, that should be
//! used for parameters past the the 6 SIMD 'slots'
//!
//! \note Since matrices use 4 SIMD 'slots', this must be used from
//! the **second** matrix onwards, even in the absence of passed
//! vectors
using LateArgMatrix = const WideMatrix&;

//! @}

}  // namespace Maths
}  // namespace frao

#endif