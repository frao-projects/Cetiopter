#ifndef FRAO_CETIOPTER_DIMENSIONAL_MATHS
#define FRAO_CETIOPTER_DIMENSIONAL_MATHS

//! \file
//!
//! \brief Header of specifically 2/3/4D mathematics
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "APIDefines.hpp"

namespace frao
{
namespace Maths
{
//! \defgroup Vector2DFunctions Vector 2D functions
//!
//! \brief Functions that operate under the assumption that the
//! underlying data is 2D
//!
//! @{

//! \brief Compute the 2D dot product, between two wide vectors
//!
//! \returns A vector containing the result of performing the 2D dot
//! product between the two vectors, replicated across every component
//! of the result vector
//!
//! \param[in] lhsVec One of the two vectors that should take part in
//! the 2D dot product operation. Its first two components will be
//! used in the operation
//!
//! \param[in] rhsVec The other of the two vectors that should take
//! part in the 2D dot product operation. Its first two components
//! will be used in the operation
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector2DDot(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;

//! \brief Get the 2D squared length of a vector, replicated to all
//! components
//!
//! \returns A vector in which each component holds the value of the
//! 2D length squared, of the input operand
//!
//! \param[in] vector The vector of which we should find the 2D length
//! square. Its first two components will be used in the operation
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector2DLengthSq(RegArgVector vector) noexcept;
//! \brief Get the 2D length of a vector, replicated to all components
//!
//! \returns A vector in which each component holds the value of the
//! 2D length of the input operand
//!
//! \param[in] vector The vector of which we should find the 2D
//! length. Its first two components will be used in the operation
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector2DLength(RegArgVector vector) noexcept;
//! \brief Get the 2D normalised form of a vector
//!
//! \details Divides each component of a vector by that vector's 2D
//! length, in order to obtain a vector with the same 'direction', but
//! with a length of 1. ie. Will obtain a unit vector
//!
//! \returns The unit vector corresponding in direction to the input
//! vector
//!
//! \param[in] vector The input operand vector, on which we should
//! carry out normalisation
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector2DNormalised(RegArgVector vector) noexcept;

//! \brief Transform (multiply) the vector by the specified matrix.
//! Will set z and w values of output to 0
//!
//! \returns A 2D vector, which has had the transformation represented
//! by the matrix applied to it. The latter two components of the
//! result will always be 0
//!
//! \param[in] vector The vector would should be multiplied
//! (transformed) by the specified matrix
//!
//! \param[in] matrix The matrix which should multiply (transform) the
//! input vector into the result vector
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector2DTransform(RegArgVector vector, RegArgMatrix matrix) noexcept;
//! \brief Calculate the determinant of a 2x2 matrix
//!
//! \note The latter two rows, and 2nd half of the first two rows,
//! will be ignored, since this is a 2D operation
//!
//! \returns The determinant of the 2x2 matrix, copied across all
//! components of the result vector
//!
//! \param[in] matrix The matrix, presumed to be 2x2, whose
//! determinant wwe should find
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
matrix2DDeterminant(RegArgMatrix matrix) noexcept;
//! \brief Attempt to calculate the inverse of a 2x2 matrix, if one
//! exists
//!
//! \returns True iff the specified matrix has an inverse
//!
//! \param[out] result The matrix variable that shall hold the
//! resulting inverse. Will not be written to, if no inverse is
//! possible to determine, and function returns false
//!
//! \param[in] matrix The 2x2 matrix that we wish to find the inverse
//! of.
CETIOPTER_LIB_API bool FRAO_VECTORCALL
matrix2DInverse(WideMatrix& result, RegArgMatrix matrix) noexcept;

//! \brief Checks if a 2x2 matrix is invertible
//!
//! \returns True iff the spcified matrix is invertible
//!
//! \param[in] matrix the matrix whose invertibility should be
//! determined
CETIOPTER_LIB_API bool FRAO_VECTORCALL
matrix2DIsInvertible(RegArgMatrix matrix) noexcept;

//! @}


//! \defgroup Vector3DFunctions Vector 3D functions
//!
//! \brief Functions that operate under the assumption that the
//! underlying data is 3D
//!
//! @{
namespace IMPL
{
// calculate the 2D determinants of a 3D matrix, that is, the 2D
// determinants of two rows of the matrix that will be used to
// calculate the 3D determinant or inverse
//
// Returns a vector with the three different determinants in
// them, arranged in such a way across the four elements of the
// vector, as to ease later calculations.
WideVector FRAO_VECTORCALL matrix3D_2DDeterminants(
	RegArgVector rowOne, RegArgVector rowTwo) noexcept;

}  // namespace IMPL

//! \brief Compute the 3D dot product, between two wide vectors
//!
//! \returns A vector containing the result of performing the 3D dot
//! product between the two vectors, replicated across every component
//! of the result vector
//!
//! \param[in] lhsVec One of the two vectors that should take part in
//! the 3D dot product operation. Its first three components will be
//! used in the operation
//!
//! \param[in] rhsVec The other of the two vectors that should take
//! part in the 3D dot product operation. Its first three components
//! will be used in the operation
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector3DDot(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compute the 3D cross product, between two wide vectors
//!
//! \returns A vector containing the result of performing the 3D cross
//! product between the two vectors
//!
//! \param[in] lhsVec The vector that should take part in the 3D cross
//! product operation, on the left hand side. Its first three
//! components will be used in the operation
//!
//! \param[in] rhsVec The vector that should take part in the 3D cross
//! product operation, on the right hand side. Its first three
//! components will be used in the operation
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector3DCross(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;

//! \brief Get the 3D squared length of a vector, replicated to all
//! components
//!
//! \returns A vector in which each component holds the value of the
//! 3D length squared, of the input operand
//!
//! \param[in] vector The vector of which we should find the 3D length
//! square. Its first three components will be used in the operation
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector3DLengthSq(RegArgVector vector) noexcept;
//! \brief Get the 3D length of a vector, replicated to all components
//!
//! \returns A vector in which each component holds the value of the
//! 3D length of the input operand
//!
//! \param[in] vector The vector of which we should find the 3D
//! length. Its first three components will be used in the operation
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector3DLength(RegArgVector vector) noexcept;
//! \brief Get the 3D normalised form of a vector
//!
//! \details Divides each component of a vector by that vector's 3D
//! length, in order to obtain a vector with the same 'direction', but
//! with a length of 1. ie. Will obtain a unit vector
//!
//! \returns The unit vector corresponding in direction to the input
//! vector
//!
//! \param[in] vector The input operand vector, on which we should
//! carry out normalisation
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector3DNormalised(RegArgVector vector) noexcept;

//! \brief Transform (multiply) the vector by the specified matrix.
//! Will set w value of output to 0
//!
//! \returns A 3D vector, which has had the transformation represented
//! by the matrix applied to it. The last component of the result will
//! always be 0
//!
//! \param[in] vector The vector would should be multiplied
//! (transformed) by the specified matrix
//!
//! \param[in] matrix The matrix which should multiply (transform) the
//! input vector into the result vector
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector3DTransform(RegArgVector vector, RegArgMatrix matrix) noexcept;
//! \brief Calculate the determinant of a 3x3 matrix
//!
//! \note The last row, and last element the first three rows,
//! will be ignored, since this is a 3D operation
//!
//! \returns The determinant of the 3x3 matrix, copied across all
//! components of the result vector
//!
//! \param[in] matrix The matrix, presumed to be 3x3, whose
//! determinant we should find
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
matrix3DDeterminant(RegArgMatrix matrix) noexcept;
//! \brief Attempt to calculate the inverse of a 3x3 matrix, if one
//! exists
//!
//! \returns True iff the specified matrix has an inverse
//!
//! \param[out] result The matrix variable that shall hold the
//! resulting inverse. Will not be written to, if no inverse is
//! possible to determine, and function returns false
//!
//! \param[in] matrix The 3x3 matrix that we wish to find the inverse
//! of.
CETIOPTER_LIB_API bool FRAO_VECTORCALL
matrix3DInverse(WideMatrix& result, RegArgMatrix matrix) noexcept;
//! \brief Checks if a 3x3 matrix is invertible
//!
//! \returns True iff the spcified matrix is invertible
//!
//! \param[in] matrix the matrix whose invertibility should be
//! determined
CETIOPTER_LIB_API bool FRAO_VECTORCALL
matrix3DIsInvertible(RegArgMatrix matrix) noexcept;

//! @}


//! \defgroup Vector4DFunctions Vector 4D functions
//!
//! \brief Functions that operate under the assumption that the
//! underlying data is 4D
//!
//! @{

// A namespace of functions that we need for other calculations, but
// which are of little use outside of those requirements
namespace IMPL
{
// calculate the 2D determinants of a 4D matrix, that is, the 2D
// determinants of two rows of the matrix that will be used to
// calculate 3D determinants, later. eg: for 4D determinant
// calculation, the determinants of rows 3 and 4 are needed
//
// The output parameters each have three different determinants in
// them, arranged in such a way across the four elements of the
// vector, as to ease later calculations. Determinants are also
// sometimes duplicated across several output vectors, for a similar
// reason. There are 6 unique 2D determinants calculated.
void FRAO_VECTORCALL matrix4D_2DDeterminants(
	RegArgVector rowOne, RegArgVector rowTwo, WideVector& outSetOne,
	WideVector& outSetTwo, WideVector& outSetThree) noexcept;
// calculate the 3D determinants of a 4D matrix, that is, the 3D
// determinants of three rows of the matrix, that will be used to
// calculate 4D determinant and rows of a cofactor matrix, later.
//
// The first three paramaters are for 2D determinants, as arranged by
// output from 'matrix4D_2DDeterminants')
//
// The second three parameters are swizzled versions of a third row
// (distinct from the first two), with rearranged elements: (2, 1, 1,
// 1), (3, 3, 2, 2), (4, 4, 4, 3)
//
// the function will then calculate dets2DOne*swizzleOne -
// dets2DTwo*swizzleTwo + dets2DThree*swizzleThree, to get a vector
// with 4 3D determinants in it
WideVector FRAO_VECTORCALL matrix4D_3DDeterminants(
	RegArgVector dets2DOne, RegArgVector dets2DTwo,
	RegArgVector dets2DThree, RegArgVector swizzleOne,
	RegArgVector swizzleTwo, RegArgVector swizzleThree) noexcept;
}  // namespace IMPL

//! \brief Compute the 4D dot product, between two wide vectors
//!
//! \returns A vector containing the result of performing the 4D dot
//! product between the two vectors, replicated across every component
//! of the result vector
//!
//! \param[in] lhsVec One of the two vectors that should take part in
//! the 4D dot product operation.
//!
//! \param[in] rhsVec The other of the two vectors that should take
//! part in the 4D dot product operation.
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector4DDot(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;

//! \brief Get the 4D squared length of a vector, replicated to all
//! components
//!
//! \returns A vector in which each component holds the value of the
//! 4D length squared, of the input operand
//!
//! \param[in] vector The vector of which we should find the 4D length
//! square.
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector4DLengthSq(RegArgVector vector) noexcept;
//! \brief Get the 4D length of a vector, replicated to all components
//!
//! \returns A vector in which each component holds the value of the
//! 4D length of the input operand
//!
//! \param[in] vector The vector of which we should find the 4D
//! length.
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector4DLength(RegArgVector vector) noexcept;
//! \brief Get the 4D normalised form of a vector
//!
//! \details Divides each component of a vector by that vector's 4D
//! length, in order to obtain a vector with the same 'direction', but
//! with a length of 1. ie. Will obtain a unit vector
//!
//! \returns The unit vector corresponding in direction to the input
//! vector
//!
//! \param[in] vector The input operand vector, on which we should
//! carry out normalisation
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector4DNormalised(RegArgVector vector) noexcept;

//! \brief Transform (multiply) the vector by the specified matrix.
//!
//! \returns A 4D vector, which has had the transformation represented
//! by the matrix applied to it.
//!
//! \param[in] vector The vector would should be multiplied
//! (transformed) by the specified matrix
//!
//! \param[in] matrix The matrix which should multiply (transform) the
//! input vector into the result vector
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
vector4DTransform(RegArgVector vector, RegArgMatrix matrix) noexcept;
//! \brief Calculate the determinant of a 4x4 matrix
//!
//! \returns The determinant of the 4x4 matrix, copied across all
//! components of the result vector
//!
//! \param[in] matrix The matrix, presumed to be 4x4, whose
//! determinant we should find
CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
matrix4DDeterminant(RegArgMatrix matrix) noexcept;

//! \brief Attempt to calculate the inverse of a 4x4 matrix, if one
//! exists
//!
//! \returns True iff the specified matrix has an inverse
//!
//! \param[out] result The matrix variable that shall hold the
//! resulting inverse. Will not be written to, if no inverse is
//! possible to determine, and function returns false
//!
//! \param[in] matrix The 4x4 matrix that we wish to find the inverse
//! of.
CETIOPTER_LIB_API bool FRAO_VECTORCALL
matrix4DInverse(WideMatrix& result, RegArgMatrix matrix) noexcept;
//! \brief Checks if a 4x4 matrix is invertible
//!
//! \returns True iff the spcified matrix is invertible
//!
//! \param[in] matrix the matrix whose invertibility should be
//! determined
CETIOPTER_LIB_API bool FRAO_VECTORCALL
matrix4DIsInvertible(RegArgMatrix matrix) noexcept;

//! @}
}  // namespace Maths
}  // namespace frao

#endif