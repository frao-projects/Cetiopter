#ifndef FRAO_CETIOPTER_SPECIAL_MATRICES
#define FRAO_CETIOPTER_SPECIAL_MATRICES

//! \file
//!
//! \brief Header of special matrices, such as the identity and
//! perspective matrices
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include "APIDefines.hpp"

namespace frao
{
namespace Maths
{
//! \defgroup MatrixCreation Matrix Creation
//!
//! \brief Functions that create matrices for specific purposes
//!
//! @{

//! \brief Get the identity matrix
//!
//! \details Gets the 4D identity matrix, for calculations
//!
//! \returns The identity matrix
CETIOPTER_LIB_API WideMatrix matrixIdentity() noexcept;

//! \brief Creates a 2D translation matrix from the specified movement
//!
//! \details Using the specified movement, will create a matrix that,
//! when multiplied by a 2D vector, will move that vector by the
//! specified amount
//!
//! \returns Matrix capable of moving a 2D location vector by the
//! specified amount.
//!
//! \param[in] move The translation required
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrix2DTranslation(RegArgVector move) noexcept;
//! \brief Creates a 2D translation matrix from the specified movement
//!
//! \details Using the specified movement, will create a matrix that,
//! when multiplied by a 2D vector, will move that vector by the
//! specified amount
//!
//! \returns Matrix capable of moving a 2D location vector by the
//! specified amount.
//!
//! \param[in] xMove The amount of x direction movement that should be
//! imparted by the resulting matrix
//!
//! \param[in] yMove The amount of y direction movement that should be
//! imparted by the resulting matrix
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrix2DTranslation(double xMove, double yMove) noexcept;
//! \brief Creates a 3D translation matrix from the specified movement
//!
//! \details Using the specified movement, will create a matrix that,
//! when multiplied by a 3D vector, will move that vector by the
//! specified amount
//!
//! \returns Matrix capable of moving a 3D location vector by the
//! specified amount.
//!
//! \param[in] move The translation required
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrix3DTranslation(RegArgVector move) noexcept;
//! \brief Creates a 3D translation matrix from the specified movement
//!
//! \details Using the specified movement, will create a matrix that,
//! when multiplied by a 3D vector, will move that vector by the
//! specified amount
//!
//! \returns Matrix capable of moving a 3D location vector by the
//! specified amount.
//!
//! \param[in] xMove The amount of x direction movement that should be
//! imparted by the resulting matrix
//!
//! \param[in] yMove The amount of y direction movement that should be
//! imparted by the resulting matrix
//!
//! \param[in] zMove The amount of z direction movement that should be
//! imparted by the resulting matrix
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL matrix3DTranslation(
	double xMove, double yMove, double zMove) noexcept;

//! \brief Creates a 2D scaling matrix from the specified scale
//! factors
//!
//! \details Using the specified scale factors, will create a matrix
//! that, when multiplied by a 2D vector, will increase the size of
//! that vector in the given dimension, by the specified amount
//!
//! \returns Matrix capable of scaling a 2D vector by the specified
//! amount.
//!
//! \param[in] scaling The scaling required
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrix2DScaling(RegArgVector scaling) noexcept;
//! \brief Creates a 2D scaling matrix from the specified scale
//! factors
//!
//! \details Using the specified scale factors, will create a matrix
//! that, when multiplied by a 2D vector, will increase the size of
//! that vector in the given dimension, by the specified amount
//!
//! \returns Matrix capable of scaling a 2D vector by the specified
//! amount.
//!
//! \param[in] xScaling The amount of x direction scaling that should
//! be imparted by the resulting matrix
//!
//! \param[in] yScaling The amount of y direction scaling that should
//! be imparted by the resulting matrix
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrix2DScaling(double xScaling, double yScaling) noexcept;
//! \brief Creates a 3D scaling matrix from the specified scale
//! factors
//!
//! \details Using the specified scale factors, will create a matrix
//! that, when multiplied by a 3D vector, will increase the size of
//! that vector in the given dimension, by the specified amount
//!
//! \returns Matrix capable of scaling a 3D location vector by the
//! specified amount.
//!
//! \param[in] scaling The scaling required
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrix3DScaling(RegArgVector scaling) noexcept;
//! \brief Creates a 3D scaling matrix from the specified scale
//! factors
//!
//! \details Using the specified scale factors, will create a matrix
//! that, when multiplied by a 3D vector, will increase the size of
//! that vector in the given dimension, by the specified amount
//!
//! \returns Matrix capable of scaling a 3D location vector by the
//! specified amount.
//!
//! \param[in] xScaling The amount of x direction scaling that should
//! be imparted by the resulting matrix
//!
//! \param[in] yScaling The amount of y direction scaling that should
//! be imparted by the resulting matrix
//!
//! \param[in] zScaling The amount of y direction scaling that should
//! be imparted by the resulting matrix
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL matrix3DScaling(
	double xScaling, double yScaling, double zScaling) noexcept;

//! \brief Construct a right handed rotation matrix, that when
//! multiplied by a 2D vector, will rotate it by the specified angle
//!
//! \details Construct a matrix capable of performing a right handed
//! anticlockwise rotation on a 2D vector by the given angle.
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] angle The angle, in radians, of the right handed
//! rotation to be performed
CETIOPTER_LIB_API WideMatrix matrix2DRotationRH(double angle) noexcept;
//! \brief Construct a left handed rotation matrix, that when
//! multiplied by a 2D vector, will rotate it by the specified angle
//!
//! \details Construct a matrix capable of performing a left handed
//! clockwise rotation on a 2D vector by the given angle.
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] angle The angle, in radians, of the left handed
//! rotation to be performed
CETIOPTER_LIB_API WideMatrix matrix2DRotationLH(double angle) noexcept;

//! \brief Construct a right handed rotation matrix, that when
//! multiplied by a 3D vector, will rotate it by the specified angle
//! in the yaw direction
//!
//! \note Yaw here would be referred to as roll in typical directx
//! systems
//!
//! \details Construct a matrix capable of performing a right handed
//! anticlockwise yaw rotation on a 3D vector by the given angle. That
//! is, will create a matrix that rotates around the z axis
//! anticlockwise.
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] angle The angle, in radians, of the right handed
//! anticlockwise rotation to be performed.
CETIOPTER_LIB_API WideMatrix matrix3DRotationYawRH(double angle) noexcept;
//! \brief Construct a left handed rotation matrix, that when
//! multiplied by a 3D vector, will rotate it by the specified angle
//! in the yaw direction
//!
//! \note Yaw here would be referred to as roll in typical directx
//! systems
//!
//! \details Construct a matrix capable of performing a left handed
//! clockwise yaw rotation on a 3D vector by the given angle. That is,
//! will create a matrix that rotates around the z axis clockwise.
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] angle The angle, in radians, of the left handed
//! clockwise rotation to be performed. Left handed angle is
//! equivalent to right handed angle negated
CETIOPTER_LIB_API WideMatrix matrix3DRotationYawLH(double angle) noexcept;
//! \brief Construct a right handed rotation matrix, that when
//! multiplied by a 3D vector, will rotate it by the specified angle
//! in the pitch direction
//!
//! \details Construct a matrix capable of performing a right handed
//! anticlockwise pitch rotation on a 3D vector by the given angle.
//! That is, will create a matrix that rotates around the x axis
//! anticlockwise.
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] angle The angle, in radians, of the right handed
//! anticlockwise rotation to be performed.
CETIOPTER_LIB_API WideMatrix
matrix3DRotationPitchRH(double angle) noexcept;
//! \brief Construct a left handed rotation matrix, that when
//! multiplied by a 3D vector, will rotate it by the specified angle
//! in the pitch direction
//!
//! \details Construct a matrix capable of performing a left handed
//! clockwise pitch rotation on a 3D vector by the given angle. That
//! is, will create a matrix that rotates around the x axis clockwise.
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] angle The angle, in radians, of the left handed
//! clockwise rotation to be performed. Left handed angle is
//! equivalent to right handed angle negated
CETIOPTER_LIB_API WideMatrix
matrix3DRotationPitchLH(double angle) noexcept;
//! \brief Construct a right handed rotation matrix, that when
//! multiplied by a 3D vector, will rotate it by the specified angle
//! in the roll direction
//!
//! \note Roll here would be referred to as yaw in typical directx
//! systems
//!
//! \details Construct a matrix capable of performing a right handed
//! anticlockwise roll rotation on a 3D vector by the given angle.
//! That is, will create a matrix that rotates around the y axis
//! anticlockwise.
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] angle The angle, in radians, of the right handed
//! anticlockwise rotation to be performed.
CETIOPTER_LIB_API WideMatrix
matrix3DRotationRollRH(double angle) noexcept;
//! \brief Construct a left handed rotation matrix, that when
//! multiplied by a 3D vector, will rotate it by the specified angle
//! in the roll direction
//!
//! \note Roll here would be referred to as yaw in typical directx
//! systems
//!
//! \details Construct a matrix capable of performing a left handed
//! clockwise roll rotation on a 3D vector by the given angle. That
//! is, will create a matrix that rotates around the y axis clockwise.
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] angle The angle, in radians, of the left handed
//! clockwise rotation to be performed. Left handed angle is
//! equivalent to right handed angle negated
CETIOPTER_LIB_API WideMatrix
matrix3DRotationRollLH(double angle) noexcept;

//! \brief Construct a right handed rotation matrix, that when
//! multiplied by a 3D vector, will rotate it by the specified angles
//! in the given directions
//!
//! \details Construct a matrix capable of performing a right handed
//! anticlockwise rotation on a 3D vector by the given set of angles,
//! in the order yaw, pitch, roll. That is, will create a matrix that
//! rotates around the yaw, pitch, and roll axes anticlockwise.
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] pitch The angle, in radians, of the anticlockwise
//! rotation around the x axis to perform
//!
//! \param[in] roll The angle, in radians, of the anticlockwise
//! rotation around the y axis to perform
//!
//! \param[in] yaw The angle, in radians, of the anticlockwise
//! rotation around the z axis to perform
CETIOPTER_LIB_API WideMatrix matrix3DRotationRH(double pitch, double roll,
											double yaw) noexcept;
//! \brief Construct a right handed rotation matrix, that when
//! multiplied by a 3D vector, will rotate it by the specified angles
//! in the given directions
//!
//! \details Construct a matrix capable of performing a right handed
//! anticlockwise rotation on a 3D vector by the given set of angles,
//! in the order yaw, pitch, roll. That is, will create a matrix that
//! rotates around the yaw, pitch, and roll axes anticlockwise.
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] rotAngles The angles, in radians, of the anticlockwise
//! rotations around the corresponding axes to perform. eg: x
//! component will describe rotation around x axis (pitch), y will be
//! roll, z will be yaw
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrix3DRotationRHFromVector(RegArgVector rotAngles) noexcept;

//! \brief Construct a left handed rotation matrix, that when
//! multiplied by a 3D vector, will rotate it by the specified angles
//! in the given directions
//!
//! \details Construct a matrix capable of performing a left handed
//! clockwise rotation on a 3D vector by the given set of angles, in
//! the order yaw, pitch, roll. That is, will create a matrix that
//! rotates around the yaw, pitch, and roll axes clockwise.
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] pitch The angle, in radians, of the clockwise rotation
//! around the x axis to perform. Left handed angle in equivalent to
//! right handed angle negated.
//!
//! \param[in] roll The angle, in radians, of the clockwise rotation
//! around the y axis to perform. Left handed angle in equivalent to
//! right handed angle negated.
//!
//! \param[in] yaw The angle, in radians, of the clockwise rotation
//! around the z axis to perform. Left handed angle in equivalent to
//! right handed angle negated.
CETIOPTER_LIB_API WideMatrix matrix3DRotationLH(double pitch, double roll,
											double yaw) noexcept;
//! \brief Construct a left handed rotation matrix, that when
//! multiplied by a 3D vector, will rotate it by the specified angles
//! in the given directions
//!
//! \details Construct a matrix capable of performing a left handed
//! clockwise rotation on a 3D vector by the given set of angles, in
//! the order yaw, pitch, roll. That is, will create a matrix that
//! rotates around the yaw, pitch, and roll axes clockwise.
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] rotAngles The angles, in radians, of the clockwise
//! rotations around the corresponding axes to perform. eg: x
//! component will describe rotation around x axis (pitch), y will be
//! roll, z will be yaw
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrix3DRotationLHFromVector(RegArgVector rotAngles) noexcept;

//! \brief Construct a right handed rotation matrix, that when
//! multiplied by a 3D vector, will rotate it by the given angle
//! around an arbitrary axis
//!
//! \note Assumes given coordinates are right handed, and rotation is
//! anticlockwise
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] axisX The x component of the axis that we should rotate
//! around. Assumes right-handed coords
//!
//! \param[in] axisY The y component of the axis that we should rotate
//! around. Assumes right-handed coords
//!
//! \param[in] axisZ The z component of the axis that we should rotate
//! around. Assumes right-handed coords
//!
//! \param[in] angle The angle, about which to rotate, around the
//! given axis. Assumes anticlockwise rotation
CETIOPTER_LIB_API WideMatrix matrix3DRotationRHAboutAxis(
	double axisX, double axisY, double axisZ, double angle) noexcept;
//! \brief Construct a right handed rotation matrix, that when
//! multiplied by a 3D vector, will rotate it by the given angle
//! around an arbitrary axis
//!
//! \note Assumes given coordinates are right handed, and rotation is
//! anticlockwise
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] axis The (x,y,z) components of the axis that we should
//! rotate around. Assumes right-handed coords
//!
//! \param[in] angle The angle, about which to rotate, around the
//! given axis. Assumes anticlockwise rotation
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrix3DRotationRHAboutAxis(RegArgVector axis, double angle) noexcept;
//! \brief Construct a left handed rotation matrix, that when
//! multiplied by a 3D vector, will rotate it by the given angle
//! around an arbitrary axis
//!
//! \note Assumes given coordinates are left handed, and rotation is
//! clockwise
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] axisX The x component of the axis that we should rotate
//! around. Assumes left-handed coords
//!
//! \param[in] axisY The y component of the axis that we should rotate
//! around. Assumes left-handed coords
//!
//! \param[in] axisZ The z component of the axis that we should rotate
//! around. Assumes left-handed coords
//!
//! \param[in] angle The angle, about which to rotate, around the
//! given axis. Assumes clockwise rotation
CETIOPTER_LIB_API WideMatrix matrix3DRotationLHAboutAxis(
	double axisX, double axisY, double axisZ, double angle) noexcept;
//! \brief Construct a left handed rotation matrix, that when
//! multiplied by a 3D vector, will rotate it by the given angle
//! around an arbitrary axis
//!
//! \note Assumes given coordinates are left handed, and rotation is
//! clockwise
//!
//! \returns A matrix capable of performing the required rotation
//!
//! \param[in] axis The (x,y,z) components of the axis that we should
//! rotate around. Assumes left-handed coords
//!
//! \param[in] angle The angle, about which to rotate, around the
//! given axis. Assumes clockwise rotation
CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
matrix3DRotationLHAboutAxis(RegArgVector axis, double angle) noexcept;

//! \brief Construct a perspective transform matrix, for right handed
//! input, from the specicifed width/height/distance of the near
//! plane, as well as the distance to the far plane.
//!
//! \details Will create a matrix that that creates perspective, and
//! transforms from right handed (forward = y, up = z) coordinates, to
//! that required by DirectX (negateZaxis = false; up = y, forward =
//! z) or Vulkan (negateZaxis = true; up = y, backwards = z)
//!
//! \returns A matrix that will transform a vector from world coords
//! to homogeneous camera plane coords (pre prerspective divide, which
//! in DX/Vulkan, will be performed by the rasteriser), creating
//! perspective as it does so.
//!
//! \param[in] viewWidth The width of the viewing plane / near
//! clipping plane, in terms of its actual size
//!
//! \param[in] viewHeight The height of the viewing plane / near
//! clipping plane, in terms of its actual size
//!
//! \param[in] viewNear The distance to the near clipping plane, from
//! the camera / origin
//!
//! \param[in] viewFar The distance to the far clipping plane, from
//! the camera / origin.
//!
//! \param[in] negateZaxis Literally, should the z axis be negated?
//! Used to ensure output in is in the correct format, for the given
//! graphics API. That is, this should be on for Vulkan and off for
//! directX
CETIOPTER_LIB_API WideMatrix matrix3DPerspectiveCameraRH(
	double viewWidth, double viewHeight, double viewNear,
	double viewFar, bool negateZaxis = false) noexcept;
//! \brief Construct a perspective transform matrix, for right handed
//! input, from the specicifed FOV/aspect ratio/distance of the near
//! plane, as well as the distance to the far plane.
//!
//! \note FOV is vertical FOV, and aspect ratio is width / height
//!
//! \details Will create a matrix that that creates perspective, and
//! transforms from right handed (forward = y, up = z) coordinates, to
//! that required by DirectX (negateZaxis = false; up = y, forward =
//! z) or Vulkan (negateZaxis = true; up = y, backwards = z)
//!
//! \returns A matrix that will transform a vector from world coords
//! to homogeneous camera plane coords (pre prerspective divide, which
//! in DX/Vulkan, will be performed by the rasteriser), creating
//! perspective as it does so.
//!
//! \param[in] verticalFOV The vertical field of view angle, of the
//! near plane
//!
//! \param[in] aspectRatio The ratio of width per unit height, of the
//! near clipping plane
//!
//! \param[in] viewNear The distance to the near clipping plane, from
//! the camera / origin
//!
//! \param[in] viewFar The distance to the far clipping plane, from
//! the camera / origin.
//!
//! \param[in] negateZaxis Literally, should the z axis be negated?
//! Used to ensure output in is in the correct format, for the given
//! graphics API. That is, this should be on for Vulkan and off for
//! directX
CETIOPTER_LIB_API WideMatrix matrix3DPerspectiveFOVCameraRH(
	double verticalFOV, double aspectRatio, double viewNear,
	double viewFar, bool negateZaxis = false) noexcept;
//! \brief Construct a orthographic transform matrix, for right handed
//! input, from the specicifed width/height/distance of the near
//! plane, as well as the distance to the far plane.
//!
//! \details Will create a matrix that that creates an orthographic
//! projection, and transforms from right handed (forward = y, up = z)
//! coordinates, to that required by DirectX (negateZaxis = false; up
//! = y, forward = z) or Vulkan (negateZaxis = true; up = y, backwards
//! = z)
//!
//! \returns A matrix that will transform a vector from world coords
//! to homogeneous camera plane coords (pre prerspective divide, which
//! in DX/Vulkan, will be performed by the rasteriser), without
//! creating perspective as it does so
//!
//! \param[in] viewWidth The width of the viewing plane / near
//! clipping plane, in terms of its actual size
//!
//! \param[in] viewHeight The height of the viewing plane / near
//! clipping plane, in terms of its actual size
//!
//! \param[in] viewNear The distance to the near clipping plane, from
//! the camera / origin
//!
//! \param[in] viewFar The distance to the far clipping plane, from
//! the camera / origin.
//!
//! \param[in] negateZaxis Literally, should the z axis be negated?
//! Used to ensure output in is in the correct format, for the given
//! graphics API. That is, this should be on for Vulkan and off for
//! directX
CETIOPTER_LIB_API WideMatrix matrix3DOrthographicCameraRH(
	double viewWidth, double viewHeight, double viewNear,
	double viewFar, bool negateZaxis = false) noexcept;
//! \brief Construct a perspective transform matrix, for left handed
//! input, from the specicifed width/height/distance of the near
//! plane, as well as the distance to the far plane.
//!
//! \details Will create a matrix that that creates perspective, and
//! transforms from left handed (forward = z, up = y) coordinates, to
//! that required by DirectX (negateZaxis = false; up = y, forward =
//! z) or Vulkan (negateZaxis = true; up = y, backwards = z)
//!
//! \returns A matrix that will transform a vector from world coords
//! to homogeneous camera plane coords (pre prerspective divide, which
//! in DX/Vulkan, will be performed by the rasteriser), creating
//! perspective as it does so.
//!
//! \param[in] viewWidth The width of the viewing plane / near
//! clipping plane, in terms of its actual size
//!
//! \param[in] viewHeight The height of the viewing plane / near
//! clipping plane, in terms of its actual size
//!
//! \param[in] viewNear The distance to the near clipping plane, from
//! the camera / origin
//!
//! \param[in] viewFar The distance to the far clipping plane, from
//! the camera / origin.
//!
//! \param[in] negateZaxis Literally, should the z axis be negated?
//! Used to ensure output in is in the correct format, for the given
//! graphics API. That is, this should be on for Vulkan and off for
//! directX
CETIOPTER_LIB_API WideMatrix matrix3DPerspectiveCameraLH(
	double viewWidth, double viewHeight, double viewNear,
	double viewFar, bool negateZaxis = false) noexcept;
//! \brief Construct a perspective transform matrix, for left handed
//! input, from the specicifed FOV/aspect ratio/distance of the near
//! plane, as well as the distance to the far plane.
//!
//! \note FOV is vertical FOV, and aspect ratio is width / height
//!
//! \details Will create a matrix that that creates perspective, and
//! transforms from left handed (forward = z, up = y) coordinates, to
//! that required by DirectX (negateZaxis = false; up = y, forward =
//! z) or Vulkan (negateZaxis = true; up = y, backwards = z)
//!
//! \returns A matrix that will transform a vector from world coords
//! to homogeneous camera plane coords (pre prerspective divide, which
//! in DX/Vulkan, will be performed by the rasteriser), creating
//! perspective as it does so.
//!
//! \param[in] verticalFOV The vertical field of view angle, of the
//! near plane
//!
//! \param[in] aspectRatio The ratio of width per unit height, of the
//! near clipping plane
//!
//! \param[in] viewNear The distance to the near clipping plane, from
//! the camera / origin
//!
//! \param[in] viewFar The distance to the far clipping plane, from
//! the camera / origin.
//!
//! \param[in] negateZaxis Literally, should the z axis be negated?
//! Used to ensure output in is in the correct format, for the given
//! graphics API. That is, this should be on for Vulkan and off for
//! directX
CETIOPTER_LIB_API WideMatrix matrix3DPerspectiveFOVCameraLH(
	double verticalFOV, double aspectRatio, double viewNear,
	double viewFar, bool negateZaxis = false) noexcept;
//! \brief Construct a orthographic transform matrix, for left handed
//! input, from the specicifed width/height/distance of the near
//! plane, as well as the distance to the far plane.
//!
//! \details Will create a matrix that that creates an orthographic
//! projection, and transforms from left handed (forward = z, up = y)
//! coordinates, to that required by DirectX (negateZaxis = false; up
//! = y, forward = z) or Vulkan (negateZaxis = true; up = y, backwards
//! = z)
//!
//! \returns A matrix that will transform a vector from world coords
//! to homogeneous camera plane coords (pre prerspective divide, which
//! in DX/Vulkan, will be performed by the rasteriser), without
//! creating perspective as it does so
//!
//! \param[in] viewWidth The width of the viewing plane / near
//! clipping plane, in terms of its actual size
//!
//! \param[in] viewHeight The height of the viewing plane / near
//! clipping plane, in terms of its actual size
//!
//! \param[in] viewNear The distance to the near clipping plane, from
//! the camera / origin
//!
//! \param[in] viewFar The distance to the far clipping plane, from
//! the camera / origin.
//!
//! \param[in] negateZaxis Literally, should the z axis be negated?
//! Used to ensure output in is in the correct format, for the given
//! graphics API. That is, this should be on for Vulkan and off for
//! directX
CETIOPTER_LIB_API WideMatrix matrix3DOrthographicCameraLH(
	double viewWidth, double viewHeight, double viewNear,
	double viewFar, bool negateZaxis = false) noexcept;

//! @}
}  // namespace Maths
}  // namespace frao

#endif