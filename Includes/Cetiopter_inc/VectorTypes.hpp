#ifndef FRAO_CETIOPTER_VECTOR_TYPES
#define FRAO_CETIOPTER_VECTOR_TYPES

//! \file
//!
//! \brief Header of vector types, specifically those contain double
//! sized floating point data and calculations
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <cstdint>	//for integer types
#include "APIDefines.hpp"
#include "DataStorageTypes.hpp"	 //for loading of data to matrices, even in AVX mode

// These have IMPL friend functions we need to see, even in AVX mode
#include "2DComparisons.hpp"
#include "3DComparisons.hpp"
#include "4DComparisons.hpp"
#include "ComponentComparison.hpp"

#ifndef FRAO_AVX
// include all functions, for friends
#include "2DComparisons.hpp"
#include "4DComparisons.hpp"
#include "ComponentAccess.hpp"
#include "ComponentMaths.hpp"
#include "Conversions.hpp"
#include "DimensionalMaths.hpp"
#include "SpecialMatrices.hpp"
#include "SpecialVectors.hpp"
#endif

namespace frao
{
namespace Maths
{
#if !defined(FRAO_AVX) || defined(FOR_DOXYGEN)
//! \brief Class to represent a 4 component float SIMD vector.
//!
//! \details Correctly aligned, 4 component float vector. Alignment
//! is mainly so we can switch to using __m128
//!
//! \note Depending on compilation mode, may be either a typedef for a
//! SIMD register, or a class. If it's a class, it will have a 4
//! component array to represent x->w components with elements 0->3
//! respectively
class CETIOPTER_LIB_API alignas(16) NarrowVector
{
	//! \brief An array of values to simulate an SSE __m128
	//! register, with 0 being the logical x component, 1 being y, 2
	//! being z, and 3 being w
	float m_Values[4];

	//! \brief Private constructor for creating a float vector from
	//! constituent values.
	//!
	//! \note Private, in order to enforce using the load functions,
	//! which are necessary so that we can use __m128 type properly,
	//! in AVX mode
	//!
	//! \param[in] x The x component of the vector to be created
	//!
	//! \param[in] y The y component of the vector to be created
	//!
	//! \param[in] z The z component of the vector to be created
	//!
	//! \param[in] w The w component of the vector to be created
	NarrowVector(float x, float y, float z, float w) noexcept;

   public:
	//! \brief Public constructor to create a default-initialised
	//! NarrowVector
	NarrowVector() noexcept;

#ifndef FOR_DOXYGEN
	// so that these functions can access internal data, while we
	// don't have __m128's to use
	friend CETIOPTER_LIB_API NarrowVector
	loadFloat2(const Float2& data) noexcept;
	friend CETIOPTER_LIB_API NarrowVector
	loadFloat3(const Float3& data) noexcept;
	friend CETIOPTER_LIB_API NarrowVector
	loadFloat4(const Float4& data) noexcept;

	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeFloat2(Float2& store, RegArgNrwVec data) noexcept;
	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeFloat3(Float3& store, RegArgNrwVec data) noexcept;
	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeFloat4(Float4& store, RegArgNrwVec data) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	toWideVector(RegArgNrwVec vector) noexcept;
	friend CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
	toNarrowVector(RegArgVector vector) noexcept;

	friend CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
												vectorSetByIndex(RegArgNrwVec vector, float number,
																 uint_least64_t elementIndex) noexcept;
	friend CETIOPTER_LIB_API float FRAO_VECTORCALL vectorGetByIndex(
		RegArgNrwVec vector, uint_least64_t elementIndex) noexcept;

	friend CETIOPTER_LIB_API float FRAO_VECTORCALL
	vectorGetX(RegArgNrwVec vector) noexcept;
	friend CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
	vectorSetX(RegArgNrwVec vector, float number) noexcept;

	friend CETIOPTER_LIB_API float FRAO_VECTORCALL
	vectorGetY(RegArgNrwVec vector) noexcept;
	friend CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
	vectorSetY(RegArgNrwVec vector, float number) noexcept;

	friend CETIOPTER_LIB_API float FRAO_VECTORCALL
	vectorGetZ(RegArgNrwVec vector) noexcept;
	friend CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
	vectorSetZ(RegArgNrwVec vector, float number) noexcept;

	friend CETIOPTER_LIB_API float FRAO_VECTORCALL
	vectorGetW(RegArgNrwVec vector) noexcept;
	friend CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
	vectorSetW(RegArgNrwVec vector, float number) noexcept;

	friend CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
	vectorSet(float x, float y, float z, float w) noexcept;
#endif
};
//! \brief Class to represent a 4 component int32 SIMD vector.
//!
//! \details Correctly aligned, 4 component int32 vector. Alignment
//! is mainly so we can switch to using __m128i
//!
//! \note Depending on compilation mode, may be either a typedef for a
//! SIMD register, or a class. If it's a class, it will have a 4
//! component array to represent x->w components with elements 0->3
//! respectively
class CETIOPTER_LIB_API alignas(16) NarrowIntVector
{
	//! \brief An array of values to simulate an SSE __m128i
	//! register, with 0 being the logical x component, 1 being y, 2
	//! being z, and 3 being w
	int_least32_t m_Values[4];

	//! \brief Private constructor for creating an int32 vector from
	//! constituent values.
	//!
	//! \note Private, in order to enforce using the load functions,
	//! which are necessary so that we can use __m128i type properly,
	//! in AVX mode
	//!
	//! \param[in] x The x component of the vector to be created
	//!
	//! \param[in] y The y component of the vector to be created
	//!
	//! \param[in] z The z component of the vector to be created
	//!
	//! \param[in] w The w component of the vector to be created
	NarrowIntVector(int_least32_t x, int_least32_t y, int_least32_t z,
					int_least32_t w) noexcept;

   public:
	//! \brief Public constructor to create a default-initialised
	//! NarrowVector
	NarrowIntVector() noexcept;

#ifndef FOR_DOXYGEN
	// so that these functions can access internal data, while we
	// don't have __m128's to use
	friend CETIOPTER_LIB_API NarrowIntVector
	loadSmallInt2(const SmallInt2& data) noexcept;
	friend CETIOPTER_LIB_API NarrowIntVector
	loadSmallInt3(const SmallInt3& data) noexcept;
	friend CETIOPTER_LIB_API NarrowIntVector
	loadSmallInt4(const SmallInt4& data) noexcept;

	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeSmallInt2(SmallInt2& store, RegIntNrwVec data) noexcept;
	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeSmallInt3(SmallInt3& store, RegIntNrwVec data) noexcept;
	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeSmallInt4(SmallInt4& store, RegIntNrwVec data) noexcept;

	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	toWideIntVector(RegIntNrwVec vector) noexcept;
	friend CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
	toSmallIntVector(RegIntVector vector) noexcept;

	friend CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
	vectorSetByIndex(RegIntNrwVec vector, int_least32_t number,
					 uint_least64_t elementIndex) noexcept;
	friend CETIOPTER_LIB_API int_least32_t FRAO_VECTORCALL
	vectorGetByIndex(RegIntNrwVec	vector,
					 uint_least64_t elementIndex) noexcept;

	friend CETIOPTER_LIB_API int_least32_t FRAO_VECTORCALL
	vectorGetX(RegIntNrwVec vector) noexcept;
	friend CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
	vectorSetX(RegIntNrwVec vector, int_least32_t number) noexcept;

	friend CETIOPTER_LIB_API int_least32_t FRAO_VECTORCALL
	vectorGetY(RegIntNrwVec vector) noexcept;
	friend CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
	vectorSetY(RegIntNrwVec vector, int_least32_t number) noexcept;

	friend CETIOPTER_LIB_API int_least32_t FRAO_VECTORCALL
	vectorGetZ(RegIntNrwVec vector) noexcept;
	friend CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
	vectorSetZ(RegIntNrwVec vector, int_least32_t number) noexcept;

	friend CETIOPTER_LIB_API int_least32_t FRAO_VECTORCALL
	vectorGetW(RegIntNrwVec vector) noexcept;
	friend CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
	vectorSetW(RegIntNrwVec vector, int_least32_t number) noexcept;

	friend CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
	vectorSet(int_least32_t x, int_least32_t y, int_least32_t z,
			  int_least32_t w) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSwizzle(RegArgVector vector, RegIntNrwVec choices) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorSwizzle(RegIntVector vector, RegIntNrwVec choices) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorPermute(RegArgVector lhsVector, RegArgVector rhsVector,
				  RegIntNrwVec choices) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorPermute(RegIntVector lhsVector, RegIntVector rhsVector,
				  RegIntNrwVec choices) noexcept;

	friend CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
	vectorAnd(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept;
	friend CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
	vectorOr(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept;
	friend CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
	vectorXor(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept;
	friend CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
	vectorNot(RegIntNrwVec vector) noexcept;

	friend CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
	vectorAddition(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept;
	friend CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
	vectorSubtract(RegIntNrwVec lhsVec, RegIntNrwVec rhsVec) noexcept;
#endif
};

//! \brief Class to represent a 4 component double SIMD vector.
//!
//! \details Correctly aligned, 4 component double vector. Alignment
//! is mainly so we can switch to using __m256d
//!
//! \note Depending on compilation mode, may be either a typedef for a
//! SIMD register, or a class. If it's a class, it will have a 4
//! component array to represent x->w components with elements 0->3
//! respectively
class CETIOPTER_LIB_API alignas(32) WideVector
{
	//! \brief An array of values to simulate an AVX __m256d
	//! register, with 0 being the logical x component, 1 being y, 2
	//! being z, and 3 being w
	double m_Values[4];

	//! \brief Private constructor for creating a double vector from
	//! constituent values.
	//!
	//! \note Private, in order to enforce using the load functions,
	//! which are necessary so that we can use __m256d type properly,
	//! in AVX mode
	//!
	//! \param[in] x The x component of the vector to be created
	//!
	//! \param[in] y The y component of the vector to be created
	//!
	//! \param[in] z The z component of the vector to be created
	//!
	//! \param[in] w The w component of the vector to be created
	WideVector(double x, double y, double z, double w) noexcept;

   public:
	//! \brief Public constructor to create a default-initialised
	//! WideVector
	WideVector() noexcept;

#ifndef FOR_DOXYGEN
	// so that these functions can access internal data, while we
	// don't have __m256d's to use
	friend CETIOPTER_LIB_API WideVector
	loadDouble2(const Double2& data) noexcept;
	friend CETIOPTER_LIB_API WideVector
	loadDouble3(const Double3& data) noexcept;
	friend CETIOPTER_LIB_API WideVector
	loadDouble4(const Double4& data) noexcept;

	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeDouble2(Double2& store, RegArgVector data) noexcept;
	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeDouble3(Double3& store, RegArgVector data) noexcept;
	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeDouble4(Double4& store, RegArgVector data) noexcept;

	friend CETIOPTER_LIB_API WideVector vectorZero() noexcept;
	friend CETIOPTER_LIB_API WideVector vectorSplatOne() noexcept;
	friend CETIOPTER_LIB_API WideVector vectorSplatQNaN() noexcept;
	friend CETIOPTER_LIB_API WideVector vectorSplatSNaN() noexcept;
	friend CETIOPTER_LIB_API WideVector vectorSplatInfinity() noexcept;
	friend CETIOPTER_LIB_API WideVector vectorSplatEpsilon() noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	reinterpretToDouble(RegIntVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	reinterpretToInteger(RegArgVector vector) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	toDoubleVector(RegIntVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	toIntegerVector(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	toWideVector(RegArgNrwVec vector) noexcept;
	friend CETIOPTER_LIB_API NarrowVector FRAO_VECTORCALL
	toNarrowVector(RegArgVector vector) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorRound(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorFloor(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorCeiling(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorTruncate(RegArgVector vector) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
												 vectorSetByIndex(RegArgVector vector, double number,
																  uint_least64_t elementIndex) noexcept;
	friend CETIOPTER_LIB_API double FRAO_VECTORCALL vectorGetByIndex(
		RegArgVector vector, uint_least64_t elementIndex) noexcept;

	friend CETIOPTER_LIB_API double FRAO_VECTORCALL
	vectorGetX(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSetX(RegArgVector vector, double number) noexcept;

	friend CETIOPTER_LIB_API double FRAO_VECTORCALL
	vectorGetY(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSetY(RegArgVector vector, double number) noexcept;

	friend CETIOPTER_LIB_API double FRAO_VECTORCALL
	vectorGetZ(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSetZ(RegArgVector vector, double number) noexcept;

	friend CETIOPTER_LIB_API double FRAO_VECTORCALL
	vectorGetW(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSetW(RegArgVector vector, double number) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSet(double x, double y, double z, double w) noexcept;

	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
	matrixSet(double r1_c1, double r1_c2, double r1_c3, double r1_c4,
			  double r2_c1, double r2_c2, double r2_c3, double r2_c4,
			  double r3_c1, double r3_c2, double r3_c3, double r3_c4,
			  double r4_c1, double r4_c2, double r4_c3,
			  double r4_c4) noexcept;

	friend CETIOPTER_LIB_API WideVector
	vectorReplicate(double value) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSplatX(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSplatY(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSplatZ(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSplatW(RegArgVector vector) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSwizzle(RegArgVector vector, RegIntNrwVec choices) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSwizzle(RegArgVector vector, uint_least8_t xChoice,
				  uint_least8_t yChoice, uint_least8_t zChoice,
				  uint_least8_t wChoice) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
													 vectorPermute(RegArgVector lhsVector, RegArgVector rhsVector,
																   RegIntNrwVec choices) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL vectorPermute(
		RegArgVector lhsVector, RegArgVector rhsVector,
		uint_least8_t xChoice, uint_least8_t yChoice,
		uint_least8_t zChoice, uint_least8_t wChoice) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL vectorRotateLeft(
		RegArgVector vector, uint_least8_t numElements) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorRotateRight(RegArgVector	vector,
					  uint_least8_t numElements) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL vectorMergeXY(
		RegArgVector lhsVector, RegArgVector rhsVector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL vectorMergeZW(
		RegArgVector lhsVector, RegArgVector rhsVector) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorAddition(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSubtract(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorMultiply(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorDivide(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorScale(RegArgVector vector, double scaleFactor) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorPow(RegArgVector baseVector,
			  RegArgVector exponentVector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSqrt(RegArgVector vector) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorSin(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorCos(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorTan(RegArgVector vector) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorArcSin(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorArcCos(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
													 vectorArcTan(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL vectorArcTan2(
		RegArgVector topVec, RegArgVector bottomVec) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorExp(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorExp2(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorLog(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorLog2(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorLog10(RegArgVector vector) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorMax(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vectorMin(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;

	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorLess(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorLessEqual(RegArgVector lhsVec,
					RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorGreater(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorGreaterEqual(RegArgVector lhsVec,
					   RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorNotEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorInBounds(RegArgVector checked, RegArgVector lowerBound,
				   RegArgVector upperBound) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorIsNaN(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorIsNaNRecord(uint_least64_t& record,
					  RegArgVector	  vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorIsInf(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorIsInfRecord(uint_least64_t& record,
					  RegArgVector	  vector) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vector2DDot(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vector2DLengthSq(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vector2DLength(RegArgVector vector) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vector3DDot(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vector3DCross(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vector3DLengthSq(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vector3DLength(RegArgVector vector) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vector4DDot(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vector4DLengthSq(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vector4DLength(RegArgVector vector) noexcept;
#endif
};
#endif

#if !defined(FRAO_AVX2) || defined(FOR_DOXYGEN)
//! \brief Class to represent a 4 component 64 bit integer SIMD
//! vector.
//!
//! \details Correctly aligned, 4 component integer vector. Alignment
//! is mainly so we can switch to using __m256i
//!
//! \note Depending on compilation mode, may be either a typedef for a
//! SIMD register, or a class. If it's a class, it will have a 4
//! component array to represent x->w components with elements 0->3
//! respectively
class CETIOPTER_LIB_API alignas(32) WideIntVector
{
	//! \brief An array of values to simulate an AVX2 __m256i
	//! register, with 0 being the logical x component, 1 being y, 2
	//! being z, and 3 being w
	int_least64_t m_Values[4];

	//! \brief Private constructor for creating an int vector from
	//! constituent values.
	//!
	//! \note Private, in order to enforce using the load functions,
	//! which are necessary so that we can use __m256i type properly,
	//! in AVX2 mode
	//!
	//! \param[in] x The x component of the vector to be created
	//!
	//! \param[in] y The y component of the vector to be created
	//!
	//! \param[in] z The z component of the vector to be created
	//!
	//! \param[in] w The w component of the vector to be created
	WideIntVector(int_least64_t x, int_least64_t y, int_least64_t z,
				  int_least64_t w) noexcept;

   public:
	//! \brief Public constructor to create a default-initialised
	//! WideIntVector
	WideIntVector() noexcept;

#ifndef FOR_DOXYGEN
	// so that these functions can access internal data, while we
	// don't have __m256i's to use
	friend CETIOPTER_LIB_API WideIntVector
	loadInteger2(const Integer2& data) noexcept;
	friend CETIOPTER_LIB_API WideIntVector
	loadInteger3(const Integer3& data) noexcept;
	friend CETIOPTER_LIB_API WideIntVector
	loadInteger4(const Integer4& data) noexcept;

	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeInteger2(Integer2& store, RegIntVector data) noexcept;
	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeInteger3(Integer3& store, RegIntVector data) noexcept;
	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeInteger4(Integer4& store, RegIntVector data) noexcept;

	friend CETIOPTER_LIB_API WideIntVector vectorZeroInt() noexcept;
	friend CETIOPTER_LIB_API WideIntVector vectorSplatAllBits() noexcept;
	friend CETIOPTER_LIB_API WideIntVector vectorSplatOneInt() noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	reinterpretToDouble(RegIntVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	reinterpretToInteger(RegArgVector vector) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	toDoubleVector(RegIntVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	toIntegerVector(RegArgVector vector) noexcept;

	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	toWideIntVector(RegIntNrwVec vector) noexcept;
	friend CETIOPTER_LIB_API NarrowIntVector FRAO_VECTORCALL
	toSmallIntVector(RegIntVector vector) noexcept;

	friend CETIOPTER_LIB_API int_least64_t FRAO_VECTORCALL
	vectorGetByIndex(RegIntVector	vector,
					 uint_least64_t elementIndex) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorSetByIndex(RegIntVector vector, int_least64_t number,
					 uint_least64_t elementIndex) noexcept;

	friend CETIOPTER_LIB_API int_least64_t FRAO_VECTORCALL
	vectorGetX(RegIntVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorSetX(RegIntVector vector, int_least64_t number) noexcept;

	friend CETIOPTER_LIB_API int_least64_t FRAO_VECTORCALL
	vectorGetY(RegIntVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorSetY(RegIntVector vector, int_least64_t number) noexcept;

	friend CETIOPTER_LIB_API int_least64_t FRAO_VECTORCALL
	vectorGetZ(RegIntVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorSetZ(RegIntVector vector, int_least64_t number) noexcept;

	friend CETIOPTER_LIB_API int_least64_t FRAO_VECTORCALL
	vectorGetW(RegIntVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorSetW(RegIntVector vector, int_least64_t number) noexcept;

	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorSet(int_least64_t x, int_least64_t y, int_least64_t z,
			  int_least64_t w) noexcept;

	friend CETIOPTER_LIB_API WideIntVector
	vectorReplicate(int_least64_t value) noexcept;

	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorSplatX(RegIntVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorSplatY(RegIntVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorSplatZ(RegIntVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorSplatW(RegIntVector vector) noexcept;

	friend CETIOPTER_LIB_API WideIntVector
	vectorSelectControl(bool useXRhs, bool useYRhs, bool useZRhs,
						bool useWRhs) noexcept;

	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorSwizzle(RegIntVector vector, RegIntNrwVec choices) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorSwizzle(RegIntVector vector, uint_least8_t xChoice,
				  uint_least8_t yChoice, uint_least8_t zChoice,
				  uint_least8_t wChoice) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
														vectorPermute(RegIntVector lhsVector, RegIntVector rhsVector,
																	  RegIntNrwVec choices) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL vectorPermute(
		RegIntVector lhsVector, RegIntVector rhsVector,
		uint_least8_t xChoice, uint_least8_t yChoice,
		uint_least8_t zChoice, uint_least8_t wChoice) noexcept;

	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorRotateLeft(RegIntVector  vector,
					 uint_least8_t numElements) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorRotateRight(RegIntVector	vector,
					  uint_least8_t numElements) noexcept;

	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL vectorMergeXY(
		RegIntVector lhsVector, RegIntVector rhsVector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL vectorMergeZW(
		RegIntVector lhsVector, RegIntVector rhsVector) noexcept;

	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorAnd(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorOr(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorXor(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorNot(RegIntVector vector) noexcept;

	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorAddition(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorSubtract(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorMultiply(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorDivide(RegIntVector lhsVec, RegIntVector rhsVec,
				 WideIntVector* remRes) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorRemainder(RegIntVector lhsVec,
					RegIntVector rhsVec) noexcept;

	friend uint_least64_t FRAO_VECTORCALL
	IMPL::convRecord(RegIntVector source) noexcept;
	friend uint_least64_t FRAO_VECTORCALL
	IMPL::conv2DRecord(RegIntVector source) noexcept;
	friend uint_least64_t FRAO_VECTORCALL
	IMPL::conv3DRecord(RegIntVector source) noexcept;

	friend bool FRAO_VECTORCALL
	IMPL::result2DAllTrue(RegIntVector fullResult) noexcept;
	friend bool FRAO_VECTORCALL
	IMPL::result2DAnyTrue(RegIntVector fullResult) noexcept;
	friend bool FRAO_VECTORCALL
	IMPL::result3DAllTrue(RegIntVector fullResult) noexcept;
	friend bool FRAO_VECTORCALL
	IMPL::result3DAnyTrue(RegIntVector fullResult) noexcept;
	friend bool FRAO_VECTORCALL
	IMPL::result4DAllTrue(RegIntVector fullResult) noexcept;
	friend bool FRAO_VECTORCALL
	IMPL::result4DAnyTrue(RegIntVector fullResult) noexcept;

	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorLess(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorLess(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorLessRecord(uint_least64_t& record, RegArgVector lhsVec,
					 RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorLessRecord(uint_least64_t& record, RegIntVector lhsVec,
					 RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorLessEqual(RegArgVector lhsVec,
					RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorLessEqual(RegIntVector lhsVec,
					RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorLessEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
						  RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorLessEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
						  RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorGreater(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorGreater(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorGreaterRecord(uint_least64_t& record, RegArgVector lhsVec,
						RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorGreaterRecord(uint_least64_t& record, RegIntVector lhsVec,
						RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorGreaterEqual(RegArgVector lhsVec,
					   RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorGreaterEqual(RegIntVector lhsVec,
					   RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorGreaterEqualRecord(uint_least64_t& record,
							 RegArgVector	 lhsVec,
							 RegArgVector	 rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorGreaterEqualRecord(uint_least64_t& record,
							 RegIntVector	 lhsVec,
							 RegIntVector	 rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					  RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					  RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorNotEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorNotEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorNotEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
						 RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorNotEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
						 RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorInBounds(RegIntVector checked, RegIntVector lowerBound,
				   RegIntVector upperBound) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorInBounds(RegArgVector checked, RegArgVector lowerBound,
				   RegArgVector upperBound) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorInBoundsRecord(uint_least64_t& record, RegIntVector checked,
						 RegIntVector lowerBound,
						 RegIntVector upperBound) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorInBoundsRecord(uint_least64_t& record, RegArgVector checked,
						 RegArgVector lowerBound,
						 RegArgVector upperBound) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorIsNaN(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorIsNaNRecord(uint_least64_t& record,
					  RegArgVector	  vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorIsInf(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
	vectorIsInfRecord(uint_least64_t& record,
					  RegArgVector	  vector) noexcept;

	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector2DLess(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector2DLess(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector2DLessRecord(uint_least64_t& record, RegArgVector lhsVec,
					   RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector2DLessRecord(uint_least64_t& record, RegIntVector lhsVec,
																  RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector2DLessEqual(
		RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector2DLessEqual(
		RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector2DLessEqualRecord(uint_least64_t& record,
							RegArgVector	lhsVec,
							RegArgVector	rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector2DLessEqualRecord(uint_least64_t& record,
																	   RegIntVector	   lhsVec,
																	   RegIntVector	   rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector2DGreater(
		RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector2DGreater(
		RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector2DGreaterRecord(uint_least64_t& record, RegArgVector lhsVec,
						  RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector2DGreaterRecord(uint_least64_t& record, RegIntVector lhsVec,
																	 RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector2DGreaterEqual(
		RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector2DGreaterEqual(
		RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector2DGreaterEqualRecord(uint_least64_t& record,
							   RegArgVector	   lhsVec,
							   RegArgVector	   rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector2DGreaterEqualRecord(uint_least64_t& record,
							   RegIntVector	   lhsVec,
							   RegIntVector	   rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector2DEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector2DEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector2DEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
						RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector2DEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
																   RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector2DNotEqual(
		RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector2DNotEqual(
		RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector2DNotEqualRecord(
		uint_least64_t& record, RegArgVector lhsVec,
		RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector2DNotEqualRecord(
		uint_least64_t& record, RegIntVector lhsVec,
		RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector2DInBounds(RegArgVector checked, RegArgVector lowerBound,
					 RegArgVector upperBound) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector2DInBounds(RegIntVector checked, RegIntVector lowerBound,
																RegIntVector upperBound) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector2DInBoundsRecord(
		uint_least64_t& record, RegArgVector checked,
		RegArgVector lowerBound, RegArgVector upperBound) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector2DInBoundsRecord(
		uint_least64_t& record, RegIntVector checked,
		RegIntVector lowerBound, RegIntVector upperBound) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector2DIsNaN(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector2DIsNaNRecord(
		uint_least64_t& record, RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector2DIsInf(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector2DIsInfRecord(
		uint_least64_t& record, RegArgVector vector) noexcept;

	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector3DLess(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector3DLess(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector3DLessRecord(uint_least64_t& record, RegArgVector lhsVec,
					   RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector3DLessRecord(uint_least64_t& record, RegIntVector lhsVec,
																  RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DLessEqual(
		RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DLessEqual(
		RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector3DLessEqualRecord(uint_least64_t& record,
							RegArgVector	lhsVec,
							RegArgVector	rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector3DLessEqualRecord(uint_least64_t& record,
																	   RegIntVector	   lhsVec,
																	   RegIntVector	   rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DGreater(
		RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DGreater(
		RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector3DGreaterRecord(uint_least64_t& record, RegArgVector lhsVec,
						  RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector3DGreaterRecord(uint_least64_t& record, RegIntVector lhsVec,
																	 RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DGreaterEqual(
		RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DGreaterEqual(
		RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector3DGreaterEqualRecord(uint_least64_t& record,
							   RegArgVector	   lhsVec,
							   RegArgVector	   rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector3DGreaterEqualRecord(uint_least64_t& record,
							   RegIntVector	   lhsVec,
							   RegIntVector	   rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector3DEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector3DEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector3DEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
						RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector3DEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
																   RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DNotEqual(
		RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DNotEqual(
		RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DNotEqualRecord(
		uint_least64_t& record, RegArgVector lhsVec,
		RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DNotEqualRecord(
		uint_least64_t& record, RegIntVector lhsVec,
		RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector3DInBounds(RegArgVector checked, RegArgVector lowerBound,
					 RegArgVector upperBound) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector3DInBounds(RegIntVector checked, RegIntVector lowerBound,
																RegIntVector upperBound) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DInBoundsRecord(
		uint_least64_t& record, RegArgVector checked,
		RegArgVector lowerBound, RegArgVector upperBound) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DInBoundsRecord(
		uint_least64_t& record, RegIntVector checked,
		RegIntVector lowerBound, RegIntVector upperBound) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector3DIsNaN(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DIsNaNRecord(
		uint_least64_t& record, RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector3DIsInf(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector3DIsInfRecord(
		uint_least64_t& record, RegArgVector vector) noexcept;

	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector4DLess(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector4DLess(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector4DLessRecord(uint_least64_t& record, RegArgVector lhsVec,
					   RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector4DLessRecord(uint_least64_t& record, RegIntVector lhsVec,
																  RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DLessEqual(
		RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DLessEqual(
		RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector4DLessEqualRecord(uint_least64_t& record,
							RegArgVector	lhsVec,
							RegArgVector	rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector4DLessEqualRecord(uint_least64_t& record,
																	   RegIntVector	   lhsVec,
																	   RegIntVector	   rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DGreater(
		RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DGreater(
		RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector4DGreaterRecord(uint_least64_t& record, RegArgVector lhsVec,
						  RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector4DGreaterRecord(uint_least64_t& record, RegIntVector lhsVec,
																	 RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DGreaterEqual(
		RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DGreaterEqual(
		RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector4DGreaterEqualRecord(uint_least64_t& record,
							   RegArgVector	   lhsVec,
							   RegArgVector	   rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector4DGreaterEqualRecord(uint_least64_t& record,
							   RegIntVector	   lhsVec,
							   RegIntVector	   rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector4DEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector4DEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector4DEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
						RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector4DEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
																   RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DNotEqual(
		RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DNotEqual(
		RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DNotEqualRecord(
		uint_least64_t& record, RegArgVector lhsVec,
		RegArgVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DNotEqualRecord(
		uint_least64_t& record, RegIntVector lhsVec,
		RegIntVector rhsVec) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	vector4DInBounds(RegArgVector checked, RegArgVector lowerBound,
					 RegArgVector upperBound) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector4DInBounds(RegIntVector checked, RegIntVector lowerBound,
																RegIntVector upperBound) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DInBoundsRecord(
		uint_least64_t& record, RegArgVector checked,
		RegArgVector lowerBound, RegArgVector upperBound) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DInBoundsRecord(
		uint_least64_t& record, RegIntVector checked,
		RegIntVector lowerBound, RegIntVector upperBound) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector4DIsNaN(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DIsNaNRecord(
		uint_least64_t& record, RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
											   vector4DIsInf(RegArgVector vector) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL vector4DIsInfRecord(
		uint_least64_t& record, RegArgVector vector) noexcept;
#endif
};
#endif

//! \brief Class to represent a matrix, with a group of 4 SIMD
//! registers / NarrowVectors
class CETIOPTER_LIB_API alignas(16) NarrowMatrix
{
	//! \brief Array of four vectors, with each representing a row of
	//! the matrix. First row is at element [0], second is at element
	//! [1], etc
	NarrowVector m_Tuples[4];

	//! \brief Construct an (implicitly 2D) matrix with the specified
	//! first two rows
	//!
	//! \details z and w elements of the first two rows will be set to
	//! 0, and the 3rd and 4th rows will be equal to the corresponding
	//! rows of the 4D identity matrix
	//!
	//! \warning Private, so as to enforce API consistency, via use of
	//! load/store functions
	//!
	//! \param[in] v0 The x and y components to be used for the first
	//! two elements of the *first* row of the matrix
	//!
	//! \param[in] v1 The x and y components to be used for the first
	//! two elements of the *second* row of the matrix
	NarrowMatrix(const Float2& v0, const Float2& v1) noexcept;
	//! \brief Construct an (implicitly 3D) matrix with the specified
	//! first three rows
	//!
	//! \details w element of the first two rows will be set to 0, and
	//! the 4th row will be equal to the corresponding row of the 4D
	//! identity matrix
	//!
	//! \warning Private, so as to enforce API consistency, via use of
	//! load/store functions
	//!
	//! \param[in] v0 The x, y, and z components to be used for the
	//! first three elements of the *first* row of the matrix
	//!
	//! \param[in] v1 The  x, y, and z components to be used for the
	//! first three elements of the *second* row of the matrix
	//!
	//! \param[in] v2 The  x, y, and z components to be used for the
	//! first three elements of the *third* row of the matrix
	NarrowMatrix(const Float3& v0, const Float3& v1,
				 const Float3& v2) noexcept;
	//! \brief Construct a (4D) matrix with the specified four rows
	//!
	//! \warning Private, so as to enforce API consistency, via use of
	//! load/store functions
	//!
	//! \param[in] v0 The data to be used for the *first* row of the
	//! matrix
	//!
	//! \param[in] v1 The data to be used for the *second* row of the
	//! matrix
	//!
	//! \param[in] v2 The data to be used for the *third* row of the
	//! matrix
	//!
	//! \param[in] v3 The data to be used for the *fourth* row of the
	//! matrix
	NarrowMatrix(const Float4& v0, const Float4& v1, const Float4& v2,
				 const Float4& v3) noexcept;
	//! \brief Construct a (4D) matrix with the specified four rows
	//!
	//! \warning Private, so as to enforce API consistency, via use of
	//! load/store functions
	//!
	//! \param[in] v0 The data to be used for the *first* row of the
	//! matrix
	//!
	//! \param[in] v1 The data to be used for the *second* row of the
	//! matrix
	//!
	//! \param[in] v2 The data to be used for the *third* row of the
	//! matrix
	//!
	//! \param[in] v3 The data to be used for the *fourth* row of the
	//! matrix
	NarrowMatrix(LateArgNrwVec v0, LateArgNrwVec v1, LateArgNrwVec v2,
				 LateArgNrwVec v3) noexcept;

   public:
	//! \brief Construct a matrix by default initialising all
	//! constituent members
	NarrowMatrix() noexcept;

#ifndef FOR_DOXYGEN
	// so that these functions can access internal data, while we
	// don't have __m256d's to use
	friend CETIOPTER_LIB_API NarrowMatrix
	loadFloat2x2(const Float2x2& data) noexcept;
	friend CETIOPTER_LIB_API NarrowMatrix
	loadFloat3x3(const Float3x3& data) noexcept;
	friend CETIOPTER_LIB_API NarrowMatrix
	loadFloat4x4(const Float4x4& data) noexcept;

	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeFloat2x2(Float2x2& store, RegArgNrwMtrx data) noexcept;
	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeFloat3x3(Float3x3& store, RegArgNrwMtrx data) noexcept;
	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeFloat4x4(Float4x4& store, RegArgNrwMtrx data) noexcept;

	friend CETIOPTER_LIB_API NarrowMatrix FRAO_VECTORCALL
	toNarrowMatrix(RegArgMatrix matrix) noexcept;
	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
	toWideMatrix(RegArgNrwMtrx matrix) noexcept;
#endif
};
//! \brief Class to represent a matrix, with a group of 4 SIMD
//! registers / WideVectors
class CETIOPTER_LIB_API alignas(32) WideMatrix
{
	//! \brief Array of four vectors, with each representing a row of
	//! the matrix. First row is at element [0], second is at element
	//! [1], etc
	WideVector m_Tuples[4];

	//! \brief Construct an (implicitly 2D) matrix with the specified
	//! first two rows
	//!
	//! \details z and w elements of the first two rows will be set to
	//! 0, and the 3rd and 4th rows will be equal to the corresponding
	//! rows of the 4D identity matrix
	//!
	//! \warning Private, so as to enforce API consistency, via use of
	//! load/store functions
	//!
	//! \param[in] v0 The x and y components to be used for the first
	//! two elements of the *first* row of the matrix
	//!
	//! \param[in] v1 The x and y components to be used for the first
	//! two elements of the *second* row of the matrix
	WideMatrix(const Double2& v0, const Double2& v1) noexcept;
	//! \brief Construct an (implicitly 3D) matrix with the specified
	//! first three rows
	//!
	//! \details w element of the first two rows will be set to 0, and
	//! the 4th row will be equal to the corresponding row of the 4D
	//! identity matrix
	//!
	//! \warning Private, so as to enforce API consistency, via use of
	//! load/store functions
	//!
	//! \param[in] v0 The x, y, and z components to be used for the
	//! first three elements of the *first* row of the matrix
	//!
	//! \param[in] v1 The  x, y, and z components to be used for the
	//! first three elements of the *second* row of the matrix
	//!
	//! \param[in] v2 The  x, y, and z components to be used for the
	//! first three elements of the *third* row of the matrix
	WideMatrix(const Double3& v0, const Double3& v1,
			   const Double3& v2) noexcept;
	//! \brief Construct a (4D) matrix with the specified four rows
	//!
	//! \warning Private, so as to enforce API consistency, via use of
	//! load/store functions
	//!
	//! \param[in] v0 The data to be used for the *first* row of the
	//! matrix
	//!
	//! \param[in] v1 The data to be used for the *second* row of the
	//! matrix
	//!
	//! \param[in] v2 The data to be used for the *third* row of the
	//! matrix
	//!
	//! \param[in] v3 The data to be used for the *fourth* row of the
	//! matrix
	WideMatrix(const Double4& v0, const Double4& v1,
			   const Double4& v2, const Double4& v3) noexcept;
	//! \brief Construct a (4D) matrix with the specified four rows
	//!
	//! \warning Private, so as to enforce API consistency, via use of
	//! load/store functions
	//!
	//! \param[in] v0 The data to be used for the *first* row of the
	//! matrix
	//!
	//! \param[in] v1 The data to be used for the *second* row of the
	//! matrix
	//!
	//! \param[in] v2 The data to be used for the *third* row of the
	//! matrix
	//!
	//! \param[in] v3 The data to be used for the *fourth* row of the
	//! matrix
	WideMatrix(LateArgVector v0, LateArgVector v1, LateArgVector v2,
			   LateArgVector v3) noexcept;

   public:
	//! \brief Construct a matrix by default initialising all
	//! constituent members
	WideMatrix() noexcept;

#ifndef FOR_DOXYGEN
	// so that these functions can access internal data, while we
	// don't have __m256d's to use
	friend CETIOPTER_LIB_API WideMatrix
	loadDouble2x2(const Double2x2& data) noexcept;
	friend CETIOPTER_LIB_API WideMatrix
	loadDouble3x3(const Double3x3& data) noexcept;
	friend CETIOPTER_LIB_API WideMatrix
	loadDouble4x4(const Double4x4& data) noexcept;

	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeDouble2x2(Double2x2& store, RegArgMatrix data) noexcept;
	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeDouble3x3(Double3x3& store, RegArgMatrix data) noexcept;
	friend CETIOPTER_LIB_API void FRAO_VECTORCALL
	storeDouble4x4(Double4x4& store, RegArgMatrix data) noexcept;

	friend CETIOPTER_LIB_API NarrowMatrix FRAO_VECTORCALL
	toNarrowMatrix(RegArgMatrix matrix) noexcept;
	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
	toWideMatrix(RegArgNrwMtrx matrix) noexcept;

	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
	matrixSetRow(RegArgMatrix matrix, RegArgVector row,
				 uint_least64_t index) noexcept;
	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
	matrixSetColumn(RegArgMatrix matrix, RegArgVector column,
					uint_least64_t index) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	matrixGetRow(RegArgMatrix matrix, uint_least64_t index) noexcept;
	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
	matrixSet(double r1_c1, double r1_c2, double r1_c3, double r1_c4,
			  double r2_c1, double r2_c2, double r2_c3, double r2_c4,
			  double r3_c1, double r3_c2, double r3_c3, double r3_c4,
			  double r4_c1, double r4_c2, double r4_c3,
			  double r4_c4) noexcept;
	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
	matrixSet(RegArgVector row1, RegArgVector row2, RegArgVector row3,
			  RegArgVector row4) noexcept;

	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL matrixAddition(
		RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept;
	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL matrixMultiply(
		RegArgMatrix multiplier, LateArgMatrix multiplicand) noexcept;
	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
	matrixScale(RegArgMatrix matrix, double scaleFactor) noexcept;
	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
	matrixTranspose(RegArgMatrix matrix) noexcept;

	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
	matrix2DScaling(RegArgVector scaling) noexcept;
	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
	matrix3DScaling(RegArgVector scaling) noexcept;

	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
	matrix3DRotationRHFromVector(RegArgVector rotAngles) noexcept;
	friend CETIOPTER_LIB_API WideMatrix FRAO_VECTORCALL
	matrix3DRotationLHFromVector(RegArgVector rotAngles) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vector2DTransform(RegArgVector vector,
					  RegArgMatrix matrix) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	matrix2DDeterminant(RegArgMatrix matrix) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	matrix2DInverse(WideMatrix& result, RegArgMatrix matrix) noexcept;

	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL matrix2DEqual(
		RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL matrix2DNotEqual(
		RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	matrix2DIsNaN(RegArgMatrix matrix) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	matrix2DIsInf(RegArgMatrix matrix) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vector3DTransform(RegArgVector vector,
					  RegArgMatrix matrix) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	matrix3DDeterminant(RegArgMatrix matrix) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	matrix3DInverse(WideMatrix& result, RegArgMatrix matrix) noexcept;

	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL matrix3DEqual(
		RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL matrix3DNotEqual(
		RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	matrix3DIsNaN(RegArgMatrix matrix) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	matrix3DIsInf(RegArgMatrix matrix) noexcept;

	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	vector4DTransform(RegArgVector vector,
					  RegArgMatrix matrix) noexcept;
	friend CETIOPTER_LIB_API WideVector FRAO_VECTORCALL
	matrix4DDeterminant(RegArgMatrix matrix) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	matrix4DInverse(WideMatrix& result, RegArgMatrix matrix) noexcept;

	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL matrix4DEqual(
		RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL matrix4DNotEqual(
		RegArgMatrix lhsMatrix, LateArgMatrix rhsMatrix) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	matrix4DIsNaN(RegArgMatrix matrix) noexcept;
	friend CETIOPTER_LIB_API bool FRAO_VECTORCALL
	matrix4DIsInf(RegArgMatrix matrix) noexcept;
#endif
};

}  // namespace Maths
}  // namespace frao

#endif