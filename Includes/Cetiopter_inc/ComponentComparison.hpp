#ifndef FRAO_CETIOPTER_COMPONENT_COMPARISON
#define FRAO_CETIOPTER_COMPONENT_COMPARISON

//! \file
//!
//! \brief Header of comparisons between vectors
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright (C) Freya Rhiannon Mayger 2022. All rights reserved.
//! Full licence available in 'LICENCE' file, in the root source code
//! folder of this project

#include <cstdint>	//for integer types
#include "APIDefines.hpp"

namespace frao
{
namespace Maths
{
//! \defgroup VectorRecordEvaluation Vector record evaluation
//!
//! \brief Functions to evaluate records of completed comparisons
//!
//! @{

//! \brief Evaluate a comparison record to see if all component
//! comparisons were true
//!
//! \returns True iff all component comparisons were true
//!
//! \param[in] record The comparison record to evaluate
CETIOPTER_LIB_API bool comparisonAllTrue(uint_least64_t record) noexcept;
//! \brief Evaluate a comparison record to see if any component
//! comparisons were true
//!
//! \returns True if any component comparisons were true
//!
//! \param[in] record The comparison record to evaluate
CETIOPTER_LIB_API bool comparisonAnyTrue(uint_least64_t record) noexcept;
//! \brief Evaluate a comparison record to see if all component
//! comparisons were false
//!
//! \returns True iff all component comparisons were **false**
//!
//! \param[in] record The comparison record to evaluate
CETIOPTER_LIB_API bool comparisonAllFalse(uint_least64_t record) noexcept;
//! \brief Evaluate a comparison record to see if any component
//! comparisons were false
//!
//! \returns True if any component comparisons were **false**
//!
//! \param[in] record The comparison record to evaluate
CETIOPTER_LIB_API bool comparisonAnyFalse(uint_least64_t record) noexcept;
//! \brief Evaluate a comparison record to see if there were both true
//! and false results of component comparisons
//!
//! \returns True iff both true and false results were recorded
//!
//! \param[in] record The comparison record to evaluate
CETIOPTER_LIB_API bool comparisonMixedResult(
	uint_least64_t record) noexcept;

//! @}

namespace IMPL
{
uint_least64_t FRAO_VECTORCALL
convRecord(RegIntVector source) noexcept;
}  // namespace IMPL

//! \defgroup VectorComponentComparisons Vector Component Comparisons
//!
//! \brief Per component camparison functions like less-than,
//! greater-than, inBounds, etc.
//!
//! @{

//! \brief Compare two double vectors component by component, setting
//! a component of the result to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was less than the right hand
//! side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was smaller
//! than the same-position rhs component. Iff the lhs was smaller,
//! result will have ULLONG_MAX in that position
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorLess(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, setting a
//! component of the result to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was less than the right hand
//! side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was smaller
//! than the same-position rhs component. Iff the lhs was smaller,
//! result will have ULLONG_MAX in that position
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the less-than sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the less-than sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorLess(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and setting a component
//! of the return value to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was less than the right hand
//! side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was smaller
//! than the same-position rhs component. Iff the lhs was smaller,
//! return value will have ULLONG_MAX in that position
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorLessRecord(uint_least64_t& record, RegArgVector lhsVec,
				 RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, recording
//! the result to an int64 variable, and setting a component of the
//! return value to ULLONG_MAX, iff for the corresponding operand
//! components, left hand side was less than the right hand side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was smaller
//! than the same-position rhs component. Iff the lhs was smaller,
//! return value will have ULLONG_MAX in that position
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the less-than sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the less-than sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorLessRecord(uint_least64_t& record, RegIntVector lhsVec,
				 RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, setting
//! a component of the result to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was less than or equal to the
//! right hand side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was smaller
//! than or equal to the same-position rhs component. Iff the lhs was
//! smaller or equally sized, result will have ULLONG_MAX in that
//! position
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorLessEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, setting a
//! component of the result to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was less than or equal to the
//! right hand side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was smaller
//! than or equal to the same-position rhs component. Iff the lhs was
//! smaller or equally sized, result will have ULLONG_MAX in that
//! position
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorLessEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and setting a component
//! of the return value to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was less than or equal to the
//! right hand side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was smaller
//! than or equal to the same-position rhs component. Iff the lhs was
//! smaller or equally sized, return value will have ULLONG_MAX in
//! that position
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorLessEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					  RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component,
//! recording the result to an int64 variable, and setting a component
//! of the return value to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was less than or equal to the
//! right hand side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was smaller
//! than or equal to the same-position rhs component. Iff the lhs was
//! smaller or equally sized, return value will have ULLONG_MAX in
//! that position
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on
//! the left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on
//! the right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorLessEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					  RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, setting
//! a component of the result to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was greater than the right hand
//! side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was larger
//! than the same-position rhs component. Iff the lhs was larger,
//! result will have ULLONG_MAX in that position
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the greater-than sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the greater-than sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorGreater(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, setting a
//! component of the result to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was greater than the right hand
//! side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was larger
//! than the same-position rhs component. Iff the lhs was larger,
//! result will have ULLONG_MAX in that position
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the greater-than sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the greater-than sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorGreater(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and setting a component
//! of the return value to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was greater than the right hand
//! side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was larger
//! than the same-position rhs component. Iff the lhs was larger,
//! return value will have ULLONG_MAX in that position
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the greater-than sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the greater-than sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorGreaterRecord(uint_least64_t& record, RegArgVector lhsVec,
					RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, recording
//! the result to an int64 variable, and setting a component of the
//! return value to ULLONG_MAX, iff for the corresponding operand
//! components, left hand side was greater than the right hand side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was larger
//! than the same-position rhs component. Iff the lhs was larger,
//! return value will have ULLONG_MAX in that position
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the greater-than sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the greater-than sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorGreaterRecord(uint_least64_t& record, RegIntVector lhsVec,
					RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, setting
//! a component of the result to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was less than or equal to the
//! right hand side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was larger
//! than or equal to the same-position rhs component. Iff the lhs was
//! larger or equally sized, result will have ULLONG_MAX in that
//! position
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the less-than-or-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the less-than-or-equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorGreaterEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, setting a
//! component of the result to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was greater than or equal to
//! the right hand side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was larger
//! than or equal to the same-position rhs component. Iff the lhs was
//! larger or equally sized, result will have ULLONG_MAX in that
//! position
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the greater-than-or-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the greater-than-or-equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorGreaterEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and setting a component
//! of the return value to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was greater than or equal to
//! the right hand side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was larger
//! than or equal to the same-position rhs component. Iff the lhs was
//! larger or equally sized, return value will have ULLONG_MAX in
//! that position
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the greater-than-or-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the greater-than-or-equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorGreaterEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
						 RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component,
//! recording the result to an int64 variable, and setting a component
//! of the return value to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was greater than or equal to
//! the right hand side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was larger
//! than or equal to the same-position rhs component. Iff the lhs was
//! larger or equally sized, return value will have ULLONG_MAX in
//! that position
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on
//! the left hand side (lhs) of the greater-than-or-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on
//! the right hand side (rhs) of the greater-than-or-equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorGreaterEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
						 RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, setting
//! a component of the result to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was equal to the right hand
//! side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was equal to
//! the same-position rhs component. Iff the lhs was equally sized,
//! result will have ULLONG_MAX in that position
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, setting a
//! component of the result to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was equal to the right hand
//! side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was equal to
//! the same-position rhs component. Iff the lhs was equally sized,
//! result will have ULLONG_MAX in that position
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and setting a component
//! of the return value to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was equal to the right hand
//! side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was equal to
//! the same-position rhs component. Iff the lhs was equally sized,
//! return value will have ULLONG_MAX in that position
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
				  RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component,
//! recording the result to an int64 variable, and setting a component
//! of the return value to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was equal to the right hand
//! side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was equal to
//! the same-position rhs component. Iff the lhs was equally sized,
//! return value will have ULLONG_MAX in that position
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on
//! the left hand side (lhs) of the equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on
//! the right hand side (rhs) of the equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
				  RegIntVector rhsVec) noexcept;

//! \brief Compare two double vectors component by component, setting
//! a component of the result to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was not equal to the right hand
//! side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was not equal
//! to the same-position rhs component. Iff the lhs was not equally
//! sized, result will have ULLONG_MAX in that position
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the not-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the not-equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorNotEqual(RegArgVector lhsVec, RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component, setting a
//! component of the result to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was not equal to the right hand
//! side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was not equal
//! to the same-position rhs component. Iff the lhs was not equally
//! sized, result will have ULLONG_MAX in that position
//!
//! \param[in] lhsVec The int vector of components to compare on the
//! left hand side (lhs) of the not-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on the
//! right hand side (rhs) of the not-equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorNotEqual(RegIntVector lhsVec, RegIntVector rhsVec) noexcept;
//! \brief Compare two double vectors component by component,
//! recording the result to an int64 variable, and setting a component
//! of the return value to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was not equal to the right hand
//! side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was not equal
//! to the same-position rhs component. Iff the lhs was not equally
//! sized, return value will have ULLONG_MAX in that position
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The double vector of components to compare on
//! the left hand side (lhs) of the not-equal-to sign
//!
//! \param[in] rhsVec The double vector of components to compare on
//! the right hand side (rhs) of the not-equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorNotEqualRecord(uint_least64_t& record, RegArgVector lhsVec,
					 RegArgVector rhsVec) noexcept;
//! \brief Compare two int vectors component by component,
//! recording the result to an int64 variable, and setting a component
//! of the return value to ULLONG_MAX, iff for the corresponding
//! operand components, left hand side was not equal to the right hand
//! side
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding lhs component was not equal
//! to the same-position rhs component. Iff the lhs was not equally
//! sized, return value will have ULLONG_MAX in that position
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] lhsVec The int vector of components to compare on
//! the left hand side (lhs) of the not-equal-to sign
//!
//! \param[in] rhsVec The int vector of components to compare on
//! the right hand side (rhs) of the not-equal-to sign
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorNotEqualRecord(uint_least64_t& record, RegIntVector lhsVec,
					 RegIntVector rhsVec) noexcept;

//! \brief Compare three double vectors component by component,
//! setting a component of the result to ULLONG_MAX, iff for the
//! corresponding operand components, the checked vector was greater
//! than or equal to the lower bound, and less than or equal to the
//! higher bound
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding checked component was
//! inclusively within the specified bounds. Iff the checked component
//! is within bounds, result will have ULLONG_MAX in that position
//!
//! \param[in] checked The double vector of components to compare
//! against both bounds
//!
//! \param[in] lowerBound The double vector of components to against
//! the checked component, to determine whether the checked component
//! is greater than or equal to the corresponding component of
//! lowerBound
//!
//! \param[in] upperBound The double vector of components to against
//! the checked component, to determine whether the checked component
//! is less than or equal to the corresponding component of upperBound
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorInBounds(RegArgVector checked, RegArgVector lowerBound,
			   RegArgVector upperBound) noexcept;
//! \brief Compare three int vectors component by component,
//! setting a component of the result to ULLONG_MAX, iff for the
//! corresponding operand components, the checked vector was greater
//! than or equal to the lower bound, and less than or equal to the
//! higher bound
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding checked component was
//! inclusively within the specified bounds. Iff the checked component
//! is within bounds, result will have ULLONG_MAX in that position
//!
//! \param[in] checked The int vector of components to compare
//! against both bounds
//!
//! \param[in] lowerBound The int vector of components to against
//! the checked component, to determine whether the checked component
//! is greater than or equal to the corresponding component of
//! lowerBound
//!
//! \param[in] upperBound The int vector of components to against
//! the checked component, to determine whether the checked component
//! is less than or equal to the corresponding component of upperBound
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorInBounds(RegIntVector checked, RegIntVector lowerBound,
			   RegIntVector upperBound) noexcept;

//! \brief Compare three double vectors component by component,
//! recording the result to an int64 variable, and setting a component
//! of the return value to ULLONG_MAX, iff for the corresponding
//! operand components, the checked vector was greater than or equal
//! to the lower bound, and less than or equal to the higher bound
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding checked component was
//! inclusively within the specified bounds. Iff the checked component
//! is within bounds, the return value will have ULLONG_MAX in that
//! position
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] checked The double vector of components to compare
//! against both bounds
//!
//! \param[in] lowerBound The double vector of components to against
//! the checked component, to determine whether the checked component
//! is greater than or equal to the corresponding component of
//! lowerBound
//!
//! \param[in] upperBound The double vector of components to against
//! the checked component, to determine whether the checked component
//! is less than or equal to the corresponding component of upperBound
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL vectorInBoundsRecord(
	uint_least64_t& record, RegArgVector checked,
	RegArgVector lowerBound, RegArgVector upperBound) noexcept;
//! \brief Compare three int vectors component by component,
//! recording the result to an int64 variable, and setting a component
//! of the return value to ULLONG_MAX, iff for the corresponding
//! operand components, the checked vector was greater than or equal
//! to the lower bound, and less than or equal to the higher bound
//!
//! \returns A vector whose components will be ULLONG_MAX or 0,
//! depending on whether the corresponding checked component was
//! inclusively within the specified bounds. Iff the checked component
//! is within bounds, the return value will have ULLONG_MAX in that
//! position
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] checked The int vector of components to compare against
//! both bounds
//!
//! \param[in] lowerBound The int vector of components to against the
//! checked component, to determine whether the checked component is
//! greater than or equal to the corresponding component of lowerBound
//!
//! \param[in] upperBound The int vector of components to against the
//! checked component, to determine whether the checked component is
//! less than or equal to the corresponding component of upperBound
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL vectorInBoundsRecord(
	uint_least64_t& record, RegIntVector checked,
	RegIntVector lowerBound, RegIntVector upperBound) noexcept;

//! \brief Check each component of a vector, and return ULLONG_MAX
//! (for the corresponding component) when a component is a type of
//! NaN
//!
//! \returns A vector with components of ULLONG_MAX or 0, depending on
//! whether the corresponding component of the input was NaN or not
//!
//! \param[in] vector The vector whose components should be checked vs
//! NaN
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorIsNaN(RegArgVector vector) noexcept;
//! \brief Check each component of a vector, return ULLONG_MAX (for
//! the corresponding component) when a component is a type of NaN,
//! and record the result to an int
//!
//! \returns A vector with components of ULLONG_MAX or 0, depending on
//! whether the corresponding component of the input was NaN or not
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] vector The vector whose components should be checked vs
//! NaN
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL vectorIsNaNRecord(
	uint_least64_t& record, RegArgVector vector) noexcept;

//! \brief Check each component of a vector, and return ULLONG_MAX
//! (for the corresponding component) when a component is a type of
//! infinity
//!
//! \returns A vector with components of ULLONG_MAX or 0, depending on
//! whether the corresponding component of the input was inf or not
//!
//! \param[in] vector The vector whose components should be checked vs
//! inf
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL
vectorIsInf(RegArgVector vector) noexcept;
//! \brief Check each component of a vector, return ULLONG_MAX (for
//! the corresponding component) when a component is a type of
//! infinity, and record the result to an int
//!
//! \returns A vector with components of ULLONG_MAX or 0, depending on
//! whether the corresponding component of the input was inf or not
//!
//! \param[out] record A reference to the variable at which to record
//! the result of the comparison
//!
//! \param[in] vector The vector whose components should be checked vs
//! inf
CETIOPTER_LIB_API WideIntVector FRAO_VECTORCALL vectorIsInfRecord(
	uint_least64_t& record, RegArgVector vector) noexcept;

//! @}
}  // namespace Maths
}  // namespace frao

#endif