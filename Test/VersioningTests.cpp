#include <doctest.h>
#include "Version/VersionIncl.hpp"

SCENARIO("Versioning Tests")
{
    constexpr const char nullChar = 0x0;

    auto version = frao::getCetiopterVersion();

	THEN("Valid Major")
    {
        constexpr const frao::small_nat8 maxMajor = 63;

        CHECK_LE(version.getMajorComponent(), maxMajor);
    }
	THEN("Valid Minor")
    {
        constexpr const frao::small_nat16 maxMinor = 1023;

        CHECK_LE(version.getMajorComponent(), maxMinor);
    }
    THEN("Valid Status members")
    {
        //We just minimally test that we don't have invalid characters
        CHECK_NE(version.getStatusCharacterComponent<0>(), nullChar);
        CHECK_NE(version.getStatusCharacterComponent<1>(), nullChar);
    }
	WHEN("Valid Status ptr")
    {
        const char* status = version.getStatusFullComponent();

        CHECK_NE(status, nullptr);

        THEN("Valid Ptr Status members")
        {
            //We just minimally test that we don't have invalid characters
            CHECK_NE(status[0], nullChar);
            CHECK_NE(status[1], nullChar);
        }
    }
}
