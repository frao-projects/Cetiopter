//! \file
//!
//! \brief Tests of Vector Types
//!
//! \author Freya Rhiannon Mayger
//!
//! \copyright Freya Rhiannon Mayger 2020. All rights reserved

#include <doctest.h>
#include <array>						//for std::array
#include <cmath>						//for std::pow
#include <Nucleus_inc\Environment.hpp>	//for PI
#include <Nucleus_inc\BitUtilities.hpp>	//for SetBits
#include <limits>						//for numeric_limits
#include <tuple>						//for tuple, std::apply
#include <type_traits>					//for is_floating_point_v
#include "2DComparisons.hpp"
#include "3DComparisons.hpp"
#include "4DComparisons.hpp"
#include "ComponentAccess.hpp"
#include "ComponentComparison.hpp"
#include "ComponentMaths.hpp"
#include "Conversions.hpp"
#include "DimensionalMaths.hpp"
#include "SpecialMatrices.hpp"
#include "SpecialVectors.hpp"
#include "VectorTypes.hpp"

using namespace frao::Maths;

// standard constants, for tests
constexpr static const int_least64_t g_TrueVal =
	0xFFFF'FFFF'FFFF'FFFF;
constexpr static const int_least64_t g_MinInt =
	std::numeric_limits<int_least64_t>::min();
constexpr static const int_least64_t g_MaxInt =
	std::numeric_limits<int_least64_t>::max();
constexpr static const double g_Epsilon =
	std::numeric_limits<double>::epsilon();
constexpr static const float g_FEpsilon =
	std::numeric_limits<float>::epsilon();
constexpr static const double g_MillionDouble = 1'000'000.0;
constexpr static const float  g_MillionFloat  = 1'000'000.0f;
constexpr static const double g_MinDouble =
	std::numeric_limits<double>::min();
constexpr static const float g_MinFloat =
	std::numeric_limits<float>::min();
constexpr static const double g_MaxDouble =
	std::numeric_limits<double>::max();
constexpr static const float g_MaxFloat =
	std::numeric_limits<float>::max();
constexpr static const double g_InfDouble =
	std::numeric_limits<double>::infinity();
constexpr static const float g_InfFloat =
	std::numeric_limits<float>::infinity();
constexpr static const double g_QNaN =
	std::numeric_limits<double>::quiet_NaN();
constexpr static const double g_PI	= frao::PI_D;
constexpr static const float  g_FPI = frao::PI_F;

void approxEqual(double lhs, double rhs, double numEpsilon) noexcept
{
	double magEpsilon = numEpsilon * g_Epsilon;

	if (std::isfinite(lhs) && std::isfinite(rhs))
	{
		if (lhs < 0.0)
		{
			rhs = -rhs;
			lhs = -lhs;
		}

		if (lhs <= 1.0)
		{
			CHECK_GE(rhs, lhs - magEpsilon);
			CHECK_LE(rhs, lhs + magEpsilon);
		} else
		{
			CHECK_GE(rhs, lhs - (magEpsilon * lhs));
			CHECK_LE(rhs, lhs + (magEpsilon * lhs));
		}
	} else if (std::isinf(lhs) || std::isinf(rhs))
	{
		// check both are same inf
		CHECK_EQ(lhs, rhs);
	} else
	{
		// check both are NaN
		CHECK_NE(lhs, lhs);
		CHECK_NE(rhs, rhs);
	}
}

template<typename DataType>
bool dataStoreEq(DataType store1, DataType store2) noexcept;

template<>
bool dataStoreEq<float>(float store1, float store2) noexcept
{
	return (store1 == store2);
}
template<>
bool dataStoreEq<Float2>(Float2 store1, Float2 store2) noexcept
{
	return (store1.m_Components.m_X == store2.m_Components.m_X)
		   && (store1.m_Components.m_Y == store2.m_Components.m_Y);
}

template<>
bool dataStoreEq<Float3>(Float3 store1, Float3 store2) noexcept
{
	return (store1.m_Components.m_X == store2.m_Components.m_X)
		   && (store1.m_Components.m_Y == store2.m_Components.m_Y)
		   && (store1.m_Components.m_Z == store2.m_Components.m_Z);
}

template<>
bool dataStoreEq<Float4>(Float4 store1, Float4 store2) noexcept
{
	return (store1.m_Components.m_X == store2.m_Components.m_X)
		   && (store1.m_Components.m_Y == store2.m_Components.m_Y)
		   && (store1.m_Components.m_Z == store2.m_Components.m_Z)
		   && (store1.m_Components.m_W == store2.m_Components.m_W);
}

template<>
bool dataStoreEq<int_least32_t>(int_least32_t store1,
								int_least32_t store2) noexcept
{
	return (store1 == store2);
}
template<>
bool dataStoreEq<SmallInt2>(SmallInt2 store1,
							SmallInt2 store2) noexcept
{
	return (store1.m_Components.m_X == store2.m_Components.m_X)
		   && (store1.m_Components.m_Y == store2.m_Components.m_Y);
}

template<>
bool dataStoreEq<SmallInt3>(SmallInt3 store1,
							SmallInt3 store2) noexcept
{
	return (store1.m_Components.m_X == store2.m_Components.m_X)
		   && (store1.m_Components.m_Y == store2.m_Components.m_Y)
		   && (store1.m_Components.m_Z == store2.m_Components.m_Z);
}

template<>
bool dataStoreEq<SmallInt4>(SmallInt4 store1,
							SmallInt4 store2) noexcept
{
	return (store1.m_Components.m_X == store2.m_Components.m_X)
		   && (store1.m_Components.m_Y == store2.m_Components.m_Y)
		   && (store1.m_Components.m_Z == store2.m_Components.m_Z)
		   && (store1.m_Components.m_W == store2.m_Components.m_W);
}

template<>
bool dataStoreEq<double>(double store1, double store2) noexcept
{
	return (store1 == store2);
}
template<>
bool dataStoreEq<Double2>(Double2 store1, Double2 store2) noexcept
{
	return (store1.m_Components.m_X == store2.m_Components.m_X)
		   && (store1.m_Components.m_Y == store2.m_Components.m_Y);
}

template<>
bool dataStoreEq<Double3>(Double3 store1, Double3 store2) noexcept
{
	return (store1.m_Components.m_X == store2.m_Components.m_X)
		   && (store1.m_Components.m_Y == store2.m_Components.m_Y)
		   && (store1.m_Components.m_Z == store2.m_Components.m_Z);
}

template<>
bool dataStoreEq<Double4>(Double4 store1, Double4 store2) noexcept
{
	return (store1.m_Components.m_X == store2.m_Components.m_X)
		   && (store1.m_Components.m_Y == store2.m_Components.m_Y)
		   && (store1.m_Components.m_Z == store2.m_Components.m_Z)
		   && (store1.m_Components.m_W == store2.m_Components.m_W);
}

template<>
bool dataStoreEq<Float2x2>(Float2x2 store1, Float2x2 store2) noexcept
{
	return (dataStoreEq(store1.m_Tuples[0], store2.m_Tuples[0])
			&& dataStoreEq(store1.m_Tuples[1], store2.m_Tuples[1]));
}

template<>
bool dataStoreEq<Float3x3>(Float3x3 store1, Float3x3 store2) noexcept
{
	return (dataStoreEq(store1.m_Tuples[0], store2.m_Tuples[0])
			&& dataStoreEq(store1.m_Tuples[1], store2.m_Tuples[1])
			&& dataStoreEq(store1.m_Tuples[2], store2.m_Tuples[2]));
}

template<>
bool dataStoreEq<Float4x4>(Float4x4 store1, Float4x4 store2) noexcept
{
	return (dataStoreEq(store1.m_Tuples[0], store2.m_Tuples[0])
			&& dataStoreEq(store1.m_Tuples[1], store2.m_Tuples[1])
			&& dataStoreEq(store1.m_Tuples[2], store2.m_Tuples[2])
			&& dataStoreEq(store1.m_Tuples[3], store2.m_Tuples[3]));
}

template<>
bool dataStoreEq<Double2x2>(Double2x2 store1,
							Double2x2 store2) noexcept
{
	return (dataStoreEq(store1.m_Tuples[0], store2.m_Tuples[0])
			&& dataStoreEq(store1.m_Tuples[1], store2.m_Tuples[1]));
}

template<>
bool dataStoreEq<Double3x3>(Double3x3 store1,
							Double3x3 store2) noexcept
{
	return (dataStoreEq(store1.m_Tuples[0], store2.m_Tuples[0])
			&& dataStoreEq(store1.m_Tuples[1], store2.m_Tuples[1])
			&& dataStoreEq(store1.m_Tuples[2], store2.m_Tuples[2]));
}

template<>
bool dataStoreEq<Double4x4>(Double4x4 store1,
							Double4x4 store2) noexcept
{
	return (dataStoreEq(store1.m_Tuples[0], store2.m_Tuples[0])
			&& dataStoreEq(store1.m_Tuples[1], store2.m_Tuples[1])
			&& dataStoreEq(store1.m_Tuples[2], store2.m_Tuples[2])
			&& dataStoreEq(store1.m_Tuples[3], store2.m_Tuples[3]));
}

template<>
bool dataStoreEq<int_least64_t>(int_least64_t store1,
								int_least64_t store2) noexcept
{
	return (store1 == store2);
}
template<>
bool dataStoreEq<Integer2>(Integer2 store1, Integer2 store2) noexcept
{
	return (store1.m_Components.m_X == store2.m_Components.m_X)
		   && (store1.m_Components.m_Y == store2.m_Components.m_Y);
}

template<>
bool dataStoreEq<Integer3>(Integer3 store1, Integer3 store2) noexcept
{
	return (store1.m_Components.m_X == store2.m_Components.m_X)
		   && (store1.m_Components.m_Y == store2.m_Components.m_Y)
		   && (store1.m_Components.m_Z == store2.m_Components.m_Z);
}

template<>
bool dataStoreEq<Integer4>(Integer4 store1, Integer4 store2) noexcept
{
	return (store1.m_Components.m_X == store2.m_Components.m_X)
		   && (store1.m_Components.m_Y == store2.m_Components.m_Y)
		   && (store1.m_Components.m_Z == store2.m_Components.m_Z)
		   && (store1.m_Components.m_W == store2.m_Components.m_W);
}

template<typename DataType>
void checkStoreEq(DataType store1, DataType store2) noexcept;


template<>
void checkStoreEq<float>(float store1, float store2) noexcept
{
	CHECK_EQ(store1, store2);
}
template<>
void checkStoreEq<Float2>(Float2 store1, Float2 store2) noexcept
{
	CHECK_EQ(store1.m_Components.m_X, store2.m_Components.m_X);
	CHECK_EQ(store1.m_Components.m_Y, store2.m_Components.m_Y);
}

template<>
void checkStoreEq<Float3>(Float3 store1, Float3 store2) noexcept
{
	CHECK_EQ(store1.m_Components.m_X, store2.m_Components.m_X);
	CHECK_EQ(store1.m_Components.m_Y, store2.m_Components.m_Y);
	CHECK_EQ(store1.m_Components.m_Z, store2.m_Components.m_Z);
}

template<>
void checkStoreEq<Float4>(Float4 store1, Float4 store2) noexcept
{
	CHECK_EQ(store1.m_Components.m_X, store2.m_Components.m_X);
	CHECK_EQ(store1.m_Components.m_Y, store2.m_Components.m_Y);
	CHECK_EQ(store1.m_Components.m_Z, store2.m_Components.m_Z);
	CHECK_EQ(store1.m_Components.m_W, store2.m_Components.m_W);
}

template<>
void checkStoreEq<int_least32_t>(int_least32_t store1,
								 int_least32_t store2) noexcept
{
	CHECK_EQ(store1, store2);
}
template<>
void checkStoreEq<SmallInt2>(SmallInt2 store1,
							 SmallInt2 store2) noexcept
{
	CHECK_EQ(store1.m_Components.m_X, store2.m_Components.m_X);
	CHECK_EQ(store1.m_Components.m_Y, store2.m_Components.m_Y);
}

template<>
void checkStoreEq<SmallInt3>(SmallInt3 store1,
							 SmallInt3 store2) noexcept
{
	CHECK_EQ(store1.m_Components.m_X, store2.m_Components.m_X);
	CHECK_EQ(store1.m_Components.m_Y, store2.m_Components.m_Y);
	CHECK_EQ(store1.m_Components.m_Z, store2.m_Components.m_Z);
}

template<>
void checkStoreEq<SmallInt4>(SmallInt4 store1,
							 SmallInt4 store2) noexcept
{
	CHECK_EQ(store1.m_Components.m_X, store2.m_Components.m_X);
	CHECK_EQ(store1.m_Components.m_Y, store2.m_Components.m_Y);
	CHECK_EQ(store1.m_Components.m_Z, store2.m_Components.m_Z);
	CHECK_EQ(store1.m_Components.m_W, store2.m_Components.m_W);
}

template<>
void checkStoreEq<double>(double store1, double store2) noexcept
{
	CHECK_EQ(store1, store2);
}
template<>
void checkStoreEq<Double2>(Double2 store1, Double2 store2) noexcept
{
	CHECK_EQ(store1.m_Components.m_X, store2.m_Components.m_X);
	CHECK_EQ(store1.m_Components.m_Y, store2.m_Components.m_Y);
}

template<>
void checkStoreEq<Double3>(Double3 store1, Double3 store2) noexcept
{
	CHECK_EQ(store1.m_Components.m_X, store2.m_Components.m_X);
	CHECK_EQ(store1.m_Components.m_Y, store2.m_Components.m_Y);
	CHECK_EQ(store1.m_Components.m_Z, store2.m_Components.m_Z);
}

template<>
void checkStoreEq<Double4>(Double4 store1, Double4 store2) noexcept
{
	CHECK_EQ(store1.m_Components.m_X, store2.m_Components.m_X);
	CHECK_EQ(store1.m_Components.m_Y, store2.m_Components.m_Y);
	CHECK_EQ(store1.m_Components.m_Z, store2.m_Components.m_Z);
	CHECK_EQ(store1.m_Components.m_W, store2.m_Components.m_W);
}

template<>
void checkStoreEq<Float2x2>(Float2x2 store1, Float2x2 store2) noexcept
{
	checkStoreEq(store1.m_Tuples[0], store2.m_Tuples[0]);
	checkStoreEq(store1.m_Tuples[1], store2.m_Tuples[1]);
}

template<>
void checkStoreEq<Float3x3>(Float3x3 store1, Float3x3 store2) noexcept
{
	checkStoreEq(store1.m_Tuples[0], store2.m_Tuples[0]);
	checkStoreEq(store1.m_Tuples[1], store2.m_Tuples[1]);
	checkStoreEq(store1.m_Tuples[2], store2.m_Tuples[2]);
}

template<>
void checkStoreEq<Float4x4>(Float4x4 store1, Float4x4 store2) noexcept
{
	checkStoreEq(store1.m_Tuples[0], store2.m_Tuples[0]);
	checkStoreEq(store1.m_Tuples[1], store2.m_Tuples[1]);
	checkStoreEq(store1.m_Tuples[2], store2.m_Tuples[2]);
	checkStoreEq(store1.m_Tuples[3], store2.m_Tuples[3]);
}

template<>
void checkStoreEq<Double2x2>(Double2x2 store1,
							 Double2x2 store2) noexcept
{
	checkStoreEq(store1.m_Tuples[0], store2.m_Tuples[0]);
	checkStoreEq(store1.m_Tuples[1], store2.m_Tuples[1]);
}

template<>
void checkStoreEq<Double3x3>(Double3x3 store1,
							 Double3x3 store2) noexcept
{
	checkStoreEq(store1.m_Tuples[0], store2.m_Tuples[0]);
	checkStoreEq(store1.m_Tuples[1], store2.m_Tuples[1]);
	checkStoreEq(store1.m_Tuples[2], store2.m_Tuples[2]);
}

template<>
void checkStoreEq<Double4x4>(Double4x4 store1,
							 Double4x4 store2) noexcept
{
	checkStoreEq(store1.m_Tuples[0], store2.m_Tuples[0]);
	checkStoreEq(store1.m_Tuples[1], store2.m_Tuples[1]);
	checkStoreEq(store1.m_Tuples[2], store2.m_Tuples[2]);
	checkStoreEq(store1.m_Tuples[3], store2.m_Tuples[3]);
}

template<>
void checkStoreEq<int_least64_t>(int_least64_t store1,
								 int_least64_t store2) noexcept
{
	CHECK_EQ(store1, store2);
}
template<>
void checkStoreEq<Integer2>(Integer2 store1, Integer2 store2) noexcept
{
	CHECK_EQ(store1.m_Components.m_X, store2.m_Components.m_X);
	CHECK_EQ(store1.m_Components.m_Y, store2.m_Components.m_Y);
}

template<>
void checkStoreEq<Integer3>(Integer3 store1, Integer3 store2) noexcept
{
	CHECK_EQ(store1.m_Components.m_X, store2.m_Components.m_X);
	CHECK_EQ(store1.m_Components.m_Y, store2.m_Components.m_Y);
	CHECK_EQ(store1.m_Components.m_Z, store2.m_Components.m_Z);
}

template<>
void checkStoreEq<Integer4>(Integer4 store1, Integer4 store2) noexcept
{
	CHECK_EQ(store1.m_Components.m_X, store2.m_Components.m_X);
	CHECK_EQ(store1.m_Components.m_Y, store2.m_Components.m_Y);
	CHECK_EQ(store1.m_Components.m_Z, store2.m_Components.m_Z);
	CHECK_EQ(store1.m_Components.m_W, store2.m_Components.m_W);
}


template<typename DataType>
void dataStoreApproxEq(DataType store1, DataType store2,
					   double numEpsilon) noexcept;

template<>
void dataStoreApproxEq<Double2>(Double2 store1, Double2 store2,
								double numEpsilon) noexcept
{
	approxEqual(store1.m_Components.m_X, store2.m_Components.m_X,
				numEpsilon);
	approxEqual(store1.m_Components.m_Y, store2.m_Components.m_Y,
				numEpsilon);
}

template<>
void dataStoreApproxEq<Double3>(Double3 store1, Double3 store2,
								double numEpsilon) noexcept
{
	approxEqual(store1.m_Components.m_X, store2.m_Components.m_X,
				numEpsilon);
	approxEqual(store1.m_Components.m_Y, store2.m_Components.m_Y,
				numEpsilon);
	approxEqual(store1.m_Components.m_Z, store2.m_Components.m_Z,
				numEpsilon);
}

template<>
void dataStoreApproxEq<Double4>(Double4 store1, Double4 store2,
								double numEpsilon) noexcept
{
	approxEqual(store1.m_Components.m_X, store2.m_Components.m_X,
				numEpsilon);
	approxEqual(store1.m_Components.m_Y, store2.m_Components.m_Y,
				numEpsilon);
	approxEqual(store1.m_Components.m_Z, store2.m_Components.m_Z,
				numEpsilon);
	approxEqual(store1.m_Components.m_W, store2.m_Components.m_W,
				numEpsilon);
}

template<>
void dataStoreApproxEq<Double2x2>(Double2x2 store1, Double2x2 store2,
								  double numEpsilon) noexcept
{
	dataStoreApproxEq(store1.m_Tuples[0], store2.m_Tuples[0],
					  numEpsilon);
	dataStoreApproxEq(store1.m_Tuples[1], store2.m_Tuples[1],
					  numEpsilon);
}

template<>
void dataStoreApproxEq<Double3x3>(Double3x3 store1, Double3x3 store2,
								  double numEpsilon) noexcept
{
	dataStoreApproxEq(store1.m_Tuples[0], store2.m_Tuples[0],
					  numEpsilon);
	dataStoreApproxEq(store1.m_Tuples[1], store2.m_Tuples[1],
					  numEpsilon);
	dataStoreApproxEq(store1.m_Tuples[2], store2.m_Tuples[2],
					  numEpsilon);
}

template<>
void dataStoreApproxEq<Double4x4>(Double4x4 store1, Double4x4 store2,
								  double numEpsilon) noexcept
{
	dataStoreApproxEq(store1.m_Tuples[0], store2.m_Tuples[0],
					  numEpsilon);
	dataStoreApproxEq(store1.m_Tuples[1], store2.m_Tuples[1],
					  numEpsilon);
	dataStoreApproxEq(store1.m_Tuples[2], store2.m_Tuples[2],
					  numEpsilon);
	dataStoreApproxEq(store1.m_Tuples[3], store2.m_Tuples[3],
					  numEpsilon);
}

template<typename StoreType, typename DataType>
StoreType dataStoreRep(DataType data) noexcept;

template<>
Float2 dataStoreRep<Float2, float>(float data) noexcept
{
	return Float2{data, data};
}
template<>
Float3 dataStoreRep<Float3, float>(float data) noexcept
{
	return Float3{data, data, data};
}
template<>
Float4 dataStoreRep<Float4, float>(float data) noexcept
{
	return Float4{data, data, data, data};
}

template<>
SmallInt2 dataStoreRep<SmallInt2, int_least32_t>(
	int_least32_t data) noexcept
{
	return SmallInt2{data, data};
}
template<>
SmallInt3 dataStoreRep<SmallInt3, int_least32_t>(
	int_least32_t data) noexcept
{
	return SmallInt3{data, data, data};
}
template<>
SmallInt4 dataStoreRep<SmallInt4, int_least32_t>(
	int_least32_t data) noexcept
{
	return SmallInt4{data, data, data, data};
}

template<>
Double2 dataStoreRep<Double2, double>(double data) noexcept
{
	return Double2{data, data};
}
template<>
Double3 dataStoreRep<Double3, double>(double data) noexcept
{
	return Double3{data, data, data};
}
template<>
Double4 dataStoreRep<Double4, double>(double data) noexcept
{
	return Double4{data, data, data, data};
}

template<>
Float2x2 dataStoreRep<Float2x2, float>(float data) noexcept
{
	Float2 intermediate{data, data};
	return Float2x2{intermediate, intermediate};
}
template<>
Float3x3 dataStoreRep<Float3x3, float>(float data) noexcept
{
	Float3 intermediate{data, data, data};
	return Float3x3{intermediate, intermediate, intermediate};
}
template<>
Float4x4 dataStoreRep<Float4x4, float>(float data) noexcept
{
	Float4 intermediate{data, data, data, data};
	return Float4x4{intermediate, intermediate, intermediate,
					intermediate};
}

template<>
Double2x2 dataStoreRep<Double2x2, double>(double data) noexcept
{
	Double2 intermediate{data, data};
	return Double2x2{intermediate, intermediate};
}
template<>
Double3x3 dataStoreRep<Double3x3, double>(double data) noexcept
{
	Double3 intermediate{data, data, data};
	return Double3x3{intermediate, intermediate, intermediate};
}
template<>
Double4x4 dataStoreRep<Double4x4, double>(double data) noexcept
{
	Double4 intermediate{data, data, data, data};
	return Double4x4{intermediate, intermediate, intermediate,
					 intermediate};
}

template<>
Integer2 dataStoreRep<Integer2, int_least64_t>(
	int_least64_t data) noexcept
{
	return Integer2{data, data};
}
template<>
Integer3 dataStoreRep<Integer3, int_least64_t>(
	int_least64_t data) noexcept
{
	return Integer3{data, data, data};
}
template<>
Integer4 dataStoreRep<Integer4, int_least64_t>(
	int_least64_t data) noexcept
{
	return Integer4{data, data, data, data};
}

template<typename StoreType, typename DataType,
		 typename CreateFunctor, typename LoadFunctor,
		 typename StoreFunctor>
void evaluateSymmetric(CreateFunctor cFunc, LoadFunctor lFunc,
					   StoreFunctor sFunc)
{
	StoreType zeroBegin = cFunc(static_cast<DataType>(0)),
			  posLBegin = cFunc(static_cast<DataType>(+3)),
			  negLBegin = cFunc(static_cast<DataType>(-2)),
			  posHBegin = cFunc(static_cast<DataType>(+4'000'000)),
			  negHBegin = cFunc(static_cast<DataType>(-8'000'000)),
			  zeroEnd, posLEnd, negLEnd, posHEnd, negHEnd;

	sFunc(zeroEnd, lFunc(zeroBegin));
	checkStoreEq(zeroBegin, zeroEnd);

	sFunc(posLEnd, lFunc(posLBegin));
	checkStoreEq(posLBegin, posLEnd);

	sFunc(negLEnd, lFunc(negLBegin));
	checkStoreEq(negLBegin, negLEnd);

	sFunc(posHEnd, lFunc(posHBegin));
	checkStoreEq(posHBegin, posHEnd);

	sFunc(negHEnd, lFunc(negHBegin));
	checkStoreEq(negHBegin, negHEnd);

	if constexpr (std::is_floating_point_v<DataType>)
	{
		StoreType posFrBegin =
					  cFunc(static_cast<DataType>(+.000'000'1)),
				  negFrBegin = cFunc(static_cast<DataType>(-.08)),
				  infPBegin	 = cFunc(static_cast<DataType>(
					   std::numeric_limits<DataType>::infinity())),
				  infNBegin	 = cFunc(static_cast<DataType>(
					   -std::numeric_limits<DataType>::infinity())),
				  NaNBegin	 = cFunc(static_cast<DataType>(
						std::numeric_limits<DataType>::quiet_NaN())),
				  posFrEnd, negFrEnd, infPEnd, infNEnd, NaNEnd;

		sFunc(posFrEnd, lFunc(posFrBegin));
		checkStoreEq(posFrBegin, posFrEnd);

		sFunc(negFrEnd, lFunc(negFrBegin));
		checkStoreEq(negFrBegin, negFrEnd);

		sFunc(infPEnd, lFunc(infPBegin));
		checkStoreEq(infPBegin, infPEnd);

		sFunc(infNEnd, lFunc(infNBegin));
		checkStoreEq(infNBegin, infNEnd);

		// All nans should compare unequal, so this just checks that
		// NaNs do this
		sFunc(NaNEnd, lFunc(NaNBegin));
		CHECK(!dataStoreEq(NaNBegin, NaNEnd));
	}
}
template<typename StoreType, typename DataType,
		 typename CreateFunctor, typename LoadFunctor1,
		 typename LoadFunctor2, typename StoreFunctor>
void evaluateLoadEquivalence(CreateFunctor cFunc, LoadFunctor1 lFunc1,
							 LoadFunctor2 lFunc2, StoreFunctor sFunc)
{
	StoreType zeroBegin = cFunc(static_cast<DataType>(0)),
			  posLBegin = cFunc(static_cast<DataType>(+3)),
			  negLBegin = cFunc(static_cast<DataType>(-2)),
			  posHBegin = cFunc(static_cast<DataType>(+4'000'000)),
			  negHBegin = cFunc(static_cast<DataType>(-8'000'000)),
			  zeroEnd1, zeroEnd2, posLEnd1, posLEnd2, negLEnd1,
			  negLEnd2, posHEnd1, posHEnd2, negHEnd1, negHEnd2;

	sFunc(zeroEnd1, lFunc1(zeroBegin));
	sFunc(zeroEnd2, lFunc2(zeroBegin));
	checkStoreEq(zeroEnd1, zeroEnd2);

	sFunc(posLEnd1, lFunc1(posLBegin));
	sFunc(posLEnd2, lFunc2(posLBegin));
	checkStoreEq(posLEnd1, posLEnd2);

	sFunc(negLEnd1, lFunc1(negLBegin));
	sFunc(negLEnd2, lFunc2(negLBegin));
	checkStoreEq(negLEnd1, negLEnd2);

	sFunc(posHEnd1, lFunc1(posHBegin));
	sFunc(posHEnd2, lFunc2(posHBegin));
	checkStoreEq(posHEnd1, posHEnd2);

	sFunc(negHEnd1, lFunc1(negHBegin));
	sFunc(negHEnd2, lFunc2(negHBegin));
	checkStoreEq(negHEnd1, negHEnd2);

	if constexpr (std::is_floating_point_v<DataType>)
	{
		StoreType posFrBegin =
					  cFunc(static_cast<DataType>(+.000'000'1)),
				  negFrBegin = cFunc(static_cast<DataType>(-.08)),
				  infPBegin	 = cFunc(static_cast<DataType>(
					   std::numeric_limits<DataType>::infinity())),
				  infNBegin	 = cFunc(static_cast<DataType>(
					   -std::numeric_limits<DataType>::infinity())),
				  NaNBegin	 = cFunc(static_cast<DataType>(
						std::numeric_limits<DataType>::quiet_NaN())),
				  posFrEnd1, posFrEnd2, negFrEnd1, negFrEnd2,
				  infPEnd1, infPEnd2, infNEnd1, infNEnd2, NaNEnd1,
				  NaNEnd2;

		sFunc(posFrEnd1, lFunc1(posFrBegin));
		sFunc(posFrEnd2, lFunc2(posFrBegin));
		checkStoreEq(posFrEnd1, posFrEnd2);

		sFunc(negFrEnd1, lFunc1(negFrBegin));
		sFunc(negFrEnd2, lFunc2(negFrBegin));
		checkStoreEq(negFrEnd1, negFrEnd2);

		sFunc(infPEnd1, lFunc1(infPBegin));
		sFunc(infPEnd2, lFunc2(infPBegin));
		checkStoreEq(infPEnd1, infPEnd2);

		sFunc(infNEnd1, lFunc1(infNBegin));
		sFunc(infNEnd2, lFunc2(infNBegin));
		checkStoreEq(infNEnd1, infNEnd2);

		// All nans should compare unequal, so this just checks that
		// NaNs do this
		sFunc(NaNEnd1, lFunc1(NaNBegin));
		sFunc(NaNEnd2, lFunc2(NaNBegin));
		CHECK(!dataStoreEq(NaNEnd1, NaNEnd2));
	}
}

template<typename StoreType, typename DataType,
		 typename CreateFunctor, typename LoadFunctor,
		 typename StoreFunctor1, typename StoreFunctor2>
void evaluateStoreEquivalence(CreateFunctor cFunc, LoadFunctor lFunc,
							  StoreFunctor1 sFunc1,
							  StoreFunctor2 sFunc2)
{
	StoreType zeroBegin = cFunc(static_cast<DataType>(0)),
			  posLBegin = cFunc(static_cast<DataType>(+3)),
			  negLBegin = cFunc(static_cast<DataType>(-2)),
			  posHBegin = cFunc(static_cast<DataType>(+4'000'000)),
			  negHBegin = cFunc(static_cast<DataType>(-8'000'000)),
			  zeroEnd1, zeroEnd2, posLEnd1, posLEnd2, negLEnd1,
			  negLEnd2, posHEnd1, posHEnd2, negHEnd1, negHEnd2;

	sFunc1(zeroEnd1, lFunc(zeroBegin));
	sFunc2(zeroEnd2, lFunc(zeroBegin));
	checkStoreEq(zeroEnd1, zeroEnd2);

	sFunc1(posLEnd1, lFunc(posLBegin));
	sFunc2(posLEnd2, lFunc(posLBegin));
	checkStoreEq(posLEnd1, posLEnd2);

	sFunc1(negLEnd1, lFunc(negLBegin));
	sFunc2(negLEnd2, lFunc(negLBegin));
	checkStoreEq(negLEnd1, negLEnd2);

	sFunc1(posHEnd1, lFunc(posHBegin));
	sFunc2(posHEnd2, lFunc(posHBegin));
	checkStoreEq(posHEnd1, posHEnd2);

	sFunc1(negHEnd1, lFunc(negHBegin));
	sFunc2(negHEnd2, lFunc(negHBegin));
	checkStoreEq(negHEnd1, negHEnd2);

	if constexpr (std::is_floating_point_v<DataType>)
	{
		StoreType posFrBegin =
					  cFunc(static_cast<DataType>(+.000'000'1)),
				  negFrBegin = cFunc(static_cast<DataType>(-.08)),
				  infPBegin	 = cFunc(static_cast<DataType>(
					   std::numeric_limits<DataType>::infinity())),
				  infNBegin	 = cFunc(static_cast<DataType>(
					   -std::numeric_limits<DataType>::infinity())),
				  NaNBegin	 = cFunc(static_cast<DataType>(
						std::numeric_limits<DataType>::quiet_NaN())),
				  posFrEnd1, posFrEnd2, negFrEnd1, negFrEnd2,
				  infPEnd1, infPEnd2, infNEnd1, infNEnd2, NaNEnd1,
				  NaNEnd2;

		sFunc1(posFrEnd1, lFunc(posFrBegin));
		sFunc2(posFrEnd2, lFunc(posFrBegin));
		checkStoreEq(posFrEnd1, posFrEnd2);

		sFunc1(negFrEnd1, lFunc(negFrBegin));
		sFunc2(negFrEnd2, lFunc(negFrBegin));
		checkStoreEq(negFrEnd1, negFrEnd2);

		sFunc1(infPEnd1, lFunc(infPBegin));
		sFunc2(infPEnd2, lFunc(infPBegin));
		checkStoreEq(infPEnd1, infPEnd2);

		sFunc1(infNEnd1, lFunc(infNBegin));
		sFunc2(infNEnd2, lFunc(infNBegin));
		checkStoreEq(infNEnd1, infNEnd2);

		// All nans should compare unequal, so this just checks that
		// NaNs do this
		sFunc1(NaNEnd1, lFunc(NaNBegin));
		sFunc2(NaNEnd2, lFunc(NaNBegin));
		CHECK(!dataStoreEq(NaNEnd1, NaNEnd2));
	}
}
namespace
{
uint_least64_t convertToRecord(const Integer4& convertee) noexcept
{
	uint_least64_t lowRecord = frao::SetBits(
					   convertee.m_Components.m_X & 0xFFFF,
					   convertee.m_Components.m_Y, 0xFFFF, 16),
				   highRecord = frao::SetBits(
					   convertee.m_Components.m_Z & 0xFFFF,
					   convertee.m_Components.m_W, 0xFFFF, 16);

	return frao::SetBits(lowRecord & 0xFFFF'FFFF, highRecord,
						 0xFFFFFFFF, 32);
}
// RecordFunc refers to whether the functor takes a uint_least64_t
// reference param as its first parameter
template<bool RecordFunc, typename... CompareType, size_t ArraySize,
		 typename CompareFunc>
void evaluateComparisons(
	CompareFunc										  doComp,
	std::array<Integer4, ArraySize>					  expectedResults,
	std::array<std::tuple<CompareType...>, ArraySize> inputValues)
{
	for (size_t index = 0; index < ArraySize; ++index)
	{
		Integer4 expectedResult = expectedResults[index], realResult;

		if constexpr (RecordFunc)
		{
			uint_least64_t expectedRecord =
							   convertToRecord(expectedResult),
						   recordResult;
			realResult = std::apply(
				doComp, std::tuple_cat(std::tie(recordResult),
									   inputValues[index]));

			CHECK_EQ(recordResult, expectedRecord);
		} else
		{
			realResult = std::apply(doComp, inputValues[index]);
		}

		checkStoreEq(realResult, expectedResult);
	}
}
template<bool RecordFunc, typename... CompareType, size_t ArraySize,
		 typename CompareFunc, typename ExpectedResFunc>
void evaluateDimensionComparisons(
	CompareFunc doComp, ExpectedResFunc getExpRes,
	std::array<Integer4, ArraySize>					  expectedResults,
	std::array<std::tuple<CompareType...>, ArraySize> inputValues)
{
	for (size_t index = 0; index < ArraySize; ++index)
	{
		Integer4 expectedResultElems = expectedResults[index];
		bool	 expectedResult		 = getExpRes(expectedResultElems),
			 realResult;

		if constexpr (RecordFunc)
		{
			uint_least64_t expectedRecord =
							   convertToRecord(expectedResultElems),
						   recordResult;
			realResult = std::apply(
				doComp, std::tuple_cat(std::tie(recordResult),
									   inputValues[index]));

			CHECK_EQ(recordResult, expectedRecord);
		} else
		{
			realResult = std::apply(doComp, inputValues[index]);
		}
		CHECK_EQ(realResult, expectedResult);
	}
}
template<typename ResultType, typename... CompareType,
		 size_t ArraySize, typename CompareFunc>
void callEvalFunc(
	CompareFunc										  doComp,
	std::array<ResultType, ArraySize>				  expectedResults,
	std::array<std::tuple<CompareType...>, ArraySize> inputValues)
{
	for (size_t index = 0; index < ArraySize; ++index)
	{
		std::apply(doComp,
				   std::tuple_cat(std::tie(expectedResults[index]),
								  inputValues[index]));
	}
}
void evaluateLoadStore()
{
	// float store checks
	evaluateSymmetric<Float2, float>(
		[](float value) -> Float2 {
			return dataStoreRep<Float2>(value);
		},
		[](const Float2& data) -> NarrowVector {
			return loadFloat2(data);
		},
		[](Float2& store, NarrowVector data) -> void {
			return storeFloat2(store, data);
		});
	evaluateSymmetric<Float3, float>(
		[](float value) -> Float3 {
			return dataStoreRep<Float3>(value);
		},
		[](const Float3& data) -> NarrowVector {
			return loadFloat3(data);
		},
		[](Float3& store, NarrowVector data) -> void {
			return storeFloat3(store, data);
		});
	evaluateSymmetric<Float4, float>(
		[](float value) -> Float4 {
			return dataStoreRep<Float4>(value);
		},
		[](const Float4& data) -> NarrowVector {
			return loadFloat4(data);
		},
		[](Float4& store, NarrowVector data) -> void {
			return storeFloat4(store, data);
		});

	// int32 store checks
	evaluateSymmetric<SmallInt2, int_least32_t>(
		[](int_least32_t value) -> SmallInt2 {
			return dataStoreRep<SmallInt2>(value);
		},
		[](const SmallInt2& data) -> NarrowIntVector {
			return loadSmallInt2(data);
		},
		[](SmallInt2& store, NarrowIntVector data) -> void {
			return storeSmallInt2(store, data);
		});
	evaluateSymmetric<SmallInt3, int_least32_t>(
		[](int_least32_t value) -> SmallInt3 {
			return dataStoreRep<SmallInt3>(value);
		},
		[](const SmallInt3& data) -> NarrowIntVector {
			return loadSmallInt3(data);
		},
		[](SmallInt3& store, NarrowIntVector data) -> void {
			return storeSmallInt3(store, data);
		});
	evaluateSymmetric<SmallInt4, int_least32_t>(
		[](int_least32_t value) -> SmallInt4 {
			return dataStoreRep<SmallInt4>(value);
		},
		[](const SmallInt4& data) -> NarrowIntVector {
			return loadSmallInt4(data);
		},
		[](SmallInt4& store, NarrowIntVector data) -> void {
			return storeSmallInt4(store, data);
		});

	// double store checks
	evaluateSymmetric<Double2, double>(
		[](double value) -> Double2 {
			return dataStoreRep<Double2>(value);
		},
		[](const Double2& data) -> WideVector {
			return loadDouble2(data);
		},
		[](Double2& store, WideVector data) -> void {
			return storeDouble2(store, data);
		});
	evaluateSymmetric<Double3, double>(
		[](double value) -> Double3 {
			return dataStoreRep<Double3>(value);
		},
		[](const Double3& data) -> WideVector {
			return loadDouble3(data);
		},
		[](Double3& store, WideVector data) -> void {
			return storeDouble3(store, data);
		});
	evaluateSymmetric<Double4, double>(
		[](double value) -> Double4 {
			return dataStoreRep<Double4>(value);
		},
		[](const Double4& data) -> WideVector {
			return loadDouble4(data);
		},
		[](Double4& store, WideVector data) -> void {
			return storeDouble4(store, data);
		});

	// int store checks
	evaluateSymmetric<Integer2, int_least64_t>(
		[](int_least64_t value) -> Integer2 {
			return dataStoreRep<Integer2>(value);
		},
		[](const Integer2& data) -> WideIntVector {
			return loadInteger2(data);
		},
		[](Integer2& store, WideIntVector data) -> void {
			return storeInteger2(store, data);
		});
	evaluateSymmetric<Integer3, int_least64_t>(
		[](int_least64_t value) -> Integer3 {
			return dataStoreRep<Integer3>(value);
		},
		[](const Integer3& data) -> WideIntVector {
			return loadInteger3(data);
		},
		[](Integer3& store, WideIntVector data) -> void {
			return storeInteger3(store, data);
		});
	evaluateSymmetric<Integer4, int_least64_t>(
		[](int_least64_t value) -> Integer4 {
			return dataStoreRep<Integer4>(value);
		},
		[](const Integer4& data) -> WideIntVector {
			return loadInteger4(data);
		},
		[](Integer4& store, WideIntVector data) -> void {
			return storeInteger4(store, data);
		});

	// float matrix store checks
	evaluateSymmetric<Float2x2, float>(
		[](float value) -> Float2x2 {
			return dataStoreRep<Float2x2>(value);
		},
		[](const Float2x2& data) -> NarrowMatrix {
			return loadFloat2x2(data);
		},
		[](Float2x2& store, NarrowMatrix data) -> void {
			return storeFloat2x2(store, data);
		});
	evaluateSymmetric<Float3x3, float>(
		[](float value) -> Float3x3 {
			return dataStoreRep<Float3x3>(value);
		},
		[](const Float3x3& data) -> NarrowMatrix {
			return loadFloat3x3(data);
		},
		[](Float3x3& store, NarrowMatrix data) -> void {
			return storeFloat3x3(store, data);
		});
	evaluateSymmetric<Float4x4, float>(
		[](float value) -> Float4x4 {
			return dataStoreRep<Float4x4>(value);
		},
		[](const Float4x4& data) -> NarrowMatrix {
			return loadFloat4x4(data);
		},
		[](Float4x4& store, NarrowMatrix data) -> void {
			return storeFloat4x4(store, data);
		});

	// double matrix store checks
	evaluateSymmetric<Double2x2, double>(
		[](double value) -> Double2x2 {
			return dataStoreRep<Double2x2>(value);
		},
		[](const Double2x2& data) -> WideMatrix {
			return loadDouble2x2(data);
		},
		[](Double2x2& store, WideMatrix data) -> void {
			return storeDouble2x2(store, data);
		});
	evaluateSymmetric<Double3x3, double>(
		[](double value) -> Double3x3 {
			return dataStoreRep<Double3x3>(value);
		},
		[](const Double3x3& data) -> WideMatrix {
			return loadDouble3x3(data);
		},
		[](Double3x3& store, WideMatrix data) -> void {
			return storeDouble3x3(store, data);
		});
	evaluateSymmetric<Double4x4, double>(
		[](double value) -> Double4x4 {
			return dataStoreRep<Double4x4>(value);
		},
		[](const Double4x4& data) -> WideMatrix {
			return loadDouble4x4(data);
		},
		[](Double4x4& store, WideMatrix data) -> void {
			return storeDouble4x4(store, data);
		});
}

void evaluateSetGet()
{
	// float get/set checks
	auto floatIDAssign = [](float value) -> float { return value; };
	evaluateSymmetric<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			return vectorSetByIndex(NarrowVector(), data, 0);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetByIndex(data, 0);
		});
	evaluateSymmetric<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			return vectorSetByIndex(NarrowVector(), data, 1);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetByIndex(data, 1);
		});
	evaluateSymmetric<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			return vectorSetByIndex(NarrowVector(), data, 2);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetByIndex(data, 2);
		});
	evaluateSymmetric<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			return vectorSetByIndex(NarrowVector(), data, 3);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetByIndex(data, 3);
		});
	evaluateSymmetric<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			return vectorSetX(NarrowVector(), data);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetX(data);
		});
	evaluateSymmetric<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			return vectorSetY(NarrowVector(), data);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetY(data);
		});
	evaluateSymmetric<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			return vectorSetZ(NarrowVector(), data);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetZ(data);
		});
	evaluateSymmetric<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			return vectorSetW(NarrowVector(), data);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetW(data);
		});

	// int32 get/set checks
	auto int32IDAssign = [](int_least32_t value) -> int_least32_t {
		return value;
	};
	evaluateSymmetric<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetByIndex(NarrowIntVector(), data, 0);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetByIndex(data, 0);
		});
	evaluateSymmetric<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetByIndex(NarrowIntVector(), data, 1);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetByIndex(data, 1);
		});
	evaluateSymmetric<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetByIndex(NarrowIntVector(), data, 2);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetByIndex(data, 2);
		});
	evaluateSymmetric<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetByIndex(NarrowIntVector(), data, 3);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetByIndex(data, 3);
		});
	evaluateSymmetric<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetX(NarrowIntVector(), data);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetX(data);
		});
	evaluateSymmetric<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetY(NarrowIntVector(), data);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetY(data);
		});
	evaluateSymmetric<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetZ(NarrowIntVector(), data);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetZ(data);
		});
	evaluateSymmetric<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetW(NarrowIntVector(), data);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetW(data);
		});

	// double get/set checks
	auto doubleIDAssign = [](double value) -> double {
		return value;
	};
	evaluateSymmetric<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			return vectorSetByIndex(WideVector(), data, 0);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetByIndex(data, 0);
		});
	evaluateSymmetric<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			return vectorSetByIndex(WideVector(), data, 1);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetByIndex(data, 1);
		});
	evaluateSymmetric<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			return vectorSetByIndex(WideVector(), data, 2);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetByIndex(data, 2);
		});
	evaluateSymmetric<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			return vectorSetByIndex(WideVector(), data, 3);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetByIndex(data, 3);
		});
	evaluateSymmetric<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			return vectorSetX(WideVector(), data);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetX(data);
		});
	evaluateSymmetric<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			return vectorSetY(WideVector(), data);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetY(data);
		});
	evaluateSymmetric<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			return vectorSetZ(WideVector(), data);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetZ(data);
		});
	evaluateSymmetric<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			return vectorSetW(WideVector(), data);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetW(data);
		});

	// int get/set checks
	auto intIDAssign = [](int_least64_t value) -> int_least64_t {
		return value;
	};
	evaluateSymmetric<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetByIndex(WideIntVector(), data, 0);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetByIndex(data, 0);
		});
	evaluateSymmetric<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetByIndex(WideIntVector(), data, 1);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetByIndex(data, 1);
		});
	evaluateSymmetric<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetByIndex(WideIntVector(), data, 2);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetByIndex(data, 2);
		});
	evaluateSymmetric<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetByIndex(WideIntVector(), data, 3);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetByIndex(data, 3);
		});
	evaluateSymmetric<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetX(WideIntVector(), data);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetX(data);
		});
	evaluateSymmetric<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetY(WideIntVector(), data);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetY(data);
		});
	evaluateSymmetric<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetZ(WideIntVector(), data);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetZ(data);
		});
	evaluateSymmetric<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetW(WideIntVector(), data);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetW(data);
		});
}
void evaluateLoadSet()
{
	// float checks
	auto floatIDAssign = [](float value) -> float { return value; };
	evaluateLoadEquivalence<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			auto dataStore = dataStoreRep<Float2>(data);

			return loadFloat2(dataStore);
		},
		[](const float& data) -> NarrowVector {
			return vectorSetByIndex(NarrowVector(), data, 0);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetByIndex(data, 0);
		});
	evaluateLoadEquivalence<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			auto dataStore = dataStoreRep<Float2>(data);

			return loadFloat2(dataStore);
		},
		[](const float& data) -> NarrowVector {
			return vectorSetByIndex(NarrowVector(), data, 1);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetByIndex(data, 1);
		});
	evaluateLoadEquivalence<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			auto dataStore = dataStoreRep<Float3>(data);

			return loadFloat3(dataStore);
		},
		[](const float& data) -> NarrowVector {
			return vectorSetByIndex(NarrowVector(), data, 2);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetByIndex(data, 2);
		});
	evaluateLoadEquivalence<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			auto dataStore = dataStoreRep<Float4>(data);

			return loadFloat4(dataStore);
		},
		[](const float& data) -> NarrowVector {
			return vectorSetByIndex(NarrowVector(), data, 3);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetByIndex(data, 3);
		});
	evaluateLoadEquivalence<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			auto dataStore = dataStoreRep<Float2>(data);

			return loadFloat2(dataStore);
		},
		[](const float& data) -> NarrowVector {
			return vectorSetX(NarrowVector(), data);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetX(data);
		});
	evaluateLoadEquivalence<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			auto dataStore = dataStoreRep<Float2>(data);

			return loadFloat2(dataStore);
		},
		[](const float& data) -> NarrowVector {
			return vectorSetY(NarrowVector(), data);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetY(data);
		});
	evaluateLoadEquivalence<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			auto dataStore = dataStoreRep<Float3>(data);

			return loadFloat3(dataStore);
		},
		[](const float& data) -> NarrowVector {
			return vectorSetZ(NarrowVector(), data);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetZ(data);
		});
	evaluateLoadEquivalence<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			auto dataStore = dataStoreRep<Float4>(data);

			return loadFloat4(dataStore);
		},
		[](const float& data) -> NarrowVector {
			return vectorSetW(NarrowVector(), data);
		},
		[](float& store, NarrowVector data) -> void {
			store = vectorGetW(data);
		});
	evaluateLoadEquivalence<float, float>(
		floatIDAssign,
		[](const float& data) -> NarrowVector {
			auto dataStore = dataStoreRep<Float4>(data);

			return loadFloat4(dataStore);
		},
		[](const float& data) -> NarrowVector {
			return vectorSet(data, data, data, data);
		},
		[](float& store, NarrowVector data) -> void {
			Float4 vecStore;
			storeFloat4(vecStore, data);

			if (vecStore.m_Components.m_X
				== vecStore.m_Components.m_X)
			{
				// ie: if we're not looking at NaN, which always
				// evaluates to false
				CHECK_EQ(vecStore.m_Components.m_X,
						 vecStore.m_Components.m_Y);
				CHECK_EQ(vecStore.m_Components.m_X,
						 vecStore.m_Components.m_Z);
				CHECK_EQ(vecStore.m_Components.m_X,
						 vecStore.m_Components.m_W);
			}
			store = vecStore.m_Components.m_X;
		});

	// int32 checks
	auto int32IDAssign = [](int_least32_t value) -> int_least32_t {
		return value;
	};
	evaluateLoadEquivalence<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			auto dataStore = dataStoreRep<SmallInt2>(data);

			return loadSmallInt2(dataStore);
		},
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetByIndex(NarrowIntVector(), data, 0);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetByIndex(data, 0);
		});
	evaluateLoadEquivalence<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			auto dataStore = dataStoreRep<SmallInt2>(data);

			return loadSmallInt2(dataStore);
		},
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetByIndex(NarrowIntVector(), data, 1);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetByIndex(data, 1);
		});
	evaluateLoadEquivalence<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			auto dataStore = dataStoreRep<SmallInt3>(data);

			return loadSmallInt3(dataStore);
		},
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetByIndex(NarrowIntVector(), data, 2);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetByIndex(data, 2);
		});
	evaluateLoadEquivalence<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			auto dataStore = dataStoreRep<SmallInt4>(data);

			return loadSmallInt4(dataStore);
		},
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetByIndex(NarrowIntVector(), data, 3);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetByIndex(data, 3);
		});
	evaluateLoadEquivalence<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			auto dataStore = dataStoreRep<SmallInt2>(data);

			return loadSmallInt2(dataStore);
		},
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetX(NarrowIntVector(), data);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetX(data);
		});
	evaluateLoadEquivalence<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			auto dataStore = dataStoreRep<SmallInt2>(data);

			return loadSmallInt2(dataStore);
		},
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetY(NarrowIntVector(), data);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetY(data);
		});
	evaluateLoadEquivalence<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			auto dataStore = dataStoreRep<SmallInt3>(data);

			return loadSmallInt3(dataStore);
		},
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetZ(NarrowIntVector(), data);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetZ(data);
		});
	evaluateLoadEquivalence<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			auto dataStore = dataStoreRep<SmallInt4>(data);

			return loadSmallInt4(dataStore);
		},
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSetW(NarrowIntVector(), data);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			store = vectorGetW(data);
		});
	evaluateLoadEquivalence<int_least32_t, int_least32_t>(
		int32IDAssign,
		[](const int_least32_t& data) -> NarrowIntVector {
			auto dataStore = dataStoreRep<SmallInt4>(data);

			return loadSmallInt4(dataStore);
		},
		[](const int_least32_t& data) -> NarrowIntVector {
			return vectorSet(data, data, data, data);
		},
		[](int_least32_t& store, NarrowIntVector data) -> void {
			SmallInt4 vecStore;
			storeSmallInt4(vecStore, data);

			if (vecStore.m_Components.m_X
				== vecStore.m_Components.m_X)
			{
				// ie: if we're not looking at NaN, which always
				// evaluates to false
				CHECK_EQ(vecStore.m_Components.m_X,
						 vecStore.m_Components.m_Y);
				CHECK_EQ(vecStore.m_Components.m_X,
						 vecStore.m_Components.m_Z);
				CHECK_EQ(vecStore.m_Components.m_X,
						 vecStore.m_Components.m_W);
			}
			store = vecStore.m_Components.m_X;
		});

	// double checks
	auto doubleIDAssign = [](double value) -> double {
		return value;
	};
	evaluateLoadEquivalence<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			auto dataStore = dataStoreRep<Double2>(data);

			return loadDouble2(dataStore);
		},
		[](const double& data) -> WideVector {
			return vectorSetByIndex(WideVector(), data, 0);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetByIndex(data, 0);
		});
	evaluateLoadEquivalence<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			auto dataStore = dataStoreRep<Double2>(data);

			return loadDouble2(dataStore);
		},
		[](const double& data) -> WideVector {
			return vectorSetByIndex(WideVector(), data, 1);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetByIndex(data, 1);
		});
	evaluateLoadEquivalence<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			auto dataStore = dataStoreRep<Double3>(data);

			return loadDouble3(dataStore);
		},
		[](const double& data) -> WideVector {
			return vectorSetByIndex(WideVector(), data, 2);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetByIndex(data, 2);
		});
	evaluateLoadEquivalence<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			auto dataStore = dataStoreRep<Double4>(data);

			return loadDouble4(dataStore);
		},
		[](const double& data) -> WideVector {
			return vectorSetByIndex(WideVector(), data, 3);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetByIndex(data, 3);
		});
	evaluateLoadEquivalence<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			auto dataStore = dataStoreRep<Double2>(data);

			return loadDouble2(dataStore);
		},
		[](const double& data) -> WideVector {
			return vectorSetX(WideVector(), data);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetX(data);
		});
	evaluateLoadEquivalence<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			auto dataStore = dataStoreRep<Double2>(data);

			return loadDouble2(dataStore);
		},
		[](const double& data) -> WideVector {
			return vectorSetY(WideVector(), data);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetY(data);
		});
	evaluateLoadEquivalence<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			auto dataStore = dataStoreRep<Double3>(data);

			return loadDouble3(dataStore);
		},
		[](const double& data) -> WideVector {
			return vectorSetZ(WideVector(), data);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetZ(data);
		});
	evaluateLoadEquivalence<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			auto dataStore = dataStoreRep<Double4>(data);

			return loadDouble4(dataStore);
		},
		[](const double& data) -> WideVector {
			return vectorSetW(WideVector(), data);
		},
		[](double& store, WideVector data) -> void {
			store = vectorGetW(data);
		});
	evaluateLoadEquivalence<double, double>(
		doubleIDAssign,
		[](const double& data) -> WideVector {
			auto dataStore = dataStoreRep<Double4>(data);

			return loadDouble4(dataStore);
		},
		[](const double& data) -> WideVector {
			return vectorSet(data, data, data, data);
		},
		[](double& store, WideVector data) -> void {
			Double4 vecStore;
			storeDouble4(vecStore, data);

			if (vecStore.m_Components.m_X
				== vecStore.m_Components.m_X)
			{
				// ie: if we're not looking at NaN, which always
				// evaluates to false
				CHECK_EQ(vecStore.m_Components.m_X,
						 vecStore.m_Components.m_Y);
				CHECK_EQ(vecStore.m_Components.m_X,
						 vecStore.m_Components.m_Z);
				CHECK_EQ(vecStore.m_Components.m_X,
						 vecStore.m_Components.m_W);
			}
			store = vecStore.m_Components.m_X;
		});

	// int checks
	auto intIDAssign = [](int_least64_t value) -> int_least64_t {
		return value;
	};
	evaluateLoadEquivalence<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			auto dataStore = dataStoreRep<Integer2>(data);

			return loadInteger2(dataStore);
		},
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetByIndex(WideIntVector(), data, 0);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetByIndex(data, 0);
		});
	evaluateLoadEquivalence<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			auto dataStore = dataStoreRep<Integer2>(data);

			return loadInteger2(dataStore);
		},
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetByIndex(WideIntVector(), data, 1);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetByIndex(data, 1);
		});
	evaluateLoadEquivalence<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			auto dataStore = dataStoreRep<Integer3>(data);

			return loadInteger3(dataStore);
		},
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetByIndex(WideIntVector(), data, 2);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetByIndex(data, 2);
		});
	evaluateLoadEquivalence<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			auto dataStore = dataStoreRep<Integer4>(data);

			return loadInteger4(dataStore);
		},
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetByIndex(WideIntVector(), data, 3);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetByIndex(data, 3);
		});
	evaluateLoadEquivalence<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			auto dataStore = dataStoreRep<Integer2>(data);

			return loadInteger2(dataStore);
		},
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetX(WideIntVector(), data);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetX(data);
		});
	evaluateLoadEquivalence<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			auto dataStore = dataStoreRep<Integer2>(data);

			return loadInteger2(dataStore);
		},
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetY(WideIntVector(), data);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetY(data);
		});
	evaluateLoadEquivalence<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			auto dataStore = dataStoreRep<Integer3>(data);

			return loadInteger3(dataStore);
		},
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetZ(WideIntVector(), data);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetZ(data);
		});
	evaluateLoadEquivalence<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			auto dataStore = dataStoreRep<Integer4>(data);

			return loadInteger4(dataStore);
		},
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSetW(WideIntVector(), data);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			store = vectorGetW(data);
		});
	evaluateLoadEquivalence<int_least64_t, int_least64_t>(
		intIDAssign,
		[](const int_least64_t& data) -> WideIntVector {
			auto dataStore = dataStoreRep<Integer4>(data);

			return loadInteger4(dataStore);
		},
		[](const int_least64_t& data) -> WideIntVector {
			return vectorSet(data, data, data, data);
		},
		[](int_least64_t& store, WideIntVector data) -> void {
			Integer4 vecStore;
			storeInteger4(vecStore, data);

			CHECK_EQ(vecStore.m_Components.m_X,
					 vecStore.m_Components.m_Y);
			CHECK_EQ(vecStore.m_Components.m_X,
					 vecStore.m_Components.m_Z);
			CHECK_EQ(vecStore.m_Components.m_X,
					 vecStore.m_Components.m_W);
			store = vecStore.m_Components.m_X;
		});
}
void evaluateStoreGet()
{
	// float checks
	evaluateStoreEquivalence<Float2, float>(
		[](const float& value) -> Float2 {
			return dataStoreRep<Float2>(value);
		},
		[](const Float2& data) -> NarrowVector {
			return loadFloat2(data);
		},
		[](Float2& store, NarrowVector data) -> void {
			storeFloat2(store, data);
		},
		[](Float2& store, NarrowVector data) -> void {
			store = dataStoreRep<Float2>(vectorGetByIndex(data, 0));
		});
	evaluateStoreEquivalence<Float2, float>(
		[](const float& value) -> Float2 {
			return dataStoreRep<Float2>(value);
		},
		[](const Float2& data) -> NarrowVector {
			return loadFloat2(data);
		},
		[](Float2& store, NarrowVector data) -> void {
			storeFloat2(store, data);
		},
		[](Float2& store, NarrowVector data) -> void {
			store = dataStoreRep<Float2>(vectorGetByIndex(data, 1));
		});
	evaluateStoreEquivalence<Float3, float>(
		[](const float& value) -> Float3 {
			return dataStoreRep<Float3>(value);
		},
		[](const Float3& data) -> NarrowVector {
			return loadFloat3(data);
		},
		[](Float3& store, NarrowVector data) -> void {
			storeFloat3(store, data);
		},
		[](Float3& store, NarrowVector data) -> void {
			store = dataStoreRep<Float3>(vectorGetByIndex(data, 2));
		});
	evaluateStoreEquivalence<Float4, float>(
		[](const float& value) -> Float4 {
			return dataStoreRep<Float4>(value);
		},
		[](const Float4& data) -> NarrowVector {
			return loadFloat4(data);
		},
		[](Float4& store, NarrowVector data) -> void {
			storeFloat4(store, data);
		},
		[](Float4& store, NarrowVector data) -> void {
			store = dataStoreRep<Float4>(vectorGetByIndex(data, 3));
		});
	evaluateStoreEquivalence<Float2, float>(
		[](const float& value) -> Float2 {
			return dataStoreRep<Float2>(value);
		},
		[](const Float2& data) -> NarrowVector {
			return loadFloat2(data);
		},
		[](Float2& store, NarrowVector data) -> void {
			storeFloat2(store, data);
		},
		[](Float2& store, NarrowVector data) -> void {
			store = dataStoreRep<Float2>(vectorGetX(data));
		});
	evaluateStoreEquivalence<Float2, float>(
		[](const float& value) -> Float2 {
			return dataStoreRep<Float2>(value);
		},
		[](const Float2& data) -> NarrowVector {
			return loadFloat2(data);
		},
		[](Float2& store, NarrowVector data) -> void {
			storeFloat2(store, data);
		},
		[](Float2& store, NarrowVector data) -> void {
			store = dataStoreRep<Float2>(vectorGetY(data));
		});
	evaluateStoreEquivalence<Float3, float>(
		[](const float& value) -> Float3 {
			return dataStoreRep<Float3>(value);
		},
		[](const Float3& data) -> NarrowVector {
			return loadFloat3(data);
		},
		[](Float3& store, NarrowVector data) -> void {
			storeFloat3(store, data);
		},
		[](Float3& store, NarrowVector data) -> void {
			store = dataStoreRep<Float3>(vectorGetZ(data));
		});
	evaluateStoreEquivalence<Float4, float>(
		[](const float& value) -> Float4 {
			return dataStoreRep<Float4>(value);
		},
		[](const Float4& data) -> NarrowVector {
			return loadFloat4(data);
		},
		[](Float4& store, NarrowVector data) -> void {
			storeFloat4(store, data);
		},
		[](Float4& store, NarrowVector data) -> void {
			store = dataStoreRep<Float4>(vectorGetW(data));
		});

	// int32 checks
	evaluateStoreEquivalence<SmallInt2, int_least32_t>(
		[](const int_least32_t& value) -> SmallInt2 {
			return dataStoreRep<SmallInt2>(value);
		},
		[](const SmallInt2& data) -> NarrowIntVector {
			return loadSmallInt2(data);
		},
		[](SmallInt2& store, NarrowIntVector data) -> void {
			storeSmallInt2(store, data);
		},
		[](SmallInt2& store, NarrowIntVector data) -> void {
			store =
				dataStoreRep<SmallInt2>(vectorGetByIndex(data, 0));
		});
	evaluateStoreEquivalence<SmallInt2, int_least32_t>(
		[](const int_least32_t& value) -> SmallInt2 {
			return dataStoreRep<SmallInt2>(value);
		},
		[](const SmallInt2& data) -> NarrowIntVector {
			return loadSmallInt2(data);
		},
		[](SmallInt2& store, NarrowIntVector data) -> void {
			storeSmallInt2(store, data);
		},
		[](SmallInt2& store, NarrowIntVector data) -> void {
			store =
				dataStoreRep<SmallInt2>(vectorGetByIndex(data, 1));
		});
	evaluateStoreEquivalence<SmallInt3, int_least32_t>(
		[](const int_least32_t& value) -> SmallInt3 {
			return dataStoreRep<SmallInt3>(value);
		},
		[](const SmallInt3& data) -> NarrowIntVector {
			return loadSmallInt3(data);
		},
		[](SmallInt3& store, NarrowIntVector data) -> void {
			storeSmallInt3(store, data);
		},
		[](SmallInt3& store, NarrowIntVector data) -> void {
			store =
				dataStoreRep<SmallInt3>(vectorGetByIndex(data, 2));
		});
	evaluateStoreEquivalence<SmallInt4, int_least32_t>(
		[](const int_least32_t& value) -> SmallInt4 {
			return dataStoreRep<SmallInt4>(value);
		},
		[](const SmallInt4& data) -> NarrowIntVector {
			return loadSmallInt4(data);
		},
		[](SmallInt4& store, NarrowIntVector data) -> void {
			storeSmallInt4(store, data);
		},
		[](SmallInt4& store, NarrowIntVector data) -> void {
			store =
				dataStoreRep<SmallInt4>(vectorGetByIndex(data, 3));
		});
	evaluateStoreEquivalence<SmallInt2, int_least32_t>(
		[](const int_least32_t& value) -> SmallInt2 {
			return dataStoreRep<SmallInt2>(value);
		},
		[](const SmallInt2& data) -> NarrowIntVector {
			return loadSmallInt2(data);
		},
		[](SmallInt2& store, NarrowIntVector data) -> void {
			storeSmallInt2(store, data);
		},
		[](SmallInt2& store, NarrowIntVector data) -> void {
			store = dataStoreRep<SmallInt2>(vectorGetX(data));
		});
	evaluateStoreEquivalence<SmallInt2, int_least32_t>(
		[](const int_least32_t& value) -> SmallInt2 {
			return dataStoreRep<SmallInt2>(value);
		},
		[](const SmallInt2& data) -> NarrowIntVector {
			return loadSmallInt2(data);
		},
		[](SmallInt2& store, NarrowIntVector data) -> void {
			storeSmallInt2(store, data);
		},
		[](SmallInt2& store, NarrowIntVector data) -> void {
			store = dataStoreRep<SmallInt2>(vectorGetY(data));
		});
	evaluateStoreEquivalence<SmallInt3, int_least32_t>(
		[](const int_least32_t& value) -> SmallInt3 {
			return dataStoreRep<SmallInt3>(value);
		},
		[](const SmallInt3& data) -> NarrowIntVector {
			return loadSmallInt3(data);
		},
		[](SmallInt3& store, NarrowIntVector data) -> void {
			storeSmallInt3(store, data);
		},
		[](SmallInt3& store, NarrowIntVector data) -> void {
			store = dataStoreRep<SmallInt3>(vectorGetZ(data));
		});
	evaluateStoreEquivalence<SmallInt4, int_least32_t>(
		[](const int_least32_t& value) -> SmallInt4 {
			return dataStoreRep<SmallInt4>(value);
		},
		[](const SmallInt4& data) -> NarrowIntVector {
			return loadSmallInt4(data);
		},
		[](SmallInt4& store, NarrowIntVector data) -> void {
			storeSmallInt4(store, data);
		},
		[](SmallInt4& store, NarrowIntVector data) -> void {
			store = dataStoreRep<SmallInt4>(vectorGetW(data));
		});

	// double checks
	evaluateStoreEquivalence<Double2, double>(
		[](const double& value) -> Double2 {
			return dataStoreRep<Double2>(value);
		},
		[](const Double2& data) -> WideVector {
			return loadDouble2(data);
		},
		[](Double2& store, WideVector data) -> void {
			storeDouble2(store, data);
		},
		[](Double2& store, WideVector data) -> void {
			store = dataStoreRep<Double2>(vectorGetByIndex(data, 0));
		});
	evaluateStoreEquivalence<Double2, double>(
		[](const double& value) -> Double2 {
			return dataStoreRep<Double2>(value);
		},
		[](const Double2& data) -> WideVector {
			return loadDouble2(data);
		},
		[](Double2& store, WideVector data) -> void {
			storeDouble2(store, data);
		},
		[](Double2& store, WideVector data) -> void {
			store = dataStoreRep<Double2>(vectorGetByIndex(data, 1));
		});
	evaluateStoreEquivalence<Double3, double>(
		[](const double& value) -> Double3 {
			return dataStoreRep<Double3>(value);
		},
		[](const Double3& data) -> WideVector {
			return loadDouble3(data);
		},
		[](Double3& store, WideVector data) -> void {
			storeDouble3(store, data);
		},
		[](Double3& store, WideVector data) -> void {
			store = dataStoreRep<Double3>(vectorGetByIndex(data, 2));
		});
	evaluateStoreEquivalence<Double4, double>(
		[](const double& value) -> Double4 {
			return dataStoreRep<Double4>(value);
		},
		[](const Double4& data) -> WideVector {
			return loadDouble4(data);
		},
		[](Double4& store, WideVector data) -> void {
			storeDouble4(store, data);
		},
		[](Double4& store, WideVector data) -> void {
			store = dataStoreRep<Double4>(vectorGetByIndex(data, 3));
		});
	evaluateStoreEquivalence<Double2, double>(
		[](const double& value) -> Double2 {
			return dataStoreRep<Double2>(value);
		},
		[](const Double2& data) -> WideVector {
			return loadDouble2(data);
		},
		[](Double2& store, WideVector data) -> void {
			storeDouble2(store, data);
		},
		[](Double2& store, WideVector data) -> void {
			store = dataStoreRep<Double2>(vectorGetX(data));
		});
	evaluateStoreEquivalence<Double2, double>(
		[](const double& value) -> Double2 {
			return dataStoreRep<Double2>(value);
		},
		[](const Double2& data) -> WideVector {
			return loadDouble2(data);
		},
		[](Double2& store, WideVector data) -> void {
			storeDouble2(store, data);
		},
		[](Double2& store, WideVector data) -> void {
			store = dataStoreRep<Double2>(vectorGetY(data));
		});
	evaluateStoreEquivalence<Double3, double>(
		[](const double& value) -> Double3 {
			return dataStoreRep<Double3>(value);
		},
		[](const Double3& data) -> WideVector {
			return loadDouble3(data);
		},
		[](Double3& store, WideVector data) -> void {
			storeDouble3(store, data);
		},
		[](Double3& store, WideVector data) -> void {
			store = dataStoreRep<Double3>(vectorGetZ(data));
		});
	evaluateStoreEquivalence<Double4, double>(
		[](const double& value) -> Double4 {
			return dataStoreRep<Double4>(value);
		},
		[](const Double4& data) -> WideVector {
			return loadDouble4(data);
		},
		[](Double4& store, WideVector data) -> void {
			storeDouble4(store, data);
		},
		[](Double4& store, WideVector data) -> void {
			store = dataStoreRep<Double4>(vectorGetW(data));
		});

	// int checks
	evaluateStoreEquivalence<Integer2, int_least64_t>(
		[](const int_least64_t& value) -> Integer2 {
			return dataStoreRep<Integer2>(value);
		},
		[](const Integer2& data) -> WideIntVector {
			return loadInteger2(data);
		},
		[](Integer2& store, WideIntVector data) -> void {
			storeInteger2(store, data);
		},
		[](Integer2& store, WideIntVector data) -> void {
			store = dataStoreRep<Integer2>(vectorGetByIndex(data, 0));
		});
	evaluateStoreEquivalence<Integer2, int_least64_t>(
		[](const int_least64_t& value) -> Integer2 {
			return dataStoreRep<Integer2>(value);
		},
		[](const Integer2& data) -> WideIntVector {
			return loadInteger2(data);
		},
		[](Integer2& store, WideIntVector data) -> void {
			storeInteger2(store, data);
		},
		[](Integer2& store, WideIntVector data) -> void {
			store = dataStoreRep<Integer2>(vectorGetByIndex(data, 1));
		});
	evaluateStoreEquivalence<Integer3, int_least64_t>(
		[](const int_least64_t& value) -> Integer3 {
			return dataStoreRep<Integer3>(value);
		},
		[](const Integer3& data) -> WideIntVector {
			return loadInteger3(data);
		},
		[](Integer3& store, WideIntVector data) -> void {
			storeInteger3(store, data);
		},
		[](Integer3& store, WideIntVector data) -> void {
			store = dataStoreRep<Integer3>(vectorGetByIndex(data, 2));
		});
	evaluateStoreEquivalence<Integer4, int_least64_t>(
		[](const int_least64_t& value) -> Integer4 {
			return dataStoreRep<Integer4>(value);
		},
		[](const Integer4& data) -> WideIntVector {
			return loadInteger4(data);
		},
		[](Integer4& store, WideIntVector data) -> void {
			storeInteger4(store, data);
		},
		[](Integer4& store, WideIntVector data) -> void {
			store = dataStoreRep<Integer4>(vectorGetByIndex(data, 3));
		});
	evaluateStoreEquivalence<Integer2, int_least64_t>(
		[](const int_least64_t& value) -> Integer2 {
			return dataStoreRep<Integer2>(value);
		},
		[](const Integer2& data) -> WideIntVector {
			return loadInteger2(data);
		},
		[](Integer2& store, WideIntVector data) -> void {
			storeInteger2(store, data);
		},
		[](Integer2& store, WideIntVector data) -> void {
			store = dataStoreRep<Integer2>(vectorGetX(data));
		});
	evaluateStoreEquivalence<Integer2, int_least64_t>(
		[](const int_least64_t& value) -> Integer2 {
			return dataStoreRep<Integer2>(value);
		},
		[](const Integer2& data) -> WideIntVector {
			return loadInteger2(data);
		},
		[](Integer2& store, WideIntVector data) -> void {
			storeInteger2(store, data);
		},
		[](Integer2& store, WideIntVector data) -> void {
			store = dataStoreRep<Integer2>(vectorGetY(data));
		});
	evaluateStoreEquivalence<Integer3, int_least64_t>(
		[](const int_least64_t& value) -> Integer3 {
			return dataStoreRep<Integer3>(value);
		},
		[](const Integer3& data) -> WideIntVector {
			return loadInteger3(data);
		},
		[](Integer3& store, WideIntVector data) -> void {
			storeInteger3(store, data);
		},
		[](Integer3& store, WideIntVector data) -> void {
			store = dataStoreRep<Integer3>(vectorGetZ(data));
		});
	evaluateStoreEquivalence<Integer4, int_least64_t>(
		[](const int_least64_t& value) -> Integer4 {
			return dataStoreRep<Integer4>(value);
		},
		[](const Integer4& data) -> WideIntVector {
			return loadInteger4(data);
		},
		[](Integer4& store, WideIntVector data) -> void {
			storeInteger4(store, data);
		},
		[](Integer4& store, WideIntVector data) -> void {
			store = dataStoreRep<Integer4>(vectorGetW(data));
		});
}

void evalMtrxOneFile()
{
	constexpr static const size_t arrSize = 16;

	using ArrayRes	= std::array<Double4, arrSize>;
	using TupleFile = std::tuple<Double4, uint_least64_t>;
	using ArrayIn	= std::array<TupleFile, arrSize>;

	Double4 rows[4] = {
		Double4{1.0, 1.0, 0.0, 0.0}, Double4{2.0, -2.0, 6.0, -6.0},
		Double4{-4'587.0, 0.001, 2.3, 5.12},
		Double4{g_MinDouble, -g_MaxDouble, g_PI, g_Epsilon}};

	auto dataIn =
		ArrayIn{TupleFile{rows[0], 0}, TupleFile{rows[0], 1},
				TupleFile{rows[0], 2}, TupleFile{rows[0], 3},
				TupleFile{rows[1], 0}, TupleFile{rows[1], 1},
				TupleFile{rows[1], 2}, TupleFile{rows[1], 3},
				TupleFile{rows[2], 0}, TupleFile{rows[2], 1},
				TupleFile{rows[2], 2}, TupleFile{rows[2], 3},
				TupleFile{rows[3], 0}, TupleFile{rows[3], 1},
				TupleFile{rows[3], 2}, TupleFile{rows[3], 3}};
	auto results =
		ArrayRes{rows[0], rows[0], rows[0], rows[0], rows[1], rows[1],
				 rows[1], rows[1], rows[2], rows[2], rows[2], rows[2],
				 rows[3], rows[3], rows[3], rows[3]};

	// eval matrixGetRow
	callEvalFunc(
		[](const Double4& expectRes, const Double4& rowData,
		   uint_least64_t rowIndex) -> void {
			Double4 defaultRows[4] = {Double4{1.0, 0.0, 0.0, 0.0},
									  Double4{0.0, 1.0, 0.0, 0.0},
									  Double4{0.0, 0.0, 1.0, 0.0},
									  Double4{0.0, 0.0, 0.0, 1.0}};

			Double4x4 inputMatrix =
				Double4x4{(rowIndex == 0) ? rowData : defaultRows[0],
						  (rowIndex == 1) ? rowData : defaultRows[1],
						  (rowIndex == 2) ? rowData : defaultRows[2],
						  (rowIndex == 3) ? rowData : defaultRows[3]};
			Double4 result;

			storeDouble4(
				result,
				matrixGetRow(loadDouble4x4(inputMatrix), rowIndex));

			approxEqual(expectRes.m_Components.m_X,
						result.m_Components.m_X, 10'000'000.0);
			approxEqual(expectRes.m_Components.m_Y,
						result.m_Components.m_Y, 10'000'000.0);
			approxEqual(expectRes.m_Components.m_Z,
						result.m_Components.m_Z, 10'000'000.0);
			approxEqual(expectRes.m_Components.m_W,
						result.m_Components.m_W, 10'000'000.0);
		},
		results, dataIn);

	// eval matrixSetRow
	callEvalFunc(
		[](const Double4& expectRes, const Double4& rowData,
		   uint_least64_t rowIndex) -> void {
			Double4x4 result;
			storeDouble4x4(
				result, matrixSetRow(matrixIdentity(),
									 loadDouble4(rowData), rowIndex));

			rowIndex &= 0b11;
			dataStoreApproxEq((rowIndex == 0)
								  ? expectRes
								  : Double4{1.0, 0.0, 0.0, 0.0},
							  result.m_Tuples[0], 10'000'000.0);
			dataStoreApproxEq((rowIndex == 1)
								  ? expectRes
								  : Double4{0.0, 1.0, 0.0, 0.0},
							  result.m_Tuples[1], 10'000'000.0);
			dataStoreApproxEq((rowIndex == 2)
								  ? expectRes
								  : Double4{0.0, 0.0, 1.0, 0.0},
							  result.m_Tuples[2], 10'000'000.0);
			dataStoreApproxEq((rowIndex == 3)
								  ? expectRes
								  : Double4{0.0, 0.0, 0.0, 1.0},
							  result.m_Tuples[3], 10'000'000.0);
		},
		results, dataIn);

	// eval matrixSetColumn
	callEvalFunc(
		[](const Double4& expectRes, const Double4& columnData,
		   uint_least64_t columnIndex) -> void {
			Double4x4 result;
			storeDouble4x4(result,
						   matrixSetColumn(matrixIdentity(),
										   loadDouble4(columnData),
										   columnIndex));

			columnIndex &= 0b11;
			approxEqual(
				(columnIndex == 0) ? expectRes.m_Components.m_X : 1.0,
				result.m_Tuples[0].m_Components.m_X, 10'000'000.0);
			approxEqual(
				(columnIndex == 1) ? expectRes.m_Components.m_X : 0.0,
				result.m_Tuples[0].m_Components.m_Y, 10'000'000.0);
			approxEqual(
				(columnIndex == 2) ? expectRes.m_Components.m_X : 0.0,
				result.m_Tuples[0].m_Components.m_Z, 10'000'000.0);
			approxEqual(
				(columnIndex == 3) ? expectRes.m_Components.m_X : 0.0,
				result.m_Tuples[0].m_Components.m_W, 10'000'000.0);

			approxEqual(
				(columnIndex == 0) ? expectRes.m_Components.m_Y : 0.0,
				result.m_Tuples[1].m_Components.m_X, 10'000'000.0);
			approxEqual(
				(columnIndex == 1) ? expectRes.m_Components.m_Y : 1.0,
				result.m_Tuples[1].m_Components.m_Y, 10'000'000.0);
			approxEqual(
				(columnIndex == 2) ? expectRes.m_Components.m_Y : 0.0,
				result.m_Tuples[1].m_Components.m_Z, 10'000'000.0);
			approxEqual(
				(columnIndex == 3) ? expectRes.m_Components.m_Y : 0.0,
				result.m_Tuples[1].m_Components.m_W, 10'000'000.0);

			approxEqual(
				(columnIndex == 0) ? expectRes.m_Components.m_Z : 0.0,
				result.m_Tuples[2].m_Components.m_X, 10'000'000.0);
			approxEqual(
				(columnIndex == 1) ? expectRes.m_Components.m_Z : 0.0,
				result.m_Tuples[2].m_Components.m_Y, 10'000'000.0);
			approxEqual(
				(columnIndex == 2) ? expectRes.m_Components.m_Z : 1.0,
				result.m_Tuples[2].m_Components.m_Z, 10'000'000.0);
			approxEqual(
				(columnIndex == 3) ? expectRes.m_Components.m_Z : 0.0,
				result.m_Tuples[2].m_Components.m_W, 10'000'000.0);

			approxEqual(
				(columnIndex == 0) ? expectRes.m_Components.m_W : 0.0,
				result.m_Tuples[3].m_Components.m_X, 10'000'000.0);
			approxEqual(
				(columnIndex == 1) ? expectRes.m_Components.m_W : 0.0,
				result.m_Tuples[3].m_Components.m_Y, 10'000'000.0);
			approxEqual(
				(columnIndex == 2) ? expectRes.m_Components.m_W : 0.0,
				result.m_Tuples[3].m_Components.m_Z, 10'000'000.0);
			approxEqual(
				(columnIndex == 3) ? expectRes.m_Components.m_W : 1.0,
				result.m_Tuples[3].m_Components.m_W, 10'000'000.0);
		},
		results, dataIn);
}
void evalMtrxAllFiles()
{
	constexpr static const size_t arrSize = 6;

	using ArrayRes	= std::array<Double4x4, arrSize>;
	using TupleMtrx = std::tuple<Double4x4>;
	using ArrayIn	= std::array<TupleMtrx, arrSize>;

	Double4x4 rawMatrices[6] = {
		Double4x4{
			Double4{1.0, 0.0, 0.0, 0.0}, Double4{0.0, 1.0, 0.0, 0.0},
			Double4{0.0, 0.0, 1.0, 0.0}, Double4{0.0, 0.0, 0.0, 1.0}},
		Double4x4{Double4{1.0, 2.0, 3.0, 4.0},
				  Double4{-1.0, -2.0, -3.0, -4.0},
				  Double4{5.0, 6.0, 7.0, 8.0},
				  Double4{-5.0, -6.0, -7.0, -8.0}},
		Double4x4{Double4{0.3, 1.5, 4.7, 2.0},
				  Double4{1'000.4, -345.2, 20.3, 1.1},
				  Double4{-4'587.0, 0.001, 2.3, 5.12},
				  Double4{0.1, 0.4, 7.8, -98.4}},
		Double4x4{
			Double4{0.0, 0.0, 0.0, 0.0}, Double4{0.0, 0.0, 0.0, 0.0},
			Double4{0.0, 0.0, 0.0, 0.0}, Double4{0.0, 0.0, 0.0, 0.0}},
		Double4x4{
			Double4{0.0, 1.0, 0.0, 0.0}, Double4{1.0, 0.0, 0.0, 0.0},
			Double4{0.0, 0.0, 0.0, 1.0}, Double4{0.0, 0.0, 1.0, 0.0}},
		Double4x4{Double4{0.0, 1.0, 1.0, 1.0},
				  Double4{0.0, 0.0, 1.0, 1.0},
				  Double4{0.0, 0.0, 0.0, 1.0},
				  Double4{0.0, 0.0, 0.0, 0.0}}};

	ArrayIn dataIn =
		ArrayIn{TupleMtrx{rawMatrices[0]}, TupleMtrx{rawMatrices[1]},
				TupleMtrx{rawMatrices[2]}, TupleMtrx{rawMatrices[3]},
				TupleMtrx{rawMatrices[4]}, TupleMtrx{rawMatrices[5]}};

	ArrayRes resRows	= ArrayRes{rawMatrices[0], rawMatrices[1],
								   rawMatrices[2], rawMatrices[3],
								   rawMatrices[4], rawMatrices[5]},
			 resColumns = ArrayRes{
				 rawMatrices[0],
				 Double4x4{Double4{1.0, -1.0, 5.0, -5.0},
						   Double4{2.0, -2.0, 6.0, -6.0},
						   Double4{3.0, -3.0, 7.0, -7.0},
						   Double4{4.0, -4.0, 8.0, -8.0}},
				 Double4x4{Double4{0.3, 1'000.4, -4'587.0, 0.1},
						   Double4{1.5, -345.2, 0.001, 0.4},
						   Double4{4.7, 20.3, 2.3, 7.8},
						   Double4{2.0, 1.1, 5.12, -98.4}},
				 rawMatrices[3],
				 rawMatrices[4],
				 Double4x4{Double4{0.0, 0.0, 0.0, 0.0},
						   Double4{1.0, 0.0, 0.0, 0.0},
						   Double4{1.0, 1.0, 0.0, 0.0},
						   Double4{1.0, 1.0, 1.0, 0.0}}};

	// eval matrixSet(elements)
	callEvalFunc(
		[](const Double4x4& expectRes,
		   const Double4x4& matrix) -> void {
			Double4x4 result;
			storeDouble4x4(
				result,
				matrixSet(matrix.m_Tuples[0].m_Components.m_X,
						  matrix.m_Tuples[0].m_Components.m_Y,
						  matrix.m_Tuples[0].m_Components.m_Z,
						  matrix.m_Tuples[0].m_Components.m_W,
						  matrix.m_Tuples[1].m_Components.m_X,
						  matrix.m_Tuples[1].m_Components.m_Y,
						  matrix.m_Tuples[1].m_Components.m_Z,
						  matrix.m_Tuples[1].m_Components.m_W,
						  matrix.m_Tuples[2].m_Components.m_X,
						  matrix.m_Tuples[2].m_Components.m_Y,
						  matrix.m_Tuples[2].m_Components.m_Z,
						  matrix.m_Tuples[2].m_Components.m_W,
						  matrix.m_Tuples[3].m_Components.m_X,
						  matrix.m_Tuples[3].m_Components.m_Y,
						  matrix.m_Tuples[3].m_Components.m_Z,
						  matrix.m_Tuples[3].m_Components.m_W));

			approxEqual(result.m_Tuples[0].m_Components.m_X,
						expectRes.m_Tuples[0].m_Components.m_X,
						10'000'000.0);
			approxEqual(result.m_Tuples[0].m_Components.m_Y,
						expectRes.m_Tuples[0].m_Components.m_Y,
						10'000'000.0);
			approxEqual(result.m_Tuples[0].m_Components.m_Z,
						expectRes.m_Tuples[0].m_Components.m_Z,
						10'000'000.0);
			approxEqual(result.m_Tuples[0].m_Components.m_W,
						expectRes.m_Tuples[0].m_Components.m_W,
						10'000'000.0);

			approxEqual(result.m_Tuples[1].m_Components.m_X,
						expectRes.m_Tuples[1].m_Components.m_X,
						10'000'000.0);
			approxEqual(result.m_Tuples[1].m_Components.m_Y,
						expectRes.m_Tuples[1].m_Components.m_Y,
						10'000'000.0);
			approxEqual(result.m_Tuples[1].m_Components.m_Z,
						expectRes.m_Tuples[1].m_Components.m_Z,
						10'000'000.0);
			approxEqual(result.m_Tuples[1].m_Components.m_W,
						expectRes.m_Tuples[1].m_Components.m_W,
						10'000'000.0);

			approxEqual(result.m_Tuples[2].m_Components.m_X,
						expectRes.m_Tuples[2].m_Components.m_X,
						10'000'000.0);
			approxEqual(result.m_Tuples[2].m_Components.m_Y,
						expectRes.m_Tuples[2].m_Components.m_Y,
						10'000'000.0);
			approxEqual(result.m_Tuples[2].m_Components.m_Z,
						expectRes.m_Tuples[2].m_Components.m_Z,
						10'000'000.0);
			approxEqual(result.m_Tuples[2].m_Components.m_W,
						expectRes.m_Tuples[2].m_Components.m_W,
						10'000'000.0);

			approxEqual(result.m_Tuples[3].m_Components.m_X,
						expectRes.m_Tuples[3].m_Components.m_X,
						10'000'000.0);
			approxEqual(result.m_Tuples[3].m_Components.m_Y,
						expectRes.m_Tuples[3].m_Components.m_Y,
						10'000'000.0);
			approxEqual(result.m_Tuples[3].m_Components.m_Z,
						expectRes.m_Tuples[3].m_Components.m_Z,
						10'000'000.0);
			approxEqual(result.m_Tuples[3].m_Components.m_W,
						expectRes.m_Tuples[3].m_Components.m_W,
						10'000'000.0);
		},
		resRows, dataIn);

	// eval matrixSet(rows)
	callEvalFunc(
		[](const Double4x4& expectRes,
		   const Double4x4& matrix) -> void {
			Double4x4 result;
			storeDouble4x4(
				result, matrixSet(loadDouble4(matrix.m_Tuples[0]),
								  loadDouble4(matrix.m_Tuples[1]),
								  loadDouble4(matrix.m_Tuples[2]),
								  loadDouble4(matrix.m_Tuples[3])));

			dataStoreApproxEq(expectRes, result, 10'000'000.0);
		},
		resRows, dataIn);


	// eval matrixSetByColumn
	callEvalFunc(
		[](const Double4x4& expectRes,
		   const Double4x4& matrix) -> void {
			Double4x4 result;
			storeDouble4x4(
				result,
				matrixSetByColumn(loadDouble4(matrix.m_Tuples[0]),
								  loadDouble4(matrix.m_Tuples[1]),
								  loadDouble4(matrix.m_Tuples[2]),
								  loadDouble4(matrix.m_Tuples[3])));

			dataStoreApproxEq(expectRes, result, 10'000'000.0);
		},
		resColumns, dataIn);
}

}  // namespace
void evalOneArgComparisons()
{
	constexpr static const size_t arrSize = 2;

	using Tuple1Dbl	  = std::tuple<Double4>;
	using ArrayIn1D	  = std::array<Tuple1Dbl, arrSize>;
	using Array1DResI = std::array<Integer4, arrSize>;

	// double comparison test data
	ArrayIn1D lookDoubles =
		ArrayIn1D{Double4{0.0, g_MinDouble, g_MaxDouble, g_InfDouble},
				  Double4{-g_InfDouble, g_QNaN, g_QNaN, g_QNaN}};

	// double comparison results, per comparison operation
	auto nanRes = Array1DResI{Integer4{0, 0, 0, 0},
							  Integer4{0, g_TrueVal, g_TrueVal,
									   g_TrueVal}},
		 infRes = Array1DResI{Integer4{0, 0, 0, g_TrueVal},
							  Integer4{g_TrueVal, 0, 0, 0}};

	// evaluate 'IsNaN'
	evaluateComparisons<false>(
		[](const Double4& testee) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorIsNaN(loadDouble4(testee)));

			return result;
		},
		nanRes, lookDoubles);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult,
		   const Double4&  testee) -> Integer4 {
			Integer4 result;
			storeInteger4(
				result,
				vectorIsNaNRecord(recordResult, loadDouble4(testee)));

			return result;
		},
		nanRes, lookDoubles);
	// evaluate 'IsInf'
	evaluateComparisons<false>(
		[](const Double4& testee) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorIsInf(loadDouble4(testee)));

			return result;
		},
		infRes, lookDoubles);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult,
		   const Double4&  testee) -> Integer4 {
			Integer4 result;
			storeInteger4(
				result,
				vectorIsInfRecord(recordResult, loadDouble4(testee)));

			return result;
		},
		infRes, lookDoubles);
}
void evalTwoArgDblComparisons()
{
	constexpr static const size_t arrSize = 5;

	using Tuple2Dbl	  = std::tuple<Double4, Double4>;
	using ArrayIn2D	  = std::array<Tuple2Dbl, arrSize>;
	using Array2DResI = std::array<Integer4, arrSize>;

	// double comparison test data
	ArrayIn2D compDoubles = ArrayIn2D{
		Tuple2Dbl{Double4{0.0, -0.0, 0.0, 0.0},
				  Double4{0.0, 0.0, -0.0, 1.0}},
		Tuple2Dbl{Double4{1.0, -1.0, 0.0, -1.0},
				  Double4{0.0, 0.0, -1.0, 1.0}},
		Tuple2Dbl{Double4{g_MinDouble, -g_MinDouble, g_MaxDouble,
						  -g_MaxDouble},
				  Double4{0.0, 0.0, g_InfDouble, -g_InfDouble}},
		Tuple2Dbl{Double4{g_MinDouble, g_MaxDouble, -g_MinDouble,
						  -g_InfDouble},
				  Double4{g_MaxDouble, -g_MaxDouble, g_MinDouble,
						  g_InfDouble}},
		Tuple2Dbl{
			Double4{g_MinDouble, g_QNaN, g_QNaN, g_InfDouble},
			Double4{g_MinDouble, g_QNaN, -g_MaxDouble, g_InfDouble}}};

	// double comparison results, per comparison operation
	auto neDblRes =
			 Array2DResI{
				 Integer4{0, 0, 0, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, g_TrueVal, g_TrueVal, 0}},
		 geDblRes =
			 Array2DResI{Integer4{g_TrueVal, g_TrueVal, g_TrueVal, 0},
						 Integer4{g_TrueVal, 0, g_TrueVal, 0},
						 Integer4{g_TrueVal, 0, 0, g_TrueVal},
						 Integer4{0, g_TrueVal, 0, 0},
						 Integer4{g_TrueVal, 0, 0, g_TrueVal}},
		 gDblRes = Array2DResI{Integer4{0, 0, 0, 0},
							   Integer4{g_TrueVal, 0, g_TrueVal, 0},
							   Integer4{g_TrueVal, 0, 0, g_TrueVal},
							   Integer4{0, g_TrueVal, 0, 0},
							   Integer4{0, 0, 0, 0}},
		 eDblRes =
			 Array2DResI{Integer4{g_TrueVal, g_TrueVal, g_TrueVal, 0},
						 Integer4{0, 0, 0, 0}, Integer4{0, 0, 0, 0},
						 Integer4{0, 0, 0, 0},
						 Integer4{g_TrueVal, 0, 0, g_TrueVal}},
		 leDblRes =
			 Array2DResI{
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, g_TrueVal, 0, g_TrueVal},
				 Integer4{0, g_TrueVal, g_TrueVal, 0},
				 Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, 0, 0, g_TrueVal}},
		 lDblRes =
			 Array2DResI{Integer4{0, 0, 0, g_TrueVal},
						 Integer4{0, g_TrueVal, 0, g_TrueVal},
						 Integer4{0, g_TrueVal, g_TrueVal, 0},
						 Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
						 Integer4{0, 0, 0, 0}};

	// evaluate 'less'
	evaluateComparisons<false>(
		[](const Double4& lhs, const Double4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorLess(loadDouble4(lhs),
											 loadDouble4(rhs)));

			return result;
		},
		lDblRes, compDoubles);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult, const Double4& lhs,
		   const Double4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorLessRecord(recordResult,
												   loadDouble4(lhs),
												   loadDouble4(rhs)));

			return result;
		},
		lDblRes, compDoubles);

	// evaluate 'LessEqual'
	evaluateComparisons<false>(
		[](const Double4& lhs, const Double4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorLessEqual(loadDouble4(lhs),
												  loadDouble4(rhs)));

			return result;
		},
		leDblRes, compDoubles);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult, const Double4& lhs,
		   const Double4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorLessEqualRecord(
									  recordResult, loadDouble4(lhs),
									  loadDouble4(rhs)));

			return result;
		},
		leDblRes, compDoubles);

	// evaluate 'Greater'
	evaluateComparisons<false>(
		[](const Double4& lhs, const Double4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorGreater(loadDouble4(lhs),
												loadDouble4(rhs)));

			return result;
		},
		gDblRes, compDoubles);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult, const Double4& lhs,
		   const Double4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorGreaterRecord(
									  recordResult, loadDouble4(lhs),
									  loadDouble4(rhs)));

			return result;
		},
		gDblRes, compDoubles);

	// evaluate 'GreaterEqual'
	evaluateComparisons<false>(
		[](const Double4& lhs, const Double4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result,
						  vectorGreaterEqual(loadDouble4(lhs),
											 loadDouble4(rhs)));

			return result;
		},
		geDblRes, compDoubles);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult, const Double4& lhs,
		   const Double4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorGreaterEqualRecord(
									  recordResult, loadDouble4(lhs),
									  loadDouble4(rhs)));

			return result;
		},
		geDblRes, compDoubles);

	// evaluate 'Equal'
	evaluateComparisons<false>(
		[](const Double4& lhs, const Double4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorEqual(loadDouble4(lhs),
											  loadDouble4(rhs)));

			return result;
		},
		eDblRes, compDoubles);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult, const Double4& lhs,
		   const Double4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorEqualRecord(
									  recordResult, loadDouble4(lhs),
									  loadDouble4(rhs)));

			return result;
		},
		eDblRes, compDoubles);

	// evaluate 'NotEqual'
	evaluateComparisons<false>(
		[](const Double4& lhs, const Double4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorNotEqual(loadDouble4(lhs),
												 loadDouble4(rhs)));

			return result;
		},
		neDblRes, compDoubles);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult, const Double4& lhs,
		   const Double4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorNotEqualRecord(
									  recordResult, loadDouble4(lhs),
									  loadDouble4(rhs)));

			return result;
		},
		neDblRes, compDoubles);
}
void evalTwoArgIntComparisons()
{
	constexpr static const size_t arrSize = 4;

	using Tuple2Int	 = std::tuple<Integer4, Integer4>;
	using ArrayInI	 = std::array<Tuple2Int, arrSize>;
	using ArrayIResI = std::array<Integer4, arrSize>;

	// int comparison test data
	ArrayInI compInts = ArrayInI{
		Tuple2Int{Integer4{0, 0, 1, -1}, Integer4{0, 1, 0, 0}},
		Tuple2Int{Integer4{0, -1, g_MinInt, 0},
				  Integer4{-1, 1, 0, g_MinInt}},
		Tuple2Int{Integer4{g_MinInt, g_MaxInt, g_MaxInt, 0},
				  Integer4{g_MaxInt, g_MinInt, 0, g_MaxInt}},
		Tuple2Int{Integer4{1, -1, g_MinInt, g_MaxInt},
				  Integer4{1, -1, g_MinInt, g_MaxInt}}};

	// int comparison results, per comparison operation
	auto neIntRes =
			 ArrayIResI{
				 Integer4{0, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, 0, 0}},
		 geIntRes = ArrayIResI{Integer4{g_TrueVal, 0, g_TrueVal, 0},
							   Integer4{g_TrueVal, 0, 0, g_TrueVal},
							   Integer4{0, g_TrueVal, g_TrueVal, 0},
							   Integer4{g_TrueVal, g_TrueVal,
										g_TrueVal, g_TrueVal}},
		 gIntRes  = ArrayIResI{Integer4{0, 0, g_TrueVal, 0},
							   Integer4{g_TrueVal, 0, 0, g_TrueVal},
							   Integer4{0, g_TrueVal, g_TrueVal, 0},
							   Integer4{0, 0, 0, 0}},
		 eIntRes =
			 ArrayIResI{Integer4{g_TrueVal, 0, 0, 0},
						Integer4{0, 0, 0, 0}, Integer4{0, 0, 0, 0},
						Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
								 g_TrueVal}},
		 leIntRes =
			 ArrayIResI{Integer4{g_TrueVal, g_TrueVal, 0, g_TrueVal},
						Integer4{0, g_TrueVal, g_TrueVal, 0},
						Integer4{g_TrueVal, 0, 0, g_TrueVal},
						Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
								 g_TrueVal}},
		 lIntRes = ArrayIResI{Integer4{0, g_TrueVal, 0, g_TrueVal},
							  Integer4{0, g_TrueVal, g_TrueVal, 0},
							  Integer4{g_TrueVal, 0, 0, g_TrueVal},
							  Integer4{0, 0, 0, 0}};

	// evaluate 'less'
	evaluateComparisons<false>(
		[](const Integer4& lhs, const Integer4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorLess(loadInteger4(lhs),
											 loadInteger4(rhs)));

			return result;
		},
		lIntRes, compInts);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult, const Integer4& lhs,
		   const Integer4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorLessRecord(
									  recordResult, loadInteger4(lhs),
									  loadInteger4(rhs)));

			return result;
		},
		lIntRes, compInts);

	// evaluate 'LessEqual'
	evaluateComparisons<false>(
		[](const Integer4& lhs, const Integer4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorLessEqual(loadInteger4(lhs),
												  loadInteger4(rhs)));

			return result;
		},
		leIntRes, compInts);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult, const Integer4& lhs,
		   const Integer4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorLessEqualRecord(
									  recordResult, loadInteger4(lhs),
									  loadInteger4(rhs)));

			return result;
		},
		leIntRes, compInts);

	// evaluate 'Greater'
	evaluateComparisons<false>(
		[](const Integer4& lhs, const Integer4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorGreater(loadInteger4(lhs),
												loadInteger4(rhs)));

			return result;
		},
		gIntRes, compInts);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult, const Integer4& lhs,
		   const Integer4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorGreaterRecord(
									  recordResult, loadInteger4(lhs),
									  loadInteger4(rhs)));

			return result;
		},
		gIntRes, compInts);

	// evaluate 'GreaterEqual'
	evaluateComparisons<false>(
		[](const Integer4& lhs, const Integer4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result,
						  vectorGreaterEqual(loadInteger4(lhs),
											 loadInteger4(rhs)));

			return result;
		},
		geIntRes, compInts);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult, const Integer4& lhs,
		   const Integer4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorGreaterEqualRecord(
									  recordResult, loadInteger4(lhs),
									  loadInteger4(rhs)));

			return result;
		},
		geIntRes, compInts);

	// evaluate 'Equal'
	evaluateComparisons<false>(
		[](const Integer4& lhs, const Integer4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorEqual(loadInteger4(lhs),
											  loadInteger4(rhs)));

			return result;
		},
		eIntRes, compInts);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult, const Integer4& lhs,
		   const Integer4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorEqualRecord(
									  recordResult, loadInteger4(lhs),
									  loadInteger4(rhs)));

			return result;
		},
		eIntRes, compInts);

	// evaluate 'NotEqual'
	evaluateComparisons<false>(
		[](const Integer4& lhs, const Integer4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorNotEqual(loadInteger4(lhs),
												 loadInteger4(rhs)));

			return result;
		},
		neIntRes, compInts);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult, const Integer4& lhs,
		   const Integer4& rhs) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorNotEqualRecord(
									  recordResult, loadInteger4(lhs),
									  loadInteger4(rhs)));

			return result;
		},
		neIntRes, compInts);
}
void evalThreeArgDblComparisons()
{
	constexpr static const size_t arrSize = 5;

	using Tuple3Dbl	  = std::tuple<Double4, Double4, Double4>;
	using ArrayInI	  = std::array<Tuple3Dbl, arrSize>;
	using Array3DResI = std::array<Integer4, arrSize>;

	// int comparison test data (checked, lower, upper)
	ArrayInI boundsDoubles = ArrayInI{
		Tuple3Dbl{
			Double4{1.0, -1.0, 1.0 + g_Epsilon, -1.0 - g_Epsilon},
			Double4{-1.0, -1.0, -1.0, -1.0},
			Double4{1.0, 1.0, 1.0, 1.0}},
		Tuple3Dbl{Double4{g_InfDouble, -g_InfDouble, g_QNaN, g_QNaN},
				  Double4{-1.0, -1.0, -1.0, -g_InfDouble},
				  Double4{1.0, 1.0, 1.0, g_InfDouble}},
		Tuple3Dbl{Double4{0.0, -1.0, g_MillionDouble,
						  -g_MillionDouble * g_MillionDouble},
				  Double4{-g_InfDouble, -g_InfDouble, -g_InfDouble,
						  -g_InfDouble},
				  Double4{g_InfDouble, g_InfDouble, g_InfDouble,
						  g_InfDouble}},
		Tuple3Dbl{
			Double4{g_MillionDouble * (1 + g_Epsilon),
					g_MillionDouble,
					g_MillionDouble * (1 - g_Epsilon),
					1.5 * g_MillionDouble},
			Double4{g_MillionDouble, g_MillionDouble, g_MillionDouble,
					g_MillionDouble},
			Double4{2.0 * g_MillionDouble, 2.0 * g_MillionDouble,
					2.0 * g_MillionDouble, 2.0 * g_MillionDouble}},
		Tuple3Dbl{
			Double4{2.0 * g_MillionDouble * (1 + g_Epsilon),
					2.0 * g_MillionDouble,
					2.0 * g_MillionDouble * (1 - g_Epsilon),
					3.0 * g_MillionDouble},
			Double4{g_MillionDouble, g_MillionDouble, g_MillionDouble,
					g_MillionDouble},
			Double4{2.0 * g_MillionDouble, 2.0 * g_MillionDouble,
					2.0 * g_MillionDouble, 2.0 * g_MillionDouble}}};

	// int comparison results
	Array3DResI boundsRes = Array3DResI{
		Integer4{g_TrueVal, g_TrueVal, 0, 0}, Integer4{0, 0, 0, 0},
		Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
		Integer4{g_TrueVal, g_TrueVal, 0, g_TrueVal},
		Integer4{0, g_TrueVal, g_TrueVal, 0}};

	// evaluate 'inBounds'
	evaluateComparisons<false>(
		[](const Double4& checked, const Double4& lower,
		   const Double4& upper) -> Integer4 {
			Integer4 result;
			storeInteger4(result, vectorInBounds(loadDouble4(checked),
												 loadDouble4(lower),
												 loadDouble4(upper)));

			return result;
		},
		boundsRes, boundsDoubles);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult, const Double4& checked,
		   const Double4& lower, const Double4& upper) -> Integer4 {
			Integer4 result;
			storeInteger4(
				result, vectorInBoundsRecord(
							recordResult, loadDouble4(checked),
							loadDouble4(lower), loadDouble4(upper)));

			return result;
		},
		boundsRes, boundsDoubles);
}
void evalThreeArgIntComparisons()
{
	constexpr static const size_t arrSize = 3;

	using Tuple3Int	  = std::tuple<Integer4, Integer4, Integer4>;
	using ArrayInI	  = std::array<Tuple3Int, arrSize>;
	using Array3IResI = std::array<Integer4, arrSize>;

	// int comparison test data (checked, lower, upper)
	ArrayInI boundsDoubles = ArrayInI{
		Tuple3Int{Integer4{-1, 0, 1, 2}, Integer4{-1, -1, -1, -1},
				  Integer4{1, 1, 1, 1}},
		Tuple3Int{Integer4{-100, -101, 100, 0},
				  Integer4{-100, -100, -100, -100},
				  Integer4{0, 0, 0, 0}},
		Tuple3Int{Integer4{g_MinInt, g_MaxInt, g_MaxInt, g_MinInt},
				  Integer4{g_MinInt, g_MinInt, g_MinInt, 0},
				  Integer4{g_MaxInt, g_MaxInt, 0, g_MaxInt}}};

	// int comparison results
	Array3IResI boundsRes =
		Array3IResI{Integer4{g_TrueVal, g_TrueVal, g_TrueVal, 0},
					Integer4{g_TrueVal, 0, 0, g_TrueVal},
					Integer4{g_TrueVal, g_TrueVal, 0, 0}};

	// evaluate 'inBounds'
	evaluateComparisons<false>(
		[](const Integer4& checked, const Integer4& lower,
		   const Integer4& upper) -> Integer4 {
			Integer4 result;
			storeInteger4(result,
						  vectorInBounds(loadInteger4(checked),
										 loadInteger4(lower),
										 loadInteger4(upper)));

			return result;
		},
		boundsRes, boundsDoubles);
	evaluateComparisons<true>(
		[](uint_least64_t& recordResult, const Integer4& checked,
		   const Integer4& lower, const Integer4& upper) -> Integer4 {
			Integer4 result;
			storeInteger4(result,
						  vectorInBoundsRecord(recordResult,
											   loadInteger4(checked),
											   loadInteger4(lower),
											   loadInteger4(upper)));

			return result;
		},
		boundsRes, boundsDoubles);
}

void evalOneArg4DComparisons()
{
	constexpr static const size_t arrSize = 5;

	using Tuple1Dbl = std::tuple<Double4>;
	using ArrayIn	= std::array<Tuple1Dbl, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResult = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X | expRes.m_Components.m_Y
				| expRes.m_Components.m_Z | expRes.m_Components.m_W)
			   != 0;
	};

	// double comparison test data
	ArrayIn checks = ArrayIn{
		Double4{0.0, g_MinDouble, g_MaxDouble, -1.0},
		Double4{g_QNaN, g_QNaN, g_QNaN, g_QNaN},
		Double4{g_InfDouble, -g_InfDouble, g_InfDouble, -g_InfDouble},
		Double4{g_InfDouble, g_QNaN, -g_InfDouble, g_QNaN},
		Double4{g_QNaN, g_InfDouble, -g_MaxDouble, g_MinDouble}};

	// double comparison results, per comparison operation
	auto nanRes = ArrayResI{Integer4{0, 0, 0, 0},
							Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
									 g_TrueVal},
							Integer4{0, 0, 0, 0},
							Integer4{0, g_TrueVal, 0, g_TrueVal},
							Integer4{g_TrueVal, 0, 0, 0}},
		 infRes = ArrayResI{
			 Integer4{0, 0, 0, 0}, Integer4{0, 0, 0, 0},
			 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
			 Integer4{g_TrueVal, 0, g_TrueVal, 0},
			 Integer4{0, g_TrueVal, 0, 0}};

	// evaluate isNaN
	evaluateDimensionComparisons<false>(
		[](const Double4& value) -> bool {
			return vector4DIsNaN(loadDouble4(value));
		},
		getExpectedResult, nanRes, checks);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double4& value) -> bool {
			return vector4DIsNaNRecord(record, loadDouble4(value));
		},
		getExpectedResult, nanRes, checks);

	// evaluate isInf
	evaluateDimensionComparisons<false>(
		[](const Double4& value) -> bool {
			return vector4DIsInf(loadDouble4(value));
		},
		getExpectedResult, infRes, checks);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double4& value) -> bool {
			return vector4DIsInfRecord(record, loadDouble4(value));
		},
		getExpectedResult, infRes, checks);
}
void evalTwoArg4DDblComparisons()
{
	constexpr static const size_t arrSize = 6;

	using Tuple2Dbl = std::tuple<Double4, Double4>;
	using ArrayIn	= std::array<Tuple2Dbl, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResultAnd = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X & expRes.m_Components.m_Y
				& expRes.m_Components.m_Z & expRes.m_Components.m_W)
			   != 0;
	};
	auto getExpectedResultOr = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X | expRes.m_Components.m_Y
				| expRes.m_Components.m_Z | expRes.m_Components.m_W)
			   != 0;
	};

	// double comparison test data
	ArrayIn comparisons = ArrayIn{
		Tuple2Dbl{Double4{0.0, -0.0, 0.0, g_MinDouble},
				  Double4{0.0, 0.0, -0.0, g_MinDouble}},
		Tuple2Dbl{Double4{0.0, -1.0, -1.0, 0.0},
				  Double4{1.0, 0.0, 1.0, g_MinDouble}},
		Tuple2Dbl{Double4{1.0, 1.0, 0.0, g_MinDouble},
				  Double4{-1.0, 0.0, -1.0, 0.0}},
		Tuple2Dbl{
			Double4{g_MinDouble, g_QNaN, g_InfDouble, -g_MaxDouble},
			Double4{0.0, -g_InfDouble, g_MaxDouble, -g_InfDouble}},
		Tuple2Dbl{
			Double4{-g_MinDouble, g_MaxDouble, g_QNaN, -g_InfDouble},
			Double4{0.0, g_InfDouble, g_InfDouble, -g_MaxDouble}},
		Tuple2Dbl{Double4{g_MaxDouble, g_InfDouble, g_QNaN, g_QNaN},
				  Double4{g_MaxDouble, g_InfDouble, g_QNaN, g_QNaN}}};

	// double comparison results, per comparison operation
	auto notEqRes =
			 ArrayResI{
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, g_TrueVal, g_TrueVal}},
		 grtrEqRes =
			 ArrayResI{
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, 0, 0}},
		 grtrRes =
			 ArrayResI{
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, 0, 0}},
		 eqRes = ArrayResI{Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
									g_TrueVal},
						   Integer4{0, 0, 0, 0},
						   Integer4{0, 0, 0, 0},
						   Integer4{0, 0, 0, 0},
						   Integer4{0, 0, 0, 0},
						   Integer4{g_TrueVal, g_TrueVal, 0, 0}},
		 lessEqRes =
			 ArrayResI{
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, 0, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, 0, 0}},
		 lessRes = ArrayResI{
			 Integer4{0, 0, 0, 0},
			 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
			 Integer4{0, 0, 0, 0},
			 Integer4{0, 0, 0, 0},
			 Integer4{g_TrueVal, g_TrueVal, 0, g_TrueVal},
			 Integer4{0, 0, 0, 0}};

	// evaluate Less
	evaluateDimensionComparisons<false>(
		[](const Double4& lhs, const Double4& rhs) -> bool {
			return vector4DLess(loadDouble4(lhs), loadDouble4(rhs));
		},
		getExpectedResultAnd, lessRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double4& lhs,
		   const Double4& rhs) -> bool {
			return vector4DLessRecord(record, loadDouble4(lhs),
									  loadDouble4(rhs));
		},
		getExpectedResultAnd, lessRes, comparisons);

	// evaluate LessEqual
	evaluateDimensionComparisons<false>(
		[](const Double4& lhs, const Double4& rhs) -> bool {
			return vector4DLessEqual(loadDouble4(lhs),
									 loadDouble4(rhs));
		},
		getExpectedResultAnd, lessEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double4& lhs,
		   const Double4& rhs) -> bool {
			return vector4DLessEqualRecord(record, loadDouble4(lhs),
										   loadDouble4(rhs));
		},
		getExpectedResultAnd, lessEqRes, comparisons);

	// evaluate Greater
	evaluateDimensionComparisons<false>(
		[](const Double4& lhs, const Double4& rhs) -> bool {
			return vector4DGreater(loadDouble4(lhs),
								   loadDouble4(rhs));
		},
		getExpectedResultAnd, grtrRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double4& lhs,
		   const Double4& rhs) -> bool {
			return vector4DGreaterRecord(record, loadDouble4(lhs),
										 loadDouble4(rhs));
		},
		getExpectedResultAnd, grtrRes, comparisons);

	// evaluate GreaterEqual
	evaluateDimensionComparisons<false>(
		[](const Double4& lhs, const Double4& rhs) -> bool {
			return vector4DGreaterEqual(loadDouble4(lhs),
										loadDouble4(rhs));
		},
		getExpectedResultAnd, grtrEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double4& lhs,
		   const Double4& rhs) -> bool {
			return vector4DGreaterEqualRecord(
				record, loadDouble4(lhs), loadDouble4(rhs));
		},
		getExpectedResultAnd, grtrEqRes, comparisons);

	// evaluate Equal
	evaluateDimensionComparisons<false>(
		[](const Double4& lhs, const Double4& rhs) -> bool {
			return vector4DEqual(loadDouble4(lhs), loadDouble4(rhs));
		},
		getExpectedResultAnd, eqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double4& lhs,
		   const Double4& rhs) -> bool {
			return vector4DEqualRecord(record, loadDouble4(lhs),
									   loadDouble4(rhs));
		},
		getExpectedResultAnd, eqRes, comparisons);

	// evaluate NotEqual
	evaluateDimensionComparisons<false>(
		[](const Double4& lhs, const Double4& rhs) -> bool {
			return vector4DNotEqual(loadDouble4(lhs),
									loadDouble4(rhs));
		},
		getExpectedResultOr, notEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double4& lhs,
		   const Double4& rhs) -> bool {
			return vector4DNotEqualRecord(record, loadDouble4(lhs),
										  loadDouble4(rhs));
		},
		getExpectedResultOr, notEqRes, comparisons);
}
void evalTwoArg4DIntComparisons()
{
	constexpr static const size_t arrSize = 6;

	using Tuple2Int = std::tuple<Integer4, Integer4>;
	using ArrayIn	= std::array<Tuple2Int, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResultAnd = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X & expRes.m_Components.m_Y
				& expRes.m_Components.m_Z & expRes.m_Components.m_W)
			   != 0;
	};
	auto getExpectedResultOr = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X | expRes.m_Components.m_Y
				| expRes.m_Components.m_Z | expRes.m_Components.m_W)
			   != 0;
	};

	// int comparison test data
	ArrayIn comparisons = ArrayIn{
		Tuple2Int{Integer4{0, 1, -1, -7}, Integer4{0, 1, -1, -7}},
		Tuple2Int{Integer4{g_MinInt, g_MaxInt, g_MinInt, 7},
				  Integer4{g_MinInt, g_MaxInt, g_MaxInt, 7}},
		Tuple2Int{Integer4{-1, g_MinInt, 0, -7},
				  Integer4{0, 0, g_MaxInt, 7}},
		Tuple2Int{Integer4{0, 1, g_MinInt, -15},
				  Integer4{1, -1, g_MaxInt, 15}},
		Tuple2Int{Integer4{1, 1, 0, 7}, Integer4{-1, 0, -1, -7}},
		Tuple2Int{Integer4{g_MaxInt, g_MaxInt, 0, 15},
				  Integer4{g_MaxInt, g_MinInt, g_MinInt, -15}}};

	// int comparison results, per comparison operation
	auto notEqRes =
			 ArrayResI{
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, g_TrueVal, 0},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, g_TrueVal, g_TrueVal, g_TrueVal}},
		 grtrEqRes =
			 ArrayResI{
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, 0, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{0, g_TrueVal, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
						  g_TrueVal}},
		 grtrRes =
			 ArrayResI{
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, 0, 0},
				 Integer4{0, g_TrueVal, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, g_TrueVal, g_TrueVal, g_TrueVal}},
		 eqRes =
			 ArrayResI{
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, 0, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, 0, 0, 0}},
		 lessEqRes =
			 ArrayResI{
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, 0, 0, 0}},
		 lessRes = ArrayResI{
			 Integer4{0, 0, 0, 0},
			 Integer4{0, 0, g_TrueVal, 0},
			 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
			 Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
			 Integer4{0, 0, 0, 0},
			 Integer4{0, 0, 0, 0}};

	// evaluate Less
	evaluateDimensionComparisons<false>(
		[](const Integer4& lhs, const Integer4& rhs) -> bool {
			return vector4DLess(loadInteger4(lhs), loadInteger4(rhs));
		},
		getExpectedResultAnd, lessRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer4& lhs,
		   const Integer4& rhs) -> bool {
			return vector4DLessRecord(record, loadInteger4(lhs),
									  loadInteger4(rhs));
		},
		getExpectedResultAnd, lessRes, comparisons);

	// evaluate LessEqual
	evaluateDimensionComparisons<false>(
		[](const Integer4& lhs, const Integer4& rhs) -> bool {
			return vector4DLessEqual(loadInteger4(lhs),
									 loadInteger4(rhs));
		},
		getExpectedResultAnd, lessEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer4& lhs,
		   const Integer4& rhs) -> bool {
			return vector4DLessEqualRecord(record, loadInteger4(lhs),
										   loadInteger4(rhs));
		},
		getExpectedResultAnd, lessEqRes, comparisons);

	// evaluate Greater
	evaluateDimensionComparisons<false>(
		[](const Integer4& lhs, const Integer4& rhs) -> bool {
			return vector4DGreater(loadInteger4(lhs),
								   loadInteger4(rhs));
		},
		getExpectedResultAnd, grtrRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer4& lhs,
		   const Integer4& rhs) -> bool {
			return vector4DGreaterRecord(record, loadInteger4(lhs),
										 loadInteger4(rhs));
		},
		getExpectedResultAnd, grtrRes, comparisons);

	// evaluate GreaterEqual
	evaluateDimensionComparisons<false>(
		[](const Integer4& lhs, const Integer4& rhs) -> bool {
			return vector4DGreaterEqual(loadInteger4(lhs),
										loadInteger4(rhs));
		},
		getExpectedResultAnd, grtrEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer4& lhs,
		   const Integer4& rhs) -> bool {
			return vector4DGreaterEqualRecord(
				record, loadInteger4(lhs), loadInteger4(rhs));
		},
		getExpectedResultAnd, grtrEqRes, comparisons);

	// evaluate Equal
	evaluateDimensionComparisons<false>(
		[](const Integer4& lhs, const Integer4& rhs) -> bool {
			return vector4DEqual(loadInteger4(lhs),
								 loadInteger4(rhs));
		},
		getExpectedResultAnd, eqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer4& lhs,
		   const Integer4& rhs) -> bool {
			return vector4DEqualRecord(record, loadInteger4(lhs),
									   loadInteger4(rhs));
		},
		getExpectedResultAnd, eqRes, comparisons);

	// evaluate NotEqual
	evaluateDimensionComparisons<false>(
		[](const Integer4& lhs, const Integer4& rhs) -> bool {
			return vector4DNotEqual(loadInteger4(lhs),
									loadInteger4(rhs));
		},
		getExpectedResultOr, notEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer4& lhs,
		   const Integer4& rhs) -> bool {
			return vector4DNotEqualRecord(record, loadInteger4(lhs),
										  loadInteger4(rhs));
		},
		getExpectedResultOr, notEqRes, comparisons);
}
void evalThreeArg4DDblComparisons()
{
	constexpr static const size_t arrSize = 6;

	using Tuple3Dbl = std::tuple<Double4, Double4, Double4>;
	using ArrayIn	= std::array<Tuple3Dbl, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResult = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X & expRes.m_Components.m_Y
				& expRes.m_Components.m_Z & expRes.m_Components.m_W)
			   != 0;
	};

	// double comparison test data (checked, lower, upper)
	ArrayIn comparisons = ArrayIn{
		Tuple3Dbl{Double4{-1.0, 0.0, 1.0, -0.5},
				  Double4{-1.0, -1.0, -1.0, -1.0},
				  Double4{1.0, 1.0, 1.0, 1.0}},
		Tuple3Dbl{Double4{-1.0 + g_Epsilon, -g_InfDouble,
						  -g_MillionDouble, -g_MaxDouble},
				  Double4{-1.0, -1.0, -1.0, -1.0},
				  Double4{1.0, 1.0, 1.0, 1.0}},
		Tuple3Dbl{Double4{1.0 - g_Epsilon, g_InfDouble,
						  g_MillionDouble, g_MaxDouble},
				  Double4{-1.0, -1.0, -1.0, -1.0},
				  Double4{1.0, 1.0, 1.0, 1.0}},
		Tuple3Dbl{
			Double4{g_MillionDouble, -g_MillionDouble,
					g_MillionDouble * (1.0 + g_Epsilon), g_MinDouble},
			Double4{-g_MillionDouble, -g_MillionDouble,
					-g_MillionDouble, -g_MillionDouble},
			Double4{g_MillionDouble, g_MillionDouble, g_MillionDouble,
					g_MillionDouble}},
		Tuple3Dbl{Double4{0.0, g_InfDouble,
						  g_MillionDouble * (-1.0 + g_Epsilon), 1.0},
				  Double4{-g_MillionDouble, -g_MillionDouble,
						  -g_MillionDouble, -g_MillionDouble},
				  Double4{g_MillionDouble, g_MillionDouble,
						  g_MillionDouble, g_MillionDouble}},
		Tuple3Dbl{Double4{g_QNaN, -g_InfDouble, 0.0, g_MaxDouble},
				  Double4{-g_InfDouble, -g_InfDouble, -g_InfDouble,
						  -g_InfDouble},
				  Double4{g_InfDouble, g_InfDouble, g_InfDouble,
						  g_InfDouble}}};

	// double comparison results
	ArrayResI boundsRes = ArrayResI{
		Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
		Integer4{g_TrueVal, 0, 0, 0},
		Integer4{g_TrueVal, 0, 0, 0},
		Integer4{g_TrueVal, g_TrueVal, 0, g_TrueVal},
		Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
		Integer4{0, g_TrueVal, g_TrueVal, g_TrueVal}};

	// evaluate InBounds
	evaluateDimensionComparisons<false>(
		[](const Double4& checked, const Double4& lower,
		   const Double4& upper) -> bool {
			return vector4DInBounds(loadDouble4(checked),
									loadDouble4(lower),
									loadDouble4(upper));
		},
		getExpectedResult, boundsRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double4& checked,
		   const Double4& lower, const Double4& upper) -> bool {
			return vector4DInBoundsRecord(
				record, loadDouble4(checked), loadDouble4(lower),
				loadDouble4(upper));
		},
		getExpectedResult, boundsRes, comparisons);
}
void evalThreeArg4DIntComparisons()
{
	constexpr static const size_t arrSize = 4;

	using Tuple3Int = std::tuple<Integer4, Integer4, Integer4>;
	using ArrayIn	= std::array<Tuple3Int, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResult = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X & expRes.m_Components.m_Y
				& expRes.m_Components.m_Z & expRes.m_Components.m_W)
			   != 0;
	};

	// int comparison test data (checked, lower, upper)
	ArrayIn comparisons = ArrayIn{
		Tuple3Int{Integer4{0, g_MinInt, g_MaxInt, 1'000'000},
				  Integer4{g_MinInt, g_MinInt, g_MinInt, g_MinInt},
				  Integer4{g_MaxInt, g_MaxInt, g_MaxInt, g_MaxInt}},
		Tuple3Int{
			Integer4{1, 1'000'000, g_MaxInt, g_MinInt},
			Integer4{-1'000'000, -1'000'000, -1'000'000, -1'000'000},
			Integer4{1'000'000, 1'000'000, 1'000'000, 1'000'000}},
		Tuple3Int{
			Integer4{-1'000'000, g_MinInt, -1, 456'978},
			Integer4{-1'000'000, -1'000'000, -1'000'000, -1'000'000},
			Integer4{1'000'000, 1'000'000, 1'000'000, 1'000'000}},
		Tuple3Int{
			Integer4{2'000'000, 560, -13'678, -578'324},
			Integer4{-1'000'000, -1'000'000, -1'000'000, -1'000'000},
			Integer4{1'000'000, 1'000'000, 1'000'000, 1'000'000}}};

	// double comparison results
	ArrayResI boundsRes = ArrayResI{
		Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
		Integer4{g_TrueVal, g_TrueVal, 0, 0},
		Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
		Integer4{0, g_TrueVal, g_TrueVal, g_TrueVal}};

	// evaluate InBounds
	evaluateDimensionComparisons<false>(
		[](const Integer4& checked, const Integer4& lower,
		   const Integer4& upper) -> bool {
			return vector4DInBounds(loadInteger4(checked),
									loadInteger4(lower),
									loadInteger4(upper));
		},
		getExpectedResult, boundsRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer4& checked,
		   const Integer4& lower, const Integer4& upper) -> bool {
			return vector4DInBoundsRecord(
				record, loadInteger4(checked), loadInteger4(lower),
				loadInteger4(upper));
		},
		getExpectedResult, boundsRes, comparisons);
}

void evalOneArg3DComparisons()
{
	constexpr static const size_t arrSize = 5;

	using Tuple1Dbl = std::tuple<Double3>;
	using ArrayIn	= std::array<Tuple1Dbl, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResult = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X | expRes.m_Components.m_Y
				| expRes.m_Components.m_Z | expRes.m_Components.m_W)
			   != 0;
	};

	// double comparison test data
	ArrayIn checks =
		ArrayIn{Double3{0.0, g_MinDouble, g_MaxDouble},
				Double3{g_QNaN, g_QNaN, g_QNaN},
				Double3{g_InfDouble, -g_InfDouble, g_InfDouble},
				Double3{g_InfDouble, g_QNaN, -g_InfDouble},
				Double3{g_QNaN, g_InfDouble, -g_MaxDouble}};

	// double comparison results, per comparison operation
	auto nanRes =
			 ArrayResI{
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, 0, 0}, Integer4{0, g_TrueVal, 0, 0},
				 Integer4{g_TrueVal, 0, 0, 0}},
		 infRes = ArrayResI{
			 Integer4{0, 0, 0, 0}, Integer4{0, 0, 0, 0},
			 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
			 Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
			 Integer4{0, g_TrueVal, 0, 0}};

	// evaluate isNaN
	evaluateDimensionComparisons<false>(
		[](const Double3& value) -> bool {
			return vector3DIsNaN(loadDouble3(value));
		},
		getExpectedResult, nanRes, checks);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double3& value) -> bool {
			return vector3DIsNaNRecord(record, loadDouble3(value));
		},
		getExpectedResult, nanRes, checks);

	// evaluate isInf
	evaluateDimensionComparisons<false>(
		[](const Double3& value) -> bool {
			return vector3DIsInf(loadDouble3(value));
		},
		getExpectedResult, infRes, checks);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double3& value) -> bool {
			return vector3DIsInfRecord(record, loadDouble3(value));
		},
		getExpectedResult, infRes, checks);
}
void evalTwoArg3DDblComparisons()
{
	constexpr static const size_t arrSize = 6;

	using Tuple2Dbl = std::tuple<Double3, Double3>;
	using ArrayIn	= std::array<Tuple2Dbl, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResultAnd = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X & expRes.m_Components.m_Y
				& expRes.m_Components.m_Z & expRes.m_Components.m_W)
			   != 0;
	};
	auto getExpectedResultOr = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X | expRes.m_Components.m_Y
				| expRes.m_Components.m_Z | expRes.m_Components.m_W)
			   != 0;
	};

	// double comparison test data
	ArrayIn comparisons = ArrayIn{
		Tuple2Dbl{Double3{0.0, -0.0, 0.0}, Double3{0.0, 0.0, -0.0}},
		Tuple2Dbl{Double3{0.0, -1.0, -1.0}, Double3{1.0, 0.0, 1.0}},
		Tuple2Dbl{Double3{1.0, 1.0, 0.0}, Double3{-1.0, 0.0, -1.0}},
		Tuple2Dbl{Double3{g_MinDouble, g_QNaN, g_InfDouble},
				  Double3{0.0, -g_InfDouble, g_MaxDouble}},
		Tuple2Dbl{Double3{-g_MinDouble, g_MaxDouble, g_QNaN},
				  Double3{0.0, g_InfDouble, g_InfDouble}},
		Tuple2Dbl{Double3{g_MaxDouble, g_InfDouble, g_QNaN},
				  Double3{g_MaxDouble, g_InfDouble, g_QNaN}}};

	// double comparison results, per comparison operation
	//
	// For our checks to work, specifically on record functions,
	// expected results must replicate expected z coord across z and w
	// coord
	auto notEqRes =
			 ArrayResI{
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, g_TrueVal, g_TrueVal}},
		 grtrEqRes =
			 ArrayResI{
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, 0, 0}},
		 grtrRes =
			 ArrayResI{
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, 0, 0}},
		 eqRes = ArrayResI{Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
									g_TrueVal},
						   Integer4{0, 0, 0, 0},
						   Integer4{0, 0, 0, 0},
						   Integer4{0, 0, 0, 0},
						   Integer4{0, 0, 0, 0},
						   Integer4{g_TrueVal, g_TrueVal, 0, 0}},
		 lessEqRes =
			 ArrayResI{
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, 0, 0}},
		 lessRes = ArrayResI{
			 Integer4{0, 0, 0, 0},
			 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
			 Integer4{0, 0, 0, 0},
			 Integer4{0, 0, 0, 0},
			 Integer4{g_TrueVal, g_TrueVal, 0, 0},
			 Integer4{0, 0, 0, 0}};

	// evaluate Less
	evaluateDimensionComparisons<false>(
		[](const Double3& lhs, const Double3& rhs) -> bool {
			return vector3DLess(loadDouble3(lhs), loadDouble3(rhs));
		},
		getExpectedResultAnd, lessRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double3& lhs,
		   const Double3& rhs) -> bool {
			return vector3DLessRecord(record, loadDouble3(lhs),
									  loadDouble3(rhs));
		},
		getExpectedResultAnd, lessRes, comparisons);

	// evaluate LessEqual
	evaluateDimensionComparisons<false>(
		[](const Double3& lhs, const Double3& rhs) -> bool {
			return vector3DLessEqual(loadDouble3(lhs),
									 loadDouble3(rhs));
		},
		getExpectedResultAnd, lessEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double3& lhs,
		   const Double3& rhs) -> bool {
			return vector3DLessEqualRecord(record, loadDouble3(lhs),
										   loadDouble3(rhs));
		},
		getExpectedResultAnd, lessEqRes, comparisons);

	// evaluate Greater
	evaluateDimensionComparisons<false>(
		[](const Double3& lhs, const Double3& rhs) -> bool {
			return vector3DGreater(loadDouble3(lhs),
								   loadDouble3(rhs));
		},
		getExpectedResultAnd, grtrRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double3& lhs,
		   const Double3& rhs) -> bool {
			return vector3DGreaterRecord(record, loadDouble3(lhs),
										 loadDouble3(rhs));
		},
		getExpectedResultAnd, grtrRes, comparisons);

	// evaluate GreaterEqual
	evaluateDimensionComparisons<false>(
		[](const Double3& lhs, const Double3& rhs) -> bool {
			return vector3DGreaterEqual(loadDouble3(lhs),
										loadDouble3(rhs));
		},
		getExpectedResultAnd, grtrEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double3& lhs,
		   const Double3& rhs) -> bool {
			return vector3DGreaterEqualRecord(
				record, loadDouble3(lhs), loadDouble3(rhs));
		},
		getExpectedResultAnd, grtrEqRes, comparisons);

	// evaluate Equal
	evaluateDimensionComparisons<false>(
		[](const Double3& lhs, const Double3& rhs) -> bool {
			return vector3DEqual(loadDouble3(lhs), loadDouble3(rhs));
		},
		getExpectedResultAnd, eqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double3& lhs,
		   const Double3& rhs) -> bool {
			return vector3DEqualRecord(record, loadDouble3(lhs),
									   loadDouble3(rhs));
		},
		getExpectedResultAnd, eqRes, comparisons);

	// evaluate NotEqual
	evaluateDimensionComparisons<false>(
		[](const Double3& lhs, const Double3& rhs) -> bool {
			return vector3DNotEqual(loadDouble3(lhs),
									loadDouble3(rhs));
		},
		getExpectedResultOr, notEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double3& lhs,
		   const Double3& rhs) -> bool {
			return vector3DNotEqualRecord(record, loadDouble3(lhs),
										  loadDouble3(rhs));
		},
		getExpectedResultOr, notEqRes, comparisons);
}
void evalTwoArg3DIntComparisons()
{
	constexpr static const size_t arrSize = 6;

	using Tuple2Int = std::tuple<Integer3, Integer3>;
	using ArrayIn	= std::array<Tuple2Int, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResultAnd = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X & expRes.m_Components.m_Y
				& expRes.m_Components.m_Z & expRes.m_Components.m_W)
			   != 0;
	};
	auto getExpectedResultOr = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X | expRes.m_Components.m_Y
				| expRes.m_Components.m_Z | expRes.m_Components.m_W)
			   != 0;
	};

	// int comparison test data
	ArrayIn comparisons =
		ArrayIn{Tuple2Int{Integer3{0, 1, -1}, Integer3{0, 1, -1}},
				Tuple2Int{Integer3{g_MinInt, g_MaxInt, g_MinInt},
						  Integer3{g_MinInt, g_MaxInt, g_MaxInt}},
				Tuple2Int{Integer3{-1, g_MinInt, 0},
						  Integer3{0, 0, g_MaxInt}},
				Tuple2Int{Integer3{0, 1, g_MinInt},
						  Integer3{1, -1, g_MaxInt}},
				Tuple2Int{Integer3{1, 1, 0}, Integer3{-1, 0, -1}},
				Tuple2Int{Integer3{g_MaxInt, g_MaxInt, 0},
						  Integer3{g_MaxInt, g_MinInt, g_MinInt}}};

	// int comparison results, per comparison operation
	//
	// For our checks to work, specifically on record functions,
	// expected results must replicate expected z coord across z and w
	// coord
	auto notEqRes =
			 ArrayResI{
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, g_TrueVal, g_TrueVal, g_TrueVal}},
		 grtrEqRes =
			 ArrayResI{
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, 0, 0},
				 Integer4{0, 0, 0, 0},
				 Integer4{0, g_TrueVal, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
						  g_TrueVal}},
		 grtrRes =
			 ArrayResI{
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, 0, 0},
				 Integer4{0, g_TrueVal, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, g_TrueVal, g_TrueVal, g_TrueVal}},
		 eqRes = ArrayResI{Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
									g_TrueVal},
						   Integer4{g_TrueVal, g_TrueVal, 0, 0},
						   Integer4{0, 0, 0, 0},
						   Integer4{0, 0, 0, 0},
						   Integer4{0, 0, 0, 0},
						   Integer4{g_TrueVal, 0, 0, 0}},
		 lessEqRes =
			 ArrayResI{
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, 0, 0, 0}},
		 lessRes = ArrayResI{
			 Integer4{0, 0, 0, 0},
			 Integer4{0, 0, g_TrueVal, g_TrueVal},
			 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
			 Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
			 Integer4{0, 0, 0, 0},
			 Integer4{0, 0, 0, 0}};

	// evaluate Less
	evaluateDimensionComparisons<false>(
		[](const Integer3& lhs, const Integer3& rhs) -> bool {
			return vector3DLess(loadInteger3(lhs), loadInteger3(rhs));
		},
		getExpectedResultAnd, lessRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer3& lhs,
		   const Integer3& rhs) -> bool {
			return vector3DLessRecord(record, loadInteger3(lhs),
									  loadInteger3(rhs));
		},
		getExpectedResultAnd, lessRes, comparisons);

	// evaluate LessEqual
	evaluateDimensionComparisons<false>(
		[](const Integer3& lhs, const Integer3& rhs) -> bool {
			return vector3DLessEqual(loadInteger3(lhs),
									 loadInteger3(rhs));
		},
		getExpectedResultAnd, lessEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer3& lhs,
		   const Integer3& rhs) -> bool {
			return vector3DLessEqualRecord(record, loadInteger3(lhs),
										   loadInteger3(rhs));
		},
		getExpectedResultAnd, lessEqRes, comparisons);

	// evaluate Greater
	evaluateDimensionComparisons<false>(
		[](const Integer3& lhs, const Integer3& rhs) -> bool {
			return vector3DGreater(loadInteger3(lhs),
								   loadInteger3(rhs));
		},
		getExpectedResultAnd, grtrRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer3& lhs,
		   const Integer3& rhs) -> bool {
			return vector3DGreaterRecord(record, loadInteger3(lhs),
										 loadInteger3(rhs));
		},
		getExpectedResultAnd, grtrRes, comparisons);

	// evaluate GreaterEqual
	evaluateDimensionComparisons<false>(
		[](const Integer3& lhs, const Integer3& rhs) -> bool {
			return vector3DGreaterEqual(loadInteger3(lhs),
										loadInteger3(rhs));
		},
		getExpectedResultAnd, grtrEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer3& lhs,
		   const Integer3& rhs) -> bool {
			return vector3DGreaterEqualRecord(
				record, loadInteger3(lhs), loadInteger3(rhs));
		},
		getExpectedResultAnd, grtrEqRes, comparisons);

	// evaluate Equal
	evaluateDimensionComparisons<false>(
		[](const Integer3& lhs, const Integer3& rhs) -> bool {
			return vector3DEqual(loadInteger3(lhs),
								 loadInteger3(rhs));
		},
		getExpectedResultAnd, eqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer3& lhs,
		   const Integer3& rhs) -> bool {
			return vector3DEqualRecord(record, loadInteger3(lhs),
									   loadInteger3(rhs));
		},
		getExpectedResultAnd, eqRes, comparisons);

	// evaluate NotEqual
	evaluateDimensionComparisons<false>(
		[](const Integer3& lhs, const Integer3& rhs) -> bool {
			return vector3DNotEqual(loadInteger3(lhs),
									loadInteger3(rhs));
		},
		getExpectedResultOr, notEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer3& lhs,
		   const Integer3& rhs) -> bool {
			return vector3DNotEqualRecord(record, loadInteger3(lhs),
										  loadInteger3(rhs));
		},
		getExpectedResultOr, notEqRes, comparisons);
}
void evalThreeArg3DDblComparisons()
{
	constexpr static const size_t arrSize = 6;

	using Tuple3Dbl = std::tuple<Double3, Double3, Double3>;
	using ArrayIn	= std::array<Tuple3Dbl, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResult = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X & expRes.m_Components.m_Y
				& expRes.m_Components.m_Z & expRes.m_Components.m_W)
			   != 0;
	};

	// double comparison test data (checked, lower, upper)
	ArrayIn comparisons = ArrayIn{
		Tuple3Dbl{Double3{-1.0, 0.0, 1.0}, Double3{-1.0, -1.0, -1.0},
				  Double3{1.0, 1.0, 1.0}},
		Tuple3Dbl{
			Double3{-1.0 + g_Epsilon, -g_InfDouble, -g_MillionDouble},
			Double3{-1.0, -1.0, -1.0}, Double3{1.0, 1.0, 1.0}},
		Tuple3Dbl{
			Double3{1.0 - g_Epsilon, g_InfDouble, g_MillionDouble},
			Double3{-1.0, -1.0, -1.0}, Double3{1.0, 1.0, 1.0}},
		Tuple3Dbl{Double3{g_MillionDouble, -g_MillionDouble,
						  g_MillionDouble * (1.0 + g_Epsilon)},
				  Double3{-g_MillionDouble, -g_MillionDouble,
						  -g_MillionDouble},
				  Double3{g_MillionDouble, g_MillionDouble,
						  g_MillionDouble}},
		Tuple3Dbl{Double3{0.0, g_InfDouble,
						  g_MillionDouble * (-1.0 + g_Epsilon)},
				  Double3{-g_MillionDouble, -g_MillionDouble,
						  -g_MillionDouble},
				  Double3{g_MillionDouble, g_MillionDouble,
						  g_MillionDouble}},
		Tuple3Dbl{Double3{g_QNaN, -g_InfDouble, 0.0},
				  Double3{-g_InfDouble, -g_InfDouble, -g_InfDouble},
				  Double3{g_InfDouble, g_InfDouble, g_InfDouble}}};

	// double comparison results
	//
	// For our checks to work, specifically on record functions,
	// expected results must replicate expected z coord across z and w
	// coord
	ArrayResI boundsRes = ArrayResI{
		Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
		Integer4{g_TrueVal, 0, 0, 0},
		Integer4{g_TrueVal, 0, 0, 0},
		Integer4{g_TrueVal, g_TrueVal, 0, 0},
		Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
		Integer4{0, g_TrueVal, g_TrueVal, g_TrueVal}};

	// evaluate InBounds
	evaluateDimensionComparisons<false>(
		[](const Double3& checked, const Double3& lower,
		   const Double3& upper) -> bool {
			return vector3DInBounds(loadDouble3(checked),
									loadDouble3(lower),
									loadDouble3(upper));
		},
		getExpectedResult, boundsRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double3& checked,
		   const Double3& lower, const Double3& upper) -> bool {
			return vector3DInBoundsRecord(
				record, loadDouble3(checked), loadDouble3(lower),
				loadDouble3(upper));
		},
		getExpectedResult, boundsRes, comparisons);
}
void evalThreeArg3DIntComparisons()
{
	constexpr static const size_t arrSize = 4;

	using Tuple3Int = std::tuple<Integer3, Integer3, Integer3>;
	using ArrayIn	= std::array<Tuple3Int, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResult = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X & expRes.m_Components.m_Y
				& expRes.m_Components.m_Z & expRes.m_Components.m_W)
			   != 0;
	};

	// int comparison test data (checked, lower, upper)
	ArrayIn comparisons = ArrayIn{
		Tuple3Int{Integer3{0, g_MinInt, g_MaxInt},
				  Integer3{g_MinInt, g_MinInt, g_MinInt},
				  Integer3{g_MaxInt, g_MaxInt, g_MaxInt}},
		Tuple3Int{Integer3{1, 1'000'000, g_MaxInt},
				  Integer3{-1'000'000, -1'000'000, -1'000'000},
				  Integer3{1'000'000, 1'000'000, 1'000'000}},
		Tuple3Int{Integer3{-1'000'000, g_MinInt, -1},
				  Integer3{-1'000'000, -1'000'000, -1'000'000},
				  Integer3{1'000'000, 1'000'000, 1'000'000}},
		Tuple3Int{Integer3{2'000'000, 560, -13'678},
				  Integer3{-1'000'000, -1'000'000, -1'000'000},
				  Integer3{1'000'000, 1'000'000, 1'000'000}}};

	// double comparison results
	//
	// For our checks to work, specifically on record functions,
	// expected results must replicate expected z coord across z and w
	// coord
	ArrayResI boundsRes = ArrayResI{
		Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
		Integer4{g_TrueVal, g_TrueVal, 0, 0},
		Integer4{g_TrueVal, 0, g_TrueVal, g_TrueVal},
		Integer4{0, g_TrueVal, g_TrueVal, g_TrueVal}};

	// evaluate InBounds
	evaluateDimensionComparisons<false>(
		[](const Integer3& checked, const Integer3& lower,
		   const Integer3& upper) -> bool {
			return vector3DInBounds(loadInteger3(checked),
									loadInteger3(lower),
									loadInteger3(upper));
		},
		getExpectedResult, boundsRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer3& checked,
		   const Integer3& lower, const Integer3& upper) -> bool {
			return vector3DInBoundsRecord(
				record, loadInteger3(checked), loadInteger3(lower),
				loadInteger3(upper));
		},
		getExpectedResult, boundsRes, comparisons);
}

void evalOneArg2DComparisons()
{
	constexpr static const size_t arrSize = 5;

	using Tuple1Dbl = std::tuple<Double2>;
	using ArrayIn	= std::array<Tuple1Dbl, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResult = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X | expRes.m_Components.m_Y
				| expRes.m_Components.m_Z | expRes.m_Components.m_W)
			   != 0;
	};

	// double comparison test data
	ArrayIn checks = ArrayIn{
		Double2{g_MinDouble, g_MaxDouble}, Double2{g_QNaN, g_QNaN},
		Double2{g_InfDouble, -g_InfDouble},
		Double2{g_InfDouble, g_QNaN}, Double2{g_QNaN, g_InfDouble}};

	// double comparison results, per comparison operation
	auto nanRes = ArrayResI{Integer4{0, 0, 0, 0},
							Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
									 g_TrueVal},
							Integer4{0, 0, 0, 0},
							Integer4{0, g_TrueVal, 0, g_TrueVal},
							Integer4{g_TrueVal, 0, g_TrueVal, 0}},
		 infRes = ArrayResI{
			 Integer4{0, 0, 0, 0}, Integer4{0, 0, 0, 0},
			 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
			 Integer4{g_TrueVal, 0, g_TrueVal, 0},
			 Integer4{0, g_TrueVal, 0, g_TrueVal}};

	// evaluate isNaN
	evaluateDimensionComparisons<false>(
		[](const Double2& value) -> bool {
			return vector2DIsNaN(loadDouble2(value));
		},
		getExpectedResult, nanRes, checks);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double2& value) -> bool {
			return vector2DIsNaNRecord(record, loadDouble2(value));
		},
		getExpectedResult, nanRes, checks);

	// evaluate isInf
	evaluateDimensionComparisons<false>(
		[](const Double2& value) -> bool {
			return vector2DIsInf(loadDouble2(value));
		},
		getExpectedResult, infRes, checks);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double2& value) -> bool {
			return vector2DIsInfRecord(record, loadDouble2(value));
		},
		getExpectedResult, infRes, checks);
}
void evalTwoArg2DDblComparisons()
{
	constexpr static const size_t arrSize = 6;

	using Tuple2Dbl = std::tuple<Double2, Double2>;
	using ArrayIn	= std::array<Tuple2Dbl, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResultAnd = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X & expRes.m_Components.m_Y
				& expRes.m_Components.m_Z & expRes.m_Components.m_W)
			   != 0;
	};
	auto getExpectedResultOr = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X | expRes.m_Components.m_Y
				| expRes.m_Components.m_Z | expRes.m_Components.m_W)
			   != 0;
	};

	// double comparison test data
	ArrayIn comparisons =
		ArrayIn{Tuple2Dbl{Double2{-0.0, 0.0}, Double2{0.0, -0.0}},
				Tuple2Dbl{Double2{0.0, -1.0}, Double2{1.0, 0.0}},
				Tuple2Dbl{Double2{1.0, 1.0}, Double2{-1.0, 0.0}},
				Tuple2Dbl{Double2{g_QNaN, g_InfDouble},
						  Double2{-g_InfDouble, g_MaxDouble}},
				Tuple2Dbl{Double2{-g_MinDouble, g_QNaN},
						  Double2{0.0, g_InfDouble}},
				Tuple2Dbl{Double2{g_MaxDouble, g_QNaN},
						  Double2{g_MaxDouble, g_QNaN}}};

	// double comparison results, per comparison operation
	//
	// For our checks to work, specifically on record functions,
	// expected results must replicate expected x,y coords across z
	// and w coords
	auto notEqRes =
			 ArrayResI{
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, g_TrueVal, 0, g_TrueVal}},
		 grtrEqRes =
			 ArrayResI{
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, g_TrueVal, 0, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, 0, g_TrueVal, 0}},
		 grtrRes = ArrayResI{Integer4{0, 0, 0, 0},
							 Integer4{0, 0, 0, 0},
							 Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
									  g_TrueVal},
							 Integer4{0, g_TrueVal, 0, g_TrueVal},
							 Integer4{0, 0, 0, 0},
							 Integer4{0, 0, 0, 0}},
		 eqRes	 = ArrayResI{Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
									  g_TrueVal},
							 Integer4{0, 0, 0, 0},
							 Integer4{0, 0, 0, 0},
							 Integer4{0, 0, 0, 0},
							 Integer4{0, 0, 0, 0},
							 Integer4{g_TrueVal, 0, g_TrueVal, 0}},
		 lessEqRes =
			 ArrayResI{
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, 0, 0, 0},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, 0, g_TrueVal, 0},
				 Integer4{g_TrueVal, 0, g_TrueVal, 0}},
		 lessRes = ArrayResI{
			 Integer4{0, 0, 0, 0},
			 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
			 Integer4{0, 0, 0, 0},
			 Integer4{0, 0, 0, 0},
			 Integer4{g_TrueVal, 0, g_TrueVal, 0},
			 Integer4{0, 0, 0, 0}};

	// evaluate Less
	evaluateDimensionComparisons<false>(
		[](const Double2& lhs, const Double2& rhs) -> bool {
			return vector2DLess(loadDouble2(lhs), loadDouble2(rhs));
		},
		getExpectedResultAnd, lessRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double2& lhs,
		   const Double2& rhs) -> bool {
			return vector2DLessRecord(record, loadDouble2(lhs),
									  loadDouble2(rhs));
		},
		getExpectedResultAnd, lessRes, comparisons);

	// evaluate LessEqual
	evaluateDimensionComparisons<false>(
		[](const Double2& lhs, const Double2& rhs) -> bool {
			return vector2DLessEqual(loadDouble2(lhs),
									 loadDouble2(rhs));
		},
		getExpectedResultAnd, lessEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double2& lhs,
		   const Double2& rhs) -> bool {
			return vector2DLessEqualRecord(record, loadDouble2(lhs),
										   loadDouble2(rhs));
		},
		getExpectedResultAnd, lessEqRes, comparisons);

	// evaluate Greater
	evaluateDimensionComparisons<false>(
		[](const Double2& lhs, const Double2& rhs) -> bool {
			return vector2DGreater(loadDouble2(lhs),
								   loadDouble2(rhs));
		},
		getExpectedResultAnd, grtrRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double2& lhs,
		   const Double2& rhs) -> bool {
			return vector2DGreaterRecord(record, loadDouble2(lhs),
										 loadDouble2(rhs));
		},
		getExpectedResultAnd, grtrRes, comparisons);

	// evaluate GreaterEqual
	evaluateDimensionComparisons<false>(
		[](const Double2& lhs, const Double2& rhs) -> bool {
			return vector2DGreaterEqual(loadDouble2(lhs),
										loadDouble2(rhs));
		},
		getExpectedResultAnd, grtrEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double2& lhs,
		   const Double2& rhs) -> bool {
			return vector2DGreaterEqualRecord(
				record, loadDouble2(lhs), loadDouble2(rhs));
		},
		getExpectedResultAnd, grtrEqRes, comparisons);

	// evaluate Equal
	evaluateDimensionComparisons<false>(
		[](const Double2& lhs, const Double2& rhs) -> bool {
			return vector2DEqual(loadDouble2(lhs), loadDouble2(rhs));
		},
		getExpectedResultAnd, eqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double2& lhs,
		   const Double2& rhs) -> bool {
			return vector2DEqualRecord(record, loadDouble2(lhs),
									   loadDouble2(rhs));
		},
		getExpectedResultAnd, eqRes, comparisons);

	// evaluate NotEqual
	evaluateDimensionComparisons<false>(
		[](const Double2& lhs, const Double2& rhs) -> bool {
			return vector2DNotEqual(loadDouble2(lhs),
									loadDouble2(rhs));
		},
		getExpectedResultOr, notEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double2& lhs,
		   const Double2& rhs) -> bool {
			return vector2DNotEqualRecord(record, loadDouble2(lhs),
										  loadDouble2(rhs));
		},
		getExpectedResultOr, notEqRes, comparisons);
}
void evalTwoArg2DIntComparisons()
{
	constexpr static const size_t arrSize = 6;

	using Tuple2Int = std::tuple<Integer2, Integer2>;
	using ArrayIn	= std::array<Tuple2Int, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResultAnd = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X & expRes.m_Components.m_Y
				& expRes.m_Components.m_Z & expRes.m_Components.m_W)
			   != 0;
	};
	auto getExpectedResultOr = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X | expRes.m_Components.m_Y
				| expRes.m_Components.m_Z | expRes.m_Components.m_W)
			   != 0;
	};

	// int comparison test data
	ArrayIn comparisons =
		ArrayIn{Tuple2Int{Integer2{1, -1}, Integer2{1, -1}},
				Tuple2Int{Integer2{g_MinInt, g_MinInt},
						  Integer2{g_MinInt, g_MaxInt}},
				Tuple2Int{Integer2{-1, 0}, Integer2{0, g_MaxInt}},
				Tuple2Int{Integer2{0, 1}, Integer2{1, -1}},
				Tuple2Int{Integer2{1, 1}, Integer2{-1, 0}},
				Tuple2Int{Integer2{g_MaxInt, 0},
						  Integer2{g_MaxInt, g_MinInt}}};

	// int comparison results, per comparison operation
	//
	// For our checks to work, specifically on record functions,
	// expected results must replicate expected x,y coords across z
	// and w coords
	auto notEqRes =
			 ArrayResI{
				 Integer4{0, 0, 0, 0},
				 Integer4{0, g_TrueVal, 0, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{0, g_TrueVal, 0, g_TrueVal}},
		 grtrEqRes =
			 ArrayResI{
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, 0, g_TrueVal, 0},
				 Integer4{0, 0, 0, 0},
				 Integer4{0, g_TrueVal, 0, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
						  g_TrueVal}},
		 grtrRes = ArrayResI{Integer4{0, 0, 0, 0},
							 Integer4{0, 0, 0, 0},
							 Integer4{0, 0, 0, 0},
							 Integer4{0, g_TrueVal, 0, g_TrueVal},
							 Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
									  g_TrueVal},
							 Integer4{0, g_TrueVal, 0, g_TrueVal}},
		 eqRes	 = ArrayResI{Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
									  g_TrueVal},
							 Integer4{g_TrueVal, 0, g_TrueVal, 0},
							 Integer4{0, 0, 0, 0},
							 Integer4{0, 0, 0, 0},
							 Integer4{0, 0, 0, 0},
							 Integer4{g_TrueVal, 0, g_TrueVal, 0}},
		 lessEqRes =
			 ArrayResI{
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
				 Integer4{g_TrueVal, 0, g_TrueVal, 0},
				 Integer4{0, 0, 0, 0},
				 Integer4{g_TrueVal, 0, g_TrueVal, 0}},
		 lessRes = ArrayResI{
			 Integer4{0, 0, 0, 0},
			 Integer4{0, g_TrueVal, 0, g_TrueVal},
			 Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
			 Integer4{g_TrueVal, 0, g_TrueVal, 0},
			 Integer4{0, 0, 0, 0},
			 Integer4{0, 0, 0, 0}};

	// evaluate Less
	evaluateDimensionComparisons<false>(
		[](const Integer2& lhs, const Integer2& rhs) -> bool {
			return vector2DLess(loadInteger2(lhs), loadInteger2(rhs));
		},
		getExpectedResultAnd, lessRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer2& lhs,
		   const Integer2& rhs) -> bool {
			return vector2DLessRecord(record, loadInteger2(lhs),
									  loadInteger2(rhs));
		},
		getExpectedResultAnd, lessRes, comparisons);

	// evaluate LessEqual
	evaluateDimensionComparisons<false>(
		[](const Integer2& lhs, const Integer2& rhs) -> bool {
			return vector2DLessEqual(loadInteger2(lhs),
									 loadInteger2(rhs));
		},
		getExpectedResultAnd, lessEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer2& lhs,
		   const Integer2& rhs) -> bool {
			return vector2DLessEqualRecord(record, loadInteger2(lhs),
										   loadInteger2(rhs));
		},
		getExpectedResultAnd, lessEqRes, comparisons);

	// evaluate Greater
	evaluateDimensionComparisons<false>(
		[](const Integer2& lhs, const Integer2& rhs) -> bool {
			return vector2DGreater(loadInteger2(lhs),
								   loadInteger2(rhs));
		},
		getExpectedResultAnd, grtrRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer2& lhs,
		   const Integer2& rhs) -> bool {
			return vector2DGreaterRecord(record, loadInteger2(lhs),
										 loadInteger2(rhs));
		},
		getExpectedResultAnd, grtrRes, comparisons);

	// evaluate GreaterEqual
	evaluateDimensionComparisons<false>(
		[](const Integer2& lhs, const Integer2& rhs) -> bool {
			return vector2DGreaterEqual(loadInteger2(lhs),
										loadInteger2(rhs));
		},
		getExpectedResultAnd, grtrEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer2& lhs,
		   const Integer2& rhs) -> bool {
			return vector2DGreaterEqualRecord(
				record, loadInteger2(lhs), loadInteger2(rhs));
		},
		getExpectedResultAnd, grtrEqRes, comparisons);

	// evaluate Equal
	evaluateDimensionComparisons<false>(
		[](const Integer2& lhs, const Integer2& rhs) -> bool {
			return vector2DEqual(loadInteger2(lhs),
								 loadInteger2(rhs));
		},
		getExpectedResultAnd, eqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer2& lhs,
		   const Integer2& rhs) -> bool {
			return vector2DEqualRecord(record, loadInteger2(lhs),
									   loadInteger2(rhs));
		},
		getExpectedResultAnd, eqRes, comparisons);

	// evaluate NotEqual
	evaluateDimensionComparisons<false>(
		[](const Integer2& lhs, const Integer2& rhs) -> bool {
			return vector2DNotEqual(loadInteger2(lhs),
									loadInteger2(rhs));
		},
		getExpectedResultOr, notEqRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer2& lhs,
		   const Integer2& rhs) -> bool {
			return vector2DNotEqualRecord(record, loadInteger2(lhs),
										  loadInteger2(rhs));
		},
		getExpectedResultOr, notEqRes, comparisons);
}
void evalThreeArg2DDblComparisons()
{
	constexpr static const size_t arrSize = 6;

	using Tuple3Dbl = std::tuple<Double2, Double2, Double2>;
	using ArrayIn	= std::array<Tuple3Dbl, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResult = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X & expRes.m_Components.m_Y
				& expRes.m_Components.m_Z & expRes.m_Components.m_W)
			   != 0;
	};

	// double comparison test data (checked, lower, upper)
	ArrayIn comparisons = ArrayIn{
		Tuple3Dbl{Double2{-1.0, 1.0}, Double2{-1.0, -1.0},
				  Double2{1.0, 1.0}},
		Tuple3Dbl{Double2{-1.0 + g_Epsilon, -g_InfDouble},
				  Double2{-1.0, -1.0}, Double2{1.0, 1.0}},
		Tuple3Dbl{Double2{1.0 - g_Epsilon, g_InfDouble},
				  Double2{-1.0, -1.0}, Double2{1.0, 1.0}},
		Tuple3Dbl{Double2{-g_MillionDouble,
						  g_MillionDouble * (1.0 + g_Epsilon)},
				  Double2{-g_MillionDouble, -g_MillionDouble},
				  Double2{g_MillionDouble, g_MillionDouble}},
		Tuple3Dbl{Double2{g_InfDouble,
						  g_MillionDouble * (-1.0 + g_Epsilon)},
				  Double2{-g_MillionDouble, -g_MillionDouble},
				  Double2{g_MillionDouble, g_MillionDouble}},
		Tuple3Dbl{Double2{g_QNaN, -g_InfDouble},
				  Double2{-g_InfDouble, -g_InfDouble},
				  Double2{g_InfDouble, g_InfDouble}}};

	// double comparison results
	//
	// For our checks to work, specifically on record functions,
	// expected results must replicate expected x,y coords across z
	// and w coords
	ArrayResI boundsRes = ArrayResI{
		Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
		Integer4{g_TrueVal, 0, g_TrueVal, 0},
		Integer4{g_TrueVal, 0, g_TrueVal, 0},
		Integer4{g_TrueVal, 0, g_TrueVal, 0},
		Integer4{0, g_TrueVal, 0, g_TrueVal},
		Integer4{0, g_TrueVal, 0, g_TrueVal}};

	// evaluate InBounds
	evaluateDimensionComparisons<false>(
		[](const Double2& checked, const Double2& lower,
		   const Double2& upper) -> bool {
			return vector2DInBounds(loadDouble2(checked),
									loadDouble2(lower),
									loadDouble2(upper));
		},
		getExpectedResult, boundsRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Double2& checked,
		   const Double2& lower, const Double2& upper) -> bool {
			return vector2DInBoundsRecord(
				record, loadDouble2(checked), loadDouble2(lower),
				loadDouble2(upper));
		},
		getExpectedResult, boundsRes, comparisons);
}
void evalThreeArg2DIntComparisons()
{
	constexpr static const size_t arrSize = 4;

	using Tuple3Int = std::tuple<Integer2, Integer2, Integer2>;
	using ArrayIn	= std::array<Tuple3Int, arrSize>;
	using ArrayResI = std::array<Integer4, arrSize>;

	auto getExpectedResult = [](const Integer4& expRes) -> bool {
		return (expRes.m_Components.m_X & expRes.m_Components.m_Y
				& expRes.m_Components.m_Z & expRes.m_Components.m_W)
			   != 0;
	};

	// int comparison test data (checked, lower, upper)
	ArrayIn comparisons =
		ArrayIn{Tuple3Int{Integer2{g_MinInt, g_MaxInt},
						  Integer2{g_MinInt, g_MinInt},
						  Integer2{g_MaxInt, g_MaxInt}},
				Tuple3Int{Integer2{1'000'000, g_MaxInt},
						  Integer2{-1'000'000, -1'000'000},
						  Integer2{1'000'000, 1'000'000}},
				Tuple3Int{Integer2{-1'000'000, g_MinInt},
						  Integer2{-1'000'000, -1'000'000},
						  Integer2{1'000'000, 1'000'000}},
				Tuple3Int{Integer2{2'000'000, -13'678},
						  Integer2{-1'000'000, -1'000'000},
						  Integer2{1'000'000, 1'000'000}}};

	// double comparison results
	//
	// For our checks to work, specifically on record functions,
	// expected results must replicate expected x,y coords across z
	// and w coords
	ArrayResI boundsRes = ArrayResI{
		Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
		Integer4{g_TrueVal, 0, g_TrueVal, 0},
		Integer4{g_TrueVal, 0, g_TrueVal, 0},
		Integer4{0, g_TrueVal, 0, g_TrueVal}};

	// evaluate InBounds
	evaluateDimensionComparisons<false>(
		[](const Integer2& checked, const Integer2& lower,
		   const Integer2& upper) -> bool {
			return vector2DInBounds(loadInteger2(checked),
									loadInteger2(lower),
									loadInteger2(upper));
		},
		getExpectedResult, boundsRes, comparisons);
	evaluateDimensionComparisons<true>(
		[](uint_least64_t& record, const Integer2& checked,
		   const Integer2& lower, const Integer2& upper) -> bool {
			return vector2DInBoundsRecord(
				record, loadInteger2(checked), loadInteger2(lower),
				loadInteger2(upper));
		},
		getExpectedResult, boundsRes, comparisons);
}

void evaluateRecordEval()
{
	uint_least64_t testRecords[6] = {
		convertToRecord(Integer4{0, 0, 0, 0}),
		convertToRecord(Integer4{g_TrueVal, g_TrueVal, 0, 0}),
		convertToRecord(Integer4{0, g_TrueVal, g_TrueVal, 0}),
		convertToRecord(Integer4{g_TrueVal, 0, 0, g_TrueVal}),
		convertToRecord(Integer4{0, 0, g_TrueVal, g_TrueVal}),
		convertToRecord(
			Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal})};

	// validate comparisonAllTrue
	CHECK_EQ(comparisonAllTrue(testRecords[0]), false);
	CHECK_EQ(comparisonAllTrue(testRecords[1]), false);
	CHECK_EQ(comparisonAllTrue(testRecords[2]), false);
	CHECK_EQ(comparisonAllTrue(testRecords[3]), false);
	CHECK_EQ(comparisonAllTrue(testRecords[4]), false);
	CHECK_EQ(comparisonAllTrue(testRecords[5]), true);

	// validate comparisonAnyTrue
	CHECK_EQ(comparisonAnyTrue(testRecords[0]), false);
	CHECK_EQ(comparisonAnyTrue(testRecords[1]), true);
	CHECK_EQ(comparisonAnyTrue(testRecords[2]), true);
	CHECK_EQ(comparisonAnyTrue(testRecords[3]), true);
	CHECK_EQ(comparisonAnyTrue(testRecords[4]), true);
	CHECK_EQ(comparisonAnyTrue(testRecords[5]), true);

	// validate comparisonAllFalse
	CHECK_EQ(comparisonAllFalse(testRecords[0]), true);
	CHECK_EQ(comparisonAllFalse(testRecords[1]), false);
	CHECK_EQ(comparisonAllFalse(testRecords[2]), false);
	CHECK_EQ(comparisonAllFalse(testRecords[3]), false);
	CHECK_EQ(comparisonAllFalse(testRecords[4]), false);
	CHECK_EQ(comparisonAllFalse(testRecords[5]), false);

	// validate comparisonAnyFalse
	CHECK_EQ(comparisonAnyFalse(testRecords[0]), true);
	CHECK_EQ(comparisonAnyFalse(testRecords[1]), true);
	CHECK_EQ(comparisonAnyFalse(testRecords[2]), true);
	CHECK_EQ(comparisonAnyFalse(testRecords[3]), true);
	CHECK_EQ(comparisonAnyFalse(testRecords[4]), true);
	CHECK_EQ(comparisonAnyFalse(testRecords[5]), false);

	// validate comparisonMixedResult
	CHECK_EQ(comparisonMixedResult(testRecords[0]), false);
	CHECK_EQ(comparisonMixedResult(testRecords[1]), true);
	CHECK_EQ(comparisonMixedResult(testRecords[2]), true);
	CHECK_EQ(comparisonMixedResult(testRecords[3]), true);
	CHECK_EQ(comparisonMixedResult(testRecords[4]), true);
	CHECK_EQ(comparisonMixedResult(testRecords[5]), false);
}

void evaluateConstantVectors()
{
	Integer4 nextIntRes;

	// test vectorZeroInt()
	storeInteger4(nextIntRes, vectorZeroInt());
	checkStoreEq(nextIntRes, Integer4{0, 0, 0, 0});

	// test vectorSplatAllBits()
	storeInteger4(nextIntRes, vectorSplatAllBits());
	checkStoreEq(nextIntRes, Integer4{g_TrueVal, g_TrueVal, g_TrueVal,
									  g_TrueVal});

	// test vectorSplatOneInt()
	storeInteger4(nextIntRes, vectorSplatOneInt());
	checkStoreEq(nextIntRes, Integer4{1, 1, 1, 1});

	Double4 nextRes;

	// test vectorZero()
	storeDouble4(nextRes, vectorZero());
	checkStoreEq(nextRes, Double4{0.0, 0.0, 0.0, 0.0});

	// test vectorSplatOne()
	storeDouble4(nextRes, vectorSplatOne());
	checkStoreEq(nextRes, Double4{1.0, 1.0, 1.0, 1.0});

	// test vectorSplatQNaN(), by ensuring it's unequal to itself
	storeDouble4(nextRes, vectorSplatQNaN());
	CHECK_NE(nextRes.m_Components.m_X, nextRes.m_Components.m_X);
	CHECK_NE(nextRes.m_Components.m_Y, nextRes.m_Components.m_Y);
	CHECK_NE(nextRes.m_Components.m_Z, nextRes.m_Components.m_Z);
	CHECK_NE(nextRes.m_Components.m_W, nextRes.m_Components.m_W);

	// test vectorSplatSNaN(), by ensuring it's unequal to itself
	storeDouble4(nextRes, vectorSplatSNaN());
	CHECK_NE(nextRes.m_Components.m_X, nextRes.m_Components.m_X);
	CHECK_NE(nextRes.m_Components.m_Y, nextRes.m_Components.m_Y);
	CHECK_NE(nextRes.m_Components.m_Z, nextRes.m_Components.m_Z);
	CHECK_NE(nextRes.m_Components.m_W, nextRes.m_Components.m_W);

	// test vectorSplatInfinity()
	storeDouble4(nextRes, vectorSplatInfinity());
	checkStoreEq(nextRes, Double4{g_InfDouble, g_InfDouble,
								  g_InfDouble, g_InfDouble});

	// test vectorSplatEpsilon(), by checking that it's the smallest
	// value, that when added to 1.0, results in a differnt value
	storeDouble4(nextRes, vectorSplatEpsilon());
	CHECK_NE(1.0 + nextRes.m_Components.m_X, 1.0);
	CHECK_NE(1.0 + nextRes.m_Components.m_Y, 1.0);
	CHECK_NE(1.0 + nextRes.m_Components.m_Z, 1.0);
	CHECK_NE(1.0 + nextRes.m_Components.m_W, 1.0);
	nextRes.m_Components.m_X	 = nextRes.m_Components.m_Y =
		nextRes.m_Components.m_Z = nextRes.m_Components.m_W =
			(nextRes.m_Components.m_X / 2.0);
	CHECK_EQ(1.0 + nextRes.m_Components.m_X, 1.0);
	CHECK_EQ(1.0 + nextRes.m_Components.m_Y, 1.0);
	CHECK_EQ(1.0 + nextRes.m_Components.m_Z, 1.0);
	CHECK_EQ(1.0 + nextRes.m_Components.m_W, 1.0);
}

void evalSmallOneArgBitFunctions()
{
	constexpr static const int_least32_t s_MinSmallInt =
		std::numeric_limits<int_least32_t>::min();
	constexpr static const int_least32_t s_MaxSmallInt =
		std::numeric_limits<int_least32_t>::max();

	constexpr static const size_t arrSize = 2;

	// int typedefs
	using TupleInt = std::tuple<SmallInt4>;
	using ArrayIn  = std::array<TupleInt, arrSize>;
	using ArrayRes = std::array<SmallInt4, arrSize>;

	// raw converted data
	int_least32_t intChecks[8] = {0,
								  -1,
								  1,
								  -1'000'000,
								  1'000'000,
								  -252'702'961,
								  s_MaxSmallInt,
								  s_MinSmallInt},
				  intRes[8]	   = {~(intChecks[0]), ~(intChecks[1]),
								  ~(intChecks[2]), ~(intChecks[3]),
								  ~(intChecks[4]), ~(intChecks[5]),
								  ~(intChecks[6]), ~(intChecks[7])};

	ArrayIn data =
		ArrayIn{TupleInt{SmallInt4{intChecks[0], intChecks[1],
								   intChecks[2], intChecks[3]}},
				TupleInt{SmallInt4{intChecks[4], intChecks[5],
								   intChecks[6], intChecks[7]}}};
	auto results = ArrayRes{
		SmallInt4{intRes[0], intRes[1], intRes[2], intRes[3]},
		SmallInt4{intRes[4], intRes[5], intRes[6], intRes[7]}};

	// eval small int not
	callEvalFunc(
		[](const SmallInt4& expectRes,
		   const SmallInt4& value) -> void {
			SmallInt4 result;
			storeSmallInt4(result, vectorNot(loadSmallInt4(value)));

			checkStoreEq(expectRes, result);
		},
		results, data);
}
void evalOneArgBitFunctions()
{
	constexpr static const size_t arrSize = 2;

	// int typedefs
	using TupleInt = std::tuple<Integer4>;
	using ArrayIn  = std::array<TupleInt, arrSize>;
	using ArrayRes = std::array<Integer4, arrSize>;

	// raw converted data
	int_least64_t intChecks[8] = {0,		 -1,
								  1,		 -1'000'000,
								  1'000'000, -71'792'680'202'469'121,
								  g_MaxInt,	 g_MinInt},
				  intRes[8]	   = {~(intChecks[0]), ~(intChecks[1]),
								  ~(intChecks[2]), ~(intChecks[3]),
								  ~(intChecks[4]), ~(intChecks[5]),
								  ~(intChecks[6]), ~(intChecks[7])};

	ArrayIn data =
		ArrayIn{TupleInt{Integer4{intChecks[0], intChecks[1],
								  intChecks[2], intChecks[3]}},
				TupleInt{Integer4{intChecks[4], intChecks[5],
								  intChecks[6], intChecks[7]}}};
	auto results = ArrayRes{
		Integer4{intRes[0], intRes[1], intRes[2], intRes[3]},
		Integer4{intRes[4], intRes[5], intRes[6], intRes[7]}};

	// eval int not
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& value) -> void {
			Integer4 result;
			storeInteger4(result, vectorNot(loadInteger4(value)));

			checkStoreEq(expectRes, result);
		},
		results, data);
}
void evalSmallTwoArgBitFunctions()
{
	constexpr static const int_least32_t s_MinSmallInt =
		std::numeric_limits<int_least32_t>::min();
	constexpr static const int_least32_t s_MaxSmallInt =
		std::numeric_limits<int_least32_t>::max();

	constexpr static const size_t arrSize = 2;

	using TupleInt = std::tuple<SmallInt4, SmallInt4>;
	using ArrayIn  = std::array<TupleInt, arrSize>;
	using ArrayRes = std::array<SmallInt4, arrSize>;

	// small int comparison test data
	ArrayIn compares = ArrayIn{
		TupleInt{SmallInt4{-1, -1, -1, 1},
				 SmallInt4{-1, s_MaxSmallInt, 252'702'960, -1}},
		TupleInt{
			SmallInt4{-252'702'961, -252'702'961, s_MinSmallInt,
					  s_MaxSmallInt},
			SmallInt4{-252'702'961, 252'702'960, -1, s_MinSmallInt}}};

	// small int comparison results, per comparison operation
	auto andRes =
			 ArrayRes{SmallInt4{-1, s_MaxSmallInt, 252'702'960, 1},
					  SmallInt4{-252'702'961, 0, s_MinSmallInt, 0}},
		 orRes = ArrayRes{SmallInt4{-1, -1, -1, -1},
						  SmallInt4{-252'702'961, -1, -1, -1}},
		 xorRes =
			 ArrayRes{SmallInt4{0, s_MinSmallInt, -252'702'961, -2},
					  SmallInt4{0, -1, s_MaxSmallInt, -1}};

	// eval and
	callEvalFunc(
		[](const SmallInt4& expectRes, const SmallInt4& lhs,
		   const SmallInt4& rhs) -> void {
			SmallInt4 result;
			storeSmallInt4(result, vectorAnd(loadSmallInt4(lhs),
											 loadSmallInt4(rhs)));

			checkStoreEq(expectRes, result);
		},
		andRes, compares);

	// eval or
	callEvalFunc(
		[](const SmallInt4& expectRes, const SmallInt4& lhs,
		   const SmallInt4& rhs) -> void {
			SmallInt4 result;
			storeSmallInt4(result, vectorOr(loadSmallInt4(lhs),
											loadSmallInt4(rhs)));

			checkStoreEq(expectRes, result);
		},
		orRes, compares);

	// eval xor
	callEvalFunc(
		[](const SmallInt4& expectRes, const SmallInt4& lhs,
		   const SmallInt4& rhs) -> void {
			SmallInt4 result;
			storeSmallInt4(result, vectorXor(loadSmallInt4(lhs),
											 loadSmallInt4(rhs)));

			checkStoreEq(expectRes, result);
		},
		xorRes, compares);
}
void evalTwoArgBitFunctions()
{
	constexpr static const size_t arrSize = 2;

	using TupleInt = std::tuple<Integer4, Integer4>;
	using ArrayIn  = std::array<TupleInt, arrSize>;
	using ArrayRes = std::array<Integer4, arrSize>;

	// int comparison test data
	ArrayIn compares = ArrayIn{
		TupleInt{Integer4{-1, -1, -1, 1},
				 Integer4{-1, g_MaxInt, 71'792'680'202'469'120, -1}},
		TupleInt{
			Integer4{-71'792'680'202'469'121, -71'792'680'202'469'121,
					 g_MinInt, g_MaxInt},
			Integer4{-71'792'680'202'469'121, 71'792'680'202'469'120,
					 -1, g_MinInt}}};

	// int comparison results, per comparison operation
	auto andRes =
			 ArrayRes{
				 Integer4{-1, g_MaxInt, 71'792'680'202'469'120, 1},
				 Integer4{-71'792'680'202'469'121, 0, g_MinInt, 0}},
		 orRes =
			 ArrayRes{Integer4{-1, -1, -1, -1},
					  Integer4{-71'792'680'202'469'121, -1, -1, -1}},
		 xorRes = ArrayRes{
			 Integer4{0, g_MinInt, -71'792'680'202'469'121, -2},
			 Integer4{0, -1, g_MaxInt, -1}};

	// eval and
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& lhs,
		   const Integer4& rhs) -> void {
			Integer4 result;
			storeInteger4(result, vectorAnd(loadInteger4(lhs),
											loadInteger4(rhs)));

			checkStoreEq(expectRes, result);
		},
		andRes, compares);

	// eval or
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& lhs,
		   const Integer4& rhs) -> void {
			Integer4 result;
			storeInteger4(result, vectorOr(loadInteger4(lhs),
										   loadInteger4(rhs)));

			checkStoreEq(expectRes, result);
		},
		orRes, compares);

	// eval xor
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& lhs,
		   const Integer4& rhs) -> void {
			Integer4 result;
			storeInteger4(result, vectorXor(loadInteger4(lhs),
											loadInteger4(rhs)));

			checkStoreEq(expectRes, result);
		},
		xorRes, compares);
}

void evaluateVectorOneDblArithmetic()
{
	constexpr static const size_t arrSize = 2;

	using TupleDbl = std::tuple<Double4>;
	using ArrayIn  = std::array<TupleDbl, arrSize>;
	using ArrayRes = std::array<Double4, arrSize>;

	// double comparison test data
	ArrayIn compares =
		ArrayIn{TupleDbl{Double4{0.0, 1.0, 4.0, 256.0}},
				TupleDbl{Double4{6.25, 96.04, 3'794'560'000.0,
								 0.000'000'228'484}}};

	// double comparison results, per comparison operation
	auto sqrtRes =
			 ArrayRes{
				 Double4{0.0, 1.0, 2.0, 16.0},
				 Double4{2.5, 9.8, 61600.0, 0.000478},
			 },
		 negRes = ArrayRes{Double4{-0.0, -1.0, -4.0, -256.0},
						   Double4{-6.25, -96.04, -3'794'560'000.0,
								   -0.000'000'228'484}};

	// eval sqrt
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorSqrt(loadDouble4(arg)));

			checkStoreEq(expectRes, result);
		},
		sqrtRes, compares);

	// eval negate
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorNegate(loadDouble4(arg)));

			checkStoreEq(expectRes, result);
		},
		negRes, compares);
}
void evaluateVectorOnePlusDblArithmetic()
{
	constexpr static const size_t arrSize = 3;

	using TupleDbl = std::tuple<Double4, double>;
	using ArrayIn  = std::array<TupleDbl, arrSize>;
	using ArrayRes = std::array<Double4, arrSize>;

	// double comparison test data
	ArrayIn compares = ArrayIn{
		TupleDbl{Double4{0.0, 1.0, 4.0, 256.0}, 0.04},
		TupleDbl{Double4{-0.5, -1.0, -6.0, -400.0}, -200.0},
		TupleDbl{
			Double4{6.25, 96.04, 3'794'560'000.0, 0.000'000'228'484},
			-3.0}};

	// double comparison results, per comparison operation
	auto scaleRes =
		ArrayRes{Double4{0.0, 0.04, 0.16, 10.24},
				 Double4{100.0, 200.0, 1200.0, 80'000.0},
				 Double4{-18.75, -288.12, -11'383'680'000.0,
						 -0.000'000'685'452}};

	// eval scale
	callEvalFunc(
		[](const Double4& expectRes, const Double4& num,
		   const double& scaleFactor) -> void {
			Double4 result;
			storeDouble4(result,
						 vectorScale(loadDouble4(num), scaleFactor));

			checkStoreEq(expectRes, result);
		},
		scaleRes, compares);
}
void evaluateVectorTwoDblArithmetic()
{
	constexpr static const size_t arrSize = 3;

	using Tuple2Dbl = std::tuple<Double4, Double4>;
	using ArrayIn	= std::array<Tuple2Dbl, arrSize>;
	using ArrayRes	= std::array<Double4, arrSize>;

	// double comparison test data
	ArrayIn compares = ArrayIn{
		Tuple2Dbl{Double4{0.0, -1.0, 1.0, 0.2},
				  Double4{1.0, 1.0, -1.0, 0.5}},
		Tuple2Dbl{Double4{4.0, g_MillionDouble, 10.0, 0.005},
				  Double4{256.0, g_MillionDouble, -g_MillionDouble,
						  1.0 / g_MillionDouble}},
		Tuple2Dbl{Double4{0.07, -10'000.0, 100.0, 0.23},
				  Double4{0.004, -345.0, -10.0, 3.4}}};

	// double comparison results, per comparison operation
	auto addRes =
			 ArrayRes{Double4{1.0, 0.0, 0.0, 0.7},
					  Double4{260.0, 2.0 * g_MillionDouble,
							  10.0 - g_MillionDouble,
							  (1.0 / g_MillionDouble) + 0.005},
					  Double4{0.07 + 0.004, -10'345.0, 90.0, 3.63}},
		 subRes =
			 ArrayRes{Double4{-1.0, -2.0, 2.0, -0.3},
					  Double4{-252.0, 0.0, 10.0 + g_MillionDouble,
							  (-1.0 / g_MillionDouble) + 0.005},
					  Double4{0.066, -9655.0, 110.0, -3.17}},
		 multRes =
			 ArrayRes{
				 Double4{0.0, -1.0, -1.0, 0.1},
				 Double4{1024.0, g_MillionDouble * g_MillionDouble,
						 -10.0 * g_MillionDouble,
						 0.005 / g_MillionDouble},
				 Double4{0.07 * 0.004, 3'450'000.0, -1000.0, 0.782}},
		 divRes = ArrayRes{
			 Double4{0.0, -1.0, -1.0, 0.4},
			 Double4{0.015'625, 1.0, -0.000'01, 5000.0},
			 Double4{17.5, 10'000.0 / 345.0, -10.0, 0.23 / 3.4}};

	// eval add
	callEvalFunc(
		[](const Double4& expectRes, const Double4& lhs,
		   const Double4& rhs) -> void {
			Double4 result;
			storeDouble4(result, vectorAddition(loadDouble4(lhs),
												loadDouble4(rhs)));

			checkStoreEq(expectRes, result);
		},
		addRes, compares);

	// eval subtract
	callEvalFunc(
		[](const Double4& expectRes, const Double4& lhs,
		   const Double4& rhs) -> void {
			Double4 result;
			storeDouble4(result, vectorSubtract(loadDouble4(lhs),
												loadDouble4(rhs)));

			checkStoreEq(expectRes, result);
		},
		subRes, compares);

	// eval multiply
	callEvalFunc(
		[](const Double4& expectRes, const Double4& lhs,
		   const Double4& rhs) -> void {
			Double4 result;
			storeDouble4(result, vectorMultiply(loadDouble4(lhs),
												loadDouble4(rhs)));

			checkStoreEq(expectRes, result);
		},
		multRes, compares);

	// eval divide
	callEvalFunc(
		[](const Double4& expectRes, const Double4& lhs,
		   const Double4& rhs) -> void {
			Double4 result;
			storeDouble4(result, vectorDivide(loadDouble4(lhs),
											  loadDouble4(rhs)));

			checkStoreEq(expectRes, result);
		},
		divRes, compares);
}
void evaluateVectorSmallIntArithmetic()
{
	constexpr static const size_t arrSize = 2;

	using Tuple2Int = std::tuple<SmallInt4, SmallInt4>;
	using ArrayIn	= std::array<Tuple2Int, arrSize>;
	using ArrayRes	= std::array<SmallInt4, arrSize>;

	// small int comparison test data
	ArrayIn compares = ArrayIn{
		Tuple2Int{SmallInt4{0, 1, -1, 5'600},
				  SmallInt4{2, 1, 1, -343}},
		Tuple2Int{SmallInt4{45, -1'000'000, 45'000'072, -876'000'000},
				  SmallInt4{32, 1'000'000, 100, -900}}};

	auto addRes =
			 ArrayRes{
				 SmallInt4{2, 2, 0, 5'257},
				 SmallInt4{77, 0, 45'000'172, -876'000'900},
			 },
		 subRes = ArrayRes{
			 SmallInt4{-2, 0, -2, 5'943},
			 SmallInt4{13, -2'000'000, 44'999'972, -875'999'100},
		 };

	// eval add
	callEvalFunc(
		[](const SmallInt4& expectRes, const SmallInt4& lhs,
		   const SmallInt4& rhs) -> void {
			SmallInt4 result;
			storeSmallInt4(result,
						   vectorAddition(loadSmallInt4(lhs),
										  loadSmallInt4(rhs)));

			checkStoreEq(expectRes, result);
		},
		addRes, compares);

	// eval subtract
	callEvalFunc(
		[](const SmallInt4& expectRes, const SmallInt4& lhs,
		   const SmallInt4& rhs) -> void {
			SmallInt4 result;
			storeSmallInt4(result,
						   vectorSubtract(loadSmallInt4(lhs),
										  loadSmallInt4(rhs)));

			checkStoreEq(expectRes, result);
		},
		subRes, compares);
}
void evaluateVectorIntArithmetic()
{
	constexpr static const size_t arrSize = 2;

	using Tuple2Int = std::tuple<Integer4, Integer4>;
	using ArrayIn	= std::array<Tuple2Int, arrSize>;
	using ArrayRes	= std::array<Integer4, arrSize>;

	// int comparison test data
	ArrayIn compares = ArrayIn{
		Tuple2Int{Integer4{0, 1, -1, 5'600}, Integer4{2, 1, 1, -343}},
		Tuple2Int{Integer4{45, -1'000'000, 45'000'072, -876'000'000},
				  Integer4{32, 1'000'000, 100, -900}}};

	// int comparison results, per comparison operation
	auto addRes =
			 ArrayRes{
				 Integer4{2, 2, 0, 5'257},
				 Integer4{77, 0, 45'000'172, -876'000'900},
			 },
		 subRes =
			 ArrayRes{
				 Integer4{-2, 0, -2, 5'943},
				 Integer4{13, -2'000'000, 44'999'972, -875'999'100},
			 },
		 multRes =
			 ArrayRes{
				 Integer4{0, 1, -1, -1'920'800},
				 Integer4{1440, -1'000'000'000'000, 4'500'007'200,
						  788'400'000'000},
			 },
		 divRes =
			 ArrayRes{
				 Integer4{0, 1, -1, -16},
				 Integer4{1, -1, 450'000, 973334},
			 },
		 remRes = ArrayRes{
			 Integer4{0, 0, 0, 112},
			 Integer4{13, 0, 72, 600},
		 };

	// eval add
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& lhs,
		   const Integer4& rhs) -> void {
			Integer4 result;
			storeInteger4(result, vectorAddition(loadInteger4(lhs),
												 loadInteger4(rhs)));

			checkStoreEq(expectRes, result);
		},
		addRes, compares);

	// eval subtract
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& lhs,
		   const Integer4& rhs) -> void {
			Integer4 result;
			storeInteger4(result, vectorSubtract(loadInteger4(lhs),
												 loadInteger4(rhs)));

			checkStoreEq(expectRes, result);
		},
		subRes, compares);

	// eval multiply
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& lhs,
		   const Integer4& rhs) -> void {
			Integer4 result;
			storeInteger4(result, vectorMultiply(loadInteger4(lhs),
												 loadInteger4(rhs)));

			checkStoreEq(expectRes, result);
		},
		multRes, compares);

	// eval divide
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& lhs,
		   const Integer4& rhs) -> void {
			Integer4 result;
			storeInteger4(result,
						  vectorDivide(loadInteger4(lhs),
									   loadInteger4(rhs), nullptr));

			checkStoreEq(expectRes, result);
		},
		divRes, compares);

	// eval remainder
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& lhs,
		   const Integer4& rhs) -> void {
			Integer4 result;
			storeInteger4(result, vectorRemainder(loadInteger4(lhs),
												  loadInteger4(rhs)));

			checkStoreEq(expectRes, result);
		},
		remRes, compares);
}

void evalForwardOneArgTrigFunctions()
{
	constexpr static const size_t arrSize = 2;

	using TupleDbl = std::tuple<Double4>;
	using ArrayIn  = std::array<TupleDbl, arrSize>;
	using ArrayRes = std::array<Double4, arrSize>;

	// we don't test PI/2 bc tan has a pole there, and given the size
	// of the change of the numbers, one cannot expect a good accuracy
	double tests[8] = {g_PI,	   (0.85 * g_PI) / 2.0,
					   -g_PI,	   -(0.85 * g_PI) / 2.0,
					   g_PI / 3.0, g_PI / 4.0,
					   g_PI / 6.0, 1.0};

	// double comparison test data
	ArrayIn compares = ArrayIn{
		TupleDbl{Double4{tests[0], tests[1], tests[2], tests[3]}},
		TupleDbl{Double4{tests[4], tests[5], tests[6], tests[7]}}};

	// double comparison results, per trig operation
	auto sinRes =
			 ArrayRes{
				 Double4{std::sin(tests[0]), std::sin(tests[1]),
						 std::sin(tests[2]), std::sin(tests[3])},
				 Double4{std::sin(tests[4]), std::sin(tests[5]),
						 std::sin(tests[6]), std::sin(tests[7])}},
		 cosRes =
			 ArrayRes{
				 Double4{std::cos(tests[0]), std::cos(tests[1]),
						 std::cos(tests[2]), std::cos(tests[3])},
				 Double4{std::cos(tests[4]), std::cos(tests[5]),
						 std::cos(tests[6]), std::cos(tests[7])}},
		 tanRes = ArrayRes{
			 Double4{std::tan(tests[0]), std::tan(tests[1]),
					 std::tan(tests[2]), std::tan(tests[3])},
			 Double4{std::tan(tests[4]), std::tan(tests[5]),
					 std::tan(tests[6]), std::tan(tests[7])}};

	// eval sin
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorSin(loadDouble4(arg)));

			dataStoreApproxEq(expectRes, result, 2.0);
		},
		sinRes, compares);

	// eval cos
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorCos(loadDouble4(arg)));

			dataStoreApproxEq(expectRes, result, 2.0);
		},
		cosRes, compares);

	// eval tan
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorTan(loadDouble4(arg)));

			dataStoreApproxEq(expectRes, result, 2.0);
		},
		tanRes, compares);
}
void evalFullRangeOneArgTrigFunctions()
{
	constexpr static const size_t arrSize = 2;

	using TupleDbl = std::tuple<Double4>;
	using ArrayIn  = std::array<TupleDbl, arrSize>;
	using ArrayRes = std::array<Double4, arrSize>;

	double tests[8] = {0.0,			1.0,		  -1.0,	  0.2,
					   g_InfDouble, -g_InfDouble, 0.0001, 100.45};

	// double comparison test data
	ArrayIn compares = ArrayIn{
		TupleDbl{Double4{tests[0], tests[1], tests[2], tests[3]}},
		TupleDbl{Double4{tests[4], tests[5], tests[6], tests[7]}}};

	// double comparison results, per trig operation
	auto arctanRes =
		ArrayRes{Double4{std::atan(tests[0]), std::atan(tests[1]),
						 std::atan(tests[2]), std::atan(tests[3])},
				 Double4{std::atan(tests[4]), std::atan(tests[5]),
						 std::atan(tests[6]), std::atan(tests[7])}};

	// eval arctan
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorArcTan(loadDouble4(arg)));

			dataStoreApproxEq(expectRes, result, 2.0);
		},
		arctanRes, compares);
}
void evalPartialRangeOneArgTrigFunctions()
{
	constexpr static const size_t arrSize = 2;

	using TupleDbl = std::tuple<Double4>;
	using ArrayIn  = std::array<TupleDbl, arrSize>;
	using ArrayRes = std::array<Double4, arrSize>;

	double tests[8] = {1.0,
					   0.0,
					   -1.0,
					   0.5,
					   std::sqrt(3.0) / 2.0,
					   std::sqrt(2.0) / 2.0,
					   (std::sqrt(6.0) - std::sqrt(2.0)) / 4.0,
					   0.8414709848};

	// double comparison test data
	ArrayIn compares = ArrayIn{
		TupleDbl{Double4{tests[0], tests[1], tests[2], tests[3]}},
		TupleDbl{Double4{tests[4], tests[5], tests[6], tests[7]}}};

	// double comparison results, per trig operation
	auto arcsinRes =
			 ArrayRes{
				 Double4{std::asin(tests[0]), std::asin(tests[1]),
						 std::asin(tests[2]), std::asin(tests[3])},
				 Double4{std::asin(tests[4]), std::asin(tests[5]),
						 std::asin(tests[6]), std::asin(tests[7])}},
		 arccosRes = ArrayRes{
			 Double4{std::acos(tests[0]), std::acos(tests[1]),
					 std::acos(tests[2]), std::acos(tests[3])},
			 Double4{std::acos(tests[4]), std::acos(tests[5]),
					 std::acos(tests[6]), std::acos(tests[7])}};

	// eval arcsin
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorArcSin(loadDouble4(arg)));

			dataStoreApproxEq(expectRes, result, 2.0);
		},
		arcsinRes, compares);

	// eval arccos
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorArcCos(loadDouble4(arg)));

			dataStoreApproxEq(expectRes, result, 2.0);
		},
		arccosRes, compares);
}
void evalTwoArgTrigFunctions()
{
	constexpr static const size_t arrSize = 3;

	using TupleDbl = std::tuple<Double4, Double4>;
	using ArrayIn  = std::array<TupleDbl, arrSize>;
	using ArrayRes = std::array<Double4, arrSize>;

	double tests[12][2] = {
		{1.0, 1.0},			  {1.0, -1.0},
		{-1.0, 1.0},		  {-1.0, -1.0},
		{0.0, 1.0},			  {0.0, -1.0},
		{1.0, 0.0},			  {-1.0, 0.0},
		{g_InfDouble, 100.0}, {-g_InfDouble, 100.0},
		{100.0, g_InfDouble}, {-g_InfDouble, -g_InfDouble}};

	// double comparison test data
	ArrayIn compares =
		ArrayIn{TupleDbl{Double4{tests[0][0], tests[1][0],
								 tests[2][0], tests[3][0]},
						 Double4{tests[0][1], tests[1][1],
								 tests[2][1], tests[3][1]}},
				TupleDbl{Double4{tests[4][0], tests[5][0],
								 tests[6][0], tests[7][0]},
						 Double4{tests[4][1], tests[5][1],
								 tests[6][1], tests[7][1]}},
				TupleDbl{Double4{tests[8][0], tests[9][0],
								 tests[10][0], tests[11][0]},
						 Double4{tests[8][1], tests[9][1],
								 tests[10][1], tests[11][1]}}};

	// double comparison results, per trig operation
	auto arctan2Res =
		ArrayRes{Double4{std::atan2(tests[0][0], tests[0][1]),
						 std::atan2(tests[1][0], tests[1][1]),
						 std::atan2(tests[2][0], tests[2][1]),
						 std::atan2(tests[3][0], tests[3][1])},
				 Double4{std::atan2(tests[4][0], tests[4][1]),
						 std::atan2(tests[5][0], tests[5][1]),
						 std::atan2(tests[6][0], tests[6][1]),
						 std::atan2(tests[7][0], tests[7][1])},
				 Double4{std::atan2(tests[8][0], tests[8][1]),
						 std::atan2(tests[9][0], tests[9][1]),
						 std::atan2(tests[10][0], tests[10][1]),
						 std::atan2(tests[11][0], tests[11][1])}};

	// eval arctan2
	callEvalFunc(
		[](const Double4& expectRes, const Double4& top,
		   const Double4& bottom) -> void {
			Double4 result;
			storeDouble4(result, vectorArcTan2(loadDouble4(top),
											   loadDouble4(bottom)));

			dataStoreApproxEq(expectRes, result, 2.0);
		},
		arctan2Res, compares);
}

void evalOneArgSpecialFunctions()
{
	constexpr static const size_t arrSize = 2;

	using TupleDbl = std::tuple<Double4>;
	using ArrayIn  = std::array<TupleDbl, arrSize>;
	using ArrayRes = std::array<Double4, arrSize>;

	// double comparison test data
	ArrayIn compares =
		ArrayIn{TupleDbl{Double4{1.0, 0.0, -1.0, 365.0}},
				TupleDbl{Double4{g_MillionDouble, -g_MillionDouble,
								 g_InfDouble, -g_InfDouble}}};

	// double comparison results, per comparison operation
	auto expRes = ArrayRes{Double4{std::exp(1.0), 1.0, std::exp(-1.0),
								   std::exp(365.0)},
						   Double4{std::exp(g_MillionDouble),
								   std::exp(-g_MillionDouble),
								   std::exp(g_InfDouble),
								   std::exp(-g_InfDouble)}},
		 exp2Res =
			 ArrayRes{Double4{std::exp2(1.0), 1.0, std::exp2(-1.0),
							  std::exp2(365.0)},
					  Double4{std::exp2(g_MillionDouble),
							  std::exp2(-g_MillionDouble),
							  std::exp2(g_InfDouble),
							  std::exp(-g_InfDouble)}},
		 logRes = ArrayRes{Double4{std::log(1.0), std::log(0.0),
								   std::log(-1.0), std::log(365.0)},
						   Double4{std::log(g_MillionDouble),
								   std::log(-g_MillionDouble),
								   std::log(g_InfDouble),
								   std::log(-g_InfDouble)}},
		 log2Res =
			 ArrayRes{Double4{std::log2(1.0), std::log2(0.0),
							  std::log2(-1.0), std::log2(365.0)},
					  Double4{std::log2(g_MillionDouble),
							  std::log2(-g_MillionDouble),
							  std::log2(g_InfDouble),
							  std::log2(-g_InfDouble)}},
		 log10Res =
			 ArrayRes{Double4{std::log10(1.0), std::log10(0.0),
							  std::log10(-1.0), std::log10(365.0)},
					  Double4{std::log10(g_MillionDouble),
							  std::log10(-g_MillionDouble),
							  std::log10(g_InfDouble),
							  std::log10(-g_InfDouble)}},
		 absRes = ArrayRes{
			 Double4{std::abs(1.0), std::abs(0.0), std::abs(-1.0),
					 std::abs(365.0)},
			 Double4{std::abs(g_MillionDouble),
					 std::abs(-g_MillionDouble),
					 std::abs(g_InfDouble), std::abs(-g_InfDouble)}};

	// eval exp
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorExp(loadDouble4(arg)));

			dataStoreApproxEq(expectRes, result, 50.0);
		},
		expRes, compares);

	// eval exp2
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorExp2(loadDouble4(arg)));

			dataStoreApproxEq(expectRes, result, 2.0);
		},
		exp2Res, compares);

	// eval log
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 resultStore;
			storeDouble4(resultStore, vectorLog(loadDouble4(arg)));

			dataStoreApproxEq(resultStore, expectRes, 2.0);
		},
		logRes, compares);

	// eval log2
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 resultStore;
			storeDouble4(resultStore, vectorLog2(loadDouble4(arg)));

			dataStoreApproxEq(resultStore, expectRes, 2.0);
		},
		log2Res, compares);

	// eval log10
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 resultStore;
			storeDouble4(resultStore, vectorLog10(loadDouble4(arg)));

			dataStoreApproxEq(resultStore, expectRes, 2.0);
		},
		log10Res, compares);

	// eval abs
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorAbs(loadDouble4(arg)));

			checkStoreEq(expectRes, result);
		},
		absRes, compares);
}
void evalPowSpecialFunction()
{
	constexpr static const size_t arrSize = 3;

	using Tuple2Dbl = std::tuple<Double4, Double4>;
	using ArrayIn	= std::array<Tuple2Dbl, arrSize>;
	using ArrayRes	= std::array<Double4, arrSize>;

	ArrayIn compares = ArrayIn{
		Tuple2Dbl{Double4{0.0, -1.0, 1.0, 0.2},
				  Double4{1.0, 1.0, -1.0, 0.5}},
		Tuple2Dbl{Double4{4.0, g_MillionDouble, 10.0, 0.005},
				  Double4{256.0, g_MillionDouble, -g_MillionDouble,
						  1.0 / g_MillionDouble}},
		Tuple2Dbl{Double4{0.07, -10'000.0, 100.0, 0.23},
				  Double4{0.004, -345.0, -10.0, 3.4}}};

	auto powRes = ArrayRes{
		Double4{0.0, -1.0, 1.0, std::pow(0.2, 0.5)},
		Double4{std::pow(4.0, 256.0), g_InfDouble, 0.0,
				std::pow(0.005, (1.0 / g_MillionDouble))},
		Double4{std::pow(0.07, 0.004), 0.0, std::pow(100.0, -10.0),
				std::pow(0.23, 3.4)}};

	// eval pow
	callEvalFunc(
		[](const Double4& expectRes, const Double4& lhs,
		   const Double4& rhs) -> void {
			Double4 result;
			storeDouble4(result, vectorPow(loadDouble4(lhs),
										   loadDouble4(rhs)));

			dataStoreApproxEq(expectRes, result, 2.0);
		},
		powRes, compares);
}
void evalTwoArgSpecialFunctions()
{
	constexpr static const size_t arrSize = 2;

	using TupleDbl = std::tuple<Double4, Double4>;
	using ArrayIn  = std::array<TupleDbl, arrSize>;
	using ArrayRes = std::array<Double4, arrSize>;

	// double comparison test data
	ArrayIn compares = ArrayIn{
		TupleDbl{Double4{1.0, 0.0, 1.0, -g_MillionDouble},
				 Double4{0.0, 1.0, -1.0, g_MillionDouble}},
		TupleDbl{
			Double4{g_InfDouble, g_QNaN, g_MillionDouble,
					g_MillionDouble},
			Double4{-g_InfDouble, -g_InfDouble, g_InfDouble, 1.0}}};

	// double comparison results, per comparison operation
	auto maxRes = ArrayRes{Double4{1.0, 1.0, 1.0, g_MillionDouble},
						   Double4{g_InfDouble, -g_InfDouble,
								   g_InfDouble, g_MillionDouble}},
		 minRes = ArrayRes{Double4{0.0, 0.0, -1.0, -g_MillionDouble},
						   Double4{-g_InfDouble, -g_InfDouble,
								   g_MillionDouble, 1.0}};

	// eval max
	callEvalFunc(
		[](const Double4& expectRes, const Double4& lhs,
		   const Double4& rhs) -> void {
			Double4 result;
			storeDouble4(result, vectorMax(loadDouble4(lhs),
										   loadDouble4(rhs)));

			checkStoreEq(expectRes, result);
		},
		maxRes, compares);

	// eval min
	callEvalFunc(
		[](const Double4& expectRes, const Double4& lhs,
		   const Double4& rhs) -> void {
			Double4 result;
			storeDouble4(result, vectorMin(loadDouble4(lhs),
										   loadDouble4(rhs)));

			checkStoreEq(expectRes, result);
		},
		minRes, compares);
}

void evalIntToDblCast()
{
	auto toIntRntrpt = [](double data) -> int_least64_t {
		return *reinterpret_cast<int_least64_t*>(&data);
	};
	auto toIntCast = [](double data) -> int_least64_t {
		return static_cast<int_least64_t>(data);
	};

	double dbleData[11] = {
		0.0,
		1.0,
		-1.0,
		0.5467843,
		g_MillionDouble,
		-g_MillionDouble,
		g_InfDouble,
		-g_InfDouble,
		234,
		-234,
		9'000'000'000'000'000'000.0 - 9'000'000'000'000'000'000.0};
	int_least64_t dbleRntrpt[8] = {toIntRntrpt(dbleData[0]),
								   toIntRntrpt(dbleData[1]),
								   toIntRntrpt(dbleData[2]),
								   toIntRntrpt(dbleData[3]),
								   toIntRntrpt(dbleData[4]),
								   toIntRntrpt(dbleData[5]),
								   toIntRntrpt(dbleData[6]),
								   toIntRntrpt(dbleData[7])},
				  dbleCast[8]	= {toIntCast(dbleData[0]),
								   toIntCast(dbleData[1]),
								   toIntCast(dbleData[2]),
								   toIntCast(dbleData[4]),
								   toIntCast(dbleData[5]),
								   toIntCast(dbleData[8]),
								   toIntCast(dbleData[9]),
								   toIntCast(dbleData[10])};

	constexpr static const size_t arrSize = 2;

	using TupleDbl = std::tuple<Integer4>;
	using ArrayIn  = std::array<TupleDbl, arrSize>;
	using ArrayRes = std::array<Double4, arrSize>;

	// eval reinterpretToDouble
	callEvalFunc(
		[](const Double4& expectRes, const Integer4& data) -> void {
			Double4 result;
			storeDouble4(result,
						 reinterpretToDouble(loadInteger4(data)));

			checkStoreEq(expectRes, result);
		},
		ArrayRes{Double4{dbleData[0], dbleData[1], dbleData[2],
						 dbleData[3]},
				 Double4{dbleData[4], dbleData[5], dbleData[6],
						 dbleData[7]}},
		ArrayIn{TupleDbl{Integer4{dbleRntrpt[0], dbleRntrpt[1],
								  dbleRntrpt[2], dbleRntrpt[3]}},
				TupleDbl{Integer4{dbleRntrpt[4], dbleRntrpt[5],
								  dbleRntrpt[6], dbleRntrpt[7]}}});

	// eval toDoubleVector
	callEvalFunc(
		[](const Double4& expectRes, const Integer4& data) -> void {
			Double4 result;
			storeDouble4(result, toDoubleVector(loadInteger4(data)));

			checkStoreEq(expectRes, result);
		},
		ArrayRes{Double4{dbleData[0], dbleData[1], dbleData[2],
						 dbleData[4]},
				 Double4{dbleData[5], dbleData[8], dbleData[9],
						 dbleData[10]}},
		ArrayIn{TupleDbl{Integer4{dbleCast[0], dbleCast[1],
								  dbleCast[2], dbleCast[3]}},
				TupleDbl{Integer4{dbleCast[4], dbleCast[5],
								  dbleCast[6], dbleCast[7]}}});
}
void evalDblToIntCast()
{
	auto toDblRntrpt = [](int_least64_t data) -> double {
		return *reinterpret_cast<double*>(&data);
	};
	auto toDblCast = [](int_least64_t data) -> double {
		return static_cast<double>(data);
	};

	// for the reinterpret, we test purely integer data, but for the
	// regular cast, we also replace some tests with tests of
	// fractional data, to ensure that the fractions are correctly
	// truncated
	int_least64_t intData[10]  = {0,
								  1,
								  -1,
								  -1'234,
								  1'000'000,
								  -1'000'000,
								  9'000'000'000'000'000'000,
								  -9'000'000'000'000'000'000,
								  2,
								  101};
	double		  intRntrpt[8] = {toDblRntrpt(intData[0]),
							  toDblRntrpt(intData[1]),
							  toDblRntrpt(intData[2]),
							  toDblRntrpt(intData[3]),
							  toDblRntrpt(intData[4]),
							  toDblRntrpt(intData[5]),
							  toDblRntrpt(intData[6]),
							  toDblRntrpt(intData[7])},
		   intCast[8]		   = {toDblCast(intData[0]),
						  toDblCast(intData[1]),
						  toDblCast(intData[2]),
						  2.6674,
						  101.4,
						  toDblCast(intData[5]),
						  toDblCast(intData[6]),
						  toDblCast(intData[7])};

	constexpr static const size_t arrSize = 2;

	using TupleDbl = std::tuple<Double4>;
	using ArrayIn  = std::array<TupleDbl, arrSize>;
	using ArrayRes = std::array<Integer4, arrSize>;

	// eval reinterpretToInteger
	callEvalFunc(
		[](const Integer4& expectRes, const Double4& data) -> void {
			Integer4 result;
			storeInteger4(result,
						  reinterpretToInteger(loadDouble4(data)));

			checkStoreEq(expectRes, result);
		},
		ArrayRes{
			Integer4{intData[0], intData[1], intData[2], intData[3]},
			Integer4{intData[4], intData[5], intData[6], intData[7]}},
		ArrayIn{TupleDbl{Double4{intRntrpt[0], intRntrpt[1],
								 intRntrpt[2], intRntrpt[3]}},
				TupleDbl{Double4{intRntrpt[4], intRntrpt[5],
								 intRntrpt[6], intRntrpt[7]}}});

	// eval toIntegerVector
	callEvalFunc(
		[](const Integer4& expectRes, const Double4& data) -> void {
			Integer4 result;
			storeInteger4(result, toIntegerVector(loadDouble4(data)));

			checkStoreEq(expectRes, result);
		},
		ArrayRes{
			Integer4{intData[0], intData[1], intData[2], intData[8]},
			Integer4{intData[9], intData[5], intData[6], intData[7]}},
		ArrayIn{TupleDbl{Double4{intCast[0], intCast[1], intCast[2],
								 intCast[3]}},
				TupleDbl{Double4{intCast[4], intCast[5], intCast[6],
								 intCast[7]}}});
}
void evalNrwToDblCast()
{
	auto toDbl = [](float data) -> double {
		return static_cast<double>(data);
	};

	float rawData[16] = {
		0.0f,			-0.0f,			 1.0f,		 -1.0f,
		g_FEpsilon,		-g_FEpsilon,	 g_InfFloat, -g_InfFloat,
		g_MillionFloat, -g_MillionFloat, g_FPI,		 g_FPI,
		0.2f,			-0.9f,			 80.5f,		 -8'000.43f};
	double rawRes[16] = {
		toDbl(rawData[0]),	toDbl(rawData[1]),	toDbl(rawData[2]),
		toDbl(rawData[3]),	toDbl(rawData[4]),	toDbl(rawData[5]),
		toDbl(rawData[6]),	toDbl(rawData[7]),	toDbl(rawData[8]),
		toDbl(rawData[9]),	toDbl(rawData[10]), toDbl(rawData[11]),
		toDbl(rawData[12]), toDbl(rawData[13]), toDbl(rawData[14]),
		toDbl(rawData[15])};

	constexpr static const size_t vecArrSize = 4;

	using TupleFltVec = std::tuple<Float4>;
	using ArrayVecIn  = std::array<TupleFltVec, vecArrSize>;
	using ArrayVecRes = std::array<Double4, vecArrSize>;

	// eval toWideVector
	callEvalFunc(
		[](const Double4& expectRes, const Float4& data) -> void {
			Double4 realRes;
			storeDouble4(realRes, toWideVector(loadFloat4(data)));

			checkStoreEq(expectRes, realRes);
		},
		ArrayVecRes{
			Double4{rawRes[0], rawRes[1], rawRes[2], rawRes[3]},
			Double4{rawRes[4], rawRes[5], rawRes[6], rawRes[7]},
			Double4{rawRes[8], rawRes[9], rawRes[10], rawRes[11]},
			Double4{rawRes[12], rawRes[13], rawRes[14], rawRes[15]}},
		ArrayVecIn{TupleFltVec{Float4{rawData[0], rawData[1],
									  rawData[2], rawData[3]}},
				   TupleFltVec{Float4{rawData[4], rawData[5],
									  rawData[6], rawData[7]}},
				   TupleFltVec{Float4{rawData[8], rawData[9],
									  rawData[10], rawData[11]}},
				   TupleFltVec{Float4{rawData[12], rawData[13],
									  rawData[14], rawData[15]}}});

	constexpr static const size_t mtrxArrSize = 2;

	using TupleFltMtrx = std::tuple<Float4x4>;
	using ArrayMtrxIn  = std::array<TupleFltMtrx, mtrxArrSize>;
	using ArrayMtrxRes = std::array<Double4x4, mtrxArrSize>;

	// eval toWideMatrix
	callEvalFunc(
		[](const Double4x4& expectRes, const Float4x4& data) -> void {
			Double4x4 realRes;
			storeDouble4x4(realRes, toWideMatrix(loadFloat4x4(data)));

			checkStoreEq(expectRes, realRes);
		},
		ArrayMtrxRes{
			Double4x4{
				Double4{rawRes[0], rawRes[1], rawRes[2], rawRes[3]},
				Double4{rawRes[4], rawRes[5], rawRes[6], rawRes[7]},
				Double4{rawRes[8], rawRes[9], rawRes[10], rawRes[11]},
				Double4{rawRes[12], rawRes[13], rawRes[14],
						rawRes[15]}},
			Double4x4{
				Double4{rawRes[15], rawRes[14], rawRes[13],
						rawRes[12]},
				Double4{rawRes[11], rawRes[10], rawRes[9], rawRes[8]},
				Double4{rawRes[7], rawRes[6], rawRes[5], rawRes[4]},
				Double4{rawRes[3], rawRes[2], rawRes[1], rawRes[0]}}},
		ArrayMtrxIn{
			TupleFltMtrx{Float4x4{Float4{rawData[0], rawData[1],
										 rawData[2], rawData[3]},
								  Float4{rawData[4], rawData[5],
										 rawData[6], rawData[7]},
								  Float4{rawData[8], rawData[9],
										 rawData[10], rawData[11]},
								  Float4{rawData[12], rawData[13],
										 rawData[14], rawData[15]}}},
			TupleFltMtrx{Float4x4{Float4{rawData[15], rawData[14],
										 rawData[13], rawData[12]},
								  Float4{rawData[11], rawData[10],
										 rawData[9], rawData[8]},
								  Float4{rawData[7], rawData[6],
										 rawData[5], rawData[4]},
								  Float4{rawData[3], rawData[2],
										 rawData[1], rawData[0]}}}});
}
void evalDblToNrwCast()
{
	auto toNrw = [](double data) -> float {
		return static_cast<float>(data);
	};

	double rawData[16] = {0.1,
						  0.621,
						  21.4,
						  -3'000.8,
						  0.0,
						  -0.0,
						  1.0,
						  -1.0,
						  g_Epsilon,
						  -g_Epsilon,
						  g_InfDouble,
						  -g_InfDouble,
						  g_MillionDouble,
						  -g_MillionDouble,
						  g_PI,
						  g_PI};
	float  rawRes[16]  = {
		  toNrw(rawData[0]),  toNrw(rawData[1]),  toNrw(rawData[2]),
		  toNrw(rawData[3]),  toNrw(rawData[4]),  toNrw(rawData[5]),
		  toNrw(rawData[6]),  toNrw(rawData[7]),  toNrw(rawData[8]),
		  toNrw(rawData[9]),  toNrw(rawData[10]), toNrw(rawData[11]),
		  toNrw(rawData[12]), toNrw(rawData[13]), toNrw(rawData[14]),
		  toNrw(rawData[15])};

	constexpr static const size_t vecArrSize = 4;

	using TupleDblVec = std::tuple<Double4>;
	using ArrayVecIn  = std::array<TupleDblVec, vecArrSize>;
	using ArrayVecRes = std::array<Float4, vecArrSize>;

	// eval toWideVector
	callEvalFunc(
		[](const Float4& expectRes, const Double4& data) -> void {
			Float4 realRes;
			storeFloat4(realRes, toNarrowVector(loadDouble4(data)));

			checkStoreEq(expectRes, realRes);
		},
		ArrayVecRes{
			Float4{rawRes[0], rawRes[1], rawRes[2], rawRes[3]},
			Float4{rawRes[4], rawRes[5], rawRes[6], rawRes[7]},
			Float4{rawRes[8], rawRes[9], rawRes[10], rawRes[11]},
			Float4{rawRes[12], rawRes[13], rawRes[14], rawRes[15]}},
		ArrayVecIn{TupleDblVec{Double4{rawData[0], rawData[1],
									   rawData[2], rawData[3]}},
				   TupleDblVec{Double4{rawData[4], rawData[5],
									   rawData[6], rawData[7]}},
				   TupleDblVec{Double4{rawData[8], rawData[9],
									   rawData[10], rawData[11]}},
				   TupleDblVec{Double4{rawData[12], rawData[13],
									   rawData[14], rawData[15]}}});

	constexpr static const size_t mtrxArrSize = 2;

	using TupleDblMtrx = std::tuple<Double4x4>;
	using ArrayMtrxIn  = std::array<TupleDblMtrx, mtrxArrSize>;
	using ArrayMtrxRes = std::array<Float4x4, mtrxArrSize>;

	// eval toNarrowMatrix
	callEvalFunc(
		[](const Float4x4& expectRes, const Double4x4& data) -> void {
			Float4x4 realRes;
			storeFloat4x4(realRes,
						  toNarrowMatrix(loadDouble4x4(data)));

			checkStoreEq(expectRes, realRes);
		},
		ArrayMtrxRes{
			Float4x4{
				Float4{rawRes[0], rawRes[1], rawRes[2], rawRes[3]},
				Float4{rawRes[4], rawRes[5], rawRes[6], rawRes[7]},
				Float4{rawRes[8], rawRes[9], rawRes[10], rawRes[11]},
				Float4{rawRes[12], rawRes[13], rawRes[14],
					   rawRes[15]}},
			Float4x4{
				Float4{rawRes[15], rawRes[14], rawRes[13],
					   rawRes[12]},
				Float4{rawRes[11], rawRes[10], rawRes[9], rawRes[8]},
				Float4{rawRes[7], rawRes[6], rawRes[5], rawRes[4]},
				Float4{rawRes[3], rawRes[2], rawRes[1], rawRes[0]}}},
		ArrayMtrxIn{TupleDblMtrx{
						Double4x4{Double4{rawData[0], rawData[1],
										  rawData[2], rawData[3]},
								  Double4{rawData[4], rawData[5],
										  rawData[6], rawData[7]},
								  Double4{rawData[8], rawData[9],
										  rawData[10], rawData[11]},
								  Double4{rawData[12], rawData[13],
										  rawData[14], rawData[15]}}},
					TupleDblMtrx{
						Double4x4{Double4{rawData[15], rawData[14],
										  rawData[13], rawData[12]},
								  Double4{rawData[11], rawData[10],
										  rawData[9], rawData[8]},
								  Double4{rawData[7], rawData[6],
										  rawData[5], rawData[4]},
								  Double4{rawData[3], rawData[2],
										  rawData[1], rawData[0]}}}});
}
void evalIntNrwWideCast()
{
	auto toWideCast = [](int_least32_t data) -> int_least64_t {
		return static_cast<int_least64_t>(data);
	};

	// for the reinterpret, we test purely integer data, but for the
	// regular cast, we also replace some tests with tests of
	// fractional data, to ensure that the fractions are correctly
	// truncated
	int_least32_t intData[8]	 = {0,			 1,			  -1,
								-1'234,		 1'000'000,	  -1'000'000,
								987'654'321, -987'654'321};
	int_least64_t intWideData[8] = {
		toWideCast(intData[0]), toWideCast(intData[1]),
		toWideCast(intData[2]), toWideCast(intData[3]),
		toWideCast(intData[4]), toWideCast(intData[5]),
		toWideCast(intData[6]), toWideCast(intData[7])};

	constexpr static const size_t arrSize = 2;

	using TupleNrw	= std::tuple<SmallInt4>;
	using ArrayNIn	= std::array<TupleNrw, arrSize>;
	using ArrayNRes = std::array<Integer4, arrSize>;

	// eval toWideIntVector
	callEvalFunc(
		[](const Integer4& expectRes, const SmallInt4& data) -> void {
			Integer4 result;
			storeInteger4(result,
						  toWideIntVector(loadSmallInt4(data)));

			checkStoreEq(expectRes, result);
		},
		ArrayNRes{Integer4{intWideData[0], intWideData[1],
						   intWideData[2], intWideData[3]},
				  Integer4{intWideData[4], intWideData[5],
						   intWideData[6], intWideData[7]}},
		ArrayNIn{TupleNrw{SmallInt4{intData[0], intData[1],
									intData[2], intData[3]}},
				 TupleNrw{SmallInt4{intData[4], intData[5],
									intData[6], intData[7]}}});

	using TupleWide = std::tuple<Integer4>;
	using ArrayWIn	= std::array<TupleWide, arrSize>;
	using ArrayWRes = std::array<SmallInt4, arrSize>;

	// eval toSmallIntVector
	callEvalFunc(
		[](const SmallInt4& expectRes, const Integer4& data) -> void {
			SmallInt4 result;
			storeSmallInt4(result,
						   toSmallIntVector(loadInteger4(data)));

			checkStoreEq(expectRes, result);
		},
		ArrayWRes{
			SmallInt4{intData[0], intData[1], intData[2], intData[3]},
			SmallInt4{intData[4], intData[5], intData[6],
					  intData[7]}},
		ArrayWIn{
			TupleWide{Integer4{intWideData[0], intWideData[1],
							   intWideData[2], intWideData[3]}},
			TupleWide{Integer4{intWideData[4], intWideData[5],
							   intWideData[6], intWideData[7]}}});
}
void evalVecRounds()
{
	constexpr static const size_t arrSize = 4;

	using TupleDbl = std::tuple<Double4>;
	using ArrayIn  = std::array<TupleDbl, arrSize>;
	using ArrayRes = std::array<Double4, arrSize>;

	// double comparison test data
	ArrayIn toRound = ArrayIn{
		TupleDbl{Double4{0.0, 1.0, -1.0, 2.0}},
		TupleDbl{Double4{0.5, -0.5, 1.5, -1.5}},
		TupleDbl{Double4{1.4, 2.4, -2.4, 3.7}},
		TupleDbl{Double4{-3.7, 4.7, g_MillionDouble, g_InfDouble}}};

	// double comparison results, per comparison operation
	auto bankersRes	 = ArrayRes{Double4{0.0, 1.0, -1.0, 2.0},
								Double4{0.0, -0.0, 2.0, -2.0},
								Double4{1.0, 2.0, -2.0, 4.0},
								Double4{-4.0, 5.0, g_MillionDouble,
										g_InfDouble}},
		 floorRes	 = ArrayRes{Double4{0.0, 1.0, -1.0, 2.0},
								Double4{0.0, -1.0, 1.0, -2.0},
								Double4{1.0, 2.0, -3.0, 3.0},
								Double4{-4.0, 4.0, g_MillionDouble,
										g_InfDouble}},
		 ceilingRes	 = ArrayRes{Double4{0.0, 1.0, -1.0, 2.0},
								Double4{1.0, 0.0, 2.0, -1.0},
								Double4{2.0, 3.0, -2.0, 4.0},
								Double4{-3.0, 5.0, g_MillionDouble,
										g_InfDouble}},
		 truncateRes = ArrayRes{
			 Double4{0.0, 1.0, -1.0, 2.0},
			 Double4{0.0, -0.0, 1.0, -1.0},
			 Double4{1.0, 2.0, -2.0, 3.0},
			 Double4{-3.0, 4.0, g_MillionDouble, g_InfDouble}};

	// eval vectorRound
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorRound(loadDouble4(arg)));

			checkStoreEq(expectRes, result);
		},
		bankersRes, toRound);

	// eval vectorFloor
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorFloor(loadDouble4(arg)));

			checkStoreEq(expectRes, result);
		},
		floorRes, toRound);

	// eval vectorCeiling
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorCeiling(loadDouble4(arg)));

			checkStoreEq(expectRes, result);
		},
		ceilingRes, toRound);

	// eval vectorTruncate
	callEvalFunc(
		[](const Double4& expectRes, const Double4& arg) -> void {
			Double4 result;
			storeDouble4(result, vectorTruncate(loadDouble4(arg)));

			checkStoreEq(expectRes, result);
		},
		truncateRes, toRound);
}

void evalReorderUtils()
{
	constexpr static const size_t arrSize = 8;

	// eval vectorSelectControl
	using TupleBool = std::tuple<bool, bool, bool, bool>;
	callEvalFunc(
		[](const Integer4& expectRes, const bool& argOne,
		   const bool& argTwo, const bool& argThree,
		   const bool& argFour) -> void {
			Integer4 result;
			storeInteger4(
				result, vectorSelectControl(argOne, argTwo, argThree,
											argFour));

			checkStoreEq(expectRes, result);
		},
		std::array<Integer4, arrSize>{
			Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
			Integer4{g_TrueVal, g_TrueVal, 0, 0},
			Integer4{g_TrueVal, 0, g_TrueVal, 0},
			Integer4{g_TrueVal, 0, 0, g_TrueVal},
			Integer4{0, g_TrueVal, g_TrueVal, 0},
			Integer4{0, g_TrueVal, 0, g_TrueVal},
			Integer4{0, 0, g_TrueVal, g_TrueVal},
			Integer4{0, 0, 0, 0}},
		std::array<TupleBool, arrSize>{
			TupleBool{true, true, true, true},
			TupleBool{true, true, false, false},
			TupleBool{true, false, true, false},
			TupleBool{true, false, false, true},
			TupleBool{false, true, true, false},
			TupleBool{false, true, false, true},
			TupleBool{false, false, true, true},
			TupleBool{false, false, false, false}});

	// eval vectorReplicate (int)
	using TupleInt = std::tuple<int_least64_t>;
	callEvalFunc(
		[](const Integer4&		expectRes,
		   const int_least64_t& value) -> void {
			Integer4 result;
			storeInteger4(result, vectorReplicate(value));

			checkStoreEq(expectRes, result);
		},
		std::array<Integer4, arrSize>{
			Integer4{0, 0, 0, 0}, Integer4{1, 1, 1, 1},
			Integer4{-1, -1, -1, -1},
			Integer4{2'000'000, 2'000'000, 2'000'000, 2'000'000},
			Integer4{-1'987, -1'987, -1'987, -1'987},
			Integer4{3'456, 3'456, 3'456, 3'456},
			Integer4{g_MinInt, g_MinInt, g_MinInt, g_MinInt},
			Integer4{g_MaxInt, g_MaxInt, g_MaxInt, g_MaxInt}},
		std::array<TupleInt, arrSize>{
			TupleInt{0}, TupleInt{1}, TupleInt{-1},
			TupleInt{2'000'000}, TupleInt{-1'987}, TupleInt{3'456},
			TupleInt{g_MinInt}, TupleInt{g_MaxInt}});

	// eval vectorReplicate (double)
	using TupleDbl = std::tuple<double>;
	callEvalFunc(
		[](const Double4& expectRes, const double& value) -> void {
			Double4 result;
			storeDouble4(result, vectorReplicate(value));

			checkStoreEq(expectRes, result);
		},
		std::array<Double4, arrSize>{
			Double4{0.0, 0.0, 0.0, 0.0}, Double4{1.0, 1.0, 1.0, 1.0},
			Double4{-1.0, -1.0, -1.0, -1.0},
			Double4{g_PI, g_PI, g_PI, g_PI},
			Double4{-10.78, -10.78, -10.78, -10.78},
			Double4{g_MillionDouble, g_MillionDouble, g_MillionDouble,
					g_MillionDouble},
			Double4{-g_MillionDouble, -g_MillionDouble,
					-g_MillionDouble, -g_MillionDouble},
			Double4{g_InfDouble, g_InfDouble, g_InfDouble,
					g_InfDouble}},
		std::array<TupleDbl, arrSize>{
			TupleDbl{0.0}, TupleDbl{1.0}, TupleDbl{-1.0},
			TupleDbl{g_PI}, TupleDbl{-10.78},
			TupleDbl{g_MillionDouble}, TupleDbl{-g_MillionDouble},
			TupleDbl{g_InfDouble}});
}
void evalDblSplats()
{
	constexpr static const size_t arrSize = 4;

	using TupleSplat = std::tuple<Double4>;
	using ArrayIn	 = std::array<TupleSplat, arrSize>;
	using ArrayRes	 = std::array<Double4, arrSize>;

	// splat data
	auto splatData =
		ArrayIn{TupleSplat{Double4{1.0, -2.0, -3.0, 4.0}},
				TupleSplat{Double4{0.0, g_PI, 0.5, -0.345}},
				TupleSplat{Double4{g_MillionDouble, -g_MillionDouble,
								   -2'340.7, 43'987.31}},
				TupleSplat{Double4{g_InfDouble, -g_InfDouble, -21.08,
								   345.4}}};

	auto splatXRes =
			 ArrayRes{Double4{1.0, 1.0, 1.0, 1.0},
					  Double4{0.0, 0.0, 0.0, 0.0},
					  Double4{g_MillionDouble, g_MillionDouble,
							  g_MillionDouble, g_MillionDouble},
					  Double4{g_InfDouble, g_InfDouble, g_InfDouble,
							  g_InfDouble}},
		 splatYRes =
			 ArrayRes{Double4{-2.0, -2.0, -2.0, -2.0},
					  Double4{g_PI, g_PI, g_PI, g_PI},
					  Double4{-g_MillionDouble, -g_MillionDouble,
							  -g_MillionDouble, -g_MillionDouble},
					  Double4{-g_InfDouble, -g_InfDouble,
							  -g_InfDouble, -g_InfDouble}},
		 splatZRes =
			 ArrayRes{Double4{-3.0, -3.0, -3.0, -3.0},
					  Double4{0.5, 0.5, 0.5, 0.5},
					  Double4{-2'340.7, -2'340.7, -2'340.7, -2'340.7},
					  Double4{-21.08, -21.08, -21.08, -21.08}},
		 splatWRes = ArrayRes{
			 Double4{4.0, 4.0, 4.0, 4.0},
			 Double4{-0.345, -0.345, -0.345, -0.345},
			 Double4{43'987.31, 43'987.31, 43'987.31, 43'987.31},
			 Double4{345.4, 345.4, 345.4, 345.4}};

	// eval vectorSplatX
	callEvalFunc(
		[](const Double4& expectRes, const Double4& data) -> void {
			Double4 result;
			storeDouble4(result, vectorSplatX(loadDouble4(data)));

			checkStoreEq(expectRes, result);
		},
		splatXRes, splatData);

	// eval vectorSplatY
	callEvalFunc(
		[](const Double4& expectRes, const Double4& data) -> void {
			Double4 result;
			storeDouble4(result, vectorSplatY(loadDouble4(data)));

			checkStoreEq(expectRes, result);
		},
		splatYRes, splatData);

	// eval vectorSplatZ
	callEvalFunc(
		[](const Double4& expectRes, const Double4& data) -> void {
			Double4 result;
			storeDouble4(result, vectorSplatZ(loadDouble4(data)));

			checkStoreEq(expectRes, result);
		},
		splatZRes, splatData);

	// eval vectorSplatW
	callEvalFunc(
		[](const Double4& expectRes, const Double4& data) -> void {
			Double4 result;
			storeDouble4(result, vectorSplatW(loadDouble4(data)));

			checkStoreEq(expectRes, result);
		},
		splatWRes, splatData);
}
void evalIntSplats()
{
	constexpr static const size_t arrSize = 4;

	using TupleSplat = std::tuple<Integer4>;
	using ArrayIn	 = std::array<TupleSplat, arrSize>;
	using ArrayRes	 = std::array<Integer4, arrSize>;

	// splat data
	auto splatData =
		ArrayIn{TupleSplat{Integer4{0, -1, 1, 10}},
				TupleSplat{Integer4{69, 420, 123, 7}},
				TupleSplat{Integer4{10'456, 876'543, 90'456, 2'456}},
				TupleSplat{Integer4{g_MaxInt, g_MinInt, 1'456'987,
									-987'765'432}}};

	auto splatXRes =
			 ArrayRes{
				 Integer4{0, 0, 0, 0}, Integer4{69, 69, 69, 69},
				 Integer4{10'456, 10'456, 10'456, 10'456},
				 Integer4{g_MaxInt, g_MaxInt, g_MaxInt, g_MaxInt}},
		 splatYRes =
			 ArrayRes{
				 Integer4{-1, -1, -1, -1},
				 Integer4{420, 420, 420, 420},
				 Integer4{876'543, 876'543, 876'543, 876'543},
				 Integer4{g_MinInt, g_MinInt, g_MinInt, g_MinInt}},
		 splatZRes =
			 ArrayRes{Integer4{1, 1, 1, 1},
					  Integer4{123, 123, 123, 123},
					  Integer4{90'456, 90'456, 90'456, 90'456},
					  Integer4{1'456'987, 1'456'987, 1'456'987,
							   1'456'987}},
		 splatWRes =
			 ArrayRes{Integer4{10, 10, 10, 10}, Integer4{7, 7, 7, 7},
					  Integer4{2'456, 2'456, 2'456, 2'456},
					  Integer4{-987'765'432, -987'765'432,
							   -987'765'432, -987'765'432}};

	// eval vectorSplatX
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& data) -> void {
			Integer4 result;
			storeInteger4(result, vectorSplatX(loadInteger4(data)));

			checkStoreEq(expectRes, result);
		},
		splatXRes, splatData);

	// eval vectorSplatY
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& data) -> void {
			Integer4 result;
			storeInteger4(result, vectorSplatY(loadInteger4(data)));

			checkStoreEq(expectRes, result);
		},
		splatYRes, splatData);

	// eval vectorSplatZ
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& data) -> void {
			Integer4 result;
			storeInteger4(result, vectorSplatZ(loadInteger4(data)));

			checkStoreEq(expectRes, result);
		},
		splatZRes, splatData);

	// eval vectorSplatW
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& data) -> void {
			Integer4 result;
			storeInteger4(result, vectorSplatW(loadInteger4(data)));

			checkStoreEq(expectRes, result);
		},
		splatWRes, splatData);
}
void evalDblRotates()
{
	constexpr static const size_t arrSize = 8;

	using TupleRot = std::tuple<Double4, int_least64_t>;
	using ArrayIn  = std::array<TupleRot, arrSize>;
	using ArrayRes = std::array<Double4, arrSize>;

	Double4 rawData[2] = {Double4{0.0, 1.0, -1.0, g_PI},
						  Double4{g_MinDouble, g_MaxDouble,
								  -g_MillionDouble, g_InfDouble}};

	// rot data
	auto rotData =
		ArrayIn{TupleRot{rawData[0], 0}, TupleRot{rawData[0], 1},
				TupleRot{rawData[0], 2}, TupleRot{rawData[0], 3},
				TupleRot{rawData[1], 0}, TupleRot{rawData[1], 1},
				TupleRot{rawData[1], 2}, TupleRot{rawData[1], 3}};

	auto rotLeftRes =
			 ArrayRes{rawData[0],
					  Double4{1.0, -1.0, g_PI, 0.0},
					  Double4{-1.0, g_PI, 0.0, 1.0},
					  Double4{g_PI, 0.0, 1.0, -1.0},
					  rawData[1],
					  Double4{g_MaxDouble, -g_MillionDouble,
							  g_InfDouble, g_MinDouble},
					  Double4{-g_MillionDouble, g_InfDouble,
							  g_MinDouble, g_MaxDouble},
					  Double4{g_InfDouble, g_MinDouble, g_MaxDouble,
							  -g_MillionDouble}},
		 rotRightRes =
			 ArrayRes{rawData[0],
					  Double4{g_PI, 0.0, 1.0, -1.0},
					  Double4{-1.0, g_PI, 0.0, 1.0},
					  Double4{1.0, -1.0, g_PI, 0.0},
					  rawData[1],
					  Double4{g_InfDouble, g_MinDouble, g_MaxDouble,
							  -g_MillionDouble},
					  Double4{-g_MillionDouble, g_InfDouble,
							  g_MinDouble, g_MaxDouble},
					  Double4{g_MaxDouble, -g_MillionDouble,
							  g_InfDouble, g_MinDouble}};

	// eval vectorRotateLeft
	callEvalFunc(
		[](const Double4& expectRes, const Double4& data,
		   const int_least64_t& amount) -> void {
			Double4 result;
			storeDouble4(result,
						 vectorRotateLeft(loadDouble4(data), amount));

			checkStoreEq(expectRes, result);
		},
		rotLeftRes, rotData);

	// eval vectorRotateRight
	callEvalFunc(
		[](const Double4& expectRes, const Double4& data,
		   const int_least64_t& amount) -> void {
			Double4 result;
			storeDouble4(
				result, vectorRotateRight(loadDouble4(data), amount));

			checkStoreEq(expectRes, result);
		},
		rotRightRes, rotData);
}
void evalIntRotates()
{
	constexpr static const size_t arrSize = 8;

	using TupleRot = std::tuple<Integer4, int_least64_t>;
	using ArrayIn  = std::array<TupleRot, arrSize>;
	using ArrayRes = std::array<Integer4, arrSize>;

	Integer4 rawData[2] = {
		Integer4{0, 1, -1, -783},
		Integer4{9'876'543, -1'234'567, g_MinInt, g_MaxInt}};

	// rot data
	auto rotData =
		ArrayIn{TupleRot{rawData[0], 0}, TupleRot{rawData[0], 1},
				TupleRot{rawData[0], 2}, TupleRot{rawData[0], 3},
				TupleRot{rawData[1], 0}, TupleRot{rawData[1], 1},
				TupleRot{rawData[1], 2}, TupleRot{rawData[1], 3}};

	auto rotLeftRes =
			 ArrayRes{
				 rawData[0],
				 Integer4{1, -1, -783, 0},
				 Integer4{-1, -783, 0, 1},
				 Integer4{-783, 0, 1, -1},
				 rawData[1],
				 Integer4{-1'234'567, g_MinInt, g_MaxInt, 9'876'543},
				 Integer4{g_MinInt, g_MaxInt, 9'876'543, -1'234'567},
				 Integer4{g_MaxInt, 9'876'543, -1'234'567, g_MinInt}},
		 rotRightRes = ArrayRes{
			 rawData[0],
			 Integer4{-783, 0, 1, -1},
			 Integer4{-1, -783, 0, 1},
			 Integer4{1, -1, -783, 0},
			 rawData[1],
			 Integer4{g_MaxInt, 9'876'543, -1'234'567, g_MinInt},
			 Integer4{g_MinInt, g_MaxInt, 9'876'543, -1'234'567},
			 Integer4{-1'234'567, g_MinInt, g_MaxInt, 9'876'543}};

	// eval vectorRotateLeft
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& data,
		   const int_least64_t& amount) -> void {
			Integer4 result;
			storeInteger4(
				result, vectorRotateLeft(loadInteger4(data), amount));

			checkStoreEq(expectRes, result);
		},
		rotLeftRes, rotData);

	// eval vectorRotateRight
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& data,
		   const int_least64_t& amount) -> void {
			Integer4 result;
			storeInteger4(result, vectorRotateRight(
									  loadInteger4(data), amount));

			checkStoreEq(expectRes, result);
		},
		rotRightRes, rotData);
}
void evalSwizzles()
{
	constexpr static const size_t arrSize = 8;

	using TupleDbl = std::tuple<Double4, int_least32_t, int_least32_t,
								int_least32_t, int_least32_t>;
	using TupleInt =
		std::tuple<Integer4, int_least32_t, int_least32_t,
				   int_least32_t, int_least32_t>;

	Double4 rawDblData[2] = {Double4{0.0, 1.0, -1.0, g_PI},
							 Double4{g_MinDouble, g_MaxDouble,
									 -g_MillionDouble, g_InfDouble}};

	// eval vectorSwizzle (double)
	callEvalFunc(
		[](const Double4& expectRes, const Double4& data,
		   const int_least32_t& szlOne, const int_least32_t& szlTwo,
		   const int_least32_t& szlThree,
		   const int_least32_t& szlFour) -> void {
			SmallInt4 combIN = {szlOne, szlTwo, szlThree, szlFour};

			Double4 sepResult, combResult;

			storeDouble4(combResult,
						 vectorSwizzle(loadDouble4(data),
									   loadSmallInt4(combIN)));
			checkStoreEq(expectRes, combResult);

			storeDouble4(sepResult,
						 vectorSwizzle(loadDouble4(data), szlOne,
									   szlTwo, szlThree, szlFour));
			checkStoreEq(expectRes, sepResult);
		},
		std::array<Double4, arrSize>{
			Double4{0.0, 1.0, -1.0, g_PI},
			Double4{0.0, -1.0, 1.0, g_PI},
			Double4{0.0, 0.0, 0.0, 0.0}, Double4{1.0, 1.0, 1.0, 1.0},
			Double4{g_MinDouble, g_MaxDouble, g_InfDouble,
					g_MinDouble},
			Double4{-g_MillionDouble, g_MaxDouble, -g_MillionDouble,
					g_MinDouble},
			Double4{-g_MillionDouble, -g_MillionDouble,
					-g_MillionDouble, -g_MillionDouble},
			Double4{g_InfDouble, g_InfDouble, g_InfDouble,
					g_InfDouble}},
		std::array<TupleDbl, arrSize>{
			TupleDbl{rawDblData[0], 0, 1, 2, 3},
			TupleDbl{rawDblData[0], 0, 2, 1, 3},
			TupleDbl{rawDblData[0], 0, 0, 0, 0},
			TupleDbl{rawDblData[0], 1, 1, 1, 1},
			TupleDbl{rawDblData[1], 0, 1, 3, 0},
			TupleDbl{rawDblData[1], 2, 1, 2, 0},
			TupleDbl{rawDblData[1], 2, 2, 2, 2},
			TupleDbl{rawDblData[1], 3, 3, 3, 3}});

	Integer4 rawIntData[2] = {
		Integer4{0, 1, -1, -9'765},
		Integer4{5'689'369, -34'899'455, g_MinInt, g_MaxInt}};

	// eval vectorSwizzle (int)
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& data,
		   const int_least32_t& szlOne, const int_least32_t& szlTwo,
		   const int_least32_t& szlThree,
		   const int_least32_t& szlFour) -> void {
			SmallInt4 combIN = {szlOne, szlTwo, szlThree, szlFour};

			Integer4 sepResult, combResult;
			storeInteger4(combResult,
						  vectorSwizzle(loadInteger4(data),
										loadSmallInt4(combIN)));
			checkStoreEq(expectRes, combResult);

			storeInteger4(sepResult,
						  vectorSwizzle(loadInteger4(data), szlOne,
										szlTwo, szlThree, szlFour));
			checkStoreEq(expectRes, sepResult);
		},
		std::array<Integer4, arrSize>{
			Integer4{0, 1, -1, -9'765}, Integer4{0, -1, 1, -9'765},
			Integer4{0, 0, 0, 0}, Integer4{1, 1, 1, 1},
			Integer4{5'689'369, -34'899'455, g_MaxInt, 5'689'369},
			Integer4{g_MinInt, -34'899'455, g_MinInt, 5'689'369},
			Integer4{g_MinInt, g_MinInt, g_MinInt, g_MinInt},
			Integer4{g_MaxInt, g_MaxInt, g_MaxInt, g_MaxInt}},
		std::array<TupleInt, arrSize>{
			TupleInt{rawIntData[0], 0, 1, 2, 3},
			TupleInt{rawIntData[0], 0, 2, 1, 3},
			TupleInt{rawIntData[0], 0, 0, 0, 0},
			TupleInt{rawIntData[0], 1, 1, 1, 1},
			TupleInt{rawIntData[1], 0, 1, 3, 0},
			TupleInt{rawIntData[1], 2, 1, 2, 0},
			TupleInt{rawIntData[1], 2, 2, 2, 2},
			TupleInt{rawIntData[1], 3, 3, 3, 3}});
}
void evalDblMerges()
{
	constexpr static const size_t arrSize = 4;

	using TupleMrg = std::tuple<Double4, Double4>;
	using ArrayIn  = std::array<TupleMrg, arrSize>;
	using ArrayRes = std::array<Double4, arrSize>;

	Double4 rawDblData[3] = {
		Double4{0.0, 1.0, -1.0, g_PI},
		Double4{g_MinDouble, g_MaxDouble, -g_MillionDouble,
				g_InfDouble},
		Double4{g_Epsilon, -g_MinDouble, -g_InfDouble, -g_PI}};

	// merge data
	auto mergeData = ArrayIn{TupleMrg{rawDblData[0], rawDblData[1]},
							 TupleMrg{rawDblData[1], rawDblData[0]},
							 TupleMrg{rawDblData[0], rawDblData[2]},
							 TupleMrg{rawDblData[2], rawDblData[1]}};

	auto mergeXYRes =
			 ArrayRes{Double4{0.0, g_MinDouble, 1.0, g_MaxDouble},
					  Double4{g_MinDouble, 0.0, g_MaxDouble, 1.0},
					  Double4{0.0, g_Epsilon, 1.0, -g_MinDouble},
					  Double4{g_Epsilon, g_MinDouble, -g_MinDouble,
							  g_MaxDouble}},
		 mergeZWRes = ArrayRes{
			 Double4{-1.0, -g_MillionDouble, g_PI, g_InfDouble},
			 Double4{-g_MillionDouble, -1.0, g_InfDouble, g_PI},
			 Double4{-1.0, -g_InfDouble, g_PI, -g_PI},
			 Double4{-g_InfDouble, -g_MillionDouble, -g_PI,
					 g_InfDouble}};

	// eval vectorMergeXY
	callEvalFunc(
		[](const Double4& expectRes, const Double4& lhsData,
		   const Double4& rhsData) -> void {
			Double4 result;
			storeDouble4(result, vectorMergeXY(loadDouble4(lhsData),
											   loadDouble4(rhsData)));

			checkStoreEq(expectRes, result);
		},
		mergeXYRes, mergeData);

	// eval vectorMergeZW
	callEvalFunc(
		[](const Double4& expectRes, const Double4& lhsData,
		   const Double4& rhsData) -> void {
			Double4 result;
			storeDouble4(result, vectorMergeZW(loadDouble4(lhsData),
											   loadDouble4(rhsData)));

			checkStoreEq(expectRes, result);
		},
		mergeZWRes, mergeData);
}
void evalIntMerges()
{
	constexpr static const size_t arrSize = 4;

	using TupleMrg = std::tuple<Integer4, Integer4>;
	using ArrayIn  = std::array<TupleMrg, arrSize>;
	using ArrayRes = std::array<Integer4, arrSize>;

	Integer4 rawIntData[3] = {
		Integer4{0, 1, -1, -9'765},
		Integer4{5'689'369, -34'899'455, g_MinInt, g_MaxInt},
		Integer4{23, -459, 69'420, -987'123'456}};

	// merge data
	auto mergeData = ArrayIn{TupleMrg{rawIntData[0], rawIntData[1]},
							 TupleMrg{rawIntData[1], rawIntData[0]},
							 TupleMrg{rawIntData[0], rawIntData[2]},
							 TupleMrg{rawIntData[2], rawIntData[1]}};

	auto mergeXYRes =
			 ArrayRes{Integer4{0, 5'689'369, 1, -34'899'455},
					  Integer4{5'689'369, 0, -34'899'455, 1},
					  Integer4{0, 23, 1, -459},
					  Integer4{23, 5'689'369, -459, -34'899'455}},
		 mergeZWRes = ArrayRes{
			 Integer4{-1, g_MinInt, -9'765, g_MaxInt},
			 Integer4{g_MinInt, -1, g_MaxInt, -9'765},
			 Integer4{-1, 69'420, -9'765, -987'123'456},
			 Integer4{69'420, g_MinInt, -987'123'456, g_MaxInt}};

	// eval vectorMergeXY
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& lhsData,
		   const Integer4& rhsData) -> void {
			Integer4 result;
			storeInteger4(result,
						  vectorMergeXY(loadInteger4(lhsData),
										loadInteger4(rhsData)));

			checkStoreEq(expectRes, result);
		},
		mergeXYRes, mergeData);

	// eval vectorMergeZW
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& lhsData,
		   const Integer4& rhsData) -> void {
			Integer4 result;
			storeInteger4(result,
						  vectorMergeZW(loadInteger4(lhsData),
										loadInteger4(rhsData)));

			checkStoreEq(expectRes, result);
		},
		mergeZWRes, mergeData);
}
void evalPermutes()
{
	constexpr static const size_t arrSize = 8;

	using TupleDbl =
		std::tuple<Double4, Double4, int_least32_t, int_least32_t,
				   int_least32_t, int_least32_t>;
	using TupleInt =
		std::tuple<Integer4, Integer4, int_least32_t, int_least32_t,
				   int_least32_t, int_least32_t>;

	Double4 rawDblData[2] = {Double4{0.0, 1.0, -1.0, g_PI},
							 Double4{g_MinDouble, g_MaxDouble,
									 -g_MillionDouble, g_InfDouble}};

	// eval vectorPermute (double)
	callEvalFunc(
		[](const Double4& expectRes, const Double4& lhsData,
		   const Double4& rhsData, const int_least32_t& szlOne,
		   const int_least32_t& szlTwo, const int_least32_t& szlThree,
		   const int_least32_t& szlFour) -> void {
			SmallInt4 combIN = {szlOne, szlTwo, szlThree, szlFour};

			Double4 sepResult, combResult;

			storeDouble4(combResult,
						 vectorPermute(loadDouble4(lhsData),
									   loadDouble4(rhsData),
									   loadSmallInt4(combIN)));
			checkStoreEq(expectRes, combResult);

			storeDouble4(sepResult,
						 vectorPermute(loadDouble4(lhsData),
									   loadDouble4(rhsData), szlOne,
									   szlTwo, szlThree, szlFour));
			checkStoreEq(expectRes, sepResult);
		},
		std::array<Double4, arrSize>{
			Double4{0.0, 1.0, -1.0, g_InfDouble},
			Double4{-1.0, -g_MillionDouble, 1.0, g_InfDouble},
			Double4{g_MinDouble, g_MinDouble, g_MinDouble,
					g_MinDouble},
			Double4{1.0, 1.0, 1.0, 1.0},
			Double4{g_MinDouble, g_MaxDouble, g_InfDouble,
					g_MinDouble},
			Double4{-g_MillionDouble, g_MaxDouble, -1.0, g_MinDouble},
			Double4{g_MinDouble, g_MaxDouble, -g_MillionDouble,
					g_InfDouble},
			Double4{g_PI, g_PI, g_InfDouble, g_InfDouble}},
		std::array<TupleDbl, arrSize>{
			TupleDbl{rawDblData[0], rawDblData[1], 0, 1, 2, 7},
			TupleDbl{rawDblData[0], rawDblData[1], 2, 6, 1, 7},
			TupleDbl{rawDblData[1], rawDblData[1], 0, 4, 4, 0},
			TupleDbl{rawDblData[0], rawDblData[0], 1, 1, 1, 1},
			TupleDbl{rawDblData[1], rawDblData[1], 4, 5, 3, 4},
			TupleDbl{rawDblData[1], rawDblData[0], 2, 1, 6, 0},
			TupleDbl{rawDblData[0], rawDblData[1], 4, 5, 6, 7},
			TupleDbl{rawDblData[1], rawDblData[0], 7, 7, 3, 3}});

	Integer4 rawIntData[2] = {
		Integer4{0, 1, -1, -9'765},
		Integer4{5'689'369, -34'899'455, g_MinInt, g_MaxInt}};

	// eval vectorPermute (int)
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& lhsData,
		   const Integer4& rhsData, const int_least32_t& szlOne,
		   const int_least32_t& szlTwo, const int_least32_t& szlThree,
		   const int_least32_t& szlFour) -> void {
			SmallInt4 combIN = {szlOne, szlTwo, szlThree, szlFour};

			Integer4 sepResult, combResult;

			storeInteger4(combResult,
						  vectorPermute(loadInteger4(lhsData),
										loadInteger4(rhsData),
										loadSmallInt4(combIN)));
			checkStoreEq(expectRes, combResult);

			storeInteger4(sepResult,
						  vectorPermute(loadInteger4(lhsData),
										loadInteger4(rhsData), szlOne,
										szlTwo, szlThree, szlFour));
			checkStoreEq(expectRes, sepResult);
		},
		std::array<Integer4, arrSize>{
			Integer4{0, 1, -1, g_MaxInt},
			Integer4{-1, g_MinInt, 1, g_MaxInt},
			Integer4{5'689'369, 5'689'369, 5'689'369, 5'689'369},
			Integer4{1, 1, 1, 1},
			Integer4{5'689'369, -34'899'455, g_MaxInt, 5'689'369},
			Integer4{g_MinInt, -34'899'455, -1, 5'689'369},
			Integer4{5'689'369, -34'899'455, g_MinInt, g_MaxInt},
			Integer4{-9'765, -9'765, g_MaxInt, g_MaxInt}},
		std::array<TupleInt, arrSize>{
			TupleInt{rawIntData[0], rawIntData[1], 0, 1, 2, 7},
			TupleInt{rawIntData[0], rawIntData[1], 2, 6, 1, 7},
			TupleInt{rawIntData[1], rawIntData[1], 0, 4, 4, 0},
			TupleInt{rawIntData[0], rawIntData[0], 1, 1, 1, 1},
			TupleInt{rawIntData[1], rawIntData[1], 4, 5, 3, 4},
			TupleInt{rawIntData[1], rawIntData[0], 2, 1, 6, 0},
			TupleInt{rawIntData[0], rawIntData[1], 4, 5, 6, 7},
			TupleInt{rawIntData[1], rawIntData[0], 7, 7, 3, 3}});
}
void evalSelects()
{
	constexpr static const size_t arrSize = 8;

	using TupleDbl = std::tuple<Double4, Double4, Integer4>;
	using TupleInt = std::tuple<Integer4, Integer4, Integer4>;

	Integer4 controls[8] = {
		Integer4{g_TrueVal, g_TrueVal, g_TrueVal, g_TrueVal},
		Integer4{g_TrueVal, g_TrueVal, 0, 0},
		Integer4{g_TrueVal, 0, g_TrueVal, 0},
		Integer4{g_TrueVal, 0, 0, g_TrueVal},
		Integer4{0, g_TrueVal, g_TrueVal, 0},
		Integer4{0, g_TrueVal, 0, g_TrueVal},
		Integer4{0, 0, g_TrueVal, g_TrueVal},
		Integer4{0, 0, 0, 0}};

	Double4 rawDblData[2] = {Double4{0.0, 1.0, -1.0, g_PI},
							 Double4{g_MinDouble, g_MaxDouble,
									 -g_MillionDouble, g_InfDouble}};

	// eval vectorSelect (double)
	callEvalFunc(
		[](const Double4& expectRes, const Double4& lhsData,
		   const Double4& rhsData, const Integer4& control) -> void {
			Double4 result;
			storeDouble4(result, vectorSelect(loadDouble4(lhsData),
											  loadDouble4(rhsData),
											  loadInteger4(control)));

			checkStoreEq(expectRes, result);
		},
		std::array<Double4, arrSize>{
			Double4{g_MinDouble, g_MaxDouble, -g_MillionDouble,
					g_InfDouble},
			Double4{g_MinDouble, g_MaxDouble, -1.0, g_PI},
			Double4{g_MinDouble, g_MaxDouble, -g_MillionDouble,
					g_InfDouble},
			Double4{0.0, 1.0, -1.0, g_PI},
			Double4{g_MinDouble, 1.0, -1.0, g_InfDouble},
			Double4{g_MinDouble, 1.0, -g_MillionDouble, g_PI},
			Double4{0.0, 1.0, -g_MillionDouble, g_InfDouble},
			Double4{g_MinDouble, g_MaxDouble, -g_MillionDouble,
					g_InfDouble}},
		std::array<TupleDbl, arrSize>{
			TupleDbl{rawDblData[0], rawDblData[1], controls[0]},
			TupleDbl{rawDblData[0], rawDblData[1], controls[1]},
			TupleDbl{rawDblData[1], rawDblData[1], controls[2]},
			TupleDbl{rawDblData[0], rawDblData[0], controls[3]},
			TupleDbl{rawDblData[1], rawDblData[0], controls[4]},
			TupleDbl{rawDblData[1], rawDblData[0], controls[5]},
			TupleDbl{rawDblData[0], rawDblData[1], controls[6]},
			TupleDbl{rawDblData[1], rawDblData[0], controls[7]}});

	Integer4 rawIntData[2] = {
		Integer4{0, 1, -1, -9'765},
		Integer4{5'689'369, -34'899'455, g_MinInt, g_MaxInt}};

	// eval vectorSelect (int)
	callEvalFunc(
		[](const Integer4& expectRes, const Integer4& lhsData,
		   const Integer4& rhsData, const Integer4& control) -> void {
			Integer4 result;
			storeInteger4(result,
						  vectorSelect(loadInteger4(lhsData),
									   loadInteger4(rhsData),
									   loadInteger4(control)));

			checkStoreEq(expectRes, result);
		},
		std::array<Integer4, arrSize>{
			Integer4{5'689'369, -34'899'455, g_MinInt, g_MaxInt},
			Integer4{5'689'369, -34'899'455, -1, -9'765},
			Integer4{5'689'369, -34'899'455, g_MinInt, g_MaxInt},
			Integer4{0, 1, -1, -9'765},
			Integer4{5'689'369, 1, -1, g_MaxInt},
			Integer4{5'689'369, 1, g_MinInt, -9'765},
			Integer4{0, 1, g_MinInt, g_MaxInt},
			Integer4{5'689'369, -34'899'455, g_MinInt, g_MaxInt}},
		std::array<TupleInt, arrSize>{
			TupleInt{rawIntData[0], rawIntData[1], controls[0]},
			TupleInt{rawIntData[0], rawIntData[1], controls[1]},
			TupleInt{rawIntData[1], rawIntData[1], controls[2]},
			TupleInt{rawIntData[0], rawIntData[0], controls[3]},
			TupleInt{rawIntData[1], rawIntData[0], controls[4]},
			TupleInt{rawIntData[1], rawIntData[0], controls[5]},
			TupleInt{rawIntData[0], rawIntData[1], controls[6]},
			TupleInt{rawIntData[1], rawIntData[0], controls[7]}});
}

void eval4DTwoMtrxComparisons()
{
	constexpr static const size_t arrSize = 6;

	using TupleComp = std::tuple<Double4x4, Double4x4>;
	using ArrayIn	= std::array<TupleComp, arrSize>;
	using ArrayRes	= std::array<bool, arrSize>;

	// raw rows
	Double4 rows[5] = {
		Double4{11.3, -1.0, 1.0, -999.111},
		Double4{-g_MaxDouble, 399'616.33, g_Epsilon, g_MaxDouble},
		Double4{g_MillionDouble, g_MinDouble, -g_MinDouble, -345.9},
		Double4{0.0, 4.5, -g_InfDouble, -90.8},
		Double4{0.0, 4.5, g_QNaN, -90.8}};

	// raw matrices
	Double4x4 matrices[3] = {
		Double4x4{rows[0], rows[1], rows[1], rows[2]},
		Double4x4{rows[2], rows[1], rows[3], rows[3]},
		Double4x4{rows[2], rows[1], rows[4], rows[3]}};

	// comp data
	auto compData = ArrayIn{TupleComp{matrices[0], matrices[0]},
							TupleComp{matrices[0], matrices[1]},
							TupleComp{matrices[0], matrices[2]},
							TupleComp{matrices[1], matrices[1]},
							TupleComp{matrices[1], matrices[2]},
							TupleComp{matrices[2], matrices[2]}};

	auto equalRes = ArrayRes{true, false, false, true, false, false},
		 notEqualRes = ArrayRes{false, true, true, false, true, true};

	// eval matrix4DEqual
	callEvalFunc(
		[](const bool& expectRes, const Double4x4& lhsComp,
		   const Double4x4& rhsComp) -> void {
			bool result = matrix4DEqual(loadDouble4x4(lhsComp),
										loadDouble4x4(rhsComp));

			CHECK_EQ(result, expectRes);
		},
		equalRes, compData);

	// eval matrix4DNotEqual
	callEvalFunc(
		[](const bool& expectRes, const Double4x4& lhsComp,
		   const Double4x4& rhsComp) -> void {
			bool result = matrix4DNotEqual(loadDouble4x4(lhsComp),
										   loadDouble4x4(rhsComp));

			CHECK_EQ(result, expectRes);
		},
		notEqualRes, compData);
}
void eval4DOneMtrxComparisons()
{
	constexpr static const size_t arrSize = 6;

	using TupleCheck = std::tuple<Double4x4>;
	using ArrayIn	 = std::array<TupleCheck, arrSize>;
	using ArrayRes	 = std::array<bool, arrSize>;

	Double4 rows[6] = {
		Double4{11.3, -1.0, 1.0, -999.111},
		Double4{-g_MaxDouble, 399'616.33, g_Epsilon, g_MaxDouble},
		Double4{g_MillionDouble, g_MinDouble, -g_MinDouble, -345.9},
		Double4{0.0, g_QNaN, g_InfDouble, -90.8},
		Double4{0.0, 4.5, -g_InfDouble, -90.8},
		Double4{0.0, 4.5, g_QNaN, -90.8}};

	// eval data
	auto evalData = ArrayIn{
		TupleCheck{Double4x4{rows[0], rows[1], rows[2], rows[5]}},
		TupleCheck{Double4x4{rows[0], rows[2], rows[2], rows[0]}},
		TupleCheck{Double4x4{rows[4], rows[2], rows[1], rows[1]}},
		TupleCheck{Double4x4{rows[2], rows[0], rows[3], rows[0]}},
		TupleCheck{Double4x4{rows[1], rows[2], rows[1], rows[2]}},
		TupleCheck{Double4x4{rows[1], rows[1], rows[0], rows[0]}}};

	auto isNaNRes = ArrayRes{true, false, false, true, false, false},
		 isInfRes = ArrayRes{false, false, true, true, false, false};

	// eval matrix4DIsNaN
	callEvalFunc(
		[](const bool& expectRes, const Double4x4& data) -> void {
			bool result = matrix4DIsNaN(loadDouble4x4(data));

			CHECK_EQ(result, expectRes);
		},
		isNaNRes, evalData);

	// eval matrix4DIsInf
	callEvalFunc(
		[](const bool& expectRes, const Double4x4& data) -> void {
			bool result = matrix4DIsInf(loadDouble4x4(data));

			CHECK_EQ(result, expectRes);
		},
		isInfRes, evalData);
}
void eval3DTwoMtrxComparisons()
{
	constexpr static const size_t arrSize = 6;

	using TupleComp = std::tuple<Double3x3, Double3x3>;
	using ArrayIn	= std::array<TupleComp, arrSize>;
	using ArrayRes	= std::array<bool, arrSize>;

	// raw rows
	Double3 rows[5] = {
		Double3{11.3, -1.0, 1.0},
		Double3{-g_MaxDouble, 399'616.33, g_Epsilon},
		Double3{g_MillionDouble, g_MinDouble, -g_MinDouble},
		Double3{0.0, 4.5, -g_InfDouble}, Double3{0.0, 4.5, g_QNaN}};

	// raw matrices
	Double3x3 matrices[3] = {Double3x3{rows[0], rows[1], rows[1]},
							 Double3x3{rows[2], rows[1], rows[3]},
							 Double3x3{rows[2], rows[3], rows[4]}};

	// comp data
	auto compData = ArrayIn{TupleComp{matrices[0], matrices[0]},
							TupleComp{matrices[0], matrices[1]},
							TupleComp{matrices[0], matrices[2]},
							TupleComp{matrices[1], matrices[1]},
							TupleComp{matrices[1], matrices[2]},
							TupleComp{matrices[2], matrices[2]}};

	auto equalRes = ArrayRes{true, false, false, true, false, false},
		 notEqualRes = ArrayRes{false, true, true, false, true, true};

	// eval matrix3DEqual
	callEvalFunc(
		[](const bool& expectRes, const Double3x3& lhsComp,
		   const Double3x3& rhsComp) -> void {
			bool result = matrix3DEqual(loadDouble3x3(lhsComp),
										loadDouble3x3(rhsComp));

			CHECK_EQ(result, expectRes);
		},
		equalRes, compData);

	// eval matrix3DNotEqual
	callEvalFunc(
		[](const bool& expectRes, const Double3x3& lhsComp,
		   const Double3x3& rhsComp) -> void {
			bool result = matrix3DNotEqual(loadDouble3x3(lhsComp),
										   loadDouble3x3(rhsComp));

			CHECK_EQ(result, expectRes);
		},
		notEqualRes, compData);
}
void eval3DOneMtrxComparisons()
{
	constexpr static const size_t arrSize = 6;

	using TupleCheck = std::tuple<Double3x3>;
	using ArrayIn	 = std::array<TupleCheck, arrSize>;
	using ArrayRes	 = std::array<bool, arrSize>;

	Double3 rows[6] = {
		Double3{11.3, -1.0, 1.0},
		Double3{-g_MaxDouble, 399'616.33, g_Epsilon},
		Double3{g_MillionDouble, g_MinDouble, -g_MinDouble},
		Double3{0.0, g_QNaN, g_InfDouble},
		Double3{0.0, 4.5, -g_InfDouble},
		Double3{0.0, 4.5, g_QNaN}};

	// eval data
	auto evalData =
		ArrayIn{TupleCheck{Double3x3{rows[0], rows[1], rows[5]}},
				TupleCheck{Double3x3{rows[0], rows[2], rows[2]}},
				TupleCheck{Double3x3{rows[4], rows[2], rows[1]}},
				TupleCheck{Double3x3{rows[2], rows[0], rows[3]}},
				TupleCheck{Double3x3{rows[1], rows[2], rows[1]}},
				TupleCheck{Double3x3{rows[1], rows[1], rows[0]}}};

	auto isNaNRes = ArrayRes{true, false, false, true, false, false},
		 isInfRes = ArrayRes{false, false, true, true, false, false};

	// eval matrix3DIsNaN
	callEvalFunc(
		[](const bool& expectRes, const Double3x3& data) -> void {
			bool result = matrix3DIsNaN(loadDouble3x3(data));

			CHECK_EQ(result, expectRes);
		},
		isNaNRes, evalData);

	// eval matrix3DIsInf
	callEvalFunc(
		[](const bool& expectRes, const Double3x3& data) -> void {
			bool result = matrix3DIsInf(loadDouble3x3(data));

			CHECK_EQ(result, expectRes);
		},
		isInfRes, evalData);
}
void eval2DTwoMtrxComparisons()
{
	constexpr static const size_t arrSize = 6;

	using TupleComp = std::tuple<Double2x2, Double2x2>;
	using ArrayIn	= std::array<TupleComp, arrSize>;
	using ArrayRes	= std::array<bool, arrSize>;

	// raw rows
	Double2 rows[5] = {
		Double2{-1.0, 1.0}, Double2{-g_MaxDouble, g_Epsilon},
		Double2{g_MillionDouble, -g_MinDouble},
		Double2{0.0, -g_InfDouble}, Double2{0.0, g_QNaN}};

	// raw matrices
	Double2x2 matrices[3] = {Double2x2{rows[0], rows[1]},
							 Double2x2{rows[2], rows[3]},
							 Double2x2{rows[3], rows[4]}};

	// comp data
	auto compData = ArrayIn{TupleComp{matrices[0], matrices[0]},
							TupleComp{matrices[0], matrices[1]},
							TupleComp{matrices[0], matrices[2]},
							TupleComp{matrices[1], matrices[1]},
							TupleComp{matrices[1], matrices[2]},
							TupleComp{matrices[2], matrices[2]}};

	auto equalRes = ArrayRes{true, false, false, true, false, false},
		 notEqualRes = ArrayRes{false, true, true, false, true, true};

	// eval matrix2DEqual
	callEvalFunc(
		[](const bool& expectRes, const Double2x2& lhsComp,
		   const Double2x2& rhsComp) -> void {
			bool result = matrix2DEqual(loadDouble2x2(lhsComp),
										loadDouble2x2(rhsComp));

			CHECK_EQ(result, expectRes);
		},
		equalRes, compData);

	// eval matrix2DNotEqual
	callEvalFunc(
		[](const bool& expectRes, const Double2x2& lhsComp,
		   const Double2x2& rhsComp) -> void {
			bool result = matrix2DNotEqual(loadDouble2x2(lhsComp),
										   loadDouble2x2(rhsComp));

			CHECK_EQ(result, expectRes);
		},
		notEqualRes, compData);
}
void eval2DOneMtrxComparisons()
{
	constexpr static const size_t arrSize = 6;

	using TupleCheck = std::tuple<Double2x2>;
	using ArrayIn	 = std::array<TupleCheck, arrSize>;
	using ArrayRes	 = std::array<bool, arrSize>;

	Double2 rows[6] = {Double2{-1.0, 1.0},
					   Double2{-g_MaxDouble, g_Epsilon},
					   Double2{g_MillionDouble, -g_MinDouble},
					   Double2{g_QNaN, g_InfDouble},
					   Double2{0.0, -g_InfDouble},
					   Double2{0.0, g_QNaN}};

	// eval data
	auto evalData = ArrayIn{TupleCheck{Double2x2{rows[0], rows[5]}},
							TupleCheck{Double2x2{rows[0], rows[2]}},
							TupleCheck{Double2x2{rows[4], rows[1]}},
							TupleCheck{Double2x2{rows[2], rows[3]}},
							TupleCheck{Double2x2{rows[1], rows[2]}},
							TupleCheck{Double2x2{rows[1], rows[1]}}};

	auto isNaNRes = ArrayRes{true, false, false, true, false, false},
		 isInfRes = ArrayRes{false, false, true, true, false, false};

	// eval matrix2DIsNaN
	callEvalFunc(
		[](const bool& expectRes, const Double2x2& data) -> void {
			bool result = matrix2DIsNaN(loadDouble2x2(data));

			CHECK_EQ(result, expectRes);
		},
		isNaNRes, evalData);

	// eval matrix2DIsInf
	callEvalFunc(
		[](const bool& expectRes, const Double2x2& data) -> void {
			bool result = matrix2DIsInf(loadDouble2x2(data));

			CHECK_EQ(result, expectRes);
		},
		isInfRes, evalData);
}

void eval4DVecMaths()
{
	constexpr static const size_t arrSize = 6;

	using ArrayRes	 = std::array<Double4, arrSize>;
	using TupleTwo	 = std::tuple<Double4, Double4>;
	using TupleOne	 = std::tuple<Double4>;
	using ArrayOneIn = std::array<TupleOne, arrSize>;

	Double4 rawVecs[6] = {Double4{1.0, 0.0, 0.0, 0.0},
						  Double4{0.0, 1.0, 0.0, 0.0},
						  Double4{5.0, -5.0, 5.0, -5.0},
						  Double4{1.5, 3.2, 3.4, 9.7},
						  Double4{-12.0, -1.0, 5.0, 0.2},
						  Double4{0.001, 0.004, 0.02, -0.04}};

	auto oneData =
		ArrayOneIn{TupleOne{rawVecs[0]}, TupleOne{rawVecs[1]},
				   TupleOne{rawVecs[2]}, TupleOne{rawVecs[3]},
				   TupleOne{rawVecs[4]}, TupleOne{rawVecs[5]}};
	auto twoData = std::array<TupleTwo, arrSize>{
		TupleTwo{rawVecs[0], rawVecs[0]},
		TupleTwo{rawVecs[0], rawVecs[1]},
		TupleTwo{rawVecs[2], rawVecs[0]},
		TupleTwo{rawVecs[2], rawVecs[1]},
		TupleTwo{rawVecs[2], rawVecs[2]},
		TupleTwo{rawVecs[2], rawVecs[3]}};

	// store the magnitudes of some harder to calculate vectors here,
	// to simplify later calculations
	double	 mags[3] = {std::sqrt(118.14), std::sqrt(170.04),
						std::sqrt(0.002017)};
	ArrayRes dotRes	 = ArrayRes{Double4{1.0, 1.0, 1.0, 1.0},
								Double4{0.0, 0.0, 0.0, 0.0},
								Double4{5.0, 5.0, 5.0, 5.0},
								Double4{-5.0, -5.0, -5.0, -5.0},
								Double4{100.0, 100.0, 100.0, 100.0},
								Double4{-40.0, -40.0, -40.0, -40.0}},
			 lengthSqRes =
				 ArrayRes{
					 Double4{1.0, 1.0, 1.0, 1.0},
					 Double4{1.0, 1.0, 1.0, 1.0},
					 Double4{100.0, 100.0, 100.0, 100.0},
					 Double4{118.14, 118.14, 118.14, 118.14},
					 Double4{170.04, 170.04, 170.04, 170.04},
					 Double4{0.002017, 0.002017, 0.002017, 0.002017}},
			 lengthRes =
				 ArrayRes{
					 Double4{1.0, 1.0, 1.0, 1.0},
					 Double4{1.0, 1.0, 1.0, 1.0},
					 Double4{10.0, 10.0, 10.0, 10.0},
					 Double4{mags[0], mags[0], mags[0], mags[0]},
					 Double4{mags[1], mags[1], mags[1], mags[1]},
					 Double4{mags[2], mags[2], mags[2], mags[2]}},
			 normalRes =
				 ArrayRes{rawVecs[0],
						  rawVecs[1],
						  Double4{0.5, -0.5, 0.5, -0.5},
						  Double4{1.5 / mags[0], 3.2 / mags[0],
								  3.4 / mags[0], 9.7 / mags[0]},
						  Double4{-12.0 / mags[1], -1.0 / mags[1],
								  5.0 / mags[1], 0.2 / mags[1]},
						  Double4{0.001 / mags[2], 0.004 / mags[2],
								  0.02 / mags[2], -0.04 / mags[2]}};

	// eval vector4DDot
	callEvalFunc(
		[](const Double4& expectRes, const Double4& lhsData,
		   const Double4& rhsData) -> void {
			Double4 result;
			storeDouble4(result, vector4DDot(loadDouble4(lhsData),
											 loadDouble4(rhsData)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		dotRes, twoData);

	// eval vector4DLengthSq
	callEvalFunc(
		[](const Double4& expectRes, const Double4& data) -> void {
			Double4 result;
			storeDouble4(result, vector4DLengthSq(loadDouble4(data)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		lengthSqRes, oneData);

	// eval vector4DLength
	callEvalFunc(
		[](const Double4& expectRes, const Double4& data) -> void {
			Double4 result;
			storeDouble4(result, vector4DLength(loadDouble4(data)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		lengthRes, oneData);

	// eval vector4DNormalised
	callEvalFunc(
		[](const Double4& expectRes, const Double4& data) -> void {
			Double4 result;
			storeDouble4(result,
						 vector4DNormalised(loadDouble4(data)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		normalRes, oneData);
}
void eval4DVecTransform()
{
	constexpr static const size_t arrSize = 6;

	using TupleData = std::tuple<Double4, Double4x4>;

	Double4 rawVecs[4] = {
		Double4{1.0, -2.0, 0.5, g_PI},
		Double4{0.0, 0.0, g_MaxDouble, 0.0},
		Double4{4.0, -2.0, 0.0, 0.0},
		Double4{1.0, -2.0, 0.5, 1000.0},
	};
	Double4 rawRowVecs[7] = {
		Double4{1.0, 0.0, 0.0, 0.0},  Double4{0.0, 1.0, 0.0, 0.0},
		Double4{0.0, 0.0, 1.0, 0.0},  Double4{0.0, 0.0, 0.0, 1.0},
		Double4{0.0, 1.0, -1.0, 0.0}, Double4{0.0, -0.5, 0.0, 23.0},
		Double4{g_PI, 0.0, 0.0, 0.0}};
	Double4x4 rawMtrx[3] = {Double4x4{rawRowVecs[0], rawRowVecs[1],
									  rawRowVecs[2], rawRowVecs[3]},
							Double4x4{rawRowVecs[4], rawRowVecs[3],
									  rawRowVecs[0], rawRowVecs[2]},
							Double4x4{rawRowVecs[1], rawRowVecs[5],
									  rawRowVecs[4], rawRowVecs[6]}};

	// eval vector4DTransform
	callEvalFunc(
		[](const Double4& expectRes, const Double4& vec,
		   const Double4x4& mtrx) -> void {
			Double4 result;
			storeDouble4(result,
						 vector4DTransform(loadDouble4(vec),
										   loadDouble4x4(mtrx)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		std::array<Double4, arrSize>{
			rawVecs[0], rawVecs[1], Double4{-2.5, g_PI, 1.0, 0.5},
			Double4{-2.0, 0.0, 4.0, 0.0},
			Double4{0.0, 0.0, -g_MaxDouble, 0.0},
			Double4{-2.0, 23001.0, -2.5, g_PI}},
		std::array<TupleData, arrSize>{
			TupleData{rawVecs[0], rawMtrx[0]},
			TupleData{rawVecs[1], rawMtrx[0]},
			TupleData{rawVecs[0], rawMtrx[1]},
			TupleData{rawVecs[2], rawMtrx[1]},
			TupleData{rawVecs[1], rawMtrx[2]},
			TupleData{rawVecs[3], rawMtrx[2]}});
}
void eval4DMtrxInversion()
{
	constexpr static const size_t arrSize = 8;

	using ArrayRes	= std::array<Double4, arrSize>;
	using TupleMtrx = std::tuple<Double4x4>;
	using ArrayIn	= std::array<TupleMtrx, arrSize>;

	Double4 rowVecs[11] = {
		Double4{1.0, 0.0, 0.0, 0.0},   Double4{0.0, 1.0, 0.0, 0.0},
		Double4{0.0, 0.0, 1.0, 0.0},   Double4{0.0, 0.0, 0.0, 1.0},
		Double4{0.0, 0.0, 0.0, 0.0},   Double4{1.0, 1.0, 1.0, 1.0},
		Double4{0.0, 1.0, -1.0, 0.0},  Double4{0.0, 1.0, 1.0, 0.0},
		Double4{10.0, 0.0, 0.0, 0.0},  Double4{0.0, 0.0, 0.0, -500.0},
		Double4{g_PI, -0.5, 0.0, 23.0}};

	auto testees =
		ArrayIn{TupleMtrx{Double4x4{rowVecs[0], rowVecs[1],
									rowVecs[2], rowVecs[3]}},
				TupleMtrx{Double4x4{rowVecs[2], rowVecs[0],
									rowVecs[1], rowVecs[3]}},
				TupleMtrx{Double4x4{rowVecs[0], rowVecs[1],
									rowVecs[2], rowVecs[4]}},
				TupleMtrx{Double4x4{rowVecs[6], rowVecs[0],
									rowVecs[7], rowVecs[3]}},
				TupleMtrx{Double4x4{rowVecs[0], rowVecs[1],
									rowVecs[1], rowVecs[3]}},
				TupleMtrx{Double4x4{rowVecs[8], rowVecs[6],
									rowVecs[9], rowVecs[7]}},
				TupleMtrx{Double4x4{rowVecs[5], rowVecs[5],
									rowVecs[5], rowVecs[5]}},
				TupleMtrx{Double4x4{rowVecs[9], rowVecs[10],
									rowVecs[6], rowVecs[8]}}};

	auto detRes = std::array<Double4, arrSize>{
		Double4{1.0, 1.0, 1.0, 1.0},
		Double4{1.0, 1.0, 1.0, 1.0},
		Double4{0.0, 0.0, 0.0, 0.0},
		Double4{-2.0, -2.0, -2.0, -2.0},
		Double4{0.0, 0.0, 0.0, 0.0},
		Double4{10'000.0, 10'000.0, 10'000.0, 10'000.0},
		Double4{0.0, 0.0, 0.0, 0.0},
		Double4{2'500.0, 2'500.0, 2'500.0, 2'500.0}};
	auto invrtblRes = std::array<bool, arrSize>{
		true, true, false, true, false, true, false, true};
	auto invsRes = std::array<std::pair<Double4x4, bool>, arrSize>{
		std::pair<Double4x4, bool>{
			Double4x4{rowVecs[0], rowVecs[1], rowVecs[2], rowVecs[3]},
			true},
		std::pair<Double4x4, bool>{
			Double4x4{rowVecs[1], rowVecs[2], rowVecs[0], rowVecs[3]},
			true},
		std::pair<Double4x4, bool>{
			Double4x4{rowVecs[0], rowVecs[1], rowVecs[2], rowVecs[4]},
			false},
		std::pair<Double4x4, bool>{
			Double4x4{rowVecs[1], Double4{0.5, 0.0, 0.5, 0.0},
					  Double4{-0.5, 0.0, 0.5, 0.0}, rowVecs[3]},
			true},
		std::pair<Double4x4, bool>{
			Double4x4{rowVecs[0], rowVecs[1], rowVecs[1], rowVecs[3]},
			false},
		std::pair<Double4x4, bool>{
			Double4x4{Double4{0.1, 0.0, 0.0, 0.0},
					  Double4{0.0, 0.5, 0.0, 0.5},
					  Double4{0.0, -0.5, 0.0, 0.5},
					  Double4{0.0, 0.0, -0.002, 0.0}},
			true},
		std::pair<Double4x4, bool>{
			Double4x4{rowVecs[5], rowVecs[5], rowVecs[5], rowVecs[5]},
			false},
		std::pair<Double4x4, bool>{
			Double4x4{Double4{0.0, 0.0, 0.0, 0.1},
					  Double4{-0.092, -2.0, 0.0, g_PI / 5.0},
					  Double4{-0.092, -2.0, -1.0, g_PI / 5.0},
					  Double4{-0.002, 0.0, 0.0, 0.0}},
			true}};

	// eval matrix4DDeterminant
	callEvalFunc(
		[](const Double4& expectRes, const Double4x4& mtrx) -> void {
			Double4 result;
			storeDouble4(result,
						 matrix4DDeterminant(loadDouble4x4(mtrx)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		detRes, testees);

	// eval matrix4DIsInvertible
	callEvalFunc(
		[](const bool& expectRes, const Double4x4& mtrx) -> void {
			bool result = matrix4DIsInvertible(loadDouble4x4(mtrx));

			CHECK_EQ(result, expectRes);
		},
		invrtblRes, testees);

	// eval matrix4DInverse
	callEvalFunc(
		[](const std::pair<Double4x4, bool>& expectRes,
		   const Double4x4&					 mtrx) -> void {
			WideMatrix inverse;
			bool	   boolRes =
				matrix4DInverse(inverse, loadDouble4x4(mtrx));

			if (!boolRes)
			{
				// then matrix was determined to be not invertible -
				// check this is true
				CHECK_EQ(expectRes.second, false);
			} else
			{
				// check this matrix was meant to be invertible
				CHECK_EQ(expectRes.second, true);

				Double4x4 dataRes;
				storeDouble4x4(dataRes, inverse);

				dataStoreApproxEq(dataRes, expectRes.first,
								  10'000'000.0);
			}
		},
		invsRes, testees);
}

void eval3DVecMaths()
{
	constexpr static const size_t arrSize = 6;

	using ArrayRes	 = std::array<Double3, arrSize>;
	using TupleTwo	 = std::tuple<Double3, Double3>;
	using TupleOne	 = std::tuple<Double3>;
	using ArrayOneIn = std::array<TupleOne, arrSize>;

	Double3 rawVecs[6] = {
		Double3{1.0, 0.0, 0.0},	  Double3{0.0, 1.0, 0.0},
		Double3{5.0, -5.0, 5.0},  Double3{1.5, 3.2, 9.7},
		Double3{-12.0, 5.0, 0.2}, Double3{0.001, 0.004, 0.02}};

	auto oneData =
		ArrayOneIn{TupleOne{rawVecs[0]}, TupleOne{rawVecs[1]},
				   TupleOne{rawVecs[2]}, TupleOne{rawVecs[3]},
				   TupleOne{rawVecs[4]}, TupleOne{rawVecs[5]}};
	auto twoData = std::array<TupleTwo, arrSize>{
		TupleTwo{rawVecs[0], rawVecs[0]},
		TupleTwo{rawVecs[0], rawVecs[1]},
		TupleTwo{rawVecs[2], rawVecs[0]},
		TupleTwo{rawVecs[2], rawVecs[1]},
		TupleTwo{rawVecs[2], rawVecs[2]},
		TupleTwo{rawVecs[2], rawVecs[3]}};

	// store the magnitudes of some harder to calculate vectors here,
	// to simplify later calculations
	double mags[4] = {std::sqrt(75.0), std::sqrt(106.58),
					  std::sqrt(169.04), std::sqrt(0.000417)};
	ArrayRes
		dotRes =
			ArrayRes{
				Double3{1.0, 1.0, 1.0},	   Double3{0.0, 0.0, 0.0},
				Double3{5.0, 5.0, 5.0},	   Double3{-5.0, -5.0, -5.0},
				Double3{75.0, 75.0, 75.0}, Double3{40.0, 40.0, 40.0}},
		crossRes =
			ArrayRes{
				Double3{0.0, 0.0, 0.0}, Double3{0.0, 0.0, 1.0},
				Double3{0.0, 5.0, 5.0}, Double3{-5.0, 0.0, 5.0},
				Double3{0.0, 0.0, 0.0}, Double3{-64.5, -41.0, 23.5}},
		lengthSqRes = ArrayRes{Double3{1.0, 1.0, 1.0},
							   Double3{1.0, 1.0, 1.0},
							   Double3{75.0, 75.0, 75.0},
							   Double3{106.58, 106.58, 106.58},
							   Double3{169.04, 169.04, 169.04},
							   Double3{0.000417, 0.000417, 0.000417}},
		lengthRes	= ArrayRes{Double3{1.0, 1.0, 1.0},
							   Double3{1.0, 1.0, 1.0},
							   Double3{mags[0], mags[0], mags[0]},
							   Double3{mags[1], mags[1], mags[1]},
							   Double3{mags[2], mags[2], mags[2]},
							   Double3{mags[3], mags[3], mags[3]}},
		normalRes	= ArrayRes{
			  rawVecs[0],
			  rawVecs[1],
			  Double3{5.0 / mags[0], -5.0 / mags[0], 5.0 / mags[0]},
			  Double3{1.5 / mags[1], 3.2 / mags[1], 9.7 / mags[1]},
			  Double3{-12.0 / mags[2], 5.0 / mags[2], 0.2 / mags[2]},
			  Double3{0.001 / mags[3], 0.004 / mags[3],
					  0.02 / mags[3]}};

	// eval vector3DDot
	callEvalFunc(
		[](const Double3& expectRes, const Double3& lhsData,
		   const Double3& rhsData) -> void {
			Double3 result;
			storeDouble3(result, vector3DDot(loadDouble3(lhsData),
											 loadDouble3(rhsData)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		dotRes, twoData);

	// eval vector3DCross
	callEvalFunc(
		[](const Double3& expectRes, const Double3& lhsData,
		   const Double3& rhsData) -> void {
			Double3 result;
			storeDouble3(result, vector3DCross(loadDouble3(lhsData),
											   loadDouble3(rhsData)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		crossRes, twoData);

	// eval vector3DLengthSq
	callEvalFunc(
		[](const Double3& expectRes, const Double3& data) -> void {
			Double3 result;
			storeDouble3(result, vector3DLengthSq(loadDouble3(data)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		lengthSqRes, oneData);

	// eval vector3DLength
	callEvalFunc(
		[](const Double3& expectRes, const Double3& data) -> void {
			Double3 result;
			storeDouble3(result, vector3DLength(loadDouble3(data)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		lengthRes, oneData);

	// eval vector3DNormalised
	callEvalFunc(
		[](const Double3& expectRes, const Double3& data) -> void {
			Double3 result;
			storeDouble3(result,
						 vector3DNormalised(loadDouble3(data)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		normalRes, oneData);
}
void eval3DVecTransform()
{
	constexpr static const size_t arrSize = 6;

	using TupleData = std::tuple<Double3, Double3x3>;

	Double3 rawVecs[4] = {
		Double3{g_PI, 0.5, -2.0},
		Double3{0.0, g_MaxDouble, 0.0},
		Double3{4.0, -2.0, 0.0},
		Double3{1.0, 20.0, 1000.0},
	};
	Double3 rawRowVecs[6] = {
		Double3{1.0, 0.0, 0.0},	  Double3{0.0, 1.0, 0.0},
		Double3{0.0, 0.0, 1.0},	  Double3{0.0, 1.0, -1.0},
		Double3{0.0, -0.5, 23.0}, Double3{g_PI, 0.0, 0.0}};
	Double3x3 rawMtrx[3] = {
		Double3x3{rawRowVecs[0], rawRowVecs[1], rawRowVecs[2]},
		Double3x3{rawRowVecs[3], rawRowVecs[0], rawRowVecs[2]},
		Double3x3{rawRowVecs[4], rawRowVecs[3], rawRowVecs[5]}};

	// eval vector3DTransform
	callEvalFunc(
		[](const Double3& expectRes, const Double3& vec,
		   const Double3x3& mtrx) -> void {
			Double3 result;
			storeDouble3(result,
						 vector3DTransform(loadDouble3(vec),
										   loadDouble3x3(mtrx)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		std::array<Double3, arrSize>{
			rawVecs[0], rawVecs[1], Double3{2.5, g_PI, -2},
			Double3{-2.0, 4.0, 0.0},
			Double3{-0.5 * g_MaxDouble, g_MaxDouble, 0.0},
			Double3{22'990.0, -980.0, g_PI}},
		std::array<TupleData, arrSize>{
			TupleData{rawVecs[0], rawMtrx[0]},
			TupleData{rawVecs[1], rawMtrx[0]},
			TupleData{rawVecs[0], rawMtrx[1]},
			TupleData{rawVecs[2], rawMtrx[1]},
			TupleData{rawVecs[1], rawMtrx[2]},
			TupleData{rawVecs[3], rawMtrx[2]}});
}
void eval3DMtrxInversion()
{
	constexpr static const size_t arrSize = 8;

	using ArrayRes	= std::array<Double3, arrSize>;
	using TupleMtrx = std::tuple<Double3x3>;
	using ArrayIn	= std::array<TupleMtrx, arrSize>;

	Double3 rowVecs[10] = {
		Double3{1.0, 0.0, 0.0},	   Double3{0.0, 1.0, 0.0},
		Double3{0.0, 0.0, 1.0},	   Double3{0.0, 0.0, 0.0},
		Double3{1.0, 1.0, 1.0},	   Double3{0.0, 1.0, -1.0},
		Double3{0.0, 1.0, 1.0},	   Double3{10.0, 0.0, 0.0},
		Double3{0.0, 0.0, -500.0}, Double3{g_PI, 0.0, 23.0}};

	auto testees = ArrayIn{
		TupleMtrx{Double3x3{rowVecs[0], rowVecs[1], rowVecs[2]}},
		TupleMtrx{Double3x3{rowVecs[2], rowVecs[0], rowVecs[1]}},
		TupleMtrx{Double3x3{rowVecs[0], rowVecs[2], rowVecs[3]}},
		TupleMtrx{Double3x3{rowVecs[5], rowVecs[0], rowVecs[6]}},
		TupleMtrx{Double3x3{rowVecs[0], rowVecs[6], rowVecs[6]}},
		TupleMtrx{Double3x3{rowVecs[7], rowVecs[5], rowVecs[8]}},
		TupleMtrx{Double3x3{rowVecs[4], rowVecs[4], rowVecs[4]}},
		TupleMtrx{Double3x3{rowVecs[8], rowVecs[9], rowVecs[5]}}};

	auto detRes = std::array<Double3, arrSize>{
		Double3{1.0, 1.0, 1.0},
		Double3{1.0, 1.0, 1.0},
		Double3{0.0, 0.0, 0.0},
		Double3{-2.0, -2.0, -2.0},
		Double3{0.0, 0.0, 0.0},
		Double3{-5'000.0, -5'000.0, -5'000.0},
		Double3{0.0, 0.0, 0.0},
		Double3{-500.0 * g_PI, -500.0 * g_PI, -500.0 * g_PI}};
	auto invrtblRes = std::array<bool, arrSize>{
		true, true, false, true, false, true, false, true};
	auto invsRes = std::array<std::pair<Double3x3, bool>, arrSize>{
		std::pair<Double3x3, bool>{
			Double3x3{rowVecs[0], rowVecs[1], rowVecs[2]}, true},
		std::pair<Double3x3, bool>{
			Double3x3{rowVecs[1], rowVecs[2], rowVecs[0]}, true},
		std::pair<Double3x3, bool>{
			Double3x3{rowVecs[0], rowVecs[2], rowVecs[3]}, false},
		std::pair<Double3x3, bool>{
			Double3x3{rowVecs[1], Double3{0.5, 0.0, 0.5},
					  Double3{-0.5, 0.0, 0.5}},
			true},
		std::pair<Double3x3, bool>{
			Double3x3{rowVecs[0], rowVecs[6], rowVecs[6]}, false},
		std::pair<Double3x3, bool>{
			Double3x3{Double3{0.1, 0.0, 0.0},
					  Double3{0.0, 1.0, -0.002},
					  Double3{0.0, 0.0, -0.002}},
			true},
		std::pair<Double3x3, bool>{
			Double3x3{rowVecs[4], rowVecs[4], rowVecs[4]}, false},
		std::pair<Double3x3, bool>{
			Double3x3{Double3{23.0 / (500.0 * g_PI), 1.0 / g_PI, 0.0},
					  Double3{-0.002, 0.0, 1.0},
					  Double3{-0.002, 0.0, 0.0}},
			true}};

	// eval matrix3DDeterminant
	callEvalFunc(
		[](const Double3& expectRes, const Double3x3& mtrx) -> void {
			Double3 result;
			storeDouble3(result,
						 matrix3DDeterminant(loadDouble3x3(mtrx)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		detRes, testees);

	// eval matrix3DIsInvertible
	callEvalFunc(
		[](const bool& expectRes, const Double3x3& mtrx) -> void {
			bool result = matrix3DIsInvertible(loadDouble3x3(mtrx));

			CHECK_EQ(result, expectRes);
		},
		invrtblRes, testees);

	// eval matrix3DInverse
	callEvalFunc(
		[](const std::pair<Double3x3, bool>& expectRes,
		   const Double3x3&					 mtrx) -> void {
			WideMatrix inverse;
			bool	   boolRes =
				matrix3DInverse(inverse, loadDouble3x3(mtrx));

			if (!boolRes)
			{
				// then matrix was determined to be not invertible -
				// check this is true
				CHECK_EQ(expectRes.second, false);
			} else
			{
				// check this matrix was meant to be invertible
				CHECK_EQ(expectRes.second, true);

				Double3x3 dataRes;
				storeDouble3x3(dataRes, inverse);

				dataStoreApproxEq(dataRes, expectRes.first,
								  10'000'000.0);
			}
		},
		invsRes, testees);
}

void eval2DVecMaths()
{
	constexpr static const size_t arrSize = 6;

	using ArrayRes	 = std::array<Double2, arrSize>;
	using TupleTwo	 = std::tuple<Double2, Double2>;
	using TupleOne	 = std::tuple<Double2>;
	using ArrayOneIn = std::array<TupleOne, arrSize>;

	Double2 rawVecs[6] = {Double2{1.0, 0.0},   Double2{0.0, 1.0},
						  Double2{5.0, -5.0},  Double2{1.5, 9.7},
						  Double2{-12.0, 0.2}, Double2{0.001, 0.004}};

	auto oneData =
		ArrayOneIn{TupleOne{rawVecs[0]}, TupleOne{rawVecs[1]},
				   TupleOne{rawVecs[2]}, TupleOne{rawVecs[3]},
				   TupleOne{rawVecs[4]}, TupleOne{rawVecs[5]}};
	auto twoData = std::array<TupleTwo, arrSize>{
		TupleTwo{rawVecs[0], rawVecs[0]},
		TupleTwo{rawVecs[0], rawVecs[1]},
		TupleTwo{rawVecs[2], rawVecs[0]},
		TupleTwo{rawVecs[2], rawVecs[1]},
		TupleTwo{rawVecs[2], rawVecs[2]},
		TupleTwo{rawVecs[2], rawVecs[3]}};

	// store the magnitudes of some harder to calculate vectors here,
	// to simplify later calculations
	double	 mags[4] = {std::sqrt(50.0), std::sqrt(96.34),
						std::sqrt(144.04), std::sqrt(0.000017)};
	ArrayRes dotRes =
				 ArrayRes{Double2{1.0, 1.0},   Double2{0.0, 0.0},
						  Double2{5.0, 5.0},   Double2{-5.0, -5.0},
						  Double2{50.0, 50.0}, Double2{-41.0, -41.0}},
			 lengthSqRes = ArrayRes{Double2{1.0, 1.0},
									Double2{1.0, 1.0},
									Double2{50.0, 50.0},
									Double2{96.34, 96.34},
									Double2{144.04, 144.04},
									Double2{0.000017, 0.000017}},
			 lengthRes	 = ArrayRes{Double2{1.0, 1.0},
									Double2{1.0, 1.0},
									Double2{mags[0], mags[0]},
									Double2{mags[1], mags[1]},
									Double2{mags[2], mags[2]},
									Double2{mags[3], mags[3]}},
			 normalRes =
				 ArrayRes{rawVecs[0],
						  rawVecs[1],
						  Double2{5.0 / mags[0], -5.0 / mags[0]},
						  Double2{1.5 / mags[1], 9.7 / mags[1]},
						  Double2{-12.0 / mags[2], 0.2 / mags[2]},
						  Double2{0.001 / mags[3], 0.004 / mags[3]}};

	// eval vector2DDot
	callEvalFunc(
		[](const Double2& expectRes, const Double2& lhsData,
		   const Double2& rhsData) -> void {
			Double2 result;
			storeDouble2(result, vector2DDot(loadDouble2(lhsData),
											 loadDouble2(rhsData)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		dotRes, twoData);

	// eval vector2DLengthSq
	callEvalFunc(
		[](const Double2& expectRes, const Double2& data) -> void {
			Double2 result;
			storeDouble2(result, vector2DLengthSq(loadDouble2(data)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		lengthSqRes, oneData);

	// eval vector2DLength
	callEvalFunc(
		[](const Double2& expectRes, const Double2& data) -> void {
			Double2 result;
			storeDouble2(result, vector2DLength(loadDouble2(data)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		lengthRes, oneData);

	// eval vector2DNormalised
	callEvalFunc(
		[](const Double2& expectRes, const Double2& data) -> void {
			Double2 result;
			storeDouble2(result,
						 vector2DNormalised(loadDouble2(data)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		normalRes, oneData);
}
void eval2DVecTransform()
{
	constexpr static const size_t arrSize = 6;

	using TupleData = std::tuple<Double2, Double2x2>;

	Double2 rawVecs[4] = {
		Double2{g_PI, -2.0},
		Double2{g_MaxDouble, 0.0},
		Double2{4.0, -2.0},
		Double2{1.0, 1000.0},
	};
	Double2	  rawRowVecs[5] = {Double2{1.0, 0.0}, Double2{0.0, 1.0},
							   Double2{1.0, -1.0}, Double2{-0.5, 23.0},
							   Double2{0.0, g_PI}};
	Double2x2 rawMtrx[3] = {Double2x2{rawRowVecs[0], rawRowVecs[1]},
							Double2x2{rawRowVecs[2], rawRowVecs[0]},
							Double2x2{rawRowVecs[3], rawRowVecs[4]}};

	// eval vector2DTransform
	callEvalFunc(
		[](const Double2& expectRes, const Double2& vec,
		   const Double2x2& mtrx) -> void {
			Double2 result;
			storeDouble2(result,
						 vector2DTransform(loadDouble2(vec),
										   loadDouble2x2(mtrx)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		std::array<Double2, arrSize>{
			rawVecs[0], rawVecs[1], Double2{2.0 + g_PI, g_PI},
			Double2{6.0, 4.0}, Double2{-0.5 * g_MaxDouble, 0.0},
			Double2{22'999.5, 1000.0 * g_PI}},
		std::array<TupleData, arrSize>{
			TupleData{rawVecs[0], rawMtrx[0]},
			TupleData{rawVecs[1], rawMtrx[0]},
			TupleData{rawVecs[0], rawMtrx[1]},
			TupleData{rawVecs[2], rawMtrx[1]},
			TupleData{rawVecs[1], rawMtrx[2]},
			TupleData{rawVecs[3], rawMtrx[2]}});
}
void eval2DMtrxInversion()
{
	constexpr static const size_t arrSize = 8;

	using ArrayRes	= std::array<Double2, arrSize>;
	using TupleMtrx = std::tuple<Double2x2>;
	using ArrayIn	= std::array<TupleMtrx, arrSize>;

	Double2 rowVecs[8] = {Double2{1.0, 0.0},	Double2{0.0, 1.0},
						  Double2{0.0, 0.0},	Double2{1.0, 1.0},
						  Double2{1.0, -1.0},	Double2{10.0, 0.0},
						  Double2{0.0, -500.0}, Double2{g_PI, 23.0}};

	auto testees =
		ArrayIn{TupleMtrx{Double2x2{rowVecs[0], rowVecs[1]}},
				TupleMtrx{Double2x2{rowVecs[1], rowVecs[0]}},
				TupleMtrx{Double2x2{rowVecs[0], rowVecs[2]}},
				TupleMtrx{Double2x2{rowVecs[4], rowVecs[3]}},
				TupleMtrx{Double2x2{rowVecs[2], rowVecs[2]}},
				TupleMtrx{Double2x2{rowVecs[5], rowVecs[6]}},
				TupleMtrx{Double2x2{rowVecs[3], rowVecs[3]}},
				TupleMtrx{Double2x2{rowVecs[6], rowVecs[7]}}};

	auto detRes = std::array<Double2, arrSize>{
		Double2{1.0, 1.0}, Double2{-1.0, -1.0},
		Double2{0.0, 0.0}, Double2{2.0, 2.0},
		Double2{0.0, 0.0}, Double2{-5'000.0, -5'000.0},
		Double2{0.0, 0.0}, Double2{500.0 * g_PI, 500.0 * g_PI}};
	auto invrtblRes = std::array<bool, arrSize>{
		true, true, false, true, false, true, false, true};
	auto invsRes = std::array<std::pair<Double2x2, bool>, arrSize>{
		std::pair<Double2x2, bool>{Double2x2{rowVecs[0], rowVecs[1]},
								   true},
		std::pair<Double2x2, bool>{Double2x2{rowVecs[1], rowVecs[0]},
								   true},
		std::pair<Double2x2, bool>{Double2x2{rowVecs[0], rowVecs[2]},
								   false},
		std::pair<Double2x2, bool>{
			Double2x2{Double2{0.5, 0.5}, Double2{-0.5, 0.5}}, true},
		std::pair<Double2x2, bool>{Double2x2{rowVecs[2], rowVecs[2]},
								   false},
		std::pair<Double2x2, bool>{
			Double2x2{Double2{0.1, 0.0}, Double2{0.0, -0.002}}, true},
		std::pair<Double2x2, bool>{Double2x2{rowVecs[3], rowVecs[3]},
								   false},
		std::pair<Double2x2, bool>{
			Double2x2{Double2{23.0 / (500.0 * g_PI), 1.0 / g_PI},
					  Double2{-0.002, 0.0}},
			true}};

	// eval matrix2DDeterminant
	callEvalFunc(
		[](const Double2& expectRes, const Double2x2& mtrx) -> void {
			Double2 result;
			storeDouble2(result,
						 matrix2DDeterminant(loadDouble2x2(mtrx)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		detRes, testees);

	// eval matrix2DIsInvertible
	callEvalFunc(
		[](const bool& expectRes, const Double2x2& mtrx) -> void {
			bool result = matrix2DIsInvertible(loadDouble2x2(mtrx));

			CHECK_EQ(result, expectRes);
		},
		invrtblRes, testees);

	// eval matrix2DInverse
	callEvalFunc(
		[](const std::pair<Double2x2, bool>& expectRes,
		   const Double2x2&					 mtrx) -> void {
			WideMatrix inverse;
			bool	   boolRes =
				matrix2DInverse(inverse, loadDouble2x2(mtrx));

			if (!boolRes)
			{
				// then matrix was determined to be not invertible -
				// check this is true
				CHECK_EQ(expectRes.second, false);
			} else
			{
				// check this matrix was meant to be invertible
				CHECK_EQ(expectRes.second, true);

				Double2x2 dataRes;
				storeDouble2x2(dataRes, inverse);

				dataStoreApproxEq(dataRes, expectRes.first,
								  10'000'000.0);
			}
		},
		invsRes, testees);
}

void evalIDMatrix()
{
	Double4x4 result;
	storeDouble4x4(result, matrixIdentity());

	dataStoreApproxEq(result.m_Tuples[0], Double4{1.0, 0.0, 0.0, 0.0},
					  10'000'000.0);
	dataStoreApproxEq(result.m_Tuples[1], Double4{0.0, 1.0, 0.0, 0.0},
					  10'000'000.0);
	dataStoreApproxEq(result.m_Tuples[2], Double4{0.0, 0.0, 1.0, 0.0},
					  10'000'000.0);
	dataStoreApproxEq(result.m_Tuples[3], Double4{0.0, 0.0, 0.0, 1.0},
					  10'000'000.0);
}
void evalMtrxTranslations()
{
	constexpr static const size_t arrSize = 6;

	using ArrayRes = std::array<Double4, arrSize>;

	using Tuple2D	= std::tuple<Double4, Double2>;
	using Array2DIn = std::array<Tuple2D, arrSize>;

	// eval matrix2DTranslation
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const Double2& translation) -> void {
			Double4 result1, result2;
			storeDouble4(
				result1,
				vector4DTransform(
					loadDouble4(source),
					matrix2DTranslation(loadDouble2(translation))));

			storeDouble4(
				result2,
				vector4DTransform(loadDouble4(source),
								  matrix2DTranslation(
									  translation.m_Components.m_X,
									  translation.m_Components.m_Y)));

			dataStoreApproxEq(result1, expectRes, 10'000'000.0);
			dataStoreApproxEq(result2, expectRes, 10'000'000.0);
		},
		ArrayRes{Double4{4.4567, 46.9, 0.0, 1.0},
				 Double4{3.7, 103.0, 0.0, 1.0},
				 Double4{2.8, 6.5, 0.0, 1.0},
				 Double4{-1.0, 4.3, 0.0, 1.0},
				 Double4{g_MaxDouble, 345.01, 0.0, 1.0},
				 Double4{10'010.4, 0.03, 0.0, 1.0}},
		Array2DIn{
			Tuple2D{Double4{3.4567, 45.9, 0.0, 1.0},
					Double2{1.0, 1.0}},
			Tuple2D{Double4{1.2, 102.5, 0.0, 1.0}, Double2{2.5, 0.5}},
			Tuple2D{Double4{2.3, 9.0, 0.0, 1.0}, Double2{0.5, -2.5}},
			Tuple2D{Double4{-0.9, 4.5, 0.0, 1.0},
					Double2{-0.1, -0.2}},
			Tuple2D{Double4{g_MaxDouble, 0.01, 0.0, 1.0},
					Double2{0.0, 345.0}},
			Tuple2D{Double4{10.4, 0.5, 0.0, 1.0},
					Double2{10'000.0, -0.47}}});

	using Tuple3D	= std::tuple<Double4, Double3>;
	using Array3DIn = std::array<Tuple3D, arrSize>;

	// eval matrix3DTranslation
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const Double3& translation) -> void {
			Double4 result1, result2;
			storeDouble4(
				result1,
				vector4DTransform(
					loadDouble4(source),
					matrix3DTranslation(loadDouble3(translation))));

			storeDouble4(
				result2,
				vector4DTransform(loadDouble4(source),
								  matrix3DTranslation(
									  translation.m_Components.m_X,
									  translation.m_Components.m_Y,
									  translation.m_Components.m_Z)));

			dataStoreApproxEq(result1, expectRes, 10'000'000.0);
			dataStoreApproxEq(result2, expectRes, 10'000'000.0);
		},
		ArrayRes{Double4{2.2, 4.4567, 46.9, 1.0},
				 Double4{-0.5, 1.5, -57.3, 1.0},
				 Double4{-9.9, 3.9, -0.2, 1.0},
				 Double4{0.62, 0.0, 5.5, 1.0},
				 Double4{g_MaxDouble, -10'000.5, 0.2984, 1.0},
				 Double4{234.0, -1001.0, -228.0, 1.0}},
		Array3DIn{
			Tuple3D{Double4{1.2, 3.4567, 45.9, 1.0},
					Double3{1.0, 1.0, 1.0}},
			Tuple3D{Double4{2.0, 1.0, -56.3, 1.0},
					Double3{-2.5, 0.5, -1.0}},
			Tuple3D{Double4{-10.9, 1.4, -0.7, 1.0},
					Double3{1.0, 2.5, 0.5}},
			Tuple3D{Double4{0.12, 1.0, 3.0, 1.0},
					Double3{0.5, -1.0, 2.5}},
			Tuple3D{Double4{g_MaxDouble, -10'000.5, 0.2984, 1.0},
					Double3{0.0, 0.0, 0.0}},
			Tuple3D{Double4{234.1, -1.0, 2.0, 1.0},
					Double3{-0.1, -1000.0, -230.0}}});
}
void evalMtrxScalings()
{
	constexpr static const size_t arrSize = 6;

	using ArrayRes = std::array<Double4, arrSize>;

	using Tuple2D	= std::tuple<Double4, Double2>;
	using Array2DIn = std::array<Tuple2D, arrSize>;

	// eval matrix2DScaling
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const Double2& scaling) -> void {
			Double4 result1, result2;
			storeDouble4(result1,
						 vector4DTransform(
							 loadDouble4(source),
							 matrix2DScaling(loadDouble2(scaling))));

			storeDouble4(result2, vector4DTransform(
									  loadDouble4(source),
									  matrix2DScaling(
										  scaling.m_Components.m_X,
										  scaling.m_Components.m_Y)));

			dataStoreApproxEq(result1, expectRes, 10'000'000.0);
			dataStoreApproxEq(result2, expectRes, 10'000'000.0);
		},
		ArrayRes{Double4{3.4567, 45.9, 0.0, 1.0},
				 Double4{3.0, 51.25, 0.0, 1.0},
				 Double4{1.15, 22.5, 0.0, 1.0},
				 Double4{0.09, -0.9, 0.0, 1.0},
				 Double4{0.0, 3.45, 0.0, 1.0},
				 Double4{104'000.0, 0.235, 0.0, 1.0}},
		Array2DIn{
			Tuple2D{Double4{3.4567, 45.9, 0.0, 1.0},
					Double2{1.0, 1.0}},
			Tuple2D{Double4{1.2, 102.5, 0.0, 1.0}, Double2{2.5, 0.5}},
			Tuple2D{Double4{2.3, 9.0, 0.0, 1.0}, Double2{0.5, 2.5}},
			Tuple2D{Double4{-0.9, 4.5, 0.0, 1.0},
					Double2{-0.1, -0.2}},
			Tuple2D{Double4{g_MaxDouble, 0.01, 0.0, 1.0},
					Double2{0.0, 345.0}},
			Tuple2D{Double4{10.4, 0.5, 0.0, 1.0},
					Double2{10'000.0, 0.47}}});

	using Tuple3D	= std::tuple<Double4, Double3>;
	using Array3DIn = std::array<Tuple3D, arrSize>;

	// eval matrix3DScaling
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const Double3& scaling) -> void {
			Double4 result1, result2;
			storeDouble4(result1,
						 vector4DTransform(
							 loadDouble4(source),
							 matrix3DScaling(loadDouble3(scaling))));

			storeDouble4(result2, vector4DTransform(
									  loadDouble4(source),
									  matrix3DScaling(
										  scaling.m_Components.m_X,
										  scaling.m_Components.m_Y,
										  scaling.m_Components.m_Z)));

			dataStoreApproxEq(result1, expectRes, 10'000'000.0);
			dataStoreApproxEq(result2, expectRes, 10'000'000.0);
		},
		ArrayRes{Double4{1.2, 3.4567, 45.9, 1.0},
				 Double4{5.0, 0.5, -56.3, 1.0},
				 Double4{-10.9, 3.5, -0.35, 1.0},
				 Double4{0.06, 1.0, 7.5, 1.0},
				 Double4{0.0, 0.0, 0.0, 1.0},
				 Double4{-23.41, 1000.0, -460.0, 1.0}},
		Array3DIn{
			Tuple3D{Double4{1.2, 3.4567, 45.9, 1.0},
					Double3{1.0, 1.0, 1.0}},
			Tuple3D{Double4{2.0, 1.0, -56.3, 1.0},
					Double3{2.5, 0.5, 1.0}},
			Tuple3D{Double4{-10.9, 1.4, -0.7, 1.0},
					Double3{1.0, 2.5, 0.5}},
			Tuple3D{Double4{0.12, 1.0, 3.0, 1.0},
					Double3{0.5, 1.0, 2.5}},
			Tuple3D{Double4{g_MaxDouble, -10'000.5, 0.2984, 1.0},
					Double3{0.0, 0.0, 0.0}},
			Tuple3D{Double4{234.1, -1.0, 2.0, 1.0},
					Double3{-0.1, -1000.0, -230.0}}});
}
void evalMtrxBasicRotations()
{
	constexpr static const size_t arrSize = 6;

	using ArrayRes = std::array<Double4, arrSize>;

	using TupleRot = std::tuple<Double4, double>;
	using ArrayIn  = std::array<TupleRot, arrSize>;

	auto rotations =
		ArrayIn{TupleRot{Double4{1.0, 0.0, 0.0, 1.0}, g_PI / 2.0},
				TupleRot{Double4{0.0, 1.0, 0.0, 1.0}, g_PI / 2.0},
				TupleRot{Double4{0.0, 0.0, 1.0, 1.0}, g_PI / 2.0},
				TupleRot{Double4{1.4, 3.2, 0.5, 1.0}, 0.0},
				TupleRot{Double4{1.0, 1.0, 0.0, 1.0}, g_PI},
				TupleRot{Double4{0.0, 0.0, 0.0, 1.0}, 1.1}};

	auto res2DRH	  = ArrayRes{Double4{0.0, 1.0, 0.0, 1.0},
							 Double4{-1.0, 0.0, 0.0, 1.0},
							 Double4{0.0, 0.0, 1.0, 1.0},
							 Double4{1.4, 3.2, 0.5, 1.0},
							 Double4{-1.0, -1.0, 0.0, 1.0},
							 Double4{0.0, 0.0, 0.0, 1.0}},
		 res2DLH	  = ArrayRes{Double4{0.0, -1.0, 0.0, 1.0},
							 Double4{1.0, 0.0, 0.0, 1.0},
							 Double4{0.0, 0.0, 1.0, 1.0},
							 Double4{1.4, 3.2, 0.5, 1.0},
							 Double4{-1.0, -1.0, 0.0, 1.0},
							 Double4{0.0, 0.0, 0.0, 1.0}},
		 res3DYawRH	  = ArrayRes{Double4{0.0, 1.0, 0.0, 1.0},
								 Double4{-1.0, 0.0, 0.0, 1.0},
								 Double4{0.0, 0.0, 1.0, 1.0},
								 Double4{1.4, 3.2, 0.5, 1.0},
								 Double4{-1.0, -1.0, 0.0, 1.0},
								 Double4{0.0, 0.0, 0.0, 1.0}},
		 res3DYawLH	  = ArrayRes{Double4{0.0, -1.0, 0.0, 1.0},
								 Double4{1.0, 0.0, 0.0, 1.0},
								 Double4{0.0, 0.0, 1.0, 1.0},
								 Double4{1.4, 3.2, 0.5, 1.0},
								 Double4{-1.0, -1.0, 0.0, 1.0},
								 Double4{0.0, 0.0, 0.0, 1.0}},
		 res3DPitchRH = ArrayRes{Double4{1.0, 0.0, 0.0, 1.0},
								 Double4{0.0, 0.0, 1.0, 1.0},
								 Double4{0.0, -1.0, 0.0, 1.0},
								 Double4{1.4, 3.2, 0.5, 1.0},
								 Double4{1.0, -1.0, 0.0, 1.0},
								 Double4{0.0, 0.0, 0.0, 1.0}},
		 res3DPitchLH = ArrayRes{Double4{1.0, 0.0, 0.0, 1.0},
								 Double4{0.0, 0.0, -1.0, 1.0},
								 Double4{0.0, 1.0, 0.0, 1.0},
								 Double4{1.4, 3.2, 0.5, 1.0},
								 Double4{1.0, -1.0, 0.0, 1.0},
								 Double4{0.0, 0.0, 0.0, 1.0}},
		 res3DRollRH  = ArrayRes{Double4{0.0, 0.0, -1.0, 1.0},
								 Double4{0.0, 1.0, 0.0, 1.0},
								 Double4{1.0, 0.0, 0.0, 1.0},
								 Double4{1.4, 3.2, 0.5, 1.0},
								 Double4{-1.0, 1.0, 0.0, 1.0},
								 Double4{0.0, 0.0, 0.0, 1.0}},
		 res3DRollLH  = ArrayRes{Double4{0.0, 0.0, 1.0, 1.0},
								 Double4{0.0, 1.0, 0.0, 1.0},
								 Double4{-1.0, 0.0, 0.0, 1.0},
								 Double4{1.4, 3.2, 0.5, 1.0},
								 Double4{-1.0, 1.0, 0.0, 1.0},
								 Double4{0.0, 0.0, 0.0, 1.0}};

	// eval matrix2DRotationRH
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const double& rot) -> void {
			Double4 result;
			storeDouble4(result,
						 vector4DTransform(loadDouble4(source),
										   matrix2DRotationRH(rot)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		res2DRH, rotations);

	// eval matrix2DRotationLH
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const double& rot) -> void {
			Double4 result;
			storeDouble4(result,
						 vector4DTransform(loadDouble4(source),
										   matrix2DRotationLH(rot)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		res2DLH, rotations);

	// eval matrix3DRotationYawRH
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const double& rot) -> void {
			Double4 result;
			storeDouble4(result, vector4DTransform(
									 loadDouble4(source),
									 matrix3DRotationYawRH(rot)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		res3DYawRH, rotations);

	// eval matrix3DRotationYawLH
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const double& rot) -> void {
			Double4 result;
			storeDouble4(result, vector4DTransform(
									 loadDouble4(source),
									 matrix3DRotationYawLH(rot)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		res3DYawLH, rotations);

	// eval matrix3DRotationPitchRH
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const double& rot) -> void {
			Double4 result;
			storeDouble4(result, vector4DTransform(
									 loadDouble4(source),
									 matrix3DRotationPitchRH(rot)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		res3DPitchRH, rotations);

	// eval matrix3DRotationPitchLH
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const double& rot) -> void {
			Double4 result;
			storeDouble4(result, vector4DTransform(
									 loadDouble4(source),
									 matrix3DRotationPitchLH(rot)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		res3DPitchLH, rotations);

	// eval matrix3DRotationRollRH
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const double& rot) -> void {
			Double4 result;
			storeDouble4(result, vector4DTransform(
									 loadDouble4(source),
									 matrix3DRotationRollRH(rot)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		res3DRollRH, rotations);

	// eval matrix3DRotationRollLH
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const double& rot) -> void {
			Double4 result;
			storeDouble4(result, vector4DTransform(
									 loadDouble4(source),
									 matrix3DRotationRollLH(rot)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		res3DRollLH, rotations);
}
void evalMtrxCompositeRotations()
{
	constexpr static const size_t arrSize = 6;

	using ArrayRes = std::array<Double4, arrSize>;
	using TupleRot = std::tuple<Double4, Double3>;
	using ArrayIn  = std::array<TupleRot, arrSize>;

	auto dataIn = ArrayIn{
		TupleRot{Double4{0.0, 1.0, 0.0, 1.0},
				 Double3{g_PI / 2.0, 0.0, 0.0}},
		TupleRot{Double4{0.0, 0.0, 1.0, 1.0},
				 Double3{0.0, g_PI / 2.0, 0.0}},
		TupleRot{Double4{1.0, 0.0, 0.0, 1.0},
				 Double3{0.0, 0.0, g_PI / 2.0}},
		TupleRot{Double4{1.2, 2.3, 3.4, 1.0}, Double3{0.0, 0.0, 0.0}},
		TupleRot{Double4{0.0, 0.0, 0.0, 1.0}, Double3{1.2, 2.3, 1.4}},
		TupleRot{Double4{0.0, 1.0, 0.0, 1.0},
				 Double3{g_PI / 2.0, 0.0, g_PI / 2.0}}};
	ArrayRes resRH = ArrayRes{Double4{0.0, 0.0, 1.0, 1.0},
							  Double4{1.0, 0.0, 0.0, 1.0},
							  Double4{0.0, 1.0, 0.0, 1.0},
							  Double4{1.2, 2.3, 3.4, 1.0},
							  Double4{0.0, 0.0, 0.0, 1.0},
							  Double4{-1.0, 0.0, 0.0, 1.0}},
			 resLH = ArrayRes{Double4{0.0, 0.0, -1.0, 1.0},
							  Double4{-1.0, 0.0, 0.0, 1.0},
							  Double4{0.0, -1.0, 0.0, 1.0},
							  Double4{1.2, 2.3, 3.4, 1.0},
							  Double4{0.0, 0.0, 0.0, 1.0},
							  Double4{0.0, 0.0, -1.0, 1.0}};

	// eval matrix3DRotationRH/FromVector
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const Double3& rotation) -> void {
			Double4 result1, result2;
			storeDouble4(result1, vector4DTransform(
									  loadDouble4(source),
									  matrix3DRotationRHFromVector(
										  loadDouble3(rotation))));

			storeDouble4(
				result2,
				vector4DTransform(
					loadDouble4(source),
					matrix3DRotationRH(rotation.m_Components.m_X,
									   rotation.m_Components.m_Y,
									   rotation.m_Components.m_Z)));

			dataStoreApproxEq(result1, expectRes, 10'000'000.0);
			dataStoreApproxEq(result2, expectRes, 10'000'000.0);
		},
		resRH, dataIn);

	// eval matrix3DRotationLH/FromVector
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const Double3& rotation) -> void {
			Double4 result1, result2;
			storeDouble4(result1, vector4DTransform(
									  loadDouble4(source),
									  matrix3DRotationLHFromVector(
										  loadDouble3(rotation))));

			storeDouble4(
				result2,
				vector4DTransform(
					loadDouble4(source),
					matrix3DRotationLH(rotation.m_Components.m_X,
									   rotation.m_Components.m_Y,
									   rotation.m_Components.m_Z)));

			dataStoreApproxEq(result1, expectRes, 10'000'000.0);
			dataStoreApproxEq(result2, expectRes, 10'000'000.0);
		},
		resLH, dataIn);
}
void evalMtrxAxisRotations()
{
	constexpr static const size_t arrSize = 6;

	using ArrayRes = std::array<Double4, arrSize>;
	using TupleRot = std::tuple<Double4, Double3, double>;
	using ArrayIn  = std::array<TupleRot, arrSize>;

	auto dataIn =
		ArrayIn{TupleRot{Double4{0.0, 1.0, 0.0, 1.0},
						 Double3{1.0, 0.0, 0.0}, g_PI / 2.0},
				TupleRot{Double4{1.0, 0.0, 0.0, 1.0},
						 Double3{0.0, 1.0, 0.0}, g_PI / 2.0},
				TupleRot{Double4{1.0, 0.0, 0.0, 1.0},
						 Double3{0.0, 0.0, 1.0}, g_PI / 2.0},
				TupleRot{Double4{10.0, 0.0, 0.0, 1.0},
						 Double3{10.0, 0.0, 0.0}, 1.675},
				TupleRot{Double4{0.0, -1.0, 0.0, 1.0},
						 Double3{-1.0, -1.0, -1.0}, g_PI},
				TupleRot{Double4{0.0, 0.0, 0.0, 1.0},
						 Double3{-1.0, 1.4, 3.6}, 1.3}};
	ArrayRes resRH = ArrayRes{Double4{0.0, 0.0, 1.0, 1.0},
							  Double4{0.0, 0.0, -1.0, 1.0},
							  Double4{0.0, 1.0, 0.0, 1.0},
							  Double4{10.0, 0.0, 0.0, 1.0},
							  Double4{-2.0 / 3.0, 1.0 / 3.0,
									  -2.0 / 3.0, 1.0},
							  Double4{0.0, 0.0, 0.0, 1.0}},
			 resLH = ArrayRes{
				 Double4{0.0, 0.0, -1.0, 1.0},
				 Double4{0.0, 0.0, 1.0, 1.0},
				 Double4{0.0, -1.0, 0.0, 1.0},
				 Double4{10.0, 0.0, 0.0, 1.0},
				 Double4{-2.0 / 3.0, 1.0 / 3.0, -2.0 / 3.0, 1.0},
				 Double4{0.0, 0.0, 0.0, 1.0}};

	// eval matrix3DRotationRHAboutAxis
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const Double3& axis, const double& angle) -> void {
			Double4 result1, result2;
			storeDouble4(result1, vector4DTransform(
									  loadDouble4(source),
									  matrix3DRotationRHAboutAxis(
										  loadDouble3(axis), angle)));

			storeDouble4(
				result2,
				vector4DTransform(
					loadDouble4(source),
					matrix3DRotationRHAboutAxis(
						axis.m_Components.m_X, axis.m_Components.m_Y,
						axis.m_Components.m_Z, angle)));

			dataStoreApproxEq(result1, expectRes, 10'000'000.0);
			dataStoreApproxEq(result2, expectRes, 10'000'000.0);
		},
		resRH, dataIn);

	// eval matrix3DRotationLHAboutAxis
	callEvalFunc(
		[](const Double4& expectRes, const Double4& source,
		   const Double3& axis, const double& angle) -> void {
			Double4 result1, result2;
			storeDouble4(result1, vector4DTransform(
									  loadDouble4(source),
									  matrix3DRotationLHAboutAxis(
										  loadDouble3(axis), angle)));

			storeDouble4(
				result2,
				vector4DTransform(
					loadDouble4(source),
					matrix3DRotationLHAboutAxis(
						axis.m_Components.m_X, axis.m_Components.m_Y,
						axis.m_Components.m_Z, angle)));

			dataStoreApproxEq(result1, expectRes, 10'000'000.0);
			dataStoreApproxEq(result2, expectRes, 10'000'000.0);
		},
		resLH, dataIn);
}
void evalPerspApatureCam()
{
	constexpr static const size_t arrSize = 6;

	using ArrayRes = std::array<Double4x4, arrSize>;
	using TupleCam = std::tuple<double, double, double, double, bool>;
	using ArrayIn  = std::array<TupleCam, arrSize>;

	auto dataIn = ArrayIn{TupleCam{1.0, 1.0, 0.1, 1.0, false},
						  TupleCam{10.0, 0.1, 0.1, 1.0, true},
						  TupleCam{0.1, 10.0, 0.1, 1.0, false},
						  TupleCam{1.8, 1.0, 0.0001, 10'000.0, true},
						  TupleCam{1.8, 1.0, 0.4, 0.5, false},
						  TupleCam{0.018, 0.01, 0.0001, 10.0, true}};
	ArrayRes
		resRH =
			ArrayRes{
				Double4x4{Double4{0.2, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, 0.2, 0.0},
						  Double4{0.0, 1.0 / 0.9, 0.0, -0.1 / 0.9},
						  Double4{0.0, 1.0, 0.0, 0.0}},
				Double4x4{Double4{0.02, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, 2.0, 0.0},
						  Double4{0.0, -1.0 / 0.9, 0.0, -0.1 / 0.9},
						  Double4{0.0, -1.0, 0.0, 0.0}},
				Double4x4{Double4{2.0, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, 0.02, 0.0},
						  Double4{0.0, 1.0 / 0.9, 0.0, -0.1 / 0.9},
						  Double4{0.0, 1.0, 0.0, 0.0}},
				Double4x4{Double4{0.0002 / 1.8, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, 0.0002, 0.0},
						  Double4{0.0, -10'000 / 9'999.999'9, 0.0,
								  -1.0 / 9'999.999'9},
						  Double4{0.0, -1.0, 0.0, 0.0}},
				Double4x4{Double4{0.8 / 1.8, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, 0.8, 0.0},
						  Double4{0.0, 5.0, 0.0, -2.0},
						  Double4{0.0, 1.0, 0.0, 0.0}},
				Double4x4{Double4{0.0002 / 0.018, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, 0.02, 0.0},
						  Double4{0.0, -10.0 / 9.999'9, 0.0,
								  -0.001 / 9.999'9},
						  Double4{0.0, -1.0, 0.0, 0.0}}},
		resLH = ArrayRes{
			Double4x4{Double4{0.2, 0.0, 0.0, 0.0},
					  Double4{0.0, 0.2, 0.0, 0.0},
					  Double4{0.0, 0.0, 1.0 / 0.9, -0.1 / 0.9},
					  Double4{0.0, 0.0, 1.0, 0.0}},
			Double4x4{Double4{0.02, 0.0, 0.0, 0.0},
					  Double4{0.0, 2.0, 0.0, 0.0},
					  Double4{0.0, 0.0, -1.0 / 0.9, -0.1 / 0.9},
					  Double4{0.0, 0.0, -1.0, 0.0}},
			Double4x4{Double4{2.0, 0.0, 0.0, 0.0},
					  Double4{0.0, 0.02, 0.0, 0.0},
					  Double4{0.0, 0.0, 1.0 / 0.9, -0.1 / 0.9},
					  Double4{0.0, 0.0, 1.0, 0.0}},
			Double4x4{Double4{0.0002 / 1.8, 0.0, 0.0, 0.0},
					  Double4{0.0, 0.0002, 0.0, 0.0},
					  Double4{0.0, 0.0, -10'000 / 9'999.999'9,
							  -1.0 / 9'999.999'9},
					  Double4{0.0, 0.0, -1.0, 0.0}},
			Double4x4{Double4{0.8 / 1.8, 0.0, 0.0, 0.0},
					  Double4{0.0, 0.8, 0.0, 0.0},
					  Double4{0.0, 0.0, 5.0, -2.0},
					  Double4{0.0, 0.0, 1.0, 0.0}},
			Double4x4{
				Double4{0.0002 / 0.018, 0.0, 0.0, 0.0},
				Double4{0.0, 0.02, 0.0, 0.0},
				Double4{0.0, 0.0, -10.0 / 9.999'9, -0.001 / 9.999'9},
				Double4{0.0, 0.0, -1.0, 0.0}}};

	// eval matrix3DPerspectiveCameraRH
	callEvalFunc(
		[](const Double4x4& expectRes, const double& width,
		   const double& height, const double& near,
		   const double& far, const bool& negateZ) -> void {
			Double4x4 result;
			storeDouble4x4(result,
						   matrix3DPerspectiveCameraRH(
							   width, height, near, far, negateZ));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		resRH, dataIn);

	// eval matrix3DPerspectiveCameraLH
	callEvalFunc(
		[](const Double4x4& expectRes, const double& width,
		   const double& height, const double& near,
		   const double& far, const bool& negateZ) -> void {
			Double4x4 result;
			storeDouble4x4(result,
						   matrix3DPerspectiveCameraLH(
							   width, height, near, far, negateZ));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		resLH, dataIn);
}
void evalPerspFOVCam()
{
	constexpr static const size_t arrSize = 6;

	using ArrayRes = std::array<Double4x4, arrSize>;
	using TupleCam = std::tuple<double, double, double, double, bool>;
	using ArrayIn  = std::array<TupleCam, arrSize>;

	// stores angle/cot angle pairs, for results calculations
	double angles[3][2] = {
		{g_PI / 2.0, std::cos(g_PI / 2.0) / std::sin(g_PI / 2.0)},
		{g_PI / 3.0, std::cos(g_PI / 3.0) / std::sin(g_PI / 3.0)},
		{g_PI * 0.786,
		 std::cos(g_PI * 0.786) / std::sin(g_PI * 0.786)}};

	auto dataIn =
		ArrayIn{TupleCam{angles[0][0], 1.8, 0.1, 1.0, false},
				TupleCam{angles[0][0], 1.0, 0.1, 1.0, true},
				TupleCam{angles[1][0], 4.0, 0.1, 1.0, false},
				TupleCam{angles[1][0], 1.6, 0.0001, 10'000.0, true},
				TupleCam{angles[2][0], 1.85, 0.4, 0.5, false},
				TupleCam{angles[2][0], 2.3, 0.0001, 10.0, true}};
	ArrayRes
		resRH =
			ArrayRes{
				Double4x4{Double4{angles[0][1] / 1.8, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, angles[0][1], 0.0},
						  Double4{0.0, 1.0 / 0.9, 0.0, -0.1 / 0.9},
						  Double4{0.0, 1.0, 0.0, 0.0}},
				Double4x4{Double4{angles[0][1], 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, angles[0][1], 0.0},
						  Double4{0.0, -1.0 / 0.9, 0.0, -0.1 / 0.9},
						  Double4{0.0, -1.0, 0.0, 0.0}},
				Double4x4{Double4{angles[1][1] / 4.0, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, angles[1][1], 0.0},
						  Double4{0.0, 1.0 / 0.9, 0.0, -0.1 / 0.9},
						  Double4{0.0, 1.0, 0.0, 0.0}},
				Double4x4{Double4{angles[1][1] / 1.6, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, angles[1][1], 0.0},
						  Double4{0.0, -10'000 / 9'999.999'9, 0.0,
								  -1.0 / 9'999.999'9},
						  Double4{0.0, -1.0, 0.0, 0.0}},
				Double4x4{Double4{angles[2][1] / 1.85, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, angles[2][1], 0.0},
						  Double4{0.0, 5.0, 0.0, -2.0},
						  Double4{0.0, 1.0, 0.0, 0.0}},
				Double4x4{Double4{angles[2][1] / 2.3, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, angles[2][1], 0.0},
						  Double4{0.0, -10.0 / 9.999'9, 0.0,
								  -0.001 / 9.999'9},
						  Double4{0.0, -1.0, 0.0, 0.0}}},
		resLH = ArrayRes{
			Double4x4{Double4{angles[0][1] / 1.8, 0.0, 0.0, 0.0},
					  Double4{0.0, angles[0][1], 0.0, 0.0},
					  Double4{0.0, 0.0, 1.0 / 0.9, -0.1 / 0.9},
					  Double4{0.0, 0.0, 1.0, 0.0}},
			Double4x4{Double4{angles[0][1], 0.0, 0.0, 0.0},
					  Double4{0.0, angles[0][1], 0.0, 0.0},
					  Double4{0.0, 0.0, -1.0 / 0.9, -0.1 / 0.9},
					  Double4{0.0, 0.0, -1.0, 0.0}},
			Double4x4{Double4{angles[1][1] / 4.0, 0.0, 0.0, 0.0},
					  Double4{0.0, angles[1][1], 0.0, 0.0},
					  Double4{0.0, 0.0, 1.0 / 0.9, -0.1 / 0.9},
					  Double4{0.0, 0.0, 1.0, 0.0}},
			Double4x4{Double4{angles[1][1] / 1.6, 0.0, 0.0, 0.0},
					  Double4{0.0, angles[1][1], 0.0, 0.0},
					  Double4{0.0, 0.0, -10'000 / 9'999.999'9,
							  -1.0 / 9'999.999'9},
					  Double4{0.0, 0.0, -1.0, 0.0}},
			Double4x4{Double4{angles[2][1] / 1.85, 0.0, 0.0, 0.0},
					  Double4{0.0, angles[2][1], 0.0, 0.0},
					  Double4{0.0, 0.0, 5.0, -2.0},
					  Double4{0.0, 0.0, 1.0, 0.0}},
			Double4x4{
				Double4{angles[2][1] / 2.3, 0.0, 0.0, 0.0},
				Double4{0.0, angles[2][1], 0.0, 0.0},
				Double4{0.0, 0.0, -10.0 / 9.999'9, -0.001 / 9.999'9},
				Double4{0.0, 0.0, -1.0, 0.0}}};

	// eval matrix3DPerspectiveFOVCameraRH
	callEvalFunc(
		[](const Double4x4& expectRes, const double& fov,
		   const double& aspectRatio, const double& near,
		   const double& far, const bool& negateZ) -> void {
			Double4x4 result;
			storeDouble4x4(result,
						   matrix3DPerspectiveFOVCameraRH(
							   fov, aspectRatio, near, far, negateZ));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		resRH, dataIn);

	// eval matrix3DPerspectiveFOVCameraLH
	callEvalFunc(
		[](const Double4x4& expectRes, const double& fov,
		   const double& aspectRatio, const double& near,
		   const double& far, const bool& negateZ) -> void {
			Double4x4 result;
			storeDouble4x4(result,
						   matrix3DPerspectiveFOVCameraLH(
							   fov, aspectRatio, near, far, negateZ));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		resLH, dataIn);
}
void evalOrthoCam()
{
	constexpr static const size_t arrSize = 6;

	using ArrayRes = std::array<Double4x4, arrSize>;
	using TupleCam = std::tuple<double, double, double, double, bool>;
	using ArrayIn  = std::array<TupleCam, arrSize>;

	auto dataIn = ArrayIn{TupleCam{1.0, 1.0, 0.1, 1.0, false},
						  TupleCam{10.0, 0.1, 0.1, 1.0, true},
						  TupleCam{0.1, 10.0, 0.1, 1.0, false},
						  TupleCam{1.8, 1.0, 0.0001, 10'000.0, true},
						  TupleCam{1.8, 1.0, 0.4, 0.5, false},
						  TupleCam{0.018, 0.01, 0.0001, 10.0, true}};
	ArrayRes
		resRH =
			ArrayRes{
				Double4x4{Double4{2.0, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, 2.0, 0.0},
						  Double4{0.0, 1.0 / 0.9, 0.0, -0.1 / 0.9},
						  Double4{0.0, 0.0, 0.0, 1.0}},
				Double4x4{Double4{0.2, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, 20.0, 0.0},
						  Double4{0.0, -1.0 / 0.9, 0.0, -0.1 / 0.9},
						  Double4{0.0, 0.0, 0.0, 1.0}},
				Double4x4{Double4{20.0, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, 0.2, 0.0},
						  Double4{0.0, 1.0 / 0.9, 0.0, -0.1 / 0.9},
						  Double4{0.0, 0.0, 0.0, 1.0}},
				Double4x4{Double4{2.0 / 1.8, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, 2.0, 0.0},
						  Double4{0.0, -1.0 / 9'999.999'9, 0.0,
								  -0.0001 / 9'999.999'9},
						  Double4{0.0, 0.0, 0.0, 1.0}},
				Double4x4{Double4{2.0 / 1.8, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, 2.0, 0.0},
						  Double4{0.0, 10.0, 0.0, -4.0},
						  Double4{0.0, 0.0, 0.0, 1.0}},
				Double4x4{Double4{2.0 / 0.018, 0.0, 0.0, 0.0},
						  Double4{0.0, 0.0, 200.0, 0.0},
						  Double4{0.0, -1.0 / 9.999'9, 0.0,
								  -0.0001 / 9.999'9},
						  Double4{0.0, 0.0, 0.0, 1.0}}},
		resLH = ArrayRes{
			Double4x4{Double4{2.0, 0.0, 0.0, 0.0},
					  Double4{0.0, 2.0, 0.0, 0.0},
					  Double4{0.0, 0.0, 1.0 / 0.9, -0.1 / 0.9},
					  Double4{0.0, 0.0, 0.0, 1.0}},
			Double4x4{Double4{0.2, 0.0, 0.0, 0.0},
					  Double4{0.0, 20.0, 0.0, 0.0},
					  Double4{0.0, 0.0, -1.0 / 0.9, -0.1 / 0.9},
					  Double4{0.0, 0.0, 0.0, 1.0}},
			Double4x4{Double4{20.0, 0.0, 0.0, 0.0},
					  Double4{0.0, 0.2, 0.0, 0.0},
					  Double4{0.0, 0.0, 1.0 / 0.9, -0.1 / 0.9},
					  Double4{0.0, 0.0, 0.0, 1.0}},
			Double4x4{Double4{2.0 / 1.8, 0.0, 0.0, 0.0},
					  Double4{0.0, 2.0, 0.0, 0.0},
					  Double4{0.0, 0.0, -1.0 / 9'999.999'9,
							  -0.0001 / 9'999.999'9},
					  Double4{0.0, 0.0, 0.0, 1.0}},
			Double4x4{Double4{2.0 / 1.8, 0.0, 0.0, 0.0},
					  Double4{0.0, 2.0, 0.0, 0.0},
					  Double4{0.0, 0.0, 10.0, -4.0},
					  Double4{0.0, 0.0, 0.0, 1.0}},
			Double4x4{
				Double4{2.0 / 0.018, 0.0, 0.0, 0.0},
				Double4{0.0, 200.0, 0.0, 0.0},
				Double4{0.0, 0.0, -1.0 / 9.999'9, -0.0001 / 9.999'9},
				Double4{0.0, 0.0, 0.0, 1.0}}};

	// eval matrix3DOrthographicCameraRH
	callEvalFunc(
		[](const Double4x4& expectRes, const double& width,
		   const double& height, const double& near,
		   const double& far, const bool& negateZ) -> void {
			Double4x4 result;
			storeDouble4x4(result,
						   matrix3DOrthographicCameraRH(
							   width, height, near, far, negateZ));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		resRH, dataIn);

	// eval matrix3DOrthographicCameraLH
	callEvalFunc(
		[](const Double4x4& expectRes, const double& width,
		   const double& height, const double& near,
		   const double& far, const bool& negateZ) -> void {
			Double4x4 result;
			storeDouble4x4(result,
						   matrix3DOrthographicCameraLH(
							   width, height, near, far, negateZ));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		resLH, dataIn);
}

void evalTwoMtrxMaths()
{
	constexpr static const size_t arrSize = 6;

	using ArrayRes = std::array<Double4x4, arrSize>;
	using TupleTwo = std::tuple<Double4x4, Double4x4>;
	using ArrayIn  = std::array<TupleTwo, arrSize>;

	Double4x4 rawMatrices[5] = {
		Double4x4{
			Double4{1.0, 0.0, 0.0, 0.0}, Double4{0.0, 1.0, 0.0, 0.0},
			Double4{0.0, 0.0, 1.0, 0.0}, Double4{0.0, 0.0, 0.0, 1.0}},
		Double4x4{Double4{1.0, 2.0, 3.0, 4.0},
				  Double4{-1.0, -2.0, -3.0, -4.0},
				  Double4{5.0, 6.0, 7.0, 8.0},
				  Double4{-5.0, -6.0, -7.0, -8.0}},
		Double4x4{Double4{0.3, 1.5, 4.7, 2.0},
				  Double4{1'000.4, -345.2, 20.3, 1.1},
				  Double4{-4'587.0, 0.001, 2.3, 5.12},
				  Double4{0.1, 0.4, 7.8, -98.4}},
		Double4x4{
			Double4{0.0, 0.0, 0.0, 0.0}, Double4{0.0, 0.0, 0.0, 0.0},
			Double4{0.0, 0.0, 0.0, 0.0}, Double4{0.0, 0.0, 0.0, 0.0}},
		Double4x4{Double4{0.0, 1.0, 0.0, 0.0},
				  Double4{1.0, 0.0, 0.0, 0.0},
				  Double4{0.0, 0.0, 0.0, 1.0},
				  Double4{0.0, 0.0, 1.0, 0.0}}};

	auto dataIn = ArrayIn{TupleTwo{rawMatrices[1], rawMatrices[0]},
						  TupleTwo{rawMatrices[0], rawMatrices[2]},
						  TupleTwo{rawMatrices[3], rawMatrices[0]},
						  TupleTwo{rawMatrices[4], rawMatrices[1]},
						  TupleTwo{rawMatrices[2], rawMatrices[4]},
						  TupleTwo{rawMatrices[2], rawMatrices[1]}};
	ArrayRes resAdd =
				 ArrayRes{
					 Double4x4{Double4{2.0, 2.0, 3.0, 4.0},
							   Double4{-1.0, -1.0, -3.0, -4.0},
							   Double4{5.0, 6.0, 8.0, 8.0},
							   Double4{-5.0, -6.0, -7.0, -7.0}},
					 Double4x4{Double4{1.3, 1.5, 4.7, 2.0},
							   Double4{1000.4, -344.2, 20.3, 1.1},
							   Double4{-4587.0, 0.001, 3.3, 5.12},
							   Double4{0.1, 0.4, 7.8, -97.4}},
					 rawMatrices[0],
					 Double4x4{Double4{1.0, 3.0, 3.0, 4.0},
							   Double4{0.0, -2.0, -3.0, -4.0},
							   Double4{5.0, 6.0, 7.0, 9.0},
							   Double4{-5.0, -6.0, -6.0, -8.0}},
					 Double4x4{Double4{0.3, 2.5, 4.7, 2.0},
							   Double4{1'001.4, -345.2, 20.3, 1.1},
							   Double4{-4'587.0, 0.001, 2.3, 6.12},
							   Double4{0.1, 0.4, 8.8, -98.4}},
					 Double4x4{Double4{1.3, 3.5, 7.7, 6.0},
							   Double4{999.4, -347.2, 17.3, -2.9},
							   Double4{-4582.0, 6.001, 9.3, 13.12},
							   Double4{-4.9, -5.6, 0.8, -106.4}}},
			 resMult = ArrayRes{
				 rawMatrices[1],
				 rawMatrices[2],
				 rawMatrices[3],
				 Double4x4{Double4{2.0, 1.0, 4.0, 3.0},
						   Double4{-2.0, -1.0, -4.0, -3.0},
						   Double4{6.0, 5.0, 8.0, 7.0},
						   Double4{-6.0, -5.0, -8.0, -7.0}},
				 Double4x4{Double4{1'000.4, -345.2, 20.3, 1.1},
						   Double4{0.3, 1.5, 4.7, 2.0},
						   Double4{0.1, 0.4, 7.8, -98.4},
						   Double4{-4'587.0, 0.001, 2.3, 5.12}},
				 Double4x4{
					 Double4{-11'759.5, -687.297, 83.4, -374.04},
					 Double4{11'759.5, 687.297, -83.4, 374.04},
					 Double4{-26'104.3, -2060.493, 223.8, -734.76},
					 Double4{26'104.3, 2060.493, -223.8, 734.76}}};

	// eval matrixAddition
	callEvalFunc(
		[](const Double4x4& expectRes, const Double4x4& lhs,
		   const Double4x4& rhs) -> void {
			Double4x4 result;
			storeDouble4x4(result,
						   matrixAddition(loadDouble4x4(lhs),
										  loadDouble4x4(rhs)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		resAdd, dataIn);

	// eval matrixMultiply
	callEvalFunc(
		[](const Double4x4& expectRes, const Double4x4& lhs,
		   const Double4x4& rhs) -> void {
			Double4x4 result;
			storeDouble4x4(result,
						   matrixMultiply(loadDouble4x4(lhs),
										  loadDouble4x4(rhs)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		resMult, dataIn);
}
void evalOneMtrxMaths()
{
	constexpr static const size_t arrSize = 6;

	using ArrayRes	   = std::array<Double4x4, arrSize>;
	using TupleScale   = std::tuple<Double4x4, double>;
	using ArrayScaleIn = std::array<TupleScale, arrSize>;
	using TupleTrans   = std::tuple<Double4x4>;
	using ArrayTransIn = std::array<TupleTrans, arrSize>;

	Double4x4 rawMatrices[6] = {
		Double4x4{
			Double4{1.0, 0.0, 0.0, 0.0}, Double4{0.0, 1.0, 0.0, 0.0},
			Double4{0.0, 0.0, 1.0, 0.0}, Double4{0.0, 0.0, 0.0, 1.0}},
		Double4x4{Double4{1.0, 2.0, 3.0, 4.0},
				  Double4{-1.0, -2.0, -3.0, -4.0},
				  Double4{5.0, 6.0, 7.0, 8.0},
				  Double4{-5.0, -6.0, -7.0, -8.0}},
		Double4x4{Double4{0.3, 1.5, 4.7, 2.0},
				  Double4{1'000.4, -345.2, 20.3, 1.1},
				  Double4{-4'587.0, 0.001, 2.3, 5.12},
				  Double4{0.1, 0.4, 7.8, -98.4}},
		Double4x4{
			Double4{0.0, 0.0, 0.0, 0.0}, Double4{0.0, 0.0, 0.0, 0.0},
			Double4{0.0, 0.0, 0.0, 0.0}, Double4{0.0, 0.0, 0.0, 0.0}},
		Double4x4{
			Double4{0.0, 1.0, 0.0, 0.0}, Double4{1.0, 0.0, 0.0, 0.0},
			Double4{0.0, 0.0, 0.0, 1.0}, Double4{0.0, 0.0, 1.0, 0.0}},
		Double4x4{Double4{0.0, 1.0, 1.0, 1.0},
				  Double4{0.0, 0.0, 1.0, 1.0},
				  Double4{0.0, 0.0, 0.0, 1.0},
				  Double4{0.0, 0.0, 0.0, 0.0}}};

	ArrayScaleIn scaleData =
		ArrayScaleIn{TupleScale{rawMatrices[0], 10.0},
					 TupleScale{rawMatrices[1], -1.0},
					 TupleScale{rawMatrices[2], 0.15},
					 TupleScale{rawMatrices[3], g_MaxDouble},
					 TupleScale{rawMatrices[4], 1.0},
					 TupleScale{rawMatrices[5], g_PI}};
	ArrayTransIn transData = ArrayTransIn{
		TupleTrans{rawMatrices[0]}, TupleTrans{rawMatrices[1]},
		TupleTrans{rawMatrices[2]}, TupleTrans{rawMatrices[3]},
		TupleTrans{rawMatrices[4]}, TupleTrans{rawMatrices[5]}};

	ArrayRes resScale =
				 ArrayRes{Double4x4{Double4{10.0, 0.0, 0.0, 0.0},
									Double4{0.0, 10.0, 0.0, 0.0},
									Double4{0.0, 0.0, 10.0, 0.0},
									Double4{0.0, 0.0, 0.0, 10.0}},
						  Double4x4{Double4{-1.0, -2.0, -3.0, -4.0},
									Double4{1.0, 2.0, 3.0, 4.0},
									Double4{-5.0, -6.0, -7.0, -8.0},
									Double4{5.0, 6.0, 7.0, 8.0}},
						  Double4x4{
							  Double4{0.045, 0.225, 0.705, 0.3},
							  Double4{150.06, -51.78, 3.045, 0.165},
							  Double4{-688.05, 0.00015, 0.345, 0.768},
							  Double4{0.015, 0.06, 1.17, -14.76}},
						  rawMatrices[3],
						  rawMatrices[4],
						  Double4x4{Double4{0.0, g_PI, g_PI, g_PI},
									Double4{0.0, 0.0, g_PI, g_PI},
									Double4{0.0, 0.0, 0.0, g_PI},
									Double4{0.0, 0.0, 0.0, 0.0}}},
			 resTrans = ArrayRes{
				 rawMatrices[0],
				 Double4x4{Double4{1.0, -1.0, 5.0, -5.0},
						   Double4{2.0, -2.0, 6.0, -6.0},
						   Double4{3.0, -3.0, 7.0, -7.0},
						   Double4{4.0, -4.0, 8.0, -8.0}},
				 Double4x4{Double4{0.3, 1'000.4, -4'587.0, 0.1},
						   Double4{1.5, -345.2, 0.001, 0.4},
						   Double4{4.7, 20.3, 2.3, 7.8},
						   Double4{2.0, 1.1, 5.12, -98.4}},
				 rawMatrices[3],
				 rawMatrices[4],
				 Double4x4{Double4{0.0, 0.0, 0.0, 0.0},
						   Double4{1.0, 0.0, 0.0, 0.0},
						   Double4{1.0, 1.0, 0.0, 0.0},
						   Double4{1.0, 1.0, 1.0, 0.0}}};

	// eval matrixScale
	callEvalFunc(
		[](const Double4x4& expectRes, const Double4x4& matrix,
		   const double& scale) -> void {
			Double4x4 result;
			storeDouble4x4(result,
						   matrixScale(loadDouble4x4(matrix), scale));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		resScale, scaleData);

	// eval matrixTranspose
	callEvalFunc(
		[](const Double4x4& expectRes,
		   const Double4x4& matrix) -> void {
			Double4x4 result;
			storeDouble4x4(result,
						   matrixTranspose(loadDouble4x4(matrix)));

			dataStoreApproxEq(result, expectRes, 10'000'000.0);
		},
		resTrans, transData);
}

SCENARIO("Vector Tests")
{
	THEN("Record evaluation functions work")
	{
		// ensure that functions for testing returned records work
		evaluateRecordEval();
	}
	GIVEN("Loads/Stores and gets/sets are symmetric")
	{
		// check that input and output, after round trip, are
		// equal
		//-for 2/x2/3/x3/4/x4 D un/aligned data stores/loads
		//-for g/set ByIndex/X/Y/Z/W (float/double/int)
		//-for positive/negative high/low, +/-0, +-inf, NaN

		evaluateLoadStore();
		evaluateSetGet();

		THEN("Loads and sets reach the same outcome")
		{
			// check by load/set then storing for both, and
			// ensuring
			//  that the data stores are equal
			//-for 2/3/4 D aligned data loads
			//-for set ByIndex/X/Y/Z/W/All (float/double/int)
			//-for positive/negative high/low, +/-0, +/-inf, NaN

			evaluateLoadSet();
		}
		THEN("Stores and gets reach the same outcome")
		{
			// check by load then store/get for both, and ensuring
			// that the data stores/gets are equal
			//-for 2/3/4 D aligned data stores
			//-for get ByIndex/X/Y/Z/W (float/double/int)
			//-for positive/negative high/low, +/-0, +/-inf, NaN

			evaluateStoreGet();
		}
		THEN("Matrix gets/sets reach the expected outcome")
		{
			evalMtrxOneFile();
			evalMtrxAllFiles();
		}
		THEN("Constant vectors return correctly")
		{
			// ensure that returned constant vectors are correct
			evaluateConstantVectors();
		}
		THEN("Dimensional Matrix comparisons work")
		{
			eval4DTwoMtrxComparisons();
			eval4DOneMtrxComparisons();
			eval3DTwoMtrxComparisons();
			eval3DOneMtrxComparisons();
			eval2DTwoMtrxComparisons();
			eval2DOneMtrxComparisons();
		}
		THEN("Matrix maths works")
		{
			evalTwoMtrxMaths();
			evalOneMtrxMaths();
		}
		WHEN("Dimensionsal vector maths works")
		{
			eval4DVecMaths();
			eval4DVecTransform();
			eval4DMtrxInversion();
			eval3DVecMaths();
			eval3DVecTransform();
			eval3DMtrxInversion();
			eval2DVecMaths();
			eval2DVecTransform();
			eval2DMtrxInversion();

			THEN("Special Matrices function correctly")
			{
				evalIDMatrix();
				evalMtrxTranslations();
				evalMtrxScalings();
				evalMtrxBasicRotations();
				evalMtrxCompositeRotations();
				evalMtrxAxisRotations();
				evalPerspApatureCam();
				evalPerspFOVCam();
				evalOrthoCam();
			}
		}
		WHEN("Normal per-component comparisons work")
		{
			// test non-record/record per-component comparisons,
			// less, lessEqual, greater, greaterEqual, equal,
			// noEqual. On Wide and int vectors

			evalOneArgComparisons();
			evalTwoArgDblComparisons();
			evalTwoArgIntComparisons();
			evalThreeArgDblComparisons();
			evalThreeArgIntComparisons();

			THEN("Vector bit functions work")
			{
				// test special functions, grouping tests by
				// number of arguments
				evalSmallOneArgBitFunctions();
				evalOneArgBitFunctions();
				evalSmallTwoArgBitFunctions();
				evalTwoArgBitFunctions();
			}
			THEN("Vector trig functions work")
			{
				// test trig functions, grouping by number of,
				// and acceptable range of arguments
				evalForwardOneArgTrigFunctions();
				evalFullRangeOneArgTrigFunctions();
				evalPartialRangeOneArgTrigFunctions();
				evalTwoArgTrigFunctions();
			}
			THEN("Vector Special functions work")
			{
				// test special functions, grouping tests by
				// number of arguments
				evalOneArgSpecialFunctions();
				evalTwoArgSpecialFunctions();
				evalPowSpecialFunction();
			}
			THEN("Vector Conversion functions work")
			{
				// test conversion functions, grouping tests
				// by type of conversion
				evalIntToDblCast();
				evalDblToIntCast();
				evalNrwToDblCast();
				evalDblToNrwCast();
				evalIntNrwWideCast();
				evalVecRounds();
			}
			THEN("Vector reordering functions work")
			{
				evalReorderUtils();
				evalDblSplats();
				evalIntSplats();
				evalDblRotates();
				evalIntRotates();
				evalSwizzles();
				evalDblMerges();
				evalIntMerges();
				evalPermutes();
				evalSelects();
			}
			THEN("Vector arithmetic works")
			{
				// check whether arithmetic fuctions work,
				// grouping tests by number and type of arguments
				evaluateVectorOneDblArithmetic();
				evaluateVectorOnePlusDblArithmetic();
				evaluateVectorTwoDblArithmetic();
				evaluateVectorSmallIntArithmetic();
				evaluateVectorIntArithmetic();
			}
			THEN("2D comparisons work")
			{
				// test 3D comparisons
				evalOneArg2DComparisons();
				evalTwoArg2DDblComparisons();
				evalTwoArg2DIntComparisons();
				evalThreeArg2DDblComparisons();
				evalThreeArg2DIntComparisons();
			}
			THEN("3D comparisons work")
			{
				// test 3D comparisons
				evalOneArg3DComparisons();
				evalTwoArg3DDblComparisons();
				evalTwoArg3DIntComparisons();
				evalThreeArg3DDblComparisons();
				evalThreeArg3DIntComparisons();
			}
			THEN("4D comparisons work")
			{
				// test 4D comparisons
				evalOneArg4DComparisons();
				evalTwoArg4DDblComparisons();
				evalTwoArg4DIntComparisons();
				evalThreeArg4DDblComparisons();
				evalThreeArg4DIntComparisons();
			}
		}
	}
}